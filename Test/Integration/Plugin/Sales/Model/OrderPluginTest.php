<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Plugin\Sales\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Interception\PluginList\PluginList;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\Order;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Model\Config;
use Reeds\GunBroker\Plugin\Sales\Model\OrderPlugin;
use Reeds\GunBroker\Service\SerializedProductDelister;

use function current;

final class OrderPluginTest extends TestCase
{
    /**
     * @magentoAppArea frontend
     */
    public function testPluginIsConfigured(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $pluginList = $objectManager->create(PluginList::class);
        $plugins = $pluginList->get(Order::class, []);

        static::assertArrayHasKey('gunbroker_delist_serialized_items', $plugins);
        static::assertSame(OrderPlugin::class, $plugins['gunbroker_delist_serialized_items']['instance']);
    }

    /**
     * @magentoAppArea frontend
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order_with_serialized_firearm.php
     */
    public function testPluginDelistsSerializedProductAfterOrderIsPlaced(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var Order $order */
        $order = $objectManager->create(Order::class);
        $serializedProductDelisterMock = $this->createMock(SerializedProductDelister::class);
        $configMock = $this->createMock(ConfigInterface::class);
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $objectManager->create(SearchCriteriaBuilder::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        $searchCriteria = $searchCriteriaBuilder->addFilter('sku', '764503037108~12345')
            ->create();
        $searchResults = $productRepository->getList($searchCriteria); // get() doesn't add the correct fields
        $product = current($searchResults->getItems());
        $productClone = clone $product; // work-around for getCustomAttribute changing product data

        $objectManager->configure(
            [
                SerializedProductDelister::class => [
                    'shared' => true
                ],
                Config::class => [
                    'shared' => true
                ],
            ]
        );
        $objectManager->addSharedInstance($serializedProductDelisterMock, SerializedProductDelister::class);
        $objectManager->addSharedInstance($configMock, Config::class);

        $configMock->method('isEnabled')
            ->willReturn(true);

        $serializedProductDelisterMock->expects(self::once())
            ->method('delist')
            ->with($product, 1)
            ->willReturn((int)$productClone->getCustomAttribute('gunbroker_item_id')->getValue());

        $order->loadByIncrementId('100000001');
        $order->place();
    }
}
