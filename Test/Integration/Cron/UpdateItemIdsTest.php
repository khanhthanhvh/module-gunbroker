<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Cron;

use Magento\Cron\Model\ConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Reeds\GunBroker\Cron\UpdateItemIds;
use Reeds\GunBroker\Service\ItemIdUpdater;

final class UpdateItemIdsTest extends TestCase
{
    public function testCronjobIsRegisteredAndConfiguredCorrectly(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $cronJobs = $objectManager->create(ConfigInterface::class)->getJobs();

        self::assertArrayHasKey('gunbroker_update_item_ids', $cronJobs['gunbroker_cron']);
        self::assertSame(
            $cronJobs['gunbroker_cron']['gunbroker_update_item_ids']['instance'],
            UpdateItemIds::class
        );
    }

    /**
     * @param int[] $expectedUpdatedProducts
     * @dataProvider updatesItemIdentifiersProvider
     */
    public function testUpdatesItemIdentifiersAndLogsResult(
        array $expectedUpdatedProducts,
        string $expectedLoggedMessage
    ): void {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $itemIdUpdaterMock = $this->createMock(ItemIdUpdater::class);
        $testLogger = $objectManager->create(TestLogger::class);
        $updateItemIdsCronJob = $objectManager->create(
            UpdateItemIds::class,
            [
                'itemIdUpdater' => $itemIdUpdaterMock,
                'logger' => $testLogger
            ]
        );

        $itemIdUpdaterMock->expects(self::once())
            ->method('updateInWebsites')
            ->willReturn($expectedUpdatedProducts);

        $updateItemIdsCronJob->execute();

        self::assertTrue($testLogger->hasInfoThatContains($expectedLoggedMessage));
    }

    /**
     * @return array<string, array<string, array<int, array<int, int>>|string>>
     */
    public function updatesItemIdentifiersProvider(): array
    {
        return [
            'no_updates' => [
                'expectedUpdatedProducts' => [],
                'expectedLoggedMessage' =>
                    (string)__('No item identifiers were available to update in products during scheduled update.')
            ],
            'updated_products' => [
                'expectedUpdatedProducts' => [
                    1 => [
                        12345 => 1,
                        67890 => 5
                    ],
                    3 => [
                        42069 => 42
                    ]
                ],
                'expectedLoggedMessage' =>
                    (string)__('%1 products were updated during scheduled item identifier update.', 3)
            ]
        ];
    }
}
