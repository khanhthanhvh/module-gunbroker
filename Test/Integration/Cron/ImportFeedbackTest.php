<?php

// phpcs:disable Generic.Files.LineLength.TooLong

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Cron;

use Magento\Cron\Model\ConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Cron\ImportFeedback;
use Reeds\GunBroker\Service\FeedbackImporter;

final class ImportFeedbackTest extends TestCase
{
    public function testCronjobIsRegisteredAndConfiguredCorrectly(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $cronJobs = $objectManager->create(ConfigInterface::class)->getJobs();

        self::assertArrayHasKey('gunbroker_import_feedback', $cronJobs['gunbroker_cron']);
        self::assertSame(
            $cronJobs['gunbroker_cron']['gunbroker_import_feedback']['instance'],
            ImportFeedback::class
        );
    }

    /**
     * @param array<int, array{succeeded: OrderItemFeedbackInterface[], failed: OrderItemFeedbackInterface[]}> $expectedImportedFeedback
     * @param string[] $expectedLoggedMessages
     * @dataProvider importsFeedbackProvider
     */
    public function testImportsFeedbackAndLogsResult(
        array $expectedImportedFeedback,
        array $expectedLoggedMessages
    ): void {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $feedbackImporterMock = $this->createMock(FeedbackImporter::class);
        $testLogger = $objectManager->create(TestLogger::class);
        $importFeedbackCronJob = $objectManager->create(
            ImportFeedback::class,
            [
                'feedbackImporter' => $feedbackImporterMock,
                'logger' => $testLogger
            ]
        );

        $feedbackImporterMock->expects(self::once())
            ->method('importInWebsites')
            ->willReturn($expectedImportedFeedback);

        $importFeedbackCronJob->execute();

        foreach ($expectedLoggedMessages as $level => $expectedLoggedMessage) {
            self::assertTrue($testLogger->hasRecordThatContains($expectedLoggedMessage, $level));
        }
    }

    /**
     * @return array<string, array<string, array<string|int, string|array<string, OrderItemFeedbackInterface[]>>>>
     */
    public function importsFeedbackProvider(): array
    {
        /** @var OrderItemFeedbackInterface $orderItemFeedback */
        $orderItemFeedback = Bootstrap::getObjectManager()->create(OrderItemFeedbackInterface::class);

        return [
            'no_imported_feedback' => [
                'expectedImportedFeedback' => [],
                'expectedLoggedMessages' => [
                    'info' => (string)__('No feedback was available to import during scheduled import.')
                ]
            ],
            'imported_feedback' => [
                'expectedImportedFeedback' => [
                    1 => [
                        'succeeded' => [
                            $orderItemFeedback,
                            $orderItemFeedback
                        ],
                        'failed' => [
                        ]
                    ],
                    3 => [
                        'succeeded' => [
                        ],
                        'failed' => [
                            $orderItemFeedback,
                            $orderItemFeedback
                        ]
                    ],
                    5 => [
                        'succeeded' => [
                            $orderItemFeedback
                        ],
                        'failed' => [
                            $orderItemFeedback
                        ]
                    ]
                ],
                'expectedLoggedMessages' => [
                    'warning' => (string)__('%1 feedback could not be imported during scheduled import.', 3),
                    'info' => (string)__('%1 feedback were successfully imported during scheduled import.', 3)
                ]
            ]
        ];
    }
}
