<?php

declare(strict_types=1);

use Magento\Catalog\Model\Product;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\AttributeFactory;
use Magento\Eav\Setup\EavSetup;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;

/** @var ObjectManager $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var EavSetup $eavSetup */
$eavSetup = $objectManager->create(EavSetup::class);
$attributes = [
    'atf_model' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['ATF Model'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'barrel_length' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Barrel Length'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'brand' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Brand'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'caliber_gauge' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Caliber'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'capacity' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Capacity'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'frame_finish' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Frame Finish'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'frame_material' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Frame Material'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'front_sights' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Front Sights'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'gtin' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['GTIN'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'is_handgun' => [
        'backend_type' => 'int',
        'frontend_label' => ['Is Handgun'],
        'frontend_input' => 'boolean',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
        'default' => 0,
    ],
    'rear_sights' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Rear Sights'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'serial_number' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Serial Number'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'slide_finish' => [
        'backend_type' => 'int',
        'frontend_label' => ['Slide Finish'],
        'frontend_input' => 'select',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
        'option' => [
            'value' => [
                ['Matte/Rough Black'],
                ['Matte/Light Black'],
                ['Slick/Shiny Black'],
                ['Custom']
            ]
        ]
    ],
    'slide_material' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Slide Material'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'stock_finish' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Stock Finish'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'style' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['Style'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ],
    'upc_ean' => [
        'backend_type' => 'varchar',
        'frontend_label' => ['UPC/EAN'],
        'frontend_input' => 'text',
        'is_required' => false,
        'is_global' => ScopedAttributeInterface::SCOPE_STORE,
        'is_user_defined' => true,
    ]
];
/** @var AttributeFactory $attributeFactory */
$attributeFactory = $objectManager->create(AttributeFactory::class);
/** @var AttributeRepositoryInterface $attributeRepository */
$attributeRepository = $objectManager->create(AttributeRepositoryInterface::class);
$entityTypeId = $eavSetup->getEntityTypeId(Product::ENTITY);
$defaultAttributeGroupId = $eavSetup->getDefaultAttributeGroupId(
    Product::ENTITY,
    $eavSetup->getAttributeSetId(Product::ENTITY, 'Firearms')
);

foreach ($attributes as $attributeCode => $attributeData) {
    $attribute = $attributeFactory->create();

    $attribute->setAttributeCode($attributeCode);
    $attribute->setEntityTypeId($entityTypeId);
    $attribute->addData($attributeData);

    $attributeRepository->save($attribute);

    //$eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeData);
    $eavSetup->addAttributeToGroup(
        Product::ENTITY,
        'Firearms',
        $defaultAttributeGroupId,
        $attributeCode
    );
}
