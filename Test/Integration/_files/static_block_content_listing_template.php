<?php

declare(strict_types=1);

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;

$objectManager = Bootstrap::getObjectManager();
/** @var BlockRepositoryInterface $blockRepository */
$blockRepository = $objectManager->create(BlockRepositoryInterface::class);
$block = $blockRepository->getById('gunbroker_listing_handgun');

$block->setContent(<<<EOH
<div>
    {{var product_name}}<img src="https://www.reedssports.com/media/logo/stores/39/northernfirearmsheader-2.png"
        align="center" width="100%" alt="Northern Firearms">
    <br>
    <br>
    <p align="center">
        <font face="Arial" color="#E31837" style="font-size: 24pt; font-weight:700">{{var product_name}}</font>
    </p>
    <br>
    <br>
    {{depend product_attributes}}
    <p align="center">
        <font face="Arial" color="#E31837" style="font-size: 16pt; font-weight:700">
            <table class="tableizer-table">
                {{for attribute in product_attributes}}
                <tr>
                    <td>{{var attribute.label}}:</td>
                    <td>&nbsp;</td>
                    <td>{{var attribute.value}}</td>
                </tr>
                {{/for}}
            </table>
        </font>
    </p>
    {{/depend}}
    <p align="left" style="padding-left:100px;padding-right:100px;">
        <font face="Arial" color="#E31837" style="font-size: 16pt; font-weight:700">Factory new in box with all factory
        supplied accessories included. No extra fees for paying with a Credit Card. Please read the Additional Terms of
        Sale for more information. Some listings offer the "TAKE A SHOT" feature, go ahead, Take A Shot, it just might
        save you some $$$.
        </font>
    </p>
    <br>
    <br>
    <br>
    <p align="center">
        <font face="Arial" color="#E31837" style="font-size: 12pt; font-weight:700">
            Specifications subject to change, please refer to manufacturers website for specifications.
            <br>Pictures are for general reference only.
        </font>
    </p>
    <br>
    <br>
    <p align="center">
        <span style="font-size: 6pt">By purchasing this firearm through our Gunbroker store, we will add you to our
        contact list. We will contact you on your order status and any follow up customer offerings that pertain to your
        interests. You can unsubscribe at any time. Please visit our
        <a href="https://www.reedssports.com/privacy-policy-cookie-restriction-mode">privacy policy</a> for more
        information.</span>
    </p>
</div>
EOH
);

$blockRepository->save($block);
