<?php

declare(strict_types=1);

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\Framework\Registry;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var Registry $registry */
$registry = $objectManager->get(Registry::class);

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', true);

/** @var ProductAttributeRepositoryInterface $attributeRepository */
$attributeRepository = $objectManager->create(ProductAttributeRepositoryInterface::class);
$attributes = [
    'atf_model',
    'barrel_length',
    'brand',
    'caliber_gauge',
    'capacity',
    'frame_finish',
    'frame_material',
    'front_sights',
    'gtin',
    'is_handgun',
    'rear_sights',
    'serial_number',
    'slide_finish',
    'slide_material',
    'stock_finish',
    'style',
    'upc_ean'
];

foreach ($attributes as $attribute) {
    try {
        $attributeRepository->deleteById($attribute);
    } catch (NoSuchEntityException | StateException $e) {
    }
}

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', false);
