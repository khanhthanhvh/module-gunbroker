<?php

declare(strict_types=1);

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\TestFramework\Helper\Bootstrap;

$objectManager = Bootstrap::getObjectManager();
/** @var CategoryRepositoryInterface $categoryRepository */
$categoryRepository = $objectManager->create(CategoryRepositoryInterface::class);
/** @var Category $category */
$category = $objectManager->create(Category::class);

$category->isObjectNew(true);
$category->setName('Firearms');
$category->setLevel(2);
$category->setAvailableSortBy(['position', 'name']);
$category->setDefaultSortBy('name');
$category->setIsActive(true);
$category->setPosition(1);
$category->setCustomAttribute('is_gunbroker_category', true);
$category->setCustomAttribute('gunbroker_category_id', 50);

$categoryRepository->save($category);
