<?php

/** @codeCoverageIgnore */

declare(strict_types=1);

use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\Website;
use Magento\TestFramework\Helper\Bootstrap;

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var Website $website */
$website = $objectManager->create(Website::class);

$website->setData(
    [
        'code' => 'gunbroker',
        'name' => 'GunBroker',
        'default_group_id' => '1',
        'is_default' => '0'
    ]
);
$website->save();

/* Refresh stores memory cache */
$objectManager->get(StoreManagerInterface::class)->reinitStores();
