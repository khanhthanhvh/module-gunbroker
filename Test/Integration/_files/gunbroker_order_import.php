<?php

declare(strict_types=1);

use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\Order;
use Magento\TestFramework\Helper\Bootstrap;
use Reeds\GunBroker\Api\Data\OrderImportInterface;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var OrderImportInterface $orderImport */
$orderImport = $objectManager->create(OrderImportInterface::class);
/** @var OrderImportRepositoryInterface $orderImportRepository */
$orderImportRepository = $objectManager->create(OrderImportRepositoryInterface::class);
/** @var Order $order */
$order = $objectManager->create(Order::class)->loadByIncrementId('100000001');

$orderImport->setGunBrokerOrderId((int)$order->getExtOrderId());
$orderImport->setMagentoOrderId((int)$order->getEntityId());
$orderImport->setImportStatus('complete');

$orderImportRepository->save($orderImport);
