<?php

declare(strict_types=1);

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Customer;
use Magento\TestFramework\Helper\Bootstrap;

$objectManager = Bootstrap::getObjectManager();
$customer = $objectManager->create(Customer::class);
$customerRepository = $objectManager->create(CustomerRepositoryInterface::class);

$customer->setWebsiteId(1);
$customer->setStoreId(1);
$customer->setEmail('jdoe@example.com');
$customer->setGroupId(1);
$customer->setIsActive(1);
$customer->setFirstname('Jane');
$customer->setLastname('Doe');

$customerRepository->save($customer->getDataModel());
