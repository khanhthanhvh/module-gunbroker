<?php

declare(strict_types=1);

use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\Order;
use Magento\TestFramework\Helper\Bootstrap;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
$buyerFeedback = $objectManager->create(OrderItemFeedbackInterface::class);
$sellerFeedback = $objectManager->create(OrderItemFeedbackInterface::class);
$feedbackRepository = $objectManager->create(OrderItemFeedbackRepositoryInterface::class);
$order = $objectManager->create(Order::class)->loadByIncrementId('100000001');
$orderItem = current($order->getItems());

$buyerFeedback->setType('buyer');
$buyerFeedback->setGunbrokerFeedbackId(123);
$buyerFeedback->setGunbrokerItemId((int)$orderItem->getExtOrderItemId());
$buyerFeedback->setMagentoOrderItemId((int)$orderItem->getItemId());
$buyerFeedback->setRatingLetter('A');
$buyerFeedback->setRatingScore(4);
$buyerFeedback->setComment('Okay transaction');
$buyerFeedback->setCommentDate('2021-06-10 19:10:21');
$buyerFeedback->setCommentator('jsmith');

$sellerFeedback->setType('seller');
$sellerFeedback->setGunbrokerFeedbackId(456);
$sellerFeedback->setGunbrokerItemId((int)$orderItem->getExtOrderItemId());
$sellerFeedback->setMagentoOrderItemId((int)$orderItem->getItemId());
$sellerFeedback->setRatingLetter('B');
$sellerFeedback->setRatingScore(3);
$sellerFeedback->setComment('Late payment');
$sellerFeedback->setCommentDate('2021-06-11 15:55:03');
$sellerFeedback->setCommentator('acme firearms');

$feedbackRepository->save($buyerFeedback);
$feedbackRepository->save($sellerFeedback);
