<?php

declare(strict_types=1);

use Magento\Catalog\Model\Category;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\TestFramework\Helper\Bootstrap;

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var Registry $registry */
$registry = $objectManager->get(Registry::class);

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', true);

/** @var Category|false $category */
$category = $objectManager->create(Category::class)
    ->loadByAttribute('name', 'Firearms');

if ($category instanceof Category && $category->getId() !== null) {
    $category->delete();
}

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', false);
