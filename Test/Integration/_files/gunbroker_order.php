<?php

declare(strict_types=1);

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\DB\Transaction;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address as OrderAddress;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magento\Sales\Model\Order\Payment;
use Magento\Store\Model\StoreManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;

/** @var \Magento\Catalog\Model\Product $product */

$addressData = include __DIR__
    . '/../../../../../../../dev/tests/integration/testsuite/Magento/Sales/_files/address_data.php';
$objectManager = Bootstrap::getObjectManager();
/** @var ProductRepositoryInterface $productRepository */
$productRepository = $objectManager->create(ProductRepositoryInterface::class);
$product = $productRepository->get('764503037108');
$billingAddress = $objectManager->create(OrderAddress::class, ['data' => $addressData]);
$shippingAddress = clone $billingAddress;
/** @var Payment $payment */
$payment = $objectManager->create(Payment::class);
/** @var OrderItem $orderItem */
$orderItem = $objectManager->create(OrderItem::class);
/** @var Order $order */
$order = $objectManager->create(Order::class);
/** @var OrderRepositoryInterface $orderRepository */
$orderRepository = $objectManager->create(OrderRepositoryInterface::class);
/** @var InvoiceManagementInterface $invoiceManagement */
$invoiceManagement = $objectManager->create(InvoiceManagementInterface::class);
/** @var Transaction $transaction */
$transaction = $objectManager->create(Transaction::class);

$billingAddress->setAddressType('billing');
$shippingAddress->setId(null)->setAddressType('shipping');

$payment->setMethod('gunbroker');

$orderItem->setProductId($product->getId())
    ->setQtyOrdered(1)
    ->setBasePrice($product->getPrice())
    ->setPrice($product->getPrice())
    ->setRowTotal($product->getPrice())
    ->setProductType('simple')
    ->setName($product->getName())
    ->setSku($product->getSku())
    ->setExtOrderItemId(11496586);

$order->setIncrementId('100000001')
    ->setState(Order::STATE_PROCESSING)
    ->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING))
    ->setSubtotal($product->getPrice())
    ->setGrandTotal($product->getPrice())
    ->setBaseSubtotal($product->getPrice())
    ->setBaseGrandTotal($product->getPrice())
    ->setOrderCurrencyCode('USD')
    ->setBaseCurrencyCode('USD')
    ->setCustomerIsGuest(true)
    ->setCustomerEmail('customer@example.com')
    ->setBillingAddress($billingAddress)
    ->setShippingAddress($shippingAddress)
    ->setStoreId($objectManager->get(StoreManagerInterface::class)->getStore()->getId())
    ->setExtOrderId(11496586)
    ->addItem($orderItem)
    ->setPayment($payment);

$orderRepository->save($order);

$invoice = $invoiceManagement->prepareInvoice($order);

/** @noinspection PhpUndefinedMethodInspection */
$invoice->setRequestedCaptureCase(Invoice::CAPTURE_OFFLINE);
$invoice->register();

/** @noinspection PhpUndefinedMethodInspection */
$order->setIsInProcess(true);

$transaction->addObject($invoice)->addObject($order)->save();
