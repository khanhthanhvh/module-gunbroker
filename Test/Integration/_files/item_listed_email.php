<?php

/** @codeCoverageIgnore  */

declare(strict_types=1);

use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterface;
use Reeds\GunBroker\Api\ItemListedEmailRepositoryInterface;

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var ItemListedEmailInterface $itemListedEmail */
$itemListedEmail = $objectManager->create(ItemListedEmailInterface::class);
/** @var ItemListedEmailRepositoryInterface $itemListedEmailRepository */
$itemListedEmailRepository = $objectManager->create(ItemListedEmailRepositoryInterface::class);

$itemListedEmail->setItemId(12345678);
$itemListedEmail->setStoreId(1);
$itemListedEmail->setItemListingUrl('https://www.sandbox.gunbroker.com/item/12345678');
$itemListedEmail->setItemData(file_get_contents(__DIR__ . '/../_data/item.json'));
$itemListedEmail->setProductUrl('https://reedssports.test/glock-gen5-g17-9mm');

$itemListedEmailRepository->save($itemListedEmail);
