<?php

/** @codeCoverageIgnore */

declare(strict_types=1);

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;

// WARNING: this class uses direct SQL queries to prevent triggering a plug-in
// which requests and stores a user identifier from GunBroker.com.
// Hic sunt dracones

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var ResourceConnection $resource */
$resource = $objectManager->get(ResourceConnection::class);
$connection = $resource->getConnection();
$table = $connection->getTableName('core_config_data');
$websiteId = $objectManager->create(WebsiteRepositoryInterface::class)->get('gunbroker')->getId();
$data = [
    [
        'scope' => 'default',
        'scope_id' => 0,
        'path' => 'gunbroker/integration/enabled',
        'value' => 0
    ],
    [
        'scope' => 'default',
        'scope_id' => 0,
        'path' => 'gunbroker/api/devkey',
        'value' => '6820a0e2-0618-4e0c-ad61-79552e06789e'
    ],
    [
        'scope' => 'websites',
        'scope_id' => $websiteId,
        'path' => 'gunbroker/integration/enabled',
        'value' => 1
    ],
    [
        'scope' => 'websites',
        'scope_id' => $websiteId,
        'path' => 'gunbroker/api/environment',
        'value' => 'sandbox'
    ],
    [
        'scope' => 'websites',
        'scope_id' => $websiteId,
        'path' => 'gunbroker/api/sandbox_username',
        'value' => 'user'
    ],
    [
        'scope' => 'websites',
        'scope_id' => $websiteId,
        'path' => 'gunbroker/api/sandbox_password',
        'value' => 'password'
    ],
    [
        'scope' => 'websites',
        'scope_id' => $websiteId,
        'path' => 'gunbroker/api/user_id',
        'value' => 12345
    ],
];

$connection->insertMultiple($table, $data);
