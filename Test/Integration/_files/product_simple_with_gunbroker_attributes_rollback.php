<?php

declare(strict_types=1);

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\TestFramework\Helper\Bootstrap;

Bootstrap::getInstance()->reinitialize();

/** @var ObjectManagerInterface $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var Registry $registry */
$registry = $objectManager->get(Registry::class);

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', true);

/** @var ProductRepositoryInterface $productRepository */
$productRepository = $objectManager->get(ProductRepositoryInterface::class);

try {
    $product = $productRepository->get('764503037108', false, null, true);
} catch (NoSuchEntityException $e) {
    $product = null;
}

if ($product instanceof Product && $product->getId() !== null) {
    $product->unsetData('gunbroker_premium_features');
    $product->unsetData('gunbroker_shipping_class_costs');
    $product->unsetData('gunbroker_shipping_classes_supported');

    try {
        $productRepository->delete($product);
    } catch (StateException $e) {
    }
}

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', false);
