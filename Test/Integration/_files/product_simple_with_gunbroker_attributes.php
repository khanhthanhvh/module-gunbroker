<?php

declare(strict_types=1);

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;
use Magento\Catalog\Model\Product\Media\Config as ProductMediaConfig;
use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;
use Magento\TestFramework\Workaround\Override\Fixture\Resolver;

Bootstrap::getInstance()->reinitialize();

/** @var ObjectManager $objectManager */
$objectManager = Bootstrap::getObjectManager();
/** @var EavSetup $eavSetup */
$eavSetup = $objectManager->create(EavSetup::class);
/** @var ProductRepositoryInterface $productRepository */
$productRepository = $objectManager->create(ProductRepositoryInterface::class);
/** @var Product $product */
$product = $objectManager->create(Product::class);
/** @var Category $category */
$category = $objectManager->create(Category::class)
    ->loadByAttribute('name', 'Firearms');
$attributeSetId = $eavSetup->getAttributeSetId(Product::ENTITY, 'Firearms');
/** @var AttributeRepositoryInterface $attributeRepository */
$attributeRepository = $objectManager->create(AttributeRepositoryInterface::class);
$slideFinishAttribute = $attributeRepository->get(Product::ENTITY, 'slide_finish');
$slideFinishAttributeOptions = $slideFinishAttribute->getOptions();
$slideFinishAttributeOptionId = 0;
/** @var ProductMediaConfig $mediaConfig */
$mediaConfig = $objectManager->create(ProductMediaConfig::class);
/** @var ReadInterface $mediaDirectory */
$mediaDirectory = $objectManager->get(Filesystem::class)
    ->getDirectoryWrite(DirectoryList::MEDIA);
$targetDirPath = $mediaConfig->getBaseMediaPath();
$targetTmpDirPath = $mediaConfig->getBaseTmpMediaPath();

$mediaDirectory->create($targetDirPath);
$mediaDirectory->create($targetTmpDirPath);

$absoluteMediaDispersionPath = $mediaDirectory->getAbsolutePath() . $mediaConfig->getBaseMediaPath()
    . '/g/l/glock_1.jpg';

if (file_exists($absoluteMediaDispersionPath)) {
    unlink($absoluteMediaDispersionPath);
}

$absoluteMediaPath = $mediaDirectory->getAbsolutePath(
    $mediaConfig->getBaseMediaPath() .  DIRECTORY_SEPARATOR . 'glock.jpg'
);

$mediaDirectory->getDriver()
    ->filePutContents($absoluteMediaPath, file_get_contents(__DIR__ . '/glock.jpg'));

foreach ($slideFinishAttributeOptions as $slideFinishAttributeOption) {
    if ($slideFinishAttributeOption->getLabel() !== 'Matte/Rough Black') {
        continue;
    }

    $slideFinishAttributeOptionId = $slideFinishAttributeOption->getValue();

    break;
}

$product->isObjectNew(true);
$product->setTypeId(ProductType::TYPE_SIMPLE);
$product->setWebsiteIds([1]);
$product->setName('Glock G17 Gen5 9mm 4.49In');
$product->setSku('764503037108');
$product->setStatus(ProductStatus::STATUS_ENABLED);
$product->setAttributeSetId($attributeSetId);
$product->setCategoryIds([$category->getId()]);
$product->setStockData(
    [
        'use_config_manage_stock' => 1,
        'qty' => 100,
        'is_in_stock' => 1
    ]
);
$product->setPrice(499.99);
$product->setWeight(2.0);
$product->addImageToMediaGallery(
    $absoluteMediaPath,
    [
        'image',
        'small_image',
    ],
    false,
    false
);
$product->setCustomAttribute('gtin', '00012345678905');
$product->setCustomAttribute('brand', 'Glock');
$product->setCustomAttribute('atf_model', 'Glock 45');
$product->setCustomAttribute('caliber_gauge', '9mm Luger (9 x 19mm)');
$product->setCustomAttribute('barrel_length', '4in');
$product->setCustomAttribute('stock_finish', 'Black');
$product->setCustomAttribute('frame_material', 'Steel');
$product->setCustomAttribute('frame_finish', 'Black');
$product->setCustomAttribute('slide_material', 'Steel');
$product->setCustomAttribute('slide_finish', $slideFinishAttributeOptionId);
$product->setCustomAttribute('capacity', '17 + 1 rounds');
$product->setCustomAttribute('front_sights', '3.7mm, polymer');
$product->setCustomAttribute('rear_sights', '4.3mm, polymer');
$product->setCustomAttribute('serial_number', '0012345');
$product->setCustomAttribute('style', 'PA1750203');
$product->setCustomAttribute('upc_ean', '764503022616');
$product->setCustomAttribute('is_gunbroker_item', true);
$product->setCustomAttribute('is_handgun', true);
$product->setCustomAttribute('gunbroker_item_id', 123);
$product->setCustomAttribute('gunbroker_auto_accept_price', $product->getPrice() - 20);
$product->setCustomAttribute('gunbroker_auto_reject_price', $product->getPrice() - 50);
$product->setCustomAttribute('gunbroker_auto_relist', 3);
$product->setCustomAttribute('gunbroker_auto_relist_fixed_count', 2);
$product->setCustomAttribute('gunbroker_buy_now_price', $product->getPrice() - 10);
$product->setCustomAttribute('gunbroker_category_id', 50);
$product->setCustomAttribute('gunbroker_can_offer', true);
$product->setCustomAttribute('gunbroker_collect_taxes', false);
$product->setCustomAttribute('gunbroker_condition', 2);
$product->setCustomAttribute('gunbroker_description_template', 'gunbroker_listing_handgun');
$product->setCustomAttribute('gunbroker_fixed_price', $product->getPrice());
$product->setCustomAttribute('gunbroker_is_ffl_required', true);
$product->setCustomAttribute('gunbroker_listing_duration', 7);
$product->setCustomAttribute(
    'gunbroker_premium_features',
    [
        [
            'record_id' => 0,
            'feature' => 'hasViewCounter',
            'enabled' => false
        ],
        [
            'record_id' => 1,
            'feature' => 'isFeaturedItem',
            'enabled' => true
        ],
        [
            'record_id' => 2,
            'feature' => 'isHighlighted',
            'enabled' => true
        ],
        [
            'record_id' => 3,
            'feature' => 'isShowCaseItem',
            'enabled' => false
        ],
        [
            'record_id' => 4,
            'feature' => 'isSponsoredOnsite',
            'enabled' => false
        ],
        [
            'record_id' => 5,
            'feature' => 'isTitleBoldface',
            'enabled' => true
        ]
    ]
);
$product->setCustomAttribute('gunbroker_premium_features_scheduled_start_date', '1970-01-01 00:00:00');
$product->setCustomAttribute('gunbroker_premium_features_subtitle', 'Test');
$product->setCustomAttribute('gunbroker_premium_features_thumbnail_url', '/t/e/test.jpg');
$product->setCustomAttribute('gunbroker_premium_features_title_color', 'Blue');
$product->setCustomAttribute('gunbroker_quantity_percentage', 66);
$product->setCustomAttribute(
    'gunbroker_shipping_class_costs',
    [
        [
            'record_id' => 0,
            'shipping_class' => 'overnight',
            'cost' => 49.00
        ],
        [
            'record_id' => 1,
            'shipping_class' => 'twoDay',
            'cost' => 39.00
        ],
        [
            'record_id' => 2,
            'shipping_class' => 'threeDay',
            'cost' => 29.00
        ],
        [
            'record_id' => 3,
            'shipping_class' => 'ground',
            'cost' => 9.99
        ]
    ]

);
$product->setCustomAttribute(
    'gunbroker_shipping_classes_supported',
    [
        [
            'record_id' => 0,
            'shipping_class' => 'overnight',
            'supported' => true
        ],
        [
            'record_id' => 1,
            'shipping_class' => 'twoDay',
            'supported' => true
        ],
        [
            'record_id' => 2,
            'shipping_class' => 'threeDay',
            'supported' => true
        ],
        [
            'record_id' => 3,
            'shipping_class' => 'ground',
            'supported' => true
        ],
        [
            'record_id' => 4,
            'shipping_class' => 'firstClass',
            'supported' => false
        ],
        [
            'record_id' => 5,
            'shipping_class' => 'priority',
            'supported' => false
        ],
        [
            'record_id' => 6,
            'shipping_class' => 'other',
            'supported' => false
        ]
    ]
);
$product->setCustomAttribute('gunbroker_shipping_profile_id', 42);
$product->setCustomAttribute('gunbroker_starting_bid', $product->getPrice() - 75);
$product->setCustomAttribute('gunbroker_who_pays_for_shipping', 2);

$productRepository->save($product);
