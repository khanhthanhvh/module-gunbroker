<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ObjectManager;
use Magento\Payment\Api\Data\PaymentMethodInterface;
use Magento\Payment\Api\PaymentMethodListInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;

use function array_map;
use function array_values;

final class PaymentMethodTest extends TestCase
{
    public function testPaymentMethodIsActive(): void
    {
        /** @var ObjectManager $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $paymentMethodList = $objectManager->get(PaymentMethodListInterface::class);
        $paymentMethodCodes = array_map(
            fn(PaymentMethodInterface $paymentMethod) => $paymentMethod->getCode(),
            $paymentMethodList->getActiveList(0)
        );

        self::assertContains('gunbroker', $paymentMethodCodes);
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     */
    public function testPaymentMethodCanBeUsedProgrammatically(): void
    {
        /** @var ObjectManager $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $objectManager->create(OrderRepositoryInterface::class);
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $objectManager->create(SearchCriteriaBuilder::class);
        $searchCriteria = $searchCriteriaBuilder->addFilter('increment_id', '100000001')
            ->setPageSize(1)
            ->setCurrentPage(1)
            ->create();
        $order = array_values($orderRepository->getList($searchCriteria)->getItems())[0];

        self::assertSame('gunbroker', $order->getPayment()->getMethod());
    }
}
