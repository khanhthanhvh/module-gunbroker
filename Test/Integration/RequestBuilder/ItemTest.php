<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\RequestBuilder;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\RequestBuilder\Item;
use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\SalesTaxes;

use function array_map;
use function file_get_contents;
use function json_decode;
use function ksort;
use function property_exists;

final class ItemTest extends TestCase
{
    /**
     * @magentoAppArea frontend
     * @magentoConfigFixture current_store general/store_information/country_id CA
     * @magentoConfigFixture current_store gunbroker/attributes/inspection_period 1
     * @magentoConfigFixture current_store gunbroker/attributes/payment_methods {"check":{"method":"check","accepted":"1"},"visa_mastercard":{"method":"visaMastercard","accepted":"1"},"cod":{"method":"cod","accepted":"0"},"escrow":{"method":"escrow","accepted":"0"},"amex":{"method":"amex","accepted":"1"},"paypal":{"method":"payPal","accepted":"0"},"discover":{"method":"discover","accepted":"0"},"see_item_desc":{"method":"seeItemDesc","accepted":"0"},"certified_check":{"method":"certifiedCheck","accepted":"0"},"usps_money_order":{"method":"uspsMoneyOrder","accepted":"0"},"money_order":{"method":"moneyOrder","accepted":"0"},"freedom_coin":{"method":"freedomCoin","accepted":"0"}}
     * @magentoConfigFixture current_store gunbroker/attributes/postal_code 56484
     * @magentoConfigFixture current_store gunbroker/attributes/sales_taxes {"_1606239956280_280":{"state":"TX","tax_rate":"6.25"},"_1606239969315_315":{"state":"MN","tax_rate":"6.875"}}
     * @magentoConfigFixture current_store gunbroker/attributes/standard_text_id 39
     * @magentoConfigFixture current_store gunbroker/attributes/use_default_sales_tax 0
     * @magentoConfigFixture current_store gunbroker/attributes/will_ship_international 0
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/static_block_content_listing_template.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testBuildsItem(): void
    {
        $expectedItemData = json_decode(
            file_get_contents(__DIR__ . '/../_data/item.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        /** @var ObjectManager $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var Item $itemBuilder */
        $itemBuilder = $objectManager->create(Item::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108', false, null, true);
        /** @var StoreManagerInterface $storeManager */
        $storeManager = $objectManager->get(StoreManagerInterface::class);
        $item = $itemBuilder->build([
            'product' => $product,
            'store' => $storeManager->getDefaultStoreView()
        ]);
        $actualItemData = $item->except(
            'paymentMethods',
            'premiumFeatures',
            'salesTaxes',
            'shippingClassCosts',
            'shippingClassesSupported'
        )->toArray();
        $actualItemData['paymentMethods'] = $this->getChildObjectData('paymentMethods', $item);
        $actualItemData['premiumFeatures'] = $this->getChildObjectData('premiumFeatures', $item);
        $actualItemData['salesTaxes'] = array_map(
            fn(SalesTaxes $salesTaxes): array => $salesTaxes->all(),
            $item->salesTaxes
        );
        $actualItemData['shippingClassCosts'] = $this->getChildObjectData('shippingClassCosts', $item);
        $actualItemData['shippingClassesSupported'] = $this->getChildObjectData('shippingClassesSupported', $item);

        ksort($actualItemData);

        self::assertEquals($expectedItemData, $actualItemData);
    }

    /**
     * @return mixed[]
     */
    private function getChildObjectData(string $childObjectName, DataTransferObject $parentObject): array
    {
        if (!property_exists($parentObject, $childObjectName)) {
            return [];
        }

        return $parentObject->{$childObjectName}->all();
    }
}
