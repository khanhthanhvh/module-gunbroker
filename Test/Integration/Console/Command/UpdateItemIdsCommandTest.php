<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Console\Command;

use Magento\Framework\Console\CommandListInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Console\Command\UpdateItemIdsCommand;
use Reeds\GunBroker\Service\ItemIdUpdater;
use Reeds\GunBroker\Service\ItemIdUpdaterFactory;
use Symfony\Component\Console\Tester\CommandTester;

use function count;
use function preg_replace;

final class UpdateItemIdsCommandTest extends TestCase
{
    public function testCommandIsRegistered(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $commands = $objectManager->get(CommandListInterface::class)->getCommands();

        self::assertArrayHasKey('gunbroker_update_item_ids_command', $commands);
        self::assertInstanceOf(UpdateItemIdsCommand::class, $commands['gunbroker_update_item_ids_command']);
    }

    public function testCommandIsConfiguredProperly(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $updateItemIdsCommand = $objectManager->create(UpdateItemIdsCommand::class);

        self::assertSame(UpdateItemIdsCommand::COMMAND_NAME, $updateItemIdsCommand->getName());
        self::assertTrue($updateItemIdsCommand->getDefinition()->hasOption('website'));
        self::assertTrue($updateItemIdsCommand->getDefinition()->hasShortcut('w'));
    }

    /**
     * @dataProvider updatesItemIdsDataProvider
     * @param int[] $websiteIds
     * @param array<int[]> $updatedProducts
     */
    public function testCommandUpdatesItemIdsInWebsites(
        array $websiteIds,
        array $updatedProducts,
        string $expectedOutput
    ): void {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $itemIdUpdaterFactoryMock = $this->createMock(ItemIdUpdaterFactory::class);
        $itemIdUpdaterMock = $this->createMock(ItemIdUpdater::class);
        /** @var UpdateItemIdsCommand $updateItemIdsCommand */
        $updateItemIdsCommand = $objectManager->create(
            UpdateItemIdsCommand::class,
            [
                'itemIdUpdaterFactory' => $itemIdUpdaterFactoryMock
            ]
        );
        $commandOptions = [];
        /** @var CommandTester $commandTester */
        $commandTester = $objectManager->create(
            CommandTester::class,
            [
                'command' => $updateItemIdsCommand
            ]
        );

        $itemIdUpdaterFactoryMock->expects(self::once())
            ->method('create')
            ->willReturn($itemIdUpdaterMock);

        $itemIdUpdaterMock->expects(self::once())
            ->method('updateInWebsites')
            ->with($websiteIds)
            ->willReturn($updatedProducts);

        if (count($websiteIds) > 0) {
            $commandOptions['--website'] = $websiteIds;
        }

        $commandTester->execute($commandOptions);

        $actualOutput = preg_replace('/\h+$/m', '', $commandTester->getDisplay()) ?? '';

        self::assertStringContainsString($expectedOutput, $actualOutput);
        self::assertSame(0, $commandTester->getStatusCode());
    }

    /**
     * @return array<string, array<string, array<int, array<int, int>|int>|string>>
     */
    public function updatesItemIdsDataProvider(): array
    {
        return [
            'no_item_ids_updated' => [
                'websiteIds' => [1, 2, 3],
                'updatedProducts' => [],
                'expectedOutput' => 'No new item identifiers are available for update.'
            ],
            'item_ids_updated' => [
                'websiteIds' => [],
                'updatedProducts' => [
                    1 => [
                        12345 => 1,
                        67890 => 5
                    ],
                    3 => [
                        42069 => 42
                    ]
                ],
                'expectedOutput' => <<<OUTPUT
                 [OK] Updated item identifiers for the following products:

                 ------------ ------------------- --------------------
                  Website ID   GunBroker Item ID   Magento Product ID
                 ------------ ------------------- --------------------
                  1            12345               1
                  1            67890               5
                  3            42069               42
                 ------------ ------------------- --------------------
                OUTPUT
            ],
        ];
    }
}
