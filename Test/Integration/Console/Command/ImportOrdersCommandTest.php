<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Console\Command;

use Magento\Framework\Console\CommandListInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Console\Command\ImportOrdersCommand;
use Reeds\GunBroker\Service\OrderImporter;
use Reeds\GunBroker\Service\OrderImporterFactory;
use Symfony\Component\Console\Tester\CommandTester;

use function count;
use function preg_replace;

final class ImportOrdersCommandTest extends TestCase
{
    public function testCommandIsRegistered(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $commandList = $objectManager->get(CommandListInterface::class);
        $commands = $commandList->getCommands();

        self::assertArrayHasKey('gunbroker_import_orders_command', $commands);
        self::assertInstanceOf(ImportOrdersCommand::class, $commands['gunbroker_import_orders_command']);
    }

    public function testCommandIsConfiguredProperly(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $importOrdersCommand = $objectManager->create(ImportOrdersCommand::class);

        self::assertSame(ImportOrdersCommand::COMMAND_NAME, $importOrdersCommand->getName());
        self::assertTrue($importOrdersCommand->getDefinition()->hasOption('website'));
        self::assertTrue($importOrdersCommand->getDefinition()->hasShortcut('w'));
        self::assertTrue($importOrdersCommand->getDefinition()->hasOption('time-frame'));
        self::assertTrue($importOrdersCommand->getDefinition()->hasShortcut('t'));
    }

    /**
     * @dataProvider importsOrdersDataProvider
     * @param int[] $websiteIds
     * @param int[]|null $importedOrderIds
     */
    public function testCommandImportsOrdersForWebsites(
        array $websiteIds,
        ?array $importedOrderIds,
        string $expectedOutput,
        int $expectedReturnCode
    ): void {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $orderImporterFactoryMock = $this->createMock(OrderImporterFactory::class);
        $orderImporterMock = $this->createMock(OrderImporter::class);
        /** @var ImportOrdersCommand $importOrdersCommand */
        $importOrdersCommand = $objectManager->create(
            ImportOrdersCommand::class,
            [
                'orderImporterFactory' => $orderImporterFactoryMock
            ]
        );
        $commandOptions = [];
        /** @var CommandTester $commandTester */
        $commandTester = $objectManager->create(
            CommandTester::class,
            [
                'command' => $importOrdersCommand
            ]
        );

        $orderImporterFactoryMock->expects(self::once())
            ->method('create')
            ->willReturn($orderImporterMock);

        $orderImporterMock->expects(self::once())
            ->method('importOrders')
            ->with($websiteIds)
            ->willReturn($importedOrderIds);

        if (count($websiteIds) > 0) {
            $commandOptions['--website'] = $websiteIds;
        }

        $commandTester->execute($commandOptions);

        $actualOutput = preg_replace('/\h+$/m', '', $commandTester->getDisplay());

        self::assertSame($expectedOutput, $actualOutput);
        self::assertSame($expectedReturnCode, $commandTester->getStatusCode());
    }

    /**
     * @return array<string, array<string, array<int|string, array<int, int|null>|int>|int|string|null>>
     */
    public function importsOrdersDataProvider(): array
    {
        return [
            'no_orders_imported' => [
                'websiteIds' => [1, 2, 3],
                'importedOrderIds' => null,
                'expectedOutput' => <<<OUTPUT

                GunBroker.com Order Import
                ==========================

                 Importing Orders from GunBroker.com...

                No orders are available for import.

                OUTPUT,
                'expectedReturnCode' => 0
            ],
            'order_imports_failed' => [
                'websiteIds' => [],
                'importedOrderIds' => [
                    'complete' => [
                    ],
                    'failed' => [
                        12345 => null,
                        67890 => null
                    ]
                ],
                'expectedOutput' => <<<OUTPUT

                GunBroker.com Order Import
                ==========================

                 Importing Orders from GunBroker.com...
                 Import complete.

                 Import Results:
                 -------------------- ------------------ --------
                  GunBroker Order ID   Magento Order ID   Status
                 -------------------- ------------------ --------
                  12345                                   Failed
                  67890                                   Failed
                 -------------------- ------------------ --------


                OUTPUT,
                'expectedReturnCode' => 1
            ],
            'order_imports_succeeded' => [
                'websiteIds' => [],
                'importedOrderIds' => [
                    'complete' => [
                        12345 => 1,
                        67890 => 2
                    ],
                    'failed' => [
                    ]
                ],
                'expectedOutput' => <<<OUTPUT

                GunBroker.com Order Import
                ==========================

                 Importing Orders from GunBroker.com...
                 Import complete.

                 Import Results:
                 -------------------- ------------------ ----------
                  GunBroker Order ID   Magento Order ID   Status
                 -------------------- ------------------ ----------
                  12345                1                  Complete
                  67890                2                  Complete
                 -------------------- ------------------ ----------


                OUTPUT,
                'expectedReturnCode' => 0
            ],
            'order_imports_succeeded_and_failed' => [
                'websiteIds' => [],
                'importedOrderIds' => [
                    'complete' => [
                        12345 => 1,
                        67890 => 2
                    ],
                    'failed' => [
                        55555 => null,
                        77777 => null
                    ]
                ],
                'expectedOutput' => <<<OUTPUT

                GunBroker.com Order Import
                ==========================

                 Importing Orders from GunBroker.com...
                 Import complete.

                 Import Results:
                 -------------------- ------------------ ----------
                  GunBroker Order ID   Magento Order ID   Status
                 -------------------- ------------------ ----------
                  12345                1                  Complete
                  67890                2                  Complete
                  55555                                   Failed
                  77777                                   Failed
                 -------------------- ------------------ ----------


                OUTPUT,
                'expectedReturnCode' => 1
            ],
        ];
    }
}
