<?php

// phpcs:disable Generic.Files.LineLength.TooLong

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Console\Command;

use Magento\Framework\Console\CommandListInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Console\Command\ImportFeedbackCommand;
use Reeds\GunBroker\Service\FeedbackImporter;
use Reeds\GunBroker\Service\FeedbackImporterFactory;
use Symfony\Component\Console\Tester\CommandTester;

use function preg_replace;

final class ImportFeedbackCommandTest extends TestCase
{
    private ObjectManagerInterface $objectManager;

    public function testCommandIsRegistered(): void
    {
        $commands = $this->objectManager->get(CommandListInterface::class)->getCommands();

        self::assertArrayHasKey('gunbroker_import_feedback_command', $commands);
        self::assertInstanceOf(ImportFeedbackCommand::class, $commands['gunbroker_import_feedback_command']);
    }

    public function testCommandIsConfigured(): void
    {
        $importFeedbackCommand = $this->objectManager->create(ImportFeedbackCommand::class);

        self::assertSame(ImportFeedbackCommand::COMMAND_NAME, $importFeedbackCommand->getName());
        self::assertTrue($importFeedbackCommand->getDefinition()->hasOption('website'));
        self::assertTrue($importFeedbackCommand->getDefinition()->hasShortcut('w'));
    }
    /**
     * @dataProvider importsFeedbackDataProvider
     * @param int[] $websiteIds
     * @param array<int, array{succeeded: OrderItemFeedbackInterface[], failed: OrderItemFeedbackInterface[]}> $importedFeedback
     */
    public function testCommandImportsFeedbackForWebsites(
        array $websiteIds,
        array $importedFeedback,
        string $expectedOutput,
        int $expectedReturnCode
    ): void {
        $feedbackImporterFactoryMock = $this->createMock(FeedbackImporterFactory::class);
        $feedbackImporterMock = $this->createMock(FeedbackImporter::class);
        /** @var ImportFeedbackCommand $importFeedbackCommand */
        $importFeedbackCommand = $this->objectManager->create(
            ImportFeedbackCommand::class,
            [
                'feedbackImporterFactory' => $feedbackImporterFactoryMock
            ]
        );
        $commandOptions = [];
        /** @var CommandTester $commandTester */
        $commandTester = $this->objectManager->create(
            CommandTester::class,
            [
                'command' => $importFeedbackCommand
            ]
        );

        $feedbackImporterFactoryMock->expects(self::once())
            ->method('create')
            ->willReturn($feedbackImporterMock);

        $feedbackImporterMock->expects(self::once())
            ->method('importInWebsites')
            ->with($websiteIds)
            ->willReturn($importedFeedback);

        if (count($websiteIds) > 0) {
            $commandOptions['--website'] = $websiteIds;
        }

        $commandTester->execute($commandOptions);

        $actualOutput = preg_replace('/\h+$/m', '', $commandTester->getDisplay());

        self::assertSame($expectedOutput, $actualOutput);
        self::assertSame($expectedReturnCode, $commandTester->getStatusCode());
    }

    /**
     * @return array<string, array<string, array<int, array<string, array<int, mixed>>|int>|int|string>>
     */
    public function importsFeedbackDataProvider(): array
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();

        return [
            'no_feedback_imported' => [
                'websiteIds' => [1, 2, 3],
                'importedFeedback' => [],
                'expectedOutput' => <<<OUTPUT

                GunBroker.com Feedback Importer
                ===============================

                 Importing feedback for order items in websites 1, 2, 3...

                 No order item feedback is available for import.

                OUTPUT,
                'expectedReturnCode' => 0
            ],
            'feedback_imported_success_and_failure' => [
                'websiteIds' => [],
                'importedFeedback' => [
                    1 => [
                        'succeeded' => [
                            $objectManager->create(
                                OrderItemFeedbackInterface::class,
                                [
                                    'data' => [
                                        'gunbroker_item_id' => 12345,
                                        'magento_order_item_id' => 12
                                    ]
                                ]
                            ),
                            $objectManager->create(
                                OrderItemFeedbackInterface::class,
                                [
                                    'data' => [
                                        'gunbroker_item_id' => 67890,
                                        'magento_order_item_id' => 27
                                    ]
                                ]
                            )
                        ],
                        'failed' => [
                        ]
                    ],
                    3 => [
                        'succeeded' => [
                        ],
                        'failed' => [
                            $objectManager->create(
                                OrderItemFeedbackInterface::class,
                                [
                                    'data' => [
                                        'gunbroker_item_id' => 54321,
                                        'magento_order_item_id' => 3
                                    ]
                                ]
                            ),
                            $objectManager->create(
                                OrderItemFeedbackInterface::class,
                                [
                                    'data' => [
                                        'gunbroker_item_id' => 97860,
                                        'magento_order_item_id' => 44
                                    ]
                                ]
                            )
                        ]
                    ],
                    5 => [
                        'succeeded' => [
                            $objectManager->create(
                                OrderItemFeedbackInterface::class,
                                [
                                    'data' => [
                                        'gunbroker_item_id' => 21435,
                                        'magento_order_item_id' => 90
                                    ]
                                ]
                            )
                        ],
                        'failed' => [
                            $objectManager->create(
                                OrderItemFeedbackInterface::class,
                                [
                                    'data' => [
                                        'gunbroker_item_id' => 76980,
                                        'magento_order_item_id' => 55
                                    ]
                                ]
                            )
                        ]
                    ]
                ],
                'expectedOutput' => <<<OUTPUT

                GunBroker.com Feedback Importer
                ===============================

                 Importing feedback for order items in all websites...
                 Import complete.

                 Import Results:
                 ------------ ------------------- ----------------- -----------
                  Website ID   GunBroker Item ID   Magento Item ID   Status
                 ------------ ------------------- ----------------- -----------
                  1            12345               12                Succeeded
                  1            67890               27                Succeeded
                  3            54321               3                 Failed
                  3            97860               44                Failed
                  5            21435               90                Succeeded
                  5            76980               55                Failed
                 ------------ ------------------- ----------------- -----------


                OUTPUT,
                'expectedReturnCode' => 1
            ],
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->objectManager = Bootstrap::getObjectManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->objectManager);
    }
}
