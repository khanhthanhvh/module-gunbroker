<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Console\Command;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Console\CommandListInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Console\Command\ListItemCommand;
use Reeds\GunBroker\Service\GunBrokerItemCreator;
use Reeds\GunBroker\Service\GunBrokerItemCreatorFactory;
use Symfony\Component\Console\Tester\CommandTester;

use function preg_replace;

final class ListItemCommandTest extends TestCase
{
    private ObjectManagerInterface $objectManager;

    public function testCommandIsRegistered(): void
    {
        $commands = $this->objectManager->get(CommandListInterface::class)->getCommands();

        self::assertArrayHasKey('gunbroker_list_item_command', $commands);
        self::assertInstanceOf(ListItemCommand::class, $commands['gunbroker_list_item_command']);
    }

    public function testCommandIsConfigured(): void
    {
        /** @var ListItemCommand $listItemCommand */
        $listItemCommand = $this->objectManager->create(ListItemCommand::class);

        self::assertSame(ListItemCommand::COMMAND_NAME, $listItemCommand->getName());
        self::assertTrue($listItemCommand->getDefinition()->hasArgument('product_id'));
        self::assertTrue($listItemCommand->getDefinition()->hasOption('queue'));
        self::assertTrue($listItemCommand->getDefinition()->hasShortcut('Q'));
        self::assertTrue($listItemCommand->getDefinition()->hasOption('notify'));
        self::assertTrue($listItemCommand->getDefinition()->hasShortcut('N'));
        self::assertTrue($listItemCommand->getDefinition()->hasOption('store'));
        self::assertTrue($listItemCommand->getDefinition()->hasShortcut('s'));
    }

    /**
     * @magentoAppArea frontend
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testCommandCreatesItemOnGunBroker(): void
    {
        $configMock = $this->createMock(ConfigInterface::class);
        $gunBrokerItemCreatorFactoryMock = $this->createMock(GunBrokerItemCreatorFactory::class);
        $gunBrokerItemCreatorMock = $this->createMock(GunBrokerItemCreator::class);
        /** @var ListItemCommand $listItemCommand */
        $listItemCommand = $this->objectManager->create(
            ListItemCommand::class,
            [
                'config' => $configMock,
                'gunBrokerItemCreatorFactory' => $gunBrokerItemCreatorFactoryMock
            ]
        );
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $this->objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108');
        $commandOptions = [
            'product_id' => $product->getId(),
            '--store' => 1,
        ];
        /** @var CommandTester $commandTester */
        $commandTester = $this->objectManager->create(
            CommandTester::class,
            [
                'command' => $listItemCommand
            ]
        );

        $configMock->method('isEnabled')
            ->with(1)
            ->willReturn(true);

        $gunBrokerItemCreatorFactoryMock->expects(self::once())
            ->method('create')
            ->willReturn($gunBrokerItemCreatorMock);
        $gunBrokerItemCreatorMock->expects(self::once())
            ->method('createItem')
            ->with($product->getId(), 1, false)
            ->willReturn(true);

        $commandTester->execute($commandOptions);

        $expectedOutput = <<<OUTPUT

        GunBroker.com Item Creator
        ==========================

         Creating item from product "764503037108"...

         [OK] The item has successfully been created on GunBroker.com.

         GunBroker.com item identifier: 123

        OUTPUT;
        $actualOutput = preg_replace('/\h+$/m', '', $commandTester->getDisplay());

        self::assertSame($expectedOutput, $actualOutput);
        self::assertSame(0, $commandTester->getStatusCode());
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->objectManager = Bootstrap::getObjectManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->objectManager);
    }
}
