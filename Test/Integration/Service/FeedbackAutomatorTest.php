<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Api\Data\OrderStatusHistoryInterface;
use Magento\Sales\Model\Order;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Model\Config;
use Reeds\GunBroker\Service\FeedbackAutomator;
use Reeds\GunBroker\Service\FeedbackCreator;

use function array_map;
use function current;

final class FeedbackAutomatorTest extends TestCase
{
    private ObjectManagerInterface $objectManager;

    /**
     * @dataProvider passesValidationDataProvider
     * @magentoDataFixture Magento/Sales/_files/order.php
     */
    public function testAutomateDoesNotSendFeedbackForInvalidOrders(
        bool $setExternalId,
        bool $setState,
        bool $isEnabled,
        bool $isFeedbackEnabled
    ): void {
        $configMock = $this->createMock(ConfigInterface::class);
        $feedbackAutomator = $this->objectManager->create(
            FeedbackAutomator::class,
            [
                'config' => $configMock
            ]
        );
        /** @var Order $order */
        $order = $this->objectManager->create(Order::class)->loadByIncrementId('100000001');

        if ($setExternalId) {
            $order->setExtOrderId('12345');
        }

        if ($setState) {
            $order->setState(Order::STATE_COMPLETE);
        }

        $configMock->method('isEnabled')
            ->willReturn($isEnabled);
        $configMock->method('isFeedbackEnabled')
            ->willReturn($isFeedbackEnabled);

        $itemIds = $feedbackAutomator->automate($order);

        self::assertEmpty($itemIds);
    }

    /**
     * @magentoConfigFixture current_store gunbroker/feedback/enabled 1
     * @magentoConfigFixture current_store gunbroker/feedback/default_rating 5
     * @magentoConfigFixture current_store gunbroker/feedback/default_comment Good buyer
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order_import.php
     */
    public function testAutomateSendsFeedbackAndAddsCommentToOrder(): void
    {
        $configMock = $this->getMockBuilder(Config::class)
            ->onlyMethods(['isEnabled'])
            ->setConstructorArgs(
                [
                    'scopeConfig' => $this->objectManager->get(ScopeConfigInterface::class),
                    'serializer' => $this->objectManager->create(SerializerInterface::class)
                ]
            )->getMock();
        $feedbackCreatorMock = $this->createMock(FeedbackCreator::class);
        $feedbackAutomator = $this->objectManager->create(
            FeedbackAutomator::class,
            [
                'config' => $configMock,
                'feedbackCreator' => $feedbackCreatorMock
            ]
        );
        /** @var Order $order */
        $order = $this->objectManager->create(Order::class)->loadByIncrementId('100000001');
        $gunBrokerItemId = (int)current($order->getItems())->getExtOrderItemId();

        $configMock->method('isEnabled')
            ->willReturn(true);

        $feedbackCreatorMock->method('createFeedback')
            ->willReturn(true);

        $order->setState(Order::STATE_COMPLETE);

        $itemIds = $feedbackAutomator->automate($order);

        self::assertContains($gunBrokerItemId, $itemIds);
        self::assertContains(
            "Feedback has automatically been sent to GunBroker for the following item: $gunBrokerItemId.",
            array_map(
                static fn(OrderStatusHistoryInterface $orderStatusHistory): string => $orderStatusHistory->getComment(),
                $order->getAllStatusHistory()
            )
        );
    }

    /**
     * @magentoConfigFixture current_store gunbroker/feedback/enabled 1
     * @magentoConfigFixture current_store gunbroker/feedback/default_rating 5
     * @magentoConfigFixture current_store gunbroker/feedback/default_comment Good buyer
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order_item_feedback.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order_import.php
     */
    public function testAutomateDoesNotSendFeedbackForItemsWithFeedback(): void
    {
        $configMock = $this->getMockBuilder(Config::class)
            ->onlyMethods(['isEnabled'])
            ->setConstructorArgs(
                [
                    'scopeConfig' => $this->objectManager->get(ScopeConfigInterface::class),
                    'serializer' => $this->objectManager->create(SerializerInterface::class)
                ]
            )->getMock();
        $feedbackAutomator = $this->objectManager->create(
            FeedbackAutomator::class,
            [
                'config' => $configMock
            ]
        );
        /** @var Order $order */
        $order = $this->objectManager->create(Order::class)->loadByIncrementId('100000001');

        $configMock->method('isEnabled')
            ->willReturn(true);

        $order->setState(Order::STATE_COMPLETE);

        $itemIds = $feedbackAutomator->automate($order);

        self::assertEmpty($itemIds);
    }

    /**
     * @return array<string, array<string, bool>>
     */
    public function passesValidationDataProvider(): array
    {
        return [
            'order_does_not_have_external_id' => [
                'setExternalId' => false,
                'setState' => false,
                'isEnabled' => true,
                'isFeedbackEnabled' => true
            ],
            'order_is_not_complete' => [
                'setExternalId' => true,
                'setState' => false,
                'isEnabled' => false,
                'isFeedbackEnabled' => false
            ],
            'extension_is_disabled' => [
                'setExternalId' => true,
                'setState' => true,
                'isEnabled' => false,
                'isFeedbackEnabled' => false
            ],
            'feedback_is_disabled' => [
                'setExternalId' => true,
                'setState' => true,
                'isEnabled' => true,
                'isFeedbackEnabled' => false
            ],
            'is_not_gunbroker_order' => [
                'setExternalId' => true,
                'setState' => true,
                'isEnabled' => true,
                'isFeedbackEnabled' => true
            ],
        ];
    }

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();

    }

    protected function tearDown(): void
    {
        unset($this->objectManager);
    }
}
