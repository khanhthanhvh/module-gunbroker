<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use JsonException;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Reeds\GunBroker\Service\GunBrokerItemCreator;
use Reeds\GunBroker\Service\ListItemApiRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Item;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

use function __;
use function file_get_contents;
use function json_decode;

final class GunBrokerItemCreatorTest extends TestCase
{
    /**
     * @magentoAppArea adminhtml
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testCreatesItemOnGunBrokerThenUpdatesItemIdAttributeAndLogsResult(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var TestLogger $testLogger */
        $testLogger = $objectManager->create(TestLogger::class);
        $listItemApiRequestMock = $this->createPartialMock(ListItemApiRequest::class, ['getResult', 'getListedItem']);
        /** @var GunBrokerItemCreator $itemCreator */
        $itemCreator = $objectManager->create(
            GunBrokerItemCreator::class,
            [
                'logger' => $testLogger,
                'listItemApiRequest' => $listItemApiRequestMock
            ]
        );
        /** @var MessageResponse $messageResponse */
        $messageResponse = $objectManager->create(
            MessageResponse::class,
            [
                'parameters' => [
                    'developerMessage' => 'Item listed with itemID: 11569005',
                    'userMessage' => 'Item listed with itemID: 11569005',
                    'gbStatusCode' => 0,
                    'links' => [
                        [
                            'href' => 'https://api.sandbox.gunbroker.com/v1/items/11569005',
                            'rel' => 'self',
                            'verb' => 'GET',
                            'contentType' => null,
                            'title' => '11569005'
                        ]
                    ]
                ]
            ]
        );
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        /** @var Product $product */
        $product = $productRepository->get('764503037108');

        $listItemApiRequestMock->method('getResult')
            ->willReturn($messageResponse);
        $listItemApiRequestMock->method('getListedItem')
            ->willReturn($this->getTestItem());

        $product->unsetData('gunbroker_item_id');
        $product->addAttributeUpdate('gunbroker_item_id', null, 0);

        $productId = (int)$product->getId();
        $result = $itemCreator->createItem($productId, 1, false);

        self::assertTrue($result);
        self::assertSame(
            '11569005',
            $productRepository->get('764503037108', false, null, true)
                ->getCustomAttribute('gunbroker_item_id')
                ->getValue()
        );
        self::assertTrue(
            $testLogger->hasInfoThatContains(
                (string)__(
                    'Product with ID "%1" was successfully listed on GunBroker.com. Item ID: %2',
                    $productId,
                    '11569005'
                )
            )
        );
    }

    /**
     * @throws JsonException
     */
    private function getTestItem(): Item
    {
        $itemData = json_decode(file_get_contents(__DIR__ . '/../_data/item.json'), true, 512, JSON_THROW_ON_ERROR);

        return new Item($itemData);
    }
}
