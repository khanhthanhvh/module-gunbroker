<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\Service\ListItemApiRequest;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Connector;

final class ListItemApiRequestTest extends TestCase
{
    /**
     * @magentoAppArea adminhtml
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testSendsListItemApiRequestAndReturnsMessageResponse(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $connectorFactoryMock = $this->createMock(GunBrokerConnectorFactory::class);
        $connectorMock = $this->getMockBuilder(Connector::class)
            ->disableOriginalConstructor()
            ->addMethods(['listItem'])
            ->getMock();
        /** @var ListItemApiRequest $listItemApiRequest */
        $listItemApiRequest = $objectManager->create(
            ListItemApiRequest::class,
            [
                'connectorFactory' => $connectorFactoryMock,
            ]
        );
        /** @var MessageResponse $messageResponse */
        $messageResponse = $objectManager->create(
            MessageResponse::class,
            [
                'parameters' => [
                    'developerMessage' => 'Item listed with itemID: 11569005',
                    'userMessage' => 'Item listed with itemID: 11569005',
                    'gbStatusCode' => 0,
                    'links' => [
                        [
                            'href' => 'https://api.sandbox.gunbroker.com/v1/items/11569005',
                            'rel' => 'self',
                            'verb' => 'GET',
                            'contentType' => null,
                            'title' => '11569005'
                        ]
                    ]
                ]
            ]
        );
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        /** @var Product $product */
        $product = $productRepository->get('764503037108');

        $connectorFactoryMock->method('create')
            ->willReturn($connectorMock);

        $connectorMock->method('listItem')
            ->willReturn($messageResponse);

        $result = $listItemApiRequest->setProduct($product)
            ->setStore($product->getStore())
            ->getResult();

        self::assertInstanceOf(MessageResponse::class, $result);
        self::assertEquals($messageResponse->toArray(), $result->toArray());
    }
}
