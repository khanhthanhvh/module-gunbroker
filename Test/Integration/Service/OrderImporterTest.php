<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Generator;
use JsonException;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\Data\OrderImportInterface;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;
use Reeds\GunBroker\Service\ConfigLocator;
use Reeds\GunBroker\Service\MagentoOrderCreator;
use Reeds\GunBroker\Service\OrderImporter;
use Reeds\GunBroker\Service\OrdersSoldApiRequest;
use Wagento\GunBrokerApi\ApiObjects\Output\OrderSold;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersSold;

use function array_fill;
use function count;
use function file_get_contents;
use function is_array;
use function json_decode;

use const JSON_THROW_ON_ERROR;

final class OrderImporterTest extends TestCase
{
    private ObjectManagerInterface $objectManager;

    public function testImportOrdersReturnsNullIfNoOrdersToImportAndNoWebsitesAreConfiguredForGunBrokerApi(): void
    {
        $orderImporterService = $this->objectManager->create(OrderImporter::class);
        $importedOrders = $orderImporterService->importOrders();

        self::assertNull($importedOrders);
    }

    /**
     * @magentoConfigFixture current_store gunbroker/integration/enabled 0
     */
    public function testImportOrdersReturnsNullIfNoOrdersToImport(): void
    {
        /** @var OrderImporter $orderImporterService */
        $orderImporterService = $this->objectManager->create(
            OrderImporter::class,
            [
                'configLocator' => $this->instantiateConfigLocatorMock()
            ]
        );
        $importedOrders = $orderImporterService->importOrders();

        self::assertNull($importedOrders);
    }

    /**
     * @dataProvider importsOrdersDataProvider
     * @param int|array<?int>|null $magentoOrderId
     * @param array{complete: array<int, ?int>, failed: array<int, ?int>} $expected
     * @throws JsonException
     */
    public function testImportOrderImportsOrders($magentoOrderId, array $expected): void
    {
        $configLocatorMock =  $this->instantiateConfigLocatorMock();
        $configMock = $this->createMock(ConfigInterface::class);
        $ordersSoldApiRequestServiceMock = $this->createPartialMock(OrdersSoldApiRequest::class, ['getResults']);
        $magentoOrderCreatorServiceMock = $this->createMock(MagentoOrderCreator::class);
        $orderImporterService = $this->objectManager->create(
            OrderImporter::class,
            [
                'configLocator' => $configLocatorMock,
                'config' => $configMock,
                'ordersSoldApiRequest' => $ordersSoldApiRequestServiceMock,
                'magentoOrderCreator' => $magentoOrderCreatorServiceMock
            ]
        );
        $gunBrokerOrder = $this->getTestGunBrokerOrder();

        $configMock->method('isEnabled')
            ->willReturn(true);

        if (!is_array($magentoOrderId)) {
            $ordersSoldApiRequestServiceMock->expects(self::once())
                ->method('getResults')
                ->willReturnCallback(
                    function () use ($gunBrokerOrder): Generator {
                        yield from [[$gunBrokerOrder]];
                    }
                );

            $magentoOrderCreatorServiceMock->expects(self::once())
                ->method('createOrder')
                ->with($gunBrokerOrder)
                ->willReturn($magentoOrderId);
        } else {
            $ordersSoldApiRequestServiceMock->expects(self::once())
                ->method('getResults')
                ->willReturnCallback(
                    function () use ($magentoOrderId, $gunBrokerOrder): Generator {
                        yield from [array_fill(0, count($magentoOrderId), $gunBrokerOrder)];
                    }
                );

            $magentoOrderCreatorServiceMock->expects(self::exactly(count($magentoOrderId)))
                ->method('createOrder')
                ->with($gunBrokerOrder)
                ->willReturnOnConsecutiveCalls(...$magentoOrderId);
        }

        $importedOrders = $orderImporterService->importOrders();

        self::assertEquals($expected, $importedOrders);
    }

    public function testImportOrdersFiltersOutImportedOrders(): void
    {
        $configLocatorMock = $this->instantiateConfigLocatorMock();
        $configMock = $this->createMock(ConfigInterface::class);
        $ordersSoldApiRequestServiceMock = $this->createPartialMock(OrdersSoldApiRequest::class, ['getResults']);
        $orderImportRepositoryMock = $this->createMock(OrderImportRepositoryInterface::class);
        $orderImportSearchResultsMock = $this->createMock(SearchResultsInterface::class);
        $orderImportMock = $this->createMock(OrderImportInterface::class);
        $orderImporterService = $this->objectManager->create(
            OrderImporter::class,
            [
                'configLocator' => $configLocatorMock,
                'config' => $configMock,
                'orderImportRepository' => $orderImportRepositoryMock,
                'ordersSoldApiRequest' => $ordersSoldApiRequestServiceMock
            ]
        );
        $gunBrokerOrder = $this->getTestGunBrokerOrder();

        $configMock->method('isEnabled')
            ->willReturn(true);

        $ordersSoldApiRequestServiceMock->expects(self::once())
            ->method('getResults')
            ->willReturnCallback(function () use ($gunBrokerOrder): Generator { yield [$gunBrokerOrder]; });

        $orderImportRepositoryMock->expects(self::once())
            ->method('getList')
            ->willReturn($orderImportSearchResultsMock);

        $orderImportSearchResultsMock->expects(self::once())
            ->method('getItems')
            ->willReturn([$orderImportMock]);

        $orderImportMock->expects(self::once())
            ->method('getGunBrokerOrderId')
            ->willReturn(1234567);

        $importedOrders = $orderImporterService->importOrders();

        self::assertNull($importedOrders);
    }

    /**
     * @return array<string, array<string, array<int|string, array<int, int|null>|int|null>|int|null>>
     */
    public function importsOrdersDataProvider(): array
    {
        return [
            'creates_new_order_and_returns_completed_order_id' => [
                'magentoOrderId' => 1,
                'expected' => [
                    'complete' => [
                        1234567 => 1
                    ],
                    'failed' => [
                    ]
                ]
            ],
            'does_not_create_new_order_and_returns_failed_order_id' => [
                'magentoOrderId' => null,
                'expected' => [
                    'complete' => [
                    ],
                    'failed' => [
                        1234567 => null
                    ]
                ]
            ],
            'creates_new_order_and_returns_complete_and_failed_order_ids' => [
                'magentoOrderId' => [1, null],
                'expected' => [
                    'complete' => [
                        1234567 => 1
                    ],
                    'failed' => [
                        1234567 => null
                    ]
                ]
            ],
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->objectManager = Bootstrap::getObjectManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->objectManager);
    }

    /**
     * @return ConfigLocator&MockObject
     */
    private function instantiateConfigLocatorMock(): MockObject
    {
        $configLocatorMock = $this->createMock(ConfigLocator::class);
        $websiteMock = $this->createMock(WebsiteInterface::class);

        $configLocatorMock->method('getConfiguredWebsites')
            ->willReturn([$websiteMock]);

        $websiteMock->method('getId')
            ->willReturn(1);

        return $configLocatorMock;
    }

    /**
     * @throws JsonException
     */
    private function getTestGunBrokerOrder(): OrderSold
    {
        $ordersSoldData = json_decode(
            file_get_contents(__DIR__ . '/../_data/orders_sold.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        return (new OrdersSold($ordersSoldData))->results[0];
    }
}
