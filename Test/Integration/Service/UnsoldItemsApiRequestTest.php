<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\Service\UnsoldItemsApiRequest;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsUnsold;
use Wagento\GunBrokerApi\Connector;

use function array_map;
use function array_merge;
use function array_slice;
use function file_get_contents;
use function json_decode;

final class UnsoldItemsApiRequestTest extends TestCase
{
    public function testRetrievesUnsoldItems(): void
    {
        $itemsUnsoldData = $this->getTestData();
        $expected = array_merge(
            [],
            ...array_map(static fn(ItemsUnsold $itemsUnsold): array => $itemsUnsold->results, $itemsUnsoldData)
        );
        $connectorFactoryMock = $this->createMock(GunBrokerConnectorFactory::class);
        $connectorMock = $this->getMockBuilder(Connector::class)
            ->disableOriginalConstructor()
            ->addMethods(['getItemsUnsoldByUser'])
            ->getMock();
        $objectManager = Bootstrap::getObjectManager();
        /** @var UnsoldItemsApiRequest $service */
        $service = $objectManager->create(
            UnsoldItemsApiRequest::class,
            [
                'connectorFactory' => $connectorFactoryMock,
                'apiResultLimit' => 10
            ]
        );

        $connectorFactoryMock->method('create')->willReturn($connectorMock);

        $connectorMock->method('getItemsUnsoldByUser')->willReturnOnConsecutiveCalls(...$itemsUnsoldData);

        $actual = [];

        foreach ($service->setWebsiteId(0)->getResults() as $result) {
            $actual[] = $result;
        }

        self::assertSame($expected, array_merge([], ...$actual));
    }

    /**
     * @return ItemsUnsold[]
     * @throws \JsonException
     */
    private function getTestData(): array
    {
        $itemsUnsoldData = json_decode(
            file_get_contents(__DIR__ . '/../_data/items_unsold.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        $itemsUnsoldData['pageSize'] = 10;
        $page1 = $itemsUnsoldData;
        $page2 = $itemsUnsoldData;
        $page3 = $itemsUnsoldData;
        $page1['results'] = array_slice($page1['results'], 0, 10);
        $page2['results'] = array_slice($page2['results'], 10, 10);
        $page3['results'] = array_slice($page3['results'], 20, 3);

        return [new ItemsUnsold($page1), new ItemsUnsold($page2), new ItemsUnsold($page3)];
    }
}
