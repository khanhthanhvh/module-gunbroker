<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\ItemUrlUpdater;

final class ItemUrlUpdaterTest extends TestCase
{
    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testUpdateByProductDoesNotUpdateItemUrlIfItExistsForItemId(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var ItemUrlUpdater $itemUrlUpdater */
        $itemUrlUpdater = $objectManager->create(ItemUrlUpdater::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        /** @var Product $product */
        $product = $productRepository->get('764503037108');

        $isUpdated = $itemUrlUpdater->updateByProduct($product, 123);

        self::assertFalse($isUpdated);
    }

    /**
     * @dataProvider updatesItemUrlDataProvider
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testUpdateByProductUpdatesItemUrl(?int $itemId, ?string $expectedUrl, bool $isNew = false): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $configMock = $this->createMock(ConfigInterface::class);
        /** @var ItemUrlUpdater $itemUrlUpdater */
        $itemUrlUpdater = $objectManager->create(
            ItemUrlUpdater::class,
            [
                'config' => $configMock
            ]
        );
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        /** @var Product $product */
        $product = $productRepository->get('764503037108', false, 0);

        $configMock->method('getApiEnvironment')
            ->willReturn('sandbox');

        if ($isNew) {
            $product->unsetData('gunbroker_item_url');
        }

        $isUpdated = $itemUrlUpdater->updateByProduct($product, $itemId);

        /** @var Product $product */
        $product = $productRepository->get('764503037108', false, 0, true);
        $actualUrl = $product->getCustomAttribute('gunbroker_item_url');

        if ($actualUrl !== null) {
            $actualUrl = $actualUrl->getValue();
        }

        self::assertTrue($isUpdated);
        self::assertSame($expectedUrl, $actualUrl);
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public function updatesItemUrlDataProvider(): array
    {
        return [
            'sets_new_item_url' => [
                'itemId' => 117924,
                'expectedUrl' => 'https://www.sandbox.gunbroker.com/item/117924',
                'isNew' => true
            ],
            'updates_existing_item_url' => [
                'itemId' => 117935,
                'expectedUrl' => 'https://www.sandbox.gunbroker.com/item/117935'
            ],
            'deletes_item_url' => [
                'itemId' => null,
                'expectedUrl' => null
            ]
        ];
    }
}
