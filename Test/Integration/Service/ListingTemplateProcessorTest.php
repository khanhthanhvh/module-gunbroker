<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Service\ListingTemplateProcessor;

use function trim;

final class ListingTemplateProcessorTest extends TestCase
{
    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/static_block_content_listing_template.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testProcessesTemplate(): void
    {
        /** @var ObjectManager $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var ListingTemplateProcessor $listingTemplateProcessor */
        $listingTemplateProcessor = $objectManager->create(ListingTemplateProcessor::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108');
        $expected = <<<HTML
        <div>
            Glock G17 Gen5 9mm 4.49In<img src="https://www.reedssports.com/media/logo/stores/39/northernfirearmsheader-2.png"
                align="center" width="100%" alt="Northern Firearms">
            <br>
            <br>
            <p align="center">
                <font face="Arial" color="#E31837" style="font-size: 24pt; font-weight:700">Glock G17 Gen5 9mm 4.49In</font>
            </p>
            <br>
            <br>
            <p align="center">
                <font face="Arial" color="#E31837" style="font-size: 16pt; font-weight:700">
                    <table class="tableizer-table">
                        <tr>
                            <td>Brand:</td>
                            <td>&nbsp;</td>
                            <td>Glock</td>
                        </tr>
                        <tr>
                            <td>Model:</td>
                            <td>&nbsp;</td>
                            <td>Glock G17 Gen5 9mm 4.49In</td>
                        </tr>
                        <tr>
                            <td>Model #:</td>
                            <td>&nbsp;</td>
                            <td>Glock 45</td>
                        </tr>
                        <tr>
                            <td>UPC:</td>
                            <td>&nbsp;</td>
                            <td>764503037108</td>
                        </tr>
                        <tr>
                            <td>Caliber:</td>
                            <td>&nbsp;</td>
                            <td>9mm Luger (9 x 19mm)</td>
                        </tr>
                        <tr>
                            <td>Barrel:</td>
                            <td>&nbsp;</td>
                            <td>4in</td>
                        </tr>
                        <tr>
                            <td>Stock Finish:</td>
                            <td>&nbsp;</td>
                            <td>Black</td>
                        </tr>
                        <tr>
                            <td>Frame Material:</td>
                            <td>&nbsp;</td>
                            <td>Steel</td>
                        </tr>
                        <tr>
                            <td>Frame Finish:</td>
                            <td>&nbsp;</td>
                            <td>Black</td>
                        </tr>
                        <tr>
                            <td>Slide Material:</td>
                            <td>&nbsp;</td>
                            <td>Steel</td>
                        </tr>
                        <tr>
                            <td>Slide Finish:</td>
                            <td>&nbsp;</td>
                            <td>Matte/Rough Black</td>
                        </tr>
                        <tr>
                            <td>Capacity:</td>
                            <td>&nbsp;</td>
                            <td>17 + 1 rounds</td>
                        </tr>
                        <tr>
                            <td>Front Sights:</td>
                            <td>&nbsp;</td>
                            <td>3.7mm, polymer</td>
                        </tr>
                        <tr>
                            <td>Rear Sights:</td>
                            <td>&nbsp;</td>
                            <td>4.3mm, polymer</td>
                        </tr>
                    </table>
                </font>
            </p>
            <p align="left" style="padding-left:100px;padding-right:100px;">
                <font face="Arial" color="#E31837" style="font-size: 16pt; font-weight:700">Factory new in box with all factory
                supplied accessories included. No extra fees for paying with a Credit Card. Please read the Additional Terms of
                Sale for more information. Some listings offer the "TAKE A SHOT" feature, go ahead, Take A Shot, it just might
                save you some $$$.
                </font>
            </p>
            <br>
            <br>
            <br>
            <p align="center">
                <font face="Arial" color="#E31837" style="font-size: 12pt; font-weight:700">
                    Specifications subject to change, please refer to manufacturers website for specifications.
                    <br>Pictures are for general reference only.
                </font>
            </p>
            <br>
            <br>
            <p align="center">
                <span style="font-size: 6pt">By purchasing this firearm through our Gunbroker store, we will add you to our
                contact list. We will contact you on your order status and any follow up customer offerings that pertain to your
                interests. You can unsubscribe at any time. Please visit our
                <a href="https://www.reedssports.com/privacy-policy-cookie-restriction-mode">privacy policy</a> for more
                information.</span>
            </p>
        </div>
        HTML;
        $actual = $listingTemplateProcessor->process($product);

        self::assertEquals($expected, trim(preg_replace('/^ {4,16}\n/m', '', $actual)));
    }
}
