<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use JsonException;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\Service\OrdersSoldApiRequest;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersSold;
use Wagento\GunBrokerApi\Connector;

use function array_map;
use function file_get_contents;
use function iterator_to_array;
use function json_decode;

final class OrdersSoldApiRequestTest extends TestCase
{
    public function testRetrievesSoldOrders(): void
    {
        $ordersSoldData = $this->getTestData();
        $expected = array_map(static fn(OrdersSold $ordersSold): array => $ordersSold->results, $ordersSoldData);
        $connectorFactoryMock = $this->createMock(GunBrokerConnectorFactory::class);
        $connectorMock = $this->getMockBuilder(Connector::class)
            ->disableOriginalConstructor()
            ->addMethods(['getSoldOrders'])
            ->getMock();
        $objectManager = Bootstrap::getObjectManager();
        /** @var OrdersSoldApiRequest $ordersSoldApiRequest */
        $ordersSoldApiRequest = $objectManager->create(
            OrdersSoldApiRequest::class,
            [
                'connectorFactory' => $connectorFactoryMock,
                'apiResultLimit' => 10
            ]
        );

        $connectorFactoryMock->method('create')->willReturn($connectorMock);

        $connectorMock->method('getSoldOrders')->willReturnOnConsecutiveCalls(...$ordersSoldData);

        $actual = iterator_to_array($ordersSoldApiRequest->setWebsiteId(0)->getResults(), false);

        self::assertSame($expected, $actual);
    }

    /**
     * @return OrdersSold[]
     * @throws JsonException
     */
    private function getTestData(): array
    {
        $ordersSoldData = json_decode(
            file_get_contents(__DIR__ . '/../_data/orders_sold.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        $ordersSoldData['count'] = 3;
        $ordersSoldData['pageSize'] = 1;
        $ordersSold = new OrdersSold($ordersSoldData);

        return [$ordersSold, clone $ordersSold, clone $ordersSold];
    }
}
