<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\MessageQueue\PublisherInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Model\UpdateItemMessage;
use Reeds\GunBroker\Service\UpdateItemMessagePublisher;

final class UpdateItemMessagePublisherTest extends TestCase
{
    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testPublishesItemUpdateMessageWithCorrectProperties(): void
    {
        /** @var ObjectManager $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        /** @var Product $product */
        $product = $productRepository->get('764503037108', true);
        $expectedUpdateItemMessage = $objectManager->create(UpdateItemMessage::class);
        $productMock = $this->createPartialMock(Product::class, ['dataHasChangedFor']);
        $messagePublisherMock = $this->createMock(PublisherInterface::class);
        $updateItemMessagePublisher = $objectManager->create(
            UpdateItemMessagePublisher::class,
            ['publisher' => $messagePublisherMock]
        );

        $expectedUpdateItemMessage->setItemId(123);
        $expectedUpdateItemMessage->setAutoAcceptPrice(479.99);
        $expectedUpdateItemMessage->setAutoRejectPrice(449.99);
        $expectedUpdateItemMessage->setBuyNowPrice(489.99);
        $expectedUpdateItemMessage->setCanOffer(true);
        $expectedUpdateItemMessage->setFixedPrice(499.99);
        $expectedUpdateItemMessage->setGtin('00012345678905');
        $expectedUpdateItemMessage->setMfgPartNumber('PA1750203');
        $expectedUpdateItemMessage->setReservePrice(0);
        $expectedUpdateItemMessage->setSku('764503037108');
        $expectedUpdateItemMessage->setStartingBid(424.99);
        $expectedUpdateItemMessage->setSubtitle('Test');
        $expectedUpdateItemMessage->setTitle('Glock G17 Gen5 9mm 4.49In');
        $expectedUpdateItemMessage->setUpc('764503022616');
        $expectedUpdateItemMessage->setWebsiteId(1);

        $productMock->method('dataHasChangedFor')->willReturn(true);
        $productMock->setData($product->getData());

        $messagePublisherMock->expects(self::once())
            ->method('publish')
            ->with('gunbroker.item.update', $expectedUpdateItemMessage);

        $updateItemMessagePublisher->publishMessage($productMock, [], 1);
    }
}
