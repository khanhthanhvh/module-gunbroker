<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use JsonException;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\ConfigLocator;
use Reeds\GunBroker\Service\UnsoldItemsApiRequest;
use Reeds\GunBroker\Service\UnsoldItemsProcessor;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemUnsold;

use function file_get_contents;
use function json_decode;

use const JSON_THROW_ON_ERROR;

final class UnsoldItemsProcessorTest extends TestCase
{
    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @throws JsonException
     */
    public function testProcessesUnsoldItems(): void
    {
        /** @var ObjectManager $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $configLocatorMock = $this->createMock(ConfigLocator::class);
        $configMock = $this->createMock(ConfigInterface::class);
        $unsoldItemsApiRequestMock = $this->createPartialMock(UnsoldItemsApiRequest::class, ['getResults']);
        $unsoldItemsProcessor = $objectManager->create(
            UnsoldItemsProcessor::class,
            [
                'configLocator' => $configLocatorMock,
                'config' => $configMock,
                'unsoldItemsApiRequest' => $unsoldItemsApiRequestMock
            ]
        );
        $website1Mock = $this->createMock(WebsiteInterface::class);
        $website2Mock = $this->createMock(WebsiteInterface::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108', true);
        $unsoldItems = $this->getUnsoldItems();

        $configLocatorMock->method('getConfiguredWebsites')
            ->willReturn([$website1Mock, $website2Mock]);

        $website1Mock->method('getId')
            ->willReturn(7);

        $website2Mock->method('getId')
            ->willReturn(42);

        $configMock->method('isEnabled')
            ->willReturnOnConsecutiveCalls(true, false);

        $unsoldItemsApiRequestMock->method('getResults')
            ->willReturnCallback(
                function () use ($unsoldItems) {
                    yield from $unsoldItems;
                }
            );

        $product->setCustomAttribute('gunbroker_item_id', $unsoldItems[0]->itemID);

        $productRepository->save($product);

        $expected = [
            $product->getId() => [
                'itemId' => $unsoldItems[0]->itemID,
                'storeId' => 1
            ]
        ];
        $actual = $unsoldItemsProcessor->process();

        self::assertEquals($expected, $actual);
    }

    /**
     * @return ItemUnsold[]
     * @throws JsonException
     */
    private function getUnsoldItems(): array
    {
        $itemsUnsoldData = json_decode(
            file_get_contents(__DIR__ . '/../_data/items_unsold.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        return [new ItemUnsold($itemsUnsoldData['results'][0])];
    }
}
