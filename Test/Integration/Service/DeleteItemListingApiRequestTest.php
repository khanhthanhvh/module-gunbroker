<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\Service\DeleteItemListingApiRequest;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Connector;

final class DeleteItemListingApiRequestTest extends TestCase
{
    public function testDeletesItemListingOnGunBrokerAndLogsResult(): void
    {
        $connectorFactoryMock = $this->createMock(GunBrokerConnectorFactory::class);
        $connectorMock = $this->getMockBuilder(Connector::class)
            ->disableOriginalConstructor()
            ->addMethods(['deleteListedItem'])
            ->getMock();
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $testLogger = $objectManager->create(TestLogger::class);
        /** @var DeleteItemListingApiRequest $deleteListingItemApiRequest */
        $deleteListingItemApiRequest = $objectManager->create(
            DeleteItemListingApiRequest::class,
            [
                'connectorFactory' => $connectorFactoryMock,
                'logger' => $testLogger
            ]
        );
        /** @var MessageResponse $messageResponse */
        $messageResponse = $objectManager->create(
            MessageResponse::class,
            [
                'parameters' => [
                    'developerMessage' => 'Item 11520066 is ended.',
                    'userMessage' => 'Item 11520066 is ended.',
                    'gbStatusCode' => 0,
                    'links' => []
                ]
            ]
        );

        $connectorFactoryMock->method('create')
            ->willReturn($connectorMock);

        $connectorMock->method('deleteListedItem')
            ->willReturn($messageResponse);

        self::assertTrue($deleteListingItemApiRequest->execute(11520066));
        self::assertTrue($testLogger->hasInfoThatContains($messageResponse->developerMessage));
    }
}
