<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service\ListItemEmailSender;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterface;
use Reeds\GunBroker\Api\ItemListedEmailRepositoryInterface;
use Reeds\GunBroker\Service\ListItemEmailSender\Asynchronous;
use Reeds\GunBroker\Service\ListItemEmailSender\Synchronous;
use Wagento\GunBrokerApi\ApiObjects\Input\Item;

use function current;
use function file_get_contents;
use function json_decode;

use const JSON_THROW_ON_ERROR;

final class AsynchronousTest extends TestCase
{
    private ObjectManagerInterface $objectManager;

    public function testQueueEmailQueuesEmailForSending(): void
    {
        /** @var Asynchronous $emailSender */
        $emailSender = $this->objectManager->create(Asynchronous::class);
        $itemData = json_decode(file_get_contents(__DIR__ . '/../../_data/item.json'), true, 512, JSON_THROW_ON_ERROR);
        $item = new Item($itemData);
        $searchCriteria = $this->objectManager->create(SearchCriteriaBuilder::class)
            ->addFilter('item_id', 12345678)
            ->create();
        /** @var ItemListedEmailRepositoryInterface $itemListedRepository */
        $itemListedRepository = $this->objectManager->create(ItemListedEmailRepositoryInterface::class);

        $isQueued = $emailSender->queueEmail(
            1,
            12345678,
            $item,
            'https://www.sandbox.gunbroker.com/item/12345678',
            'https://reedssports.test/glock-gen5-g17-9mm'
        );

        $itemListedEmails = $itemListedRepository->getList($searchCriteria)->getItems();
        /** @var ItemListedEmailInterface $itemListedEmail */
        $itemListedEmail = current($itemListedEmails);

        self::assertTrue($isQueued);
        self::assertNotFalse($itemListedEmail);
        self::assertEquals($itemData, $itemListedEmail->getItemData());
        self::assertSame('https://www.sandbox.gunbroker.com/item/12345678', $itemListedEmail->getItemListingUrl());
        self::assertSame('https://reedssports.test/glock-gen5-g17-9mm', $itemListedEmail->getProductUrl());
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/item_listed_email.php
     */
    public function testSendEmailsSendsQueuedEmails(): void
    {
        $itemData = json_decode(file_get_contents(__DIR__ . '/../../_data/item.json'), true, 512, JSON_THROW_ON_ERROR);
        $item = new Item($itemData);
        $dateTime = gmdate('Y-m-d H:i:s');
        $synchronousEmailSenderMock = $this->createMock(Synchronous::class);
        $dateTimeMock = $this->createMock(DateTime::class);
        /** @var Asynchronous $asynchronousEmailSender */
        $asynchronousEmailSender = $this->objectManager->create(
            Asynchronous::class,
            [
                'synchronousEmailSender' => $synchronousEmailSenderMock,
                'dateTime' => $dateTimeMock
            ]
        );
        $searchCriteria = $this->objectManager->create(SearchCriteriaBuilder::class)
            ->addFilter('item_id', 12345678)
            ->create();
        /** @var ItemListedEmailRepositoryInterface $itemListedRepository */
        $itemListedRepository = $this->objectManager->create(ItemListedEmailRepositoryInterface::class);

        $synchronousEmailSenderMock->expects(self::once())
            ->method('sendEmail')
            ->with(
                1,
                $item,
                'https://www.sandbox.gunbroker.com/item/12345678',
                'https://reedssports.test/glock-gen5-g17-9mm'
            )
            ->willReturn(true);

        $dateTimeMock->method('gmtDate')
            ->willReturn($dateTime);

        $isSent = $asynchronousEmailSender->sendEmails();

        $itemListedEmails = $itemListedRepository->getList($searchCriteria)->getItems();
        /** @var ItemListedEmailInterface $itemListedEmail */
        $itemListedEmail = current($itemListedEmails);

        self::assertTrue($isSent);
        self::assertSame($dateTime, $itemListedEmail->getSentAt());
    }

    protected function setUp(): void
    {
        $this->objectManager = Bootstrap::getObjectManager();
    }

    protected function tearDown(): void
    {
        unset($this->objectManager);
    }
}
