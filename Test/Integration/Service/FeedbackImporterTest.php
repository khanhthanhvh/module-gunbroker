<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use Generator;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;
use Reeds\GunBroker\Service\FeedbackImporter;
use Reeds\GunBroker\Service\FeedbackSearchApiRequest;
use Wagento\GunBrokerApi\ApiObjects\Output\FeedbackSearch;
use Wagento\GunBrokerApi\ApiObjects\Output\FeedbackSearchResult;

use function __;
use function array_filter;
use function current;
use function file_get_contents;
use function json_decode;

use const JSON_THROW_ON_ERROR;

final class FeedbackImporterTest extends TestCase
{
    use ArraySubsetAsserts;

    private ObjectManagerInterface $objectManager;

    public function testImportByWebsiteDoesNotImportFeedbackIfExtensionIsDisabled(): void
    {
        $configMock = $this->createMock(ConfigInterface::class);
        /** @var FeedbackImporter $feedbackImporter */
        $feedbackImporter = $this->objectManager->create(
            FeedbackImporter::class,
            [
                'config' => $configMock
            ]
        );

        $configMock->method('isEnabled')
            ->willReturn(false);

        $importedFeedback = $feedbackImporter->importByWebsite(1);

        self::assertEmpty(array_filter($importedFeedback));
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     */
    public function testImportByWebsiteImportsFeedbackForOrderItemsInWebsite(): void
    {
        $configMock = $this->createMock(ConfigInterface::class);
        $feedbackSearchApiRequestMock = $this->createMock(FeedbackSearchApiRequest::class);
        /** @var FeedbackImporter $feedbackImporter */
        $feedbackImporter = $this->objectManager->create(
            FeedbackImporter::class,
            [
                'config' => $configMock,
                'feedbackSearchApiRequest' => $feedbackSearchApiRequestMock
            ]
        );

        $configMock->method('getApiCredentials')
            ->willReturn(['username' => 'acme firearms']);
        $configMock->method('isEnabled')
            ->willReturn(true);

        $feedbackSearchApiRequestMock->method('setWebsiteId')
            ->willReturnSelf();
        $feedbackSearchApiRequestMock->method('getResults')
            ->willReturnCallback(
                function (): Generator {
                    yield from $this->getTestFeedbackSearchResults();
                }
            );

        $importedFeedback = $feedbackImporter->importByWebsite(1);
        $expectedFeedbackData = [
            'type' => 'buyer',
            'gunbroker_feedback_id' => 456,
            'gunbroker_item_id' => 11496586,
            'magento_order_item_id' => $this->getOrderItemId(),
            'rating_letter' => 'B',
            'rating_score' => 3,
            'comment' => 'Good purchase',
            'comment_date' => '2021-06-01T09:12:17Z',
            'commentator' => 'jdoe'
        ];
        $actualFeedbackData = $importedFeedback['succeeded'][0]->getData();

        self::assertEmpty($importedFeedback['failed']);
        self::assertArraySubset($expectedFeedbackData, $actualFeedbackData, true);
        self::assertArrayHasKey('id', $actualFeedbackData);
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     */
    public function testImportByWebsiteReturnsFailedImports(): void
    {
        $configMock = $this->createMock(ConfigInterface::class);
        $feedbackSearchApiRequestMock = $this->createMock(FeedbackSearchApiRequest::class);
        $orderItemFeedbackRepositoryMock = $this->createMock(OrderItemFeedbackRepositoryInterface::class);
        $searchResultsMock = $this->createMock(SearchResultsInterface::class);
        /** @var FeedbackImporter $feedbackImporter */
        $feedbackImporter = $this->objectManager->create(
            FeedbackImporter::class,
            [
                'config' => $configMock,
                'feedbackSearchApiRequest' => $feedbackSearchApiRequestMock,
                'orderItemFeedbackRepository' => $orderItemFeedbackRepositoryMock
            ]
        );

        $configMock->method('getApiCredentials')
            ->willReturn(['username' => 'acme firearms']);
        $configMock->method('isEnabled')
            ->willReturn(true);

        $feedbackSearchApiRequestMock->method('setWebsiteId')
            ->willReturnSelf();
        $feedbackSearchApiRequestMock->method('getResults')
            ->willReturnCallback(
                function (): Generator {
                    yield from $this->getTestFeedbackSearchResults();
                }
            );

        $orderItemFeedbackRepositoryMock->method('getList')
            ->willReturn($searchResultsMock);
        $orderItemFeedbackRepositoryMock->method('save')
            ->willThrowException(new CouldNotSaveException(__('Unknown Error')));

        $searchResultsMock->method('getItems')
            ->willReturn([]);

        $importedFeedback = $feedbackImporter->importByWebsite(1);
        $expectedFeedbackData = [
            'type' => 'buyer',
            'gunbroker_feedback_id' => 456,
            'gunbroker_item_id' => 11496586,
            'magento_order_item_id' => $this->getOrderItemId(),
            'rating_letter' => 'B',
            'rating_score' => 3,
            'comment' => 'Good purchase',
            'comment_date' => '2021-06-01T09:12:17Z',
            'commentator' => 'jdoe'
        ];
        $actualFeedbackData = $importedFeedback['failed'][0]->getData();

        self::assertEmpty($importedFeedback['succeeded']);
        self::assertArraySubset($expectedFeedbackData, $actualFeedbackData, true);
        self::assertArrayNotHasKey('id', $actualFeedbackData);
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order_item_feedback.php
     */
    public function testImportByWebsiteDoesNotImportExistingFeedbackInWebsite(): void
    {
        $configMock = $this->createMock(ConfigInterface::class);
        $feedbackSearchApiRequestMock = $this->createMock(FeedbackSearchApiRequest::class);
        /** @var FeedbackImporter $feedbackImporter */
        $feedbackImporter = $this->objectManager->create(
            FeedbackImporter::class,
            [
                'config' => $configMock,
                'feedbackSearchApiRequest' => $feedbackSearchApiRequestMock
            ]
        );

        $configMock->method('getApiCredentials')
            ->willReturn(['username' => 'acme firearms']);
        $configMock->method('isEnabled')
            ->willReturn(true);

        $feedbackSearchApiRequestMock->method('setWebsiteId')
            ->willReturnSelf();
        $feedbackSearchApiRequestMock->method('getResults')
            ->willReturnCallback(
                function (): Generator {
                    yield from $this->getTestFeedbackSearchResults();
                }
            );

        $importedFeedback = $feedbackImporter->importByWebsite(1);

        self::assertEmpty(array_filter($importedFeedback));
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     */
    public function testImportInWebsitesImportsFeedbackForGivenWebsites(): void
    {
        $configMock = $this->createMock(ConfigInterface::class);
        $feedbackSearchApiRequestMock = $this->createMock(FeedbackSearchApiRequest::class);
        /** @var FeedbackImporter $feedbackImporter */
        $feedbackImporter = $this->objectManager->create(
            FeedbackImporter::class,
            [
                'config' => $configMock,
                'feedbackSearchApiRequest' => $feedbackSearchApiRequestMock
            ]
        );

        $configMock->method('getApiCredentials')
            ->willReturn(['username' => 'acme firearms']);
        $configMock->method('isEnabled')
            ->willReturn(true);

        $feedbackSearchApiRequestMock->method('setWebsiteId')
            ->willReturnSelf();
        $feedbackSearchApiRequestMock->method('getResults')
            ->willReturnCallback(
                function (): Generator {
                    yield from $this->getTestFeedbackSearchResults();
                }
            );

        $importedFeedback = $feedbackImporter->importInWebsites([1]);

        self::assertArrayHasKey(1, $importedFeedback);
        self::assertNotEmpty($importedFeedback[1]['succeeded']);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->objectManager = Bootstrap::getObjectManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->objectManager);
    }

    /**
     * @return FeedbackSearchResult[][]
     */
    private function getTestFeedbackSearchResults(): array
    {
        $feedbackData = json_decode(
            file_get_contents(__DIR__ . '/../_data/feedback.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        return [(new FeedbackSearch($feedbackData))->results];
    }

    private function getOrderItemId(): int
    {
        /** @var OrderRepositoryInterface $orderRepository */
        $orderRepository = $this->objectManager->create(OrderRepositoryInterface::class);
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $this->objectManager->create(SearchCriteriaBuilder::class);
        $orderSearchCriteria = $searchCriteriaBuilder->addFilter('increment_id', '100000001')
            ->setPageSize(1)
            ->setCurrentPage(1)
            ->create();
        $orderSearchResults = $orderRepository->getList($orderSearchCriteria)->getItems();
        $orderItems = current($orderSearchResults)->getItems();

        return (int)current($orderItems)->getItemId();
    }
}
