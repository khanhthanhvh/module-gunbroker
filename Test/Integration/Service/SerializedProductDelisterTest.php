<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\AttributeInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\DeleteItemListingApiRequest;
use Reeds\GunBroker\Service\SerializedProductDelister;

final class SerializedProductDelisterTest extends TestCase
{
    /**
     * @dataProvider productValidationDataProvider
     */
    public function testDelistDoesNotDeleteListingsForInvalidProducts(
        bool $isEnabled,
        bool $isGunBrokerItem,
        bool $isListedItem,
        bool $isSerializedFirearm
    ): void {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $configMock = $this->createMock(ConfigInterface::class);
        $serializedProductDelister = $objectManager->create(
            SerializedProductDelister::class,
            [
                'config' => $configMock
            ]
        );
        $productMock = $this->createMock(ProductInterface::class);
        $isGunBrokerItemAttribute = $objectManager->create(AttributeInterface::class);
        $gunBrokerItemIdAttribute = $objectManager->create(AttributeInterface::class);
        $serialNumberAttribute = $objectManager->create(AttributeInterface::class);

        $configMock->method('isEnabled')
            ->willReturn($isEnabled);

        $productMock->method('getCustomAttribute')
            ->withConsecutive(['is_gunbroker_item'], ['gunbroker_item_id'], ['serial_number'])
            ->willReturnOnConsecutiveCalls(
                $isGunBrokerItemAttribute,
                $gunBrokerItemIdAttribute,
                $serialNumberAttribute
            );

        $isGunBrokerItemAttribute->setAttributeCode('is_gunbroker_item');
        $isGunBrokerItemAttribute->setValue($isGunBrokerItem);

        $gunBrokerItemIdAttribute->setAttributeCode('gunbroker_item_id');
        $gunBrokerItemIdAttribute->setValue($isListedItem ? 12345 : null);

        $serialNumberAttribute->setAttributeCode('serial_number');
        $serialNumberAttribute->setValue($isSerializedFirearm ? 'ABC123' : null);

        self::assertNull($serializedProductDelister->delist($productMock, 0, 0));
    }

    /**
     * @dataProvider delistResultDataProvider
     * @magentoAppArea frontend
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testDelistDeletesItemListingAndRemovesItemIdentifierOrReturnsNullOnError(
        bool $isDelisted,
        bool $returnsNull
    ): void {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        $configMock = $this->createMock(ConfigInterface::class);
        $deleteItemListingApiRequestMock = $this->createMock(DeleteItemListingApiRequest::class);
        $serializedProductDelister = $objectManager->create(
            SerializedProductDelister::class,
            [
                'config' => $configMock,
                'deleteItemListingApiRequest' => $deleteItemListingApiRequestMock
            ]
        );
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108');
        $gunBrokerItemId = (int)$product->getCustomAttribute('gunbroker_item_id')->getValue();

        $configMock->method('isEnabled')
            ->willReturn(true);

        $deleteItemListingApiRequestMock->method('execute')
            ->willReturn($isDelisted);

        $delistedItemId = $serializedProductDelister->delist($product, 1, 1);

        if (!$returnsNull) {
            self::assertSame($gunBrokerItemId, $delistedItemId);
            self::assertNull($product->getCustomAttribute('gunbroker_item_id')->getValue());
        } else {
            self::assertNull($delistedItemId);
            self::assertNotNull($product->getCustomAttribute('gunbroker_item_id')->getValue());
        }
    }

    /**
     * @return bool[][]
     */
    public function productValidationDataProvider(): array
    {
        return [
            'extension_is_disabled' => [
                'isEnabled' => false,
                'isGunBrokerItem' => false,
                'isListedItem' => false,
                'isSerializedFirearm' => false
            ],
            'is_not_gunbroker_item' => [
                'isEnabled' => true,
                'isGunBrokerItem' => false,
                'isListedItem' => false,
                'isSerializedFirearm' => false
            ],
            'is_not_listed' => [
                'isEnabled' => true,
                'isGunBrokerItem' => true,
                'isListedItem' => false,
                'isSerializedFirearm' => false
            ],
            'is_not_serialized' => [
                'isEnabled' => true,
                'isGunBrokerItem' => true,
                'isListedItem' => true,
                'isSerializedFirearm' => false
            ],
        ];
    }

    /**
     * @return bool[][]
     */
    public function delistResultDataProvider(): array
    {
        return [
            'delists_product_and_removes_item_identifier' => [
                'isDelisted' => true,
                'returnsNull' => false
            ],
            'returns_null_on_error' => [
                'isDelisted' => false,
                'returnsNull' => true
            ]
        ];
    }
}
