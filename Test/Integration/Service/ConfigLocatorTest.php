<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Service\ConfigLocator;

use function array_map;

final class ConfigLocatorTest extends TestCase
{
    /**
     * @magentoCache config disabled
     * @magentoCache full_page disabled
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_website.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_api_config.php
     */
    public function testGetConfiguredWebsitesReturnsAllConfiguredSites(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var ConfigLocator $configLocator */
        $configLocator = $objectManager->create(ConfigLocator::class);
        $websiteId = (int)$objectManager->create(WebsiteRepositoryInterface::class)
            ->get('gunbroker')
            ->getId();

        $configuredWebsiteIds = array_map(
            static fn(WebsiteInterface $website): int => (int)$website->getId(),
            $configLocator->getConfiguredWebsites()
        );

        self::assertContains($websiteId, $configuredWebsiteIds);
        self::assertNotContains(0, $configuredWebsiteIds);
    }
}
