<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use JsonException;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\Service\UpdateItemApiRequest;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\Connector;

use function file_get_contents;
use function json_decode;

final class UpdateItemApiRequestTest extends TestCase
{
    public function testUpdatesItemAndLogsResult(): void
    {
        $connectorFactoryMock = $this->createMock(GunBrokerConnectorFactory::class);
        $connectorMock = $this->getMockBuilder(Connector::class)
            ->disableOriginalConstructor()
            ->addMethods(['updateListedItem'])
            ->getMock();
        $objectManager = Bootstrap::getObjectManager();
        /** @var TestLogger $testLogger */
        $testLogger = $objectManager->create(TestLogger::class);
        /** @var UpdateItemApiRequest $updateItemApiRequest */
        $updateItemApiRequest = $objectManager->create(
            UpdateItemApiRequest::class,
            [
                'connectorFactory' => $connectorFactoryMock,
                'logger' => $testLogger
            ]
        );
        $expectedResponse = new MessageResponse(
            [
                'userMessage' => 'Item with ID 123456 was updated',
                'developerMessage' => 'Item with ID 123456 was updated',
                'gbStatusCode' => 0,
                'links' => []
            ]
        );

        $connectorFactoryMock->method('create')
            ->willReturn($connectorMock);

        $connectorMock->method('updateListedItem')
            ->willReturn($expectedResponse);

        $actualResponse = $updateItemApiRequest->setWebsiteId(1)
            ->setItemId(123456)
            ->setUpdatedItemData($this->getTestItemData())
            ->getResult();

        self::assertSame($expectedResponse, $actualResponse);
        self::assertTrue($testLogger->hasInfoThatContains('Item with ID 123456 was updated'));
        self::assertSame(20, $updateItemApiRequest->getUpdatedItemQuantity());
    }

    /**
     * @return array<string, Float|int|bool>
     * @throws JsonException
     */
    private function getTestItemData(): array
    {
        return json_decode(
            file_get_contents(__DIR__ . '/../_data/updated_item.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }
}
