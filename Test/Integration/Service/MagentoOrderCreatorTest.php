<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Reeds\GunBroker\Service\MagentoOrderCreator;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersSold;

use function __;
use function file_get_contents;
use function json_decode;

use const JSON_THROW_ON_ERROR;

final class MagentoOrderCreatorTest extends TestCase
{
    private ObjectManagerInterface $objectManager;
    private TestLogger $testLogger;
    private MagentoOrderCreator $magentoOrderCreatorService;
    private OrdersSold $gunBrokerOrder;

    public function testCreateOrderReturnsNullAndLogsErrorIfItemCountDoesNotMatchProductCount(): void
    {
        $magentoOrderId = $this->magentoOrderCreatorService->createOrder($this->gunBrokerOrder->results[0], 1);

        self::assertNull($magentoOrderId);
        self::assertTrue(
            $this->testLogger->hasCriticalThatContains(
                (string)__(
                    'Could not import order %1 from GunBroker.com. One or more items do not exist in Magento.',
                    '1234567'
                )
            )
        );
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testCreateOrderCreatesGuestOrderWithInvoice(): void
    {
        $productRepository = $this->objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108', true);
        $orderRepository = $this->objectManager->create(OrderRepositoryInterface::class);

        $product->setCustomAttribute('gunbroker_item_id', 11472400);

        $productRepository->save($product);

        $magentoOrderId = $this->magentoOrderCreatorService->createOrder($this->gunBrokerOrder->results[0], 1);
        $order = $orderRepository->get($magentoOrderId);

        self::assertNotNull($magentoOrderId);
        self::assertTrue((bool)$order->getCustomerIsGuest());
        self::assertEquals($this->gunBrokerOrder->results[0]->totalPrice, $order->getTotalInvoiced());
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_customer.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testCreateOrderCreatesCustomerOrderWithInvoice(): void
    {
        $productRepository = $this->objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108', true);
        $orderRepository = $this->objectManager->create(OrderRepositoryInterface::class);

        $product->setCustomAttribute('gunbroker_item_id', 11472400);

        $productRepository->save($product);

        $magentoOrderId = $this->magentoOrderCreatorService->createOrder($this->gunBrokerOrder->results[0], 1);
        $order = $orderRepository->get($magentoOrderId);

        self::assertNotNull($magentoOrderId);
        self::assertFalse((bool)$order->getCustomerIsGuest());
        self::assertEquals($this->gunBrokerOrder->results[0]->totalPrice, $order->getTotalInvoiced());
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->objectManager = Bootstrap::getObjectManager();
        $this->testLogger = new TestLogger();
        $this->magentoOrderCreatorService = $this->objectManager->create(
            MagentoOrderCreator::class,
            [
                'logger' => $this->testLogger
            ]
        );
        $this->gunBrokerOrder = $this->getTestGunBrokerOrder();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        unset($this->objectManager, $this->testLogger, $this->magentoOrderCreatorService, $this->gunBrokerOrder);
    }

    private function getTestGunBrokerOrder(): OrdersSold
    {
        $ordersSoldData = json_decode(
            file_get_contents(__DIR__ . '/../_data/orders_sold.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        return new OrdersSold($ordersSoldData);
    }
}
