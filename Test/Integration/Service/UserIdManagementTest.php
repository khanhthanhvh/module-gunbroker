<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Magento\Config\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Service\UserIdManagement;

final class UserIdManagementTest extends TestCase
{
    /**
     * @magentoCache config disabled
     * @magentoCache full_page disabled
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_website.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_api_config.php
     */
    public function testUpdateUserIdRemovesIdIfCredentialsAreNotSet()
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var UserIdManagement $userIdManagement */
        $userIdManagement = $objectManager->create(UserIdManagement::class);
        /** @var WriterInterface $configWriter */
        $configWriter = $objectManager->get(WriterInterface::class);
        /** @var int|string|null $websiteId */
        $websiteId = $objectManager->create(WebsiteRepositoryInterface::class)
            ->get('gunbroker')
            ->getId();
        $updateCallback = function () use ($objectManager, $configWriter, $websiteId): Config {
            $configWriter->save(
                'gunbroker/api/sandbox_password',
                'P@55w0rd!',
                ScopeInterface::SCOPE_WEBSITES,
                $websiteId
            );
            $configWriter->delete('gunbroker/api/sandbox_username', ScopeInterface::SCOPE_WEBSITES, $websiteId);

            /** @var Config $config */
            $config = $objectManager->create(Config::class);

            $config->setScope(ScopeInterface::SCOPE_WEBSITES);
            $config->setScopeId($websiteId);
            $config->setSection('gunbroker/api');
            $config->load();

            return $config;
        };

        $userIdManagement->updateUserId('gunbroker', $updateCallback);

        /** @var int|string|null $userId */
        $userId = $objectManager->create(ScopeConfigInterface::class)
            ->getValue('gunbroker/api/user_id', ScopeInterface::SCOPE_WEBSITES, $websiteId);

        self::assertNull($userId);
    }
}
