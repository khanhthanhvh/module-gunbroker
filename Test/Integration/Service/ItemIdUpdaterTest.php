<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use Generator;
use JsonException;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\ItemIdUpdater;
use Reeds\GunBroker\Service\ItemsSellingApiRequest;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemSelling;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSelling;

use function array_slice;
use function file_get_contents;
use function json_decode;

use const JSON_THROW_ON_ERROR;

final class ItemIdUpdaterTest extends TestCase
{
    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @throws JsonException
     * @throws NoSuchEntityException
     */
    public function testUpdateByWebsiteUpdatesProductItemId(): void
    {
        $objectManager = Bootstrap::getObjectManager();
        $itemsSellingApiRequestMock = $this->getMockBuilder(ItemsSellingApiRequest::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getResults'])
            ->getMock();
        $configMock = $this->createMock(ConfigInterface::class);
        /** @var ItemIdUpdater $itemIdUpdater */
        $itemIdUpdater = $objectManager->create(
            ItemIdUpdater::class,
            [
                'config' => $configMock,
                'itemsSellingApiRequest' => $itemsSellingApiRequestMock
            ]
        );
        $configMock->method('isEnabled')->willReturn(true); // Config fixtures do not work at the website scope.

        $itemsSellingApiRequestMock->method('getResults')
            ->willReturnCallback(
                function (): Generator {
                    yield from $this->getTestData();
                }
            );

        $this->updateProductDetails();

        $itemIdUpdater->updateByWebsite(1);

        self::assertSame(
            '11516758',
            $objectManager->create(ProductRepositoryInterface::class)
                ->get('764503037108')
                ->getCustomAttribute('gunbroker_item_id')
                ->getValue()
        );
    }

    /**
     * @return ItemSelling[][]
     * @throws JsonException
     */
    private function getTestData(): array
    {
        $itemsUnsoldData = json_decode(
            file_get_contents(__DIR__ . '/../_data/items_selling.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        $itemsUnsoldData['pageSize'] = 3;
        $page1 = $itemsUnsoldData;
        $page2 = $itemsUnsoldData;
        $page3 = $itemsUnsoldData;
        $page1['results'] = array_slice($page1['results'], 0, 3);
        $page2['results'] = array_slice($page2['results'], 3, 3);
        $page3['results'] = array_slice($page3['results'], 6, 3);

        return [
            (new ItemsSelling($page1))->results,
            (new ItemsSelling($page2))->results,
            (new ItemsSelling($page3))->results
        ];
    }

    private function updateProductDetails(): void
    {
        $productRepository = Bootstrap::getObjectManager()->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('764503037108', true);

        $product->setCustomAttribute('gunbroker_item_id', '01234567');

        $productRepository->save($product);
    }
}
