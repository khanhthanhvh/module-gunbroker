<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Service;

use JsonException;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\Service\ItemsSellingApiRequest;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsSelling;
use Wagento\GunBrokerApi\Connector;

use function array_merge;
use function array_slice;
use function file_get_contents;
use function json_decode;

use const JSON_THROW_ON_ERROR;

final class ItemsSellingApiRequestTest extends TestCase
{
    /**
     * @throws JsonException
     */
    public function testGetResultsReturnsAllResults(): void
    {
        $objectManager = Bootstrap::getObjectManager();
        $connectorFactoryMock = $this->createMock(GunBrokerConnectorFactory::class);
        $connectorMock = $this->getMockBuilder(Connector::class)
            ->disableOriginalConstructor()
            ->addMethods(['getActiveItems'])
            ->getMock();
        /** @var ItemsSellingApiRequest $itemsSellingApiRequest */
        $itemsSellingApiRequest = $objectManager->create(
            ItemsSellingApiRequest::class,
            [
                'connectorFactory' => $connectorFactoryMock,
                'apiResultLimit' => 10
            ]
        );
        $itemsSellingResults = $this->getTestData();
        $results = [];

        $connectorFactoryMock->method('create')->willReturn($connectorMock);

        $connectorMock->method('getActiveItems')->willReturnOnConsecutiveCalls(...$itemsSellingResults);

        foreach ($itemsSellingApiRequest->setWebsiteId(1)->getResults() as $result) {
            $results[] = $result;
        }

        self::assertCount(9, array_merge([], ...$results));
    }

    /**
     * @return ItemsSelling[]
     * @throws JsonException
     */
    private function getTestData(): array
    {
        $itemsUnsoldData = json_decode(
            file_get_contents(__DIR__ . '/../_data/items_selling.json'),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        $itemsUnsoldData['pageSize'] = 3;
        $page1 = $itemsUnsoldData;
        $page2 = $itemsUnsoldData;
        $page3 = $itemsUnsoldData;
        $page1['results'] = array_slice($page1['results'], 0, 3);
        $page2['results'] = array_slice($page2['results'], 3, 3);
        $page3['results'] = array_slice($page3['results'], 6, 3);

        return [new ItemsSelling($page1), new ItemsSelling($page2), new ItemsSelling($page3)];
    }
}
