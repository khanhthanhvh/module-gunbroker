<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Observer;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\ConfigInterface as EventObserverConfig;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Model\Config;
use Reeds\GunBroker\Observer\ProductSaveObserver;
use Reeds\GunBroker\Service\ListItemMessagePublisher;
use Reeds\GunBroker\Service\UpdateItemMessagePublisher;

final class ProductSaveObserverTest extends TestCase
{
    /**
     * @magentoAppArea frontend
     */
    public function testEventObserverIsConfigured(): void
    {
        $objectManager = Bootstrap::getObjectManager();
        /** @var \Magento\Framework\Event\ConfigInterface $observerConfig */
        $observerConfig = $objectManager->create(EventObserverConfig::class);
        $observers = $observerConfig->getObservers('catalog_product_save_after');

        self::assertArrayHasKey('gunbroker_queue_item', $observers);
        self::assertSame(ltrim(ProductSaveObserver::class, '\\'), $observers['gunbroker_queue_item']['instance']);
    }

    /**
     * @dataProvider productSaverDataProvider
     * @magentoAppArea frontend
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testObserverQueuesItemForListingWhenProductIsSaved(string $saver): void
    {
        $objectManager = Bootstrap::getObjectManager();
        $listItemMessagePublisherMock = $this->createMock(ListItemMessagePublisher::class);
        $configMock = $this->createMock(ConfigInterface::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        /** @var ProductInterface|Product $product */
        $product = $productRepository->get('764503037108');

        $configMock->method('isEnabled')->willReturn(true); // Using a config fixture is not working.

        $objectManager->configure(
            [
                ListItemMessagePublisher::class => ['shared' => true],
                Config::class => ['shared' => true]
            ]
        );
        $objectManager->addSharedInstance($listItemMessagePublisherMock, ListItemMessagePublisher::class);
        $objectManager->addSharedInstance($configMock, Config::class);

        $listItemMessagePublisherMock->expects(self::once())
            ->method('publishMessage')
            ->with($product->getId(), 1);

        $product->unsetData('gunbroker_item_id');
        $product->addAttributeUpdate('gunbroker_item_id', null, 0);

        if ($saver === 'repository') {
            $productRepository->save($product);
        } else {
            /** @noinspection PhpDeprecationInspection */
            $product->save();
        }
    }

    /**
     * @dataProvider productSaverDataProvider
     * @magentoAppArea frontend
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     */
    public function testObserverQueuesItemForUpdateWhenProductIsSaved(string $saver): void
    {
        $objectManager = Bootstrap::getObjectManager();
        $updateItemMessagePublisherMock = $this->createMock(UpdateItemMessagePublisher::class);
        $configMock = $this->createMock(ConfigInterface::class);
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        /** @var ProductInterface|Product $product */
        $product = $productRepository->get('764503037108');

        $configMock->method('isEnabled')->willReturn(true);

        $objectManager->configure(
            [
                UpdateItemMessagePublisher::class => ['shared' => true],
                Config::class => ['shared' => true]
            ]
        );
        $objectManager->addSharedInstance($updateItemMessagePublisherMock, UpdateItemMessagePublisher::class);
        $objectManager->addSharedInstance($configMock, Config::class);

        $updateItemMessagePublisherMock->expects(self::once())
            ->method('publishMessage');

        if ($saver === 'repository') {
            $productRepository->save($product);
        } else {
            /** @noinspection PhpDeprecationInspection */
            $product->save();
        }
    }

    /**
     * @return string[][]
     */
    public function productSaverDataProvider(): array
    {
        return [
            'saved_by_repository' => [
                'saver' => 'repository'
            ],
            'saved_by_product_model' => [
                'saver' => 'model'
            ]
        ];
    }
}
