<?php

/**
 * @noinspection PhpUndefinedClassInspection
 * @noinspection PhpUndefinedMethodInspection
 * @noinspection PhpDeprecationInspection
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Integration\Observer;

use Magento\Framework\Event\ConfigInterface as EventObserverConfig;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\Order;
use Magento\TestFramework\Helper\Bootstrap;
use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Observer\OrderSaveObserver;
use Reeds\GunBroker\Service\FeedbackAutomator;

final class OrderSaveObserverTest extends TestCase
{
    /**
     * @magentoAppArea frontend
     */
    public function testEventObserverIsConfigured(): void
    {
        /** @var EventObserverConfig $observerConfig */
        $observerConfig = Bootstrap::getObjectManager()->create(EventObserverConfig::class);
        $observers = $observerConfig->getObservers('sales_order_save_after');

        self::assertArrayHasKey('gunbroker_add_feedback_to_completed_order', $observers);
        self::assertSame(
            ltrim(OrderSaveObserver::class, '\\'),
            $observers['gunbroker_add_feedback_to_completed_order']['instance']
        );
    }

    /**
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/category_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_attributes_handgun.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/product_simple_with_gunbroker_attributes.php
     * @magentoDataFixture Reeds_GunBroker::Test/Integration/_files/gunbroker_order.php
     * @magentoAppArea adminhtml
     */
    public function testObserverAutomatesFeedback(): void
    {
        /** @var ObjectManagerInterface $objectManager */
        $objectManager = Bootstrap::getObjectManager();
        /** @var Order $order */
        $order = $objectManager->create(Order::class)->loadByIncrementId('100000001');
        /** @var ManagerInterface $eventManager */
        $eventManager = $objectManager->create(ManagerInterface::class);
        $feedbackAutomatorMock = $this->createMock(FeedbackAutomator::class);

        $objectManager->configure(
            [
                FeedbackAutomator::class => [
                    'shared' => true
                ]
            ]
        );
        $objectManager->addSharedInstance($feedbackAutomatorMock, FeedbackAutomator::class);

        $feedbackAutomatorMock->expects(self::once())
            ->method('automate')
            ->with($order);

        $eventManager->dispatch('sales_order_save_after', ['order' => $order]);
    }
}
