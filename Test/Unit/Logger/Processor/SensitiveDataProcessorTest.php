<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Test\Unit\Logger\Processor;

use PHPUnit\Framework\TestCase;
use Reeds\GunBroker\Logger\Processor\SensitiveDataProcessor;
use stdClass;

use function array_replace_recursive;
use function call_user_func;

final class SensitiveDataProcessorTest extends TestCase
{
    public function testProcessorIsCallable(): void
    {
        $sensitiveDataProcessor = new SensitiveDataProcessor();

        self::assertIsCallable($sensitiveDataProcessor);
    }

    public function testRedactsSensitiveDataInRecordContext(): void
    {
        $testObject = new stdClass();
        $testObject->field0 = 'foo';
        $testObject->field1 = 'bar';
        $testObject->field2 = 'sensitive';
        $testObject->field3 = new stdClass();
        $testObject->field3->subfield0 = 'foo';

        $resultObject = clone $testObject;
        $resultObject->field2 = '*********';

        $record = [
            'context' => [
                'password' => 'foobar123',
                'fruits' => [
                    'apple',
                    'orange',
                    'cherry'
                ],
                'object' => $testObject
            ],
            'extra' => [],
        ];
        $expected = array_replace_recursive($record, [
            'context' => [
                'password' => '*********',
                'object' => $resultObject
            ]
        ]);
        $sensitiveDataProcessor = new SensitiveDataProcessor(['field2']);
        $actual = call_user_func($sensitiveDataProcessor, $record);

        self::assertEquals($expected, $actual);
    }
}
