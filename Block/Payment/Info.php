<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Payment;

use Magento\Framework\View\Element\AbstractBlock;
use Magento\Payment\Block\Info as PaymentInfoBlock;
use Reeds\GunBroker\Block\Payment\Info\PaymentDate;

class Info extends PaymentInfoBlock
{
    /**
     * @inheritDoc
     */
    protected function _beforeToHtml() // phpcs:ignore PSR2.Methods.MethodDeclaration.Underscore
    {
        $parentBlock = $this->getParentBlock();

        if ($parentBlock instanceof AbstractBlock) {
            $container = $parentBlock->getParentBlock();
        } else {
            $container = $this;
        }

        if (!$container instanceof AbstractBlock) {
            return $this;
        }

        $block = $this->_layout->createBlock(
            PaymentDate::class,
            '',
            [
                'data' => [
                    'info' => $this->getInfo()
                ]
            ]
        );

        $container->setChild('order_payment_additional', $block);

        return $this;
    }
}
