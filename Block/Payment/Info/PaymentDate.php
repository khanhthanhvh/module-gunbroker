<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Payment\Info;

use IntlDateFormatter;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Block\Payment\Info;

use function array_key_exists;
use function date_default_timezone_get;

class PaymentDate extends Info
{
    // phpcs:ignore PSR2.Classes.PropertyDeclaration.Underscore
    protected $_template = 'Reeds_GunBroker::payment/info/payment_date.phtml';
    private StoreManagerInterface $storeManager;

    /**
     * @inheritDoc
     * @param mixed[] $data
     */
    public function __construct(Context $context, StoreManagerInterface $storeManager, array $data = [])
    {
        parent::__construct($context, $data);

        $this->storeManager = $storeManager;
    }

    public function toPdf()
    {
        $this->setTemplate('Reeds_GunBroker::payment/info/pdf/payment_date.phtml');
        return $this->toHtml();
    }

    public function getPaymentDate(): string
    {
        $additionalInformation = $this->getInfo()->getAdditionalInformation();

        if (
            count($additionalInformation) === 0
            || !array_key_exists('payment_date', $additionalInformation)
        ) {
            return '';
        }

        return $this->formatDate(
            $additionalInformation['payment_date'],
            IntlDateFormatter::MEDIUM,
            true,
            $this->getTimezoneForStore()
        );
    }

    private function getTimezoneForStore(): string
    {
        $storeId = $this->getMethod()->getStore();

        try {
            $store = $this->storeManager->getStore($storeId);
        } catch (NoSuchEntityException $e) {
            return date_default_timezone_get();
        }

        return $this->_localeDate->getConfigTimezone(ScopeInterface::SCOPE_STORE, $store->getCode());
    }
}
