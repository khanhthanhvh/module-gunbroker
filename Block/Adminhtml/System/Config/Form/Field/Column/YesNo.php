<?php

// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\System\Config\Form\Field\Column;

use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Html\Select;

use function count;

class YesNo extends Select
{
    public function setInputName(string $name): self
    {
        return $this->setData('name', $name);
    }

    public function setInputId(string $id): self
    {
        return $this->setId($id);
    }

    /**
     * @inheritDoc
     */
    protected function _toHtml(): string
    {
        if (count($this->getOptions()) === 0) {
            $this->setOptions($this->getSourceOptions());
        }

        return parent::_toHtml();
    }

    /**
     * @return array<array{value: int, label: Phrase}>
     */
    private function getSourceOptions(): array
    {
        return [
            [
                'value' => 0,
                'label' => __('No')
            ],
            [
                'value' => 1,
                'label' => __('Yes')
            ]
        ];
    }
}
