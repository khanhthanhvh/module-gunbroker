<?php

// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\System\Config\Form\Field\Column;

use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Html\Select;

use function __;
use function count;

class PaymentMethod extends Select
{
    public function setInputName(string $name): self
    {
        return $this->setData('name', $name);
    }

    public function setInputId(string $id): self
    {
        return $this->setId($id);
    }

    /**
     * @inheritDoc
     */
    protected function _toHtml(): string
    {
        if (count($this->getOptions()) === 0) {
            $this->setOptions($this->getSourceOptions());
        }

        return parent::_toHtml();
    }

    /**
     * @return array<array{value: string, label: string|Phrase}>
     */
    private function getSourceOptions(): array
    {
        return [
            [
                'value' => 'check',
                'label' => __('Check')
            ],
            [
                'value' => 'visaMastercard',
                'label' => 'Visa/Mastercard'
            ],
            [
                'value' => 'cod',
                'label' => __('Cash on Delivery (COD)')
            ],
            [
                'value' => 'escrow',
                'label' => __('Escrow')
            ],
            [
                'value' => 'amex',
                'label' => 'American Express'
            ],
            [
                'value' => 'payPal',
                'label' => 'PayPal'
            ],
            [
                'value' => 'discover',
                'label' => 'Discover'
            ],
            [
                'value' => 'seeItemDesc',
                'label' => __('See Item Description')
            ],
            [
                'value' => 'certifiedCheck',
                'label' => __('Certified Check')
            ],
            [
                'value' => 'uspsMoneyOrder',
                'label' => __('USPS Money Order')
            ],
            [
                'value' => 'moneyOrder',
                'label' => __('Money Order')
            ],
            [
                'value' => 'freedomCoin',
                'label' => 'FreedomCoin'
            ]
        ];
    }
}
