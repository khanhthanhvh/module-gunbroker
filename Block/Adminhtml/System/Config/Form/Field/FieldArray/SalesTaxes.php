<?php

// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\System\Config\Form\Field\FieldArray;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

use function __;

class SalesTaxes extends AbstractFieldArray
{
    /**
     * @inheritDoc
     */
    protected function _prepareToRender(): void
    {
        parent::_prepareToRender();

        $this->addColumn(
            'state',
            [
                'label' => __('State'),
                'class' => 'required-entry'
            ]
        );
        $this->addColumn(
            'tax_rate',
            [
                'label' => __('Tax Rate'),
                'class' => 'required-entry'
            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Sales Tax');
    }
}
