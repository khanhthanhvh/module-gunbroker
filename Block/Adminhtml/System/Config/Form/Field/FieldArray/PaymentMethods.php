<?php

// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\System\Config\Form\Field\FieldArray;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\BlockInterface;
use Reeds\GunBroker\Block\Adminhtml\System\Config\Form\Field\Column\PaymentMethod as PaymentMethodColumn;
use Reeds\GunBroker\Block\Adminhtml\System\Config\Form\Field\Column\YesNo as YesNoColumn;

use function __;

class PaymentMethods extends AbstractFieldArray
{
    private ?BlockInterface $paymentMethodColumnRenderer = null;
    private ?BlockInterface $yesNoColumnRenderer = null;

    protected function _prepareArrayRow(DataObject $row): void
    {
        $method = $row->getData('method');
        $accepted = $row->getData('accepted');

        if ($method !== null) {
            $row->setData(
                'option_extra_attrs',
                ['option_' . $this->getPaymentMethodColumnRenderer()->calcOptionHash($method) => 'selected="selected"']
            );
        }

        if ($accepted !== null) {
            $row->setData(
                'option_extra_attrs',
                ['option_' . $this->getYesNoColumnRenderer()->calcOptionHash($accepted) => 'selected="selected"']
            );
        }
    }

    /**
     * @inheritDoc
     */
    protected function _prepareToRender(): void
    {
        $this->addColumn(
            'method',
            [
                'label' => __('Method'),
                'renderer' => $this->getPaymentMethodColumnRenderer()
            ]
        );
        $this->addColumn(
            'accepted',
            [
                'label' => __('Accepted'),
                'renderer' => $this->getYesNoColumnRenderer()
            ]
        );

        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Payment Method');
    }

    private function getPaymentMethodColumnRenderer(): BlockInterface
    {
        if ($this->paymentMethodColumnRenderer !== null) {
            return $this->paymentMethodColumnRenderer;
        }

        $this->paymentMethodColumnRenderer = $this->getLayout()->createBlock(
            PaymentMethodColumn::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        );

        return $this->paymentMethodColumnRenderer;
    }

    private function getYesNoColumnRenderer(): BlockInterface
    {
        if ($this->yesNoColumnRenderer !== null) {
            return $this->yesNoColumnRenderer;
        }

        $this->yesNoColumnRenderer = $this->getLayout()->createBlock(
            YesNoColumn::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        );

        return $this->yesNoColumnRenderer;
    }
}
