<?php

/**
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Reeds\GunBroker\Api\ConfigInterface;

use function trim;

class GunBrokerUserId extends Field
{
    protected $_template = 'Reeds_GunBroker::system/config/form/field/gunbroker_user_id.phtml';
    private ConfigInterface $config;

    /**
     * @inheritDoc
     * @param mixed[] $data
     */
    public function __construct(
        Context $context,
        ConfigInterface $config,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        parent::__construct($context, $data, $secureRenderer);

        $this->config = $config;
    }

    public function render(AbstractElement $element): string
    {
        $element->unsCanUseWebsiteValue()
            ->unsCanUseDefaultValue();

        return parent::render($element);
    }

    public function getConfiguredUserId(): ?int
    {
        return $this->config->getApiUserId((int)$this->_request->getParam('website', 0));
    }

    public function isButtonEnabled(): bool
    {
        ['username' => $username, 'password' => $password] = $this->config->getApiCredentials();

        return !empty(trim($username)) && !empty(trim($password)) && $this->getConfiguredUserId() === null;
    }

    public function getAjaxRequestUrl(): string
    {
        return $this->getUrl('gunbroker/apiuserid/get', ['website' => $this->_request->getParam('website', 0)]);
    }

    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }
}
