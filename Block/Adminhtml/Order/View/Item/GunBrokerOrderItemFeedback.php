<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\Order\View\Item;

use Magento\Backend\Block\Template;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Sales\Api\Data\OrderItemInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Block\Adminhtml\Order\View\Item\GunBrokerOrderItemFeedback\Renderer;

use function array_key_exists;

class GunBrokerOrderItemFeedback extends Template
{
    /**
     * @var array{buyer: ?OrderItemFeedbackInterface, seller: ?OrderItemFeedbackInterface}
     */
    private array $feedback = ['buyer' => null, 'seller' => null];

    public function toHtml()
    {
        if (
            !$this->isViewFeedbackAllowed()
            || $this->getItem() === null
            || $this->getItem()->getExtOrderItemId() === null
        ) {
            return '';
        }

        return parent::toHtml();
    }

    public function getItem(): ?OrderItemInterface
    {
        $parentBlock = $this->getParentBlock();

        if (!$parentBlock instanceof AbstractBlock) {
            return null;
        }

        return $parentBlock->getData('item');
    }

    public function getGunBrokerOrderItemFeedback(string $type): ?OrderItemFeedbackInterface
    {
        if (array_key_exists($type, $this->feedback) && $this->feedback[$type] !== null) {
            return $this->feedback[$type];
        }

        $orderItem = $this->getItem();

        if (
            $orderItem === null
            || $orderItem->getExtensionAttributes() === null
            || $orderItem->getExtensionAttributes()->getGunbrokerOrderItemFeedback() === null
        ) {
            return null;
        }

        foreach ($orderItem->getExtensionAttributes()->getGunbrokerOrderItemFeedback() as $orderItemFeedback) {
            $this->feedback[$orderItemFeedback->getType()] = $orderItemFeedback;
        }

        return $this->feedback[$type];
    }

    public function getBuyerFeedbackHtml(): string
    {
        $childBlock = $this->getChildBlock('feedback_renderer');

        if (!$childBlock instanceof Renderer) {
            return '';
        }

        return $childBlock->setOrderItemFeedback($this->getGunBrokerOrderItemFeedback('buyer'))
            ->toHtml();
    }

    public function getSellerFeedbackHtml(): string
    {
        $childBlock = $this->getChildBlock('feedback_renderer');

        if (!$childBlock instanceof Renderer) {
            return '';
        }

        return $childBlock->setOrderItemFeedback($this->getGunBrokerOrderItemFeedback('seller'))
            ->toHtml();
    }

    public function getGunBrokerItemId(): ?int
    {
        $orderItem = $this->getItem();

        if ($orderItem === null || $orderItem->getExtOrderItemId() === null) {
            return null;
        }

        return (int)$orderItem->getExtOrderItemId();
    }

    public function isViewFeedbackAllowed(): bool
    {
        return $this->_authorization->isAllowed('Reeds_GunBroker::admin_order_item_feedback_view');
    }

    public function isSaveFeedbackAllowed(): bool
    {
        return $this->_authorization->isAllowed('Reeds_GunBroker::admin_order_item_feedback_save');
    }
}
