<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\Order\View\Item\GunBrokerOrderItemFeedback;

use Magento\Backend\Block\Template;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;

class Renderer extends Template
{
    private ?OrderItemFeedbackInterface $orderItemFeedback = null;

    public function setOrderItemFeedback(?OrderItemFeedbackInterface $orderItemFeedback): self
    {
        $this->orderItemFeedback = $orderItemFeedback;

        return $this;
    }

    public function getOrderItemFeedback(): ?OrderItemFeedbackInterface
    {
        return $this->orderItemFeedback;
    }

    public function toHtml()
    {
        if ($this->orderItemFeedback === null) {
            return '';
        }

        return parent::toHtml();
    }
}
