<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Block\Adminhtml\Order\View;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\Registry;
use Magento\Sales\Api\Data\OrderInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;

class GunBrokerOrder extends Template
{
    private Registry $registry;
    private ConfigInterface $gunBrokerConfig;
    private OrderImportRepositoryInterface $orderImportRepository;

    /**
     * @inheritDoc
     * @param mixed[] $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        OrderImportRepositoryInterface $orderImportRepository,
        ConfigInterface $gunBrokerConfig,
        array $data = [],
        ?JsonHelper $jsonHelper = null,
        ?DirectoryHelper $directoryHelper = null
    ) {
        parent::__construct($context, $data, $jsonHelper, $directoryHelper);

        $this->registry = $registry;
        $this->gunBrokerConfig = $gunBrokerConfig;
        $this->orderImportRepository = $orderImportRepository;
    }

    /**
     * @inheritDoc
     */
    public function toHtml()
    {
        if (!$this->isGunBrokerOrder()) {
            return '';
        }

        return parent::toHtml();
    }

    public function getOrder(): OrderInterface
    {
        if ($this->registry->registry('current_order')) {
            return $this->registry->registry('current_order');
        }

        if ($this->registry->registry('order')) {
            return $this->registry->registry('order');
        }

        if ($this->registry->registry('current_invoice')) {
            return $this->registry->registry('current_invoice')->getOrder();
        }

        throw new LocalizedException(__('Could not retrieve the current order instance.'));
    }

    public function getGunBrokerOrderUrl(): string
    {
        $gunBrokerOrderUrl = 'https://www.';

        if ($this->gunBrokerConfig->getApiEnvironment() === 'sandbox') {
            $gunBrokerOrderUrl .= 'sandbox.';
        }

        return $gunBrokerOrderUrl . 'gunbroker.com/order?orderid=' . $this->getOrder()->getExtOrderId();
    }

    private function isGunBrokerOrder(): bool
    {
        if ($this->getOrder()->getExtOrderId() === null) {
            return false;
        }

        try {
            $this->orderImportRepository->getByGunBrokerOrderId((int)$this->getOrder()->getExtOrderId());
        } catch (LocalizedException | NoSuchEntityException $e) {
            return false;
        }

        return true;
    }
}
