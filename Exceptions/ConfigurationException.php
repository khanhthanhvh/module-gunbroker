<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Exceptions;

use Magento\Framework\Exception\LocalizedException;

class ConfigurationException extends LocalizedException
{
}
