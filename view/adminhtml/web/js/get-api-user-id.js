define(['jquery', 'underscore', 'mage/url', 'mage/translate', 'Magento_Ui/js/modal/alert'], function ($, _, url, $t, alert) {
    'use strict';

    return function (config) {
        const $getUserIdButton = $('#gunbroker-get-api-user-id-button');

        function handleAjaxError() {
            alert({
                content: $t('Could not get API User ID. See extension log for details.'),
                actions: {
                    always: function () {
                    }
                }
            });

            $getUserIdButton.prop('disabled', false);
        }

        $getUserIdButton.on('click', function () {
            const $self = $(this);

            $self.prop('disabled', true);

            $.ajax({
                url: config.request_url,
                method: 'GET',
                showLoader: true
            }).done(function (result) {
                const $userIdContainer = $('#gunbroker-user-id');

                if (!_.isObject(result) || !result.hasOwnProperty('userId') || result.userId === null) {
                    handleAjaxError();

                    return;
                }

                $self.hide();

                $userIdContainer.text(_.escape(result.userId));
                $userIdContainer.show();
            }).fail(handleAjaxError);
        });
    }
});