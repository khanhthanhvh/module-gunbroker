define(
    ['jquery', 'Magento_Ui/js/modal/modal-component', 'Magento_Ui/js/modal/alert', 'mage/translate'],
    function ($, Modal, alert, $t) {
        'use strict';

        let magentoOrderItemId = null,
            gunBrokerItemId = null;

        return Modal.extend({
            setItemIds: function (mageOrderItemId, gbItemId) {
                magentoOrderItemId = mageOrderItemId;
                gunBrokerItemId = gbItemId;
            },
            handleAjaxError: function (response) {
                alert({
                    content: response.message || $t('Could not save seller feedback. See extension log for details.'),
                    actions: {
                        always: function () {
                        }
                    }
                });
            },
            clear: function () {
                this._super();

                magentoOrderItemId = null;
                gunBrokerItemId = null;
            },
            actionDone: function () {
                const data = {
                    magentoOrderItemId: magentoOrderItemId,
                    gunBrokerItemId: gunBrokerItemId
                };

                this.valid = true;
                this.elems().forEach(this.validate, this);

                if (!this.valid) {
                    return;
                }

                this.applyData();

                for (const key in this.applied) {
                    const field = key.substr(key.lastIndexOf('.') + 1);

                    data[field] = this.applied[key];
                }

                $.ajax({
                    type: 'POST',
                    url: window.BASE_URL.replace('sales/index/index', 'gunbroker/feedback_seller/save'),
                    data: data,
                    showLoader: true
                }).done(function (result) {
                    if (result.error) {
                        this.handleAjaxError(result);

                        return;
                    }

                    this.clear();
                    this.closeModal();

                    window.location.reload();
                }.bind(this)).fail(this.handleAjaxError);
            }
        });
    }
);