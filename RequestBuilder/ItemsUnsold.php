<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsUnsold as ItemsUnsoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsUnsoldFactory as ItemsUnsoldRequestFactory;

class ItemsUnsold implements RequestBuilderInterface
{
    private ItemsUnsoldRequestFactory $itemsUnsoldRequestFactory;

    public function __construct(ItemsUnsoldRequestFactory $itemsUnsoldRequestFactory)
    {
        $this->itemsUnsoldRequestFactory = $itemsUnsoldRequestFactory;
    }

    /**
     * @param array{limit: int, page: int} $itemsUnsoldRequestData
     */
    public function build(array $itemsUnsoldRequestData): ?ItemsUnsoldRequest
    {
        /** @var ItemsUnsoldRequest $itemsUnsoldRequest */
        $itemsUnsoldRequest = $this->itemsUnsoldRequestFactory->create();

        $itemsUnsoldRequest->timeFrame = 8;
        $itemsUnsoldRequest->pageSize = $itemsUnsoldRequestData['limit'];
        $itemsUnsoldRequest->pageIndex = $itemsUnsoldRequestData['page'];

        return $itemsUnsoldRequest;
    }
}
