<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\Data\UpdateItemMessageInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\UpdatedItem as UpdatedItemRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\UpdatedItemFactory as UpdatedItemRequestFactory;

use function array_filter;
use function count;

class UpdatedItem implements RequestBuilderInterface
{
    private UpdatedItemRequestFactory $updatedItemRequestFactory;

    public function __construct(UpdatedItemRequestFactory $updatedItemRequestFactory)
    {
        $this->updatedItemRequestFactory = $updatedItemRequestFactory;
    }

    /**
     * @param mixed[] $updatedItemRequestData
     * @see UpdateItemMessageInterface or the following URL for data to provide
     * to method.
     * @link https://api.sandbox.gunbroker.com/User/Help/ItemsPut
     */
    public function build(array $updatedItemRequestData): ?UpdatedItemRequest
    {
        /** @var UpdatedItemRequest $updatedItemRequest */
        $updatedItemRequest = $this->updatedItemRequestFactory->create(['parameters' => $updatedItemRequestData]);

        if ($updatedItemRequest->canOffer === false) {
            $updatedItemRequest->cancelOpenOffers = true;
        }

        return count(array_filter($updatedItemRequest->all())) > 0 ? $updatedItemRequest : null;
    }
}
