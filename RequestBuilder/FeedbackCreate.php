<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackCreate as FeedbackCreateRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackCreateFactory as FeedbackCreateRequestFactory;

class FeedbackCreate implements RequestBuilderInterface
{
    private FeedbackCreateRequestFactory $feedbackCreateRequestFactory;

    public function __construct(FeedbackCreateRequestFactory $feedbackCreateRequestFactory)
    {
        $this->feedbackCreateRequestFactory = $feedbackCreateRequestFactory;
    }

    /**
     * @param array{gunBrokerItemId: int, comment: string, rating: int} $feedbackCreateRequestData
     */
    public function build(array $feedbackCreateRequestData): FeedbackCreateRequest
    {
        /** @var FeedbackCreateRequest $feedbackCreateRequest */
        $feedbackCreateRequest = $this->feedbackCreateRequestFactory->create();

        $feedbackCreateRequest->itemID = $feedbackCreateRequestData['gunBrokerItemId'];
        $feedbackCreateRequest->comment = $feedbackCreateRequestData['comment'];
        $feedbackCreateRequest->rating = $feedbackCreateRequestData['rating'];

        return $feedbackCreateRequest;
    }
}
