<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use DateTime;
use DateTimeZone;
use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Store\Model\Information as StoreInformation;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\ListingTemplateProcessorInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\Service\ItemInventoryCalculator;
use RuntimeException;
use Wagento\GunBrokerApi\ApiObjects\Input\Item as ItemRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\PaymentMethods;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\PaymentMethodsFactory;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\PremiumFeatures;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\PremiumFeaturesFactory;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\SalesTaxes;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\SalesTaxesFactory;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\ShippingClassCosts;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\ShippingClassCostsFactory;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\ShippingClassesSupported;
use Wagento\GunBrokerApi\ApiObjects\Input\Item\ShippingClassesSupportedFactory;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemFactory as ItemRequestFactory;

use function array_filter;
use function array_map;
use function array_values;
use function is_array;
use function is_numeric;
use function ltrim;

class Item implements RequestBuilderInterface
{
    private StoreManagerInterface $storeManager;
    private ListingTemplateProcessorInterface $listingTemplateProcessor;
    private ItemRequestFactory $itemRequestFactory;
    private SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory;
    private CategoryListInterface $categoryListRepository;
    private StoreInformation $storeInformation;
    private ConfigInterface $config;
    private PaymentMethodsFactory $paymentMethodsFactory;
    private ItemInventoryCalculator $itemInventoryCalculator;
    private PremiumFeaturesFactory $premiumFeaturesFactory;
    private SalesTaxesFactory $salesTaxesFactory;
    private ShippingClassCostsFactory $shippingClassCostsFactory;
    private ShippingClassesSupportedFactory $shippingClassesSupportedFactory;
    private Product $product;
    private ?Store $store = null;

    public function __construct(
        StoreManagerInterface $storeManager,
        ListingTemplateProcessorInterface $listingTemplateProcessor,
        ItemRequestFactory $itemRequestFactory,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        CategoryListInterface $categoryListRepository,
        StoreInformation $storeInformation,
        ConfigInterface $config,
        PaymentMethodsFactory $paymentMethodsFactory,
        ItemInventoryCalculator $itemInventoryCalculator,
        PremiumFeaturesFactory $premiumFeaturesFactory,
        SalesTaxesFactory $salesTaxesFactory,
        ShippingClassCostsFactory $shippingClassCostsFactory,
        ShippingClassesSupportedFactory $shippingClassesSupportedFactory
    ) {
        $this->storeManager = $storeManager;
        $this->listingTemplateProcessor = $listingTemplateProcessor;
        $this->itemRequestFactory = $itemRequestFactory;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->categoryListRepository = $categoryListRepository;
        $this->storeInformation = $storeInformation;
        $this->config = $config;
        $this->paymentMethodsFactory = $paymentMethodsFactory;
        $this->itemInventoryCalculator = $itemInventoryCalculator;
        $this->premiumFeaturesFactory = $premiumFeaturesFactory;
        $this->salesTaxesFactory = $salesTaxesFactory;
        $this->shippingClassCostsFactory = $shippingClassCostsFactory;
        $this->shippingClassesSupportedFactory = $shippingClassesSupportedFactory;
    }

    /**
     * @param array{product: Product, store: ?Store} $requestData
     * @throws RuntimeException
     */
    public function build(array $requestData): ?ItemRequest
    {
        $this->product = $requestData['product'];
        $this->store = $requestData['store'];

        if (!(bool)$this->getProductAttribute('is_gunbroker_item')) {
            return null;
        }

        try {
            $itemDescription = $this->listingTemplateProcessor->process($this->product);
        } catch (LocalizedException $e) {
            $itemDescription = '';
        }

        $storeId = (int)$this->getStore()->getId();
        $collectTaxes = $this->getProductAttribute('gunbroker_collect_taxes');
        /** @var ItemRequest $itemRequest */
        $itemRequest = $this->itemRequestFactory->create();

        $itemRequest->autoAcceptPrice = $this->getProductAttribute('gunbroker_auto_accept_price') ?? 0.00;
        $itemRequest->autoRejectPrice = $this->getProductAttribute('gunbroker_auto_reject_price') ?? 0.00;
        $itemRequest->autoRelist = $this->getProductAttribute('gunbroker_auto_relist') ?? 1;
        $itemRequest->autoRelistFixedCount = $this->getProductAttribute('gunbroker_auto_relist_fixed_count');
        $itemRequest->buyNowPrice = $this->getProductAttribute('gunbroker_buy_now_price');
        $itemRequest->categoryID = $this->getGunBrokerCategoryId();
        $itemRequest->canOffer = (bool)($this->getProductAttribute('gunbroker_can_offer') ?? false);
        $itemRequest->collectTaxes = $collectTaxes !== null ? (bool)$collectTaxes : null;
        $itemRequest->condition = $this->getProductAttribute('gunbroker_condition') ?? 1;
        $itemRequest->countryCode = $this->store !== null
            ? $this->storeInformation->getStoreInformationObject($this->getStore())->getCountryId() ?? 'US'
            : 'US';
        $itemRequest->description = $itemDescription;
        $itemRequest->fixedPrice = $this->getProductAttribute('gunbroker_fixed_price');
        $itemRequest->gtin = $this->getProductAttribute('gtin', false);
        $itemRequest->inspectionPeriod = $this->config->getInspectionPeriod($storeId);
        $itemRequest->isFFLRequired = (bool)($this->getProductAttribute('gunbroker_is_ffl_required') ?? false);
        $itemRequest->listingDuration = $this->getProductAttribute('gunbroker_listing_duration') ?? 1;
        $itemRequest->mfgPartNumber = $this->getProductAttribute('style', false) ?? $this->product->getSku();
        $itemRequest->paymentMethods = $this->buildPaymentMethods($this->config->getPaymentMethods($storeId));
        $itemRequest->pictureURLs = $this->getPictureUrls();
        $itemRequest->postalCode = $this->config->getPostalCode($storeId);
        $itemRequest->quantity = $this->getProductQuantity();
        $itemRequest->premiumFeatures = $this->buildPremiumFeatures();
        $itemRequest->salesTaxes = $this->buildSalesTaxes($this->config->getSalesTaxes($storeId));
        $itemRequest->serialNumber = $this->getProductAttribute('serial_number', false);
        $itemRequest->shippingClassCosts = $this->buildShippingClassCosts(
            $this->getProductAttribute('gunbroker_shipping_class_costs') ?? []
        );
        $itemRequest->shippingClassesSupported = $this->buildShippingClassesSupported(
            $this->getProductAttribute('gunbroker_shipping_classes_supported') ?? []
        );
        $itemRequest->shippingProfileID = $this->getProductAttribute('gunbroker_shipping_profile_id');
        $itemRequest->sku = $this->product->getSku();
        $itemRequest->standardTextID = $this->config->getStandardTextId($storeId);
        $itemRequest->startingBid = $this->getProductAttribute('gunbroker_starting_bid');
        $itemRequest->title = $this->product->getName() ?? '';
        $itemRequest->upc = $this->getProductAttribute('upc_ean', false) ?? $this->product->getSku();
        $itemRequest->weight = $this->product->hasWeight() ? (float)$this->product->getWeight() : null;
        $itemRequest->weightUnit = $itemRequest->weight !== null ? 1 : null; // 1=LB,2=KG
        $itemRequest->whoPaysForShipping = (int)($this->getProductAttribute('gunbroker_who_pays_for_shipping') ?? 1);
        $itemRequest->willShipInternational = $this->config->getWillShipInternational($storeId);

        return $itemRequest;
    }

    /**
     * @throws RuntimeException
     */
    private function getStore(): Store
    {
        if ($this->store !== null) {
            return $this->store;
        }

        /** @var Store|null $defaultStoreView */
        $defaultStoreView = $this->storeManager->getDefaultStoreView();

        if ($defaultStoreView === null) {
            throw new RuntimeException('Could not get default store.');
        }

        return $defaultStoreView;
    }

    /**
     * @return mixed
     */
    private function getProductAttribute(string $attributeCode, bool $normalize = true)
    {
        $attribute = $this->product->getCustomAttribute($attributeCode);

        if ($attribute === null) {
            return null;
        }

        $value = $attribute->getValue();

        if ($normalize && is_numeric($value)) {
            $value += 0;
        }

        return $value;
    }

    private function getGunBrokerCategoryId(): int
    {
        $gunBrokerCategoryId = 0;
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();
        $searchCriteria = $searchCriteriaBuilder->addFilter('entity_id', $this->product->getCategoryIds(), 'in')
            ->create();
        $searchResults = $this->categoryListRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return $gunBrokerCategoryId;
        }

        foreach ($searchResults->getItems() as $category) {
            $isGunBrokerCategoryAttribute = $category->getCustomAttribute('is_gunbroker_category');

            if ($isGunBrokerCategoryAttribute === null || !$isGunBrokerCategoryAttribute->getValue()) {
                continue;
            }

            $gunBrokerCategoryIdAttribute = $category->getCustomAttribute('gunbroker_category_id');

            if ($gunBrokerCategoryIdAttribute === null) {
                continue;
            }

            $gunBrokerCategoryId = (int)$gunBrokerCategoryIdAttribute->getValue();

            break;
        }

        return $gunBrokerCategoryId;
    }

    /**
     * @param array<string, array{method: string, accepted: int}> $paymentMethodsConfig
     */
    private function buildPaymentMethods(array $paymentMethodsConfig): PaymentMethods
    {
        $parameters = [];

        foreach ($paymentMethodsConfig as $paymentMethod) {
            $parameters[$paymentMethod['method']] = (bool)$paymentMethod['accepted'];
        }

        return $this->paymentMethodsFactory->create(['parameters' => $parameters]);
    }

    /**
     * @return string[]
     */
    private function getPictureUrls(): array
    {
        return array_filter(
            array_map(
                fn(ProductAttributeMediaGalleryEntryInterface $mediaGalleryEntry): ?string =>
                    $mediaGalleryEntry->getData('media_type') === 'image'
                    ? $this->getProductImageUrl($mediaGalleryEntry->getFile() ?? '')
                    : null,
                $this->product->getMediaGalleryEntries() ?? []
            )
        );
    }

    private function getProductImageUrl(string $filePath): string
    {
        return $this->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $filePath;
    }

    private function getProductQuantity(): int
    {
        if ($this->store !== null && $this->store->getId() != 0) {
            $websiteCode = $this->store->getWebsite()->getCode();
        } else {
            $defaultStore = $this->storeManager->getDefaultStoreView();

            try {
                $websiteCode = $defaultStore !== null && $defaultStore->getWebsite() instanceof WebsiteInterface
                    ? $defaultStore->getWebsite()->getCode()
                    : 'base';
            } catch (NoSuchEntityException $e) {
                $websiteCode = 'base';
            }
        }

        return $this->itemInventoryCalculator->calculate($websiteCode, $this->product);
    }

    private function buildPremiumFeatures(): PremiumFeatures
    {
        /** @var PremiumFeatures $premiumFeatures */
        $premiumFeatures = $this->premiumFeaturesFactory->create();
        $premiumFeaturesAttribute = $this->getProductAttribute('gunbroker_premium_features');
        $premiumFeaturesScheduledStartDateAttribute =
            $this->getProductAttribute('gunbroker_premium_features_scheduled_start_date');
        $premiumFeaturesThumbnailUrlAttribute = $this->getProductAttribute('gunbroker_premium_features_thumbnail_url');

        if (is_array($premiumFeaturesAttribute)) {
            foreach ($premiumFeaturesAttribute as $premiumFeature) {
                $premiumFeatures->{$premiumFeature['feature']} = (bool)$premiumFeature['enabled'];
            }
        }

        if ($premiumFeaturesScheduledStartDateAttribute !== null) {
            $premiumFeatures->scheduledStartingDate = (
                new DateTime($premiumFeaturesScheduledStartDateAttribute, new DateTimeZone('UTC'))
            )->format('Y-m-d\TH:i:s\Z');
        }

        if ($premiumFeaturesThumbnailUrlAttribute !== null) {
            $premiumFeatures->thumbnailURL = $this->getProductImageUrl(
                '/' . ltrim($premiumFeaturesThumbnailUrlAttribute, '/')
            );
        }

        $premiumFeatures->subTitle = $this->getProductAttribute('gunbroker_premium_features_subtitle');
        $premiumFeatures->titleColor = $this->getProductAttribute('gunbroker_premium_features_title_color');

        return $premiumFeatures;
    }

    /**
     * @param array<string, array{state: string, tax_rate: float}> $salesTaxesConfig
     * @return SalesTaxes[]
     */
    private function buildSalesTaxes(array $salesTaxesConfig): array
    {
        if (count($salesTaxesConfig) === 0) {
            return [];
        }

        return array_values(
            array_map(
                function (array $salesTaxConfig): SalesTaxes {
                    /** @var SalesTaxes $salesTaxes */
                    $salesTaxes = $this->salesTaxesFactory->create();
                    $salesTaxes->state = $salesTaxConfig['state'];
                    $salesTaxes->taxRate = (string)$salesTaxConfig['tax_rate'];

                    return $salesTaxes;
                },
                $salesTaxesConfig
            )
        );
    }

    /**
     * @param array{shipping_class_costs: array{shipping_class: string, cost: float}} $shippingClassCostsData
     */
    private function buildShippingClassCosts(array $shippingClassCostsData): ShippingClassCosts
    {
        $shippingClassCosts = [];

        foreach ($shippingClassCostsData as $shippingClassCost) {
            $shippingClassCosts[$shippingClassCost['shipping_class']] = (float)$shippingClassCost['cost'];
        }

        return $this->shippingClassCostsFactory->create(['parameters' => $shippingClassCosts]);
    }

    /**
     * @param array{shipping_classes_supported: array{shipping_class: string, supported: bool}} $shippingClassesSupportedData
     */
    private function buildShippingClassesSupported(array $shippingClassesSupportedData): ShippingClassesSupported
    {
        $shippingClassesSupported = [];

        foreach ($shippingClassesSupportedData as $shippingClassSupported) {
            $shippingClassesSupported[$shippingClassSupported['shipping_class']] =
                (bool)$shippingClassSupported['supported'];
        }

        return $this->shippingClassesSupportedFactory->create(['parameters' => $shippingClassesSupported]);
    }
}
