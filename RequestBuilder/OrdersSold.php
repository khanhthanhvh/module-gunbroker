<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersSold as OrdersSoldRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\OrdersSoldFactory as OrdersSoldRequestFactory;

class OrdersSold implements RequestBuilderInterface
{
    private OrdersSoldRequestFactory $ordersSoldRequestFactory;

    public function __construct(OrdersSoldRequestFactory $ordersSoldRequestFactory)
    {
        $this->ordersSoldRequestFactory = $ordersSoldRequestFactory;
    }

    /**
     * @param array{limit: int, page: int, timeFrame: int} $ordersSoldRequestData
     */
    public function build(array $ordersSoldRequestData): ?OrdersSoldRequest
    {
        /** @var OrdersSoldRequest $ordersSoldRequest */
        $ordersSoldRequest = $this->ordersSoldRequestFactory->create();

        $ordersSoldRequest->pageSize = $ordersSoldRequestData['limit'];
        $ordersSoldRequest->pageIndex = $ordersSoldRequestData['page'];
        $ordersSoldRequest->sort = 3; // Sort by order date
        $ordersSoldRequest->timeFrame = $ordersSoldRequestData['timeFrame'] ?? 1; // Search within last 24 hours by default
        $ordersSoldRequest->orderStatus = 4; // Search for orders that are pending shipment

        return $ordersSoldRequest;
    }
}
