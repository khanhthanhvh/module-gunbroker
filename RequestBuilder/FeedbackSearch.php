<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackSearch as FeedbackSearchRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\FeedbackSearchFactory as FeedbackSearchRequestFactory;

class FeedbackSearch implements RequestBuilderInterface
{
    private FeedbackSearchRequestFactory $feedbackSearchRequestFactory;

    public function __construct(FeedbackSearchRequestFactory $feedbackSearchRequestFactory)
    {
        $this->feedbackSearchRequestFactory = $feedbackSearchRequestFactory;
    }

    /**
     * @param array{timeFrame: int, limit: int, page: int} $feedbackSearchRequestData
     */
    public function build(array $feedbackSearchRequestData): FeedbackSearchRequest
    {
        /** @var FeedbackSearchRequest $feedbackSearchRequest */
        $feedbackSearchRequest = $this->feedbackSearchRequestFactory->create();

        $feedbackSearchRequest->feedbackSearchBy = 2; // buyer feedback
        $feedbackSearchRequest->timeframe = $feedbackSearchRequestData['timeframe'] ?? 2; // 2 = last 24 hours
        $feedbackSearchRequest->pageSize = $feedbackSearchRequestData['limit'] ?? 25;
        $feedbackSearchRequest->pageIndex = $feedbackSearchRequestData['page'] ?? 1;

        return $feedbackSearchRequest;
    }
}
