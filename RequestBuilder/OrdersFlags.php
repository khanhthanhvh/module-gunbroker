<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\Flags as OrdersFlagsRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\FlagsFactory as OrdersFlagsRequestFactory;

use function array_key_exists;

class OrdersFlags implements RequestBuilderInterface
{
    private OrdersFlagsRequestFactory $ordersFlagsRequestFactory;

    public function __construct(OrdersFlagsRequestFactory $ordersFlagsRequestFactory)
    {
        $this->ordersFlagsRequestFactory = $ordersFlagsRequestFactory;
    }

    /**
     * @param array{cancelOrder?: bool, orderShipped?: bool} $orderFlagsRequestData
     */
    public function build(array $orderFlagsRequestData): OrdersFlagsRequest
    {
        /** @var OrdersFlagsRequest $ordersFlagsRequest */
        $ordersFlagsRequest = $this->ordersFlagsRequestFactory->create();

        if (array_key_exists('cancelOrder', $orderFlagsRequestData)) {
            $ordersFlagsRequest->cancelOrder = $orderFlagsRequestData['cancelOrder'];
        }

        if (array_key_exists('orderShipped', $orderFlagsRequestData)) {
            $ordersFlagsRequest->orderShipped = $orderFlagsRequestData['orderShipped'];
        }

        return $ordersFlagsRequest;
    }
}
