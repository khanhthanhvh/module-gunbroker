<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSelling as ItemsSellingRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemsSellingFactory as ItemsSellingRequestFactory;

class ItemsSelling implements RequestBuilderInterface
{
    private ItemsSellingRequestFactory $itemsSellingRequestFactory;

    public function __construct(ItemsSellingRequestFactory $itemsSellingRequestFactory)
    {
        $this->itemsSellingRequestFactory = $itemsSellingRequestFactory;
    }

    /**
     * @param array{sort: int, limit: int, page: int} $itemsSellingRequestData
     */
    public function build(array $itemsSellingRequestData): ?ItemsSellingRequest
    {
        /** @var ItemsSellingRequest $itemsSellingRequest */
        $itemsSellingRequest = $this->itemsSellingRequestFactory->create();

        $itemsSellingRequest->sort = $itemsSellingRequestData['sort'];
        $itemsSellingRequest->pageSize = $itemsSellingRequestData['limit'];
        $itemsSellingRequest->pageIndex = $itemsSellingRequestData['page'];

        return $itemsSellingRequest;
    }
}
