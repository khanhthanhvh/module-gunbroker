<?php

declare(strict_types=1);

namespace Reeds\GunBroker\RequestBuilder;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\Shipping as OrdersShippingRequest;
use Wagento\GunBrokerApi\ApiObjects\Input\Orders\ShippingFactory as OrdersShippingRequestFactory;

class OrdersShipping implements RequestBuilderInterface
{
    private OrdersShippingRequestFactory $ordersShippingRequestFactory;

    public function __construct(OrdersShippingRequestFactory $ordersShippingRequestFactory)
    {
        $this->ordersShippingRequestFactory = $ordersShippingRequestFactory;
    }

    /**
     * @param array{trackingNumber: string, carrier: int} $ordersShippingRequestData
     */
    public function build(array $ordersShippingRequestData): OrdersShippingRequest
    {
        /** @var OrdersShippingRequest $ordersShippingRequest */
        $ordersShippingRequest = $this->ordersShippingRequestFactory->create();

        $ordersShippingRequest->carrier = $ordersShippingRequestData['carrier'];
        $ordersShippingRequest->trackingNumber = $ordersShippingRequestData['trackingNumber'];

        return $ordersShippingRequest;
    }
}
