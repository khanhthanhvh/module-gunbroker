<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api\Data;

interface ListItemMessageInterface
{
    public const KEY_PRODUCT_ID = 'product_id';
    public const KEY_STORE_ID = 'store_id';

    /**
     * @param int $productId
     * @return ListItemMessageInterface
     */
    public function setProductId(int $productId): ListItemMessageInterface;

    /**
     * @return int
     */
    public function getProductId(): int;

    /**
     * @param int $storeId
     * @return ListItemMessageInterface
     */
    public function setStoreId(int $storeId): ListItemMessageInterface;

    /**
     * @return int
     */
    public function getStoreId(): int;
}
