<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api\Data;

interface UpdateItemMessageInterface
{
    public const ITEM_ID = 'itemId';
    public const AUTO_ACCEPT_PRICE = 'autoAcceptPrice';
    public const AUTO_REJECT_PRICE = 'autoRejectPrice';
    public const BUY_NOW_PRICE = 'buyNowPrice';
    public const CAN_OFFER = 'canOffer';
    public const FIXED_PRICE = 'fixedPrice';
    public const GTIN = 'gtin';
    public const MFG_PART_NUMBER = 'mfgPartNumber';
    public const QUANTITY = 'quantity';
    public const RESERVE_PRICE = 'reservePrice';
    public const SKU = 'sku';
    public const STARTING_BID = 'startingBid';
    public const SUBTITLE = 'subTitle';
    public const TITLE = 'title';
    public const UPC = 'upc';
    public const WEBSITE_ID = 'websiteId';

    /**
     * @param int $itemId
     * @return UpdateItemMessageInterface
     */
    public function setItemId(int $itemId): UpdateItemMessageInterface;

    /**
     * @return int
     */
    public function getItemId(): int;

    /**
     * @param float $autoAcceptPrice
     * @return UpdateItemMessageInterface
     */
    public function setAutoAcceptPrice(float $autoAcceptPrice): UpdateItemMessageInterface;

    /**
     * @return float|null
     */
    public function getAutoAcceptPrice(): ?float;

    /**
     * @param float $autoRejectPrice
     * @return UpdateItemMessageInterface
     */
    public function setAutoRejectPrice(float $autoRejectPrice): UpdateItemMessageInterface;

    /**
     * @return float|null
     */
    public function getAutoRejectPrice(): ?float;

    /**
     * @param float $buyNowPrice
     * @return UpdateItemMessageInterface
     */
    public function setBuyNowPrice(float $buyNowPrice): UpdateItemMessageInterface;

    /**
     * @return float|null
     */
    public function getBuyNowPrice(): ?float;

    /**
     * @param bool $canOffer
     * @return UpdateItemMessageInterface
     */
    public function setCanOffer(bool $canOffer): UpdateItemMessageInterface;

    /**
     * @return bool|null
     */
    public function getCanOffer(): ?bool;

    /**
     * @param float $fixedPrice
     * @return UpdateItemMessageInterface
     */
    public function setFixedPrice(float $fixedPrice): UpdateItemMessageInterface;

    /**
     * @return float|null
     */
    public function getFixedPrice(): ?float;

    /**
     * @param string $gtin
     * @return UpdateItemMessageInterface
     */
    public function setGtin(string $gtin): UpdateItemMessageInterface;

    /**
     * @return string|null
     */
    public function getGtin(): ?string;

    /**
     * @param string $mfgPartNumber
     * @return UpdateItemMessageInterface
     */
    public function setMfgPartNumber(string $mfgPartNumber): UpdateItemMessageInterface;

    /**
     * @return string|null
     */
    public function getMfgPartNumber(): ?string;

    /**
     * @param int $quantity
     * @return UpdateItemMessageInterface
     */
    public function setQuantity(int $quantity): UpdateItemMessageInterface;

    /**
     * @return int|null
     */
    public function getQuantity(): ?int;

    /**
     * @param float $reservePrice
     * @return UpdateItemMessageInterface
     */
    public function setReservePrice(float $reservePrice): UpdateItemMessageInterface;

    /**
     * @return float|null
     */
    public function getReservePrice(): ?float;

    /**
     * @param string $sku
     * @return UpdateItemMessageInterface
     */
    public function setSku(string $sku): UpdateItemMessageInterface;

    /**
     * @return string|null
     */
    public function getSku(): ?string;

    /**
     * @param float $startingBid
     * @return UpdateItemMessageInterface
     */
    public function setStartingBid(float $startingBid): UpdateItemMessageInterface;

    /**
     * @return float|null
     */
    public function getStartingBid(): ?float;

    /**
     * @param string $subtitle
     * @return UpdateItemMessageInterface
     */
    public function setSubtitle(string $subtitle): UpdateItemMessageInterface;

    /**
     * @return string|null
     */
    public function getSubtitle(): ?string;

    /**
     * @param string $title
     * @return UpdateItemMessageInterface
     */
    public function setTitle(string $title): UpdateItemMessageInterface;

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @param string $upc
     * @return UpdateItemMessageInterface
     */
    public function setUpc(string $upc): UpdateItemMessageInterface;

    /**
     * @return string|null
     */
    public function getUpc(): ?string;

    /**
     * @param int $websiteId
     * @return UpdateItemMessageInterface
     */
    public function setWebsiteId(int $websiteId): UpdateItemMessageInterface;

    /**
     * @return int|null
     */
    public function getWebsiteId(): ?int;

    /**
     * @return mixed[]
     * @see \Magento\Framework\Api\AbstractSimpleObject::__toArray()
     */
    public function __toArray();
}
