<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api\Data;

use DateTime;
use JsonException;

interface ApiLogInterface
{
    public const LOG_ID = 'log_id';
    public const HTTP_METHOD = 'http_method';
    public const HTTP_STATUS = 'http_status';
    public const REQUEST_URL = 'request_url';
    public const MESSAGE_DATA = 'message_data';
    public const EXCEPTION_MESSAGE = 'exception_message';
    public const STATUS = 'status';
    public const RECORDED_AT = 'recorded_at';

    public function setId($id);

    public function getId();

    public function setLogId(int $logId): ApiLogInterface;

    public function getLogId(): ?int;

    public function setHttpMethod(string $httpMethod): ApiLogInterface;

    public function getHttpMethod(): string;

    public function setHttpStatus(string $httpStatus): ApiLogInterface;

    public function getHttpStatus(): string;

    public function setRequestUrl(string $requestUrl): ApiLogInterface;

    public function getRequestUrl(): string;

    /**
     * @param string|mixed[]|object $messageData
     * @throws JsonException
     */
    public function setMessageData($messageData): ApiLogInterface;

    /**
     * @return object|string|null
     * @throws JsonException
     */
    public function getMessageData(bool $asJson = true);

    public function setExceptionMessage(string $exceptionMessage): ApiLogInterface;

    public function getExceptionMessage(): string;

    public function setStatus(string $status): ApiLogInterface;

    public function getStatus(): string;

    /**
     * @param string|DateTime $recordedAt
     */
    public function setRecordedAt($recordedAt): ApiLogInterface;

    /**
     * @return string|DateTime
     */
    public function getRecordedAt(bool $raw = true);
}
