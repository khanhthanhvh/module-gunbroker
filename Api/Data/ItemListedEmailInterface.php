<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api\Data;

use DateTimeInterface;
use Exception;
use JsonException;

interface ItemListedEmailInterface
{
    public const EMAIL_ID = 'email_id';
    public const ITEM_ID = 'item_id';
    public const STORE_ID = 'store_id';
    public const ITEM_DATA = 'item_data';
    public const ITEM_LISTING_URL = 'item_url';
    public const PRODUCT_URL = 'product_url';
    public const SENT_AT = 'sent_at';

    /**
     * @param int $id
     * @return ItemListedEmailInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    public function setEmailId(int $emailId): ItemListedEmailInterface;

    public function getEmailId(): ?int;

    public function setItemId(int $itemId): ItemListedEmailInterface;

    public function getItemId(): int;

    public function setStoreId(int $storeId): ItemListedEmailInterface;

    public function getStoreId(): int;

    /**
     * @param string|mixed[] $itemData
     * @throws JsonException
     */
    public function setItemData($itemData): ItemListedEmailInterface;

    /**
     * @return mixed[]
     * @throws JsonException
     */
    public function getItemData(): array;

    public function setItemListingUrl(string $itemListingUrl): ItemListedEmailInterface;

    public function getItemListingUrl(): string;

    public function setProductUrl(string $magentoProductUrl): ItemListedEmailInterface;

    public function getProductUrl(): string;

    /**
     * @param string|DateTimeInterface $sentAt
     */
    public function setSentAt($sentAt): ItemListedEmailInterface;

    /**
     * @return string|DateTimeInterface
     * @throws Exception
     */
    public function getSentAt(bool $raw = true);
}
