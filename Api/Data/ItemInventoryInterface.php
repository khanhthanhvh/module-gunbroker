<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api\Data;

interface ItemInventoryInterface
{
    public const KEY_INVENTORY_ID = 'inventory_id';
    public const KEY_PRODUCT_SKU = 'product_sku';
    public const KEY_ITEM_ID = 'item_id';
    public const KEY_QUANTITY = 'quantity';
    public const KEY_STATUS = 'status';
    public const KEY_UPDATED = 'updated';
    public const STATUS_PENDING = 'pending';
    public const STATUS_UPDATED = 'updated';

    /**
     * @param int $id
     * @return ItemInventoryInterface
     */
    public function setId($id);

    /**
     * @return int|string|null
     */
    public function getId();

    public function setInventoryId(?int $inventoryId): ItemInventoryInterface;

    public function getInventoryId(): ?int;

    public function setProductSku(string $productSku): ItemInventoryInterface;

    public function getProductSku(): string;

    public function setItemId(int $itemId): ItemInventoryInterface;

    public function getItemId(): int;

    public function setQuantity(int $quantity): ItemInventoryInterface;

    public function getQuantity(): int;

    public function setStatus(string $status): ItemInventoryInterface;

    public function getStatus(): string;

    public function setUpdated(string $updated): ItemInventoryInterface;

    public function getUpdated(): string;
}
