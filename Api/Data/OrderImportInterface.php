<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api\Data;

interface OrderImportInterface
{
    public const IMPORT_ID = 'import_id';
    public const GUNBROKER_ORDER_ID = 'gunbroker_order_id';
    public const MAGENTO_ORDER_ID = 'magento_order_id';
    public const IMPORT_STATUS = 'import_status';
    public const IMPORT_CREATED = 'import_created';
    public const IMPORT_UPDATED = 'import_updated';

    /**
     * @return string|int
     */
    public function getId();

    public function setImportId(int $importId): OrderImportInterface;

    public function getImportId(): ?int;

    public function setGunBrokerOrderId(int $gunBrokerOrderId): OrderImportInterface;

    public function getGunBrokerOrderId(): int;

    public function setMagentoOrderId(int $magentoOrderId): OrderImportInterface;

    public function getMagentoOrderId(): ?int;

    public function setImportStatus(string $importStatus): OrderImportInterface;

    public function getImportStatus(): string;

    public function setImportCreated(string $importCreated): OrderImportInterface;

    public function getImportCreated(): string;

    public function setImportUpdated(string $importUpdated): OrderImportInterface;

    public function getImportUpdated(): string;
}
