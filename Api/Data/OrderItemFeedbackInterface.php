<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api\Data;

use Magento\Framework\Exception\InputException;

interface OrderItemFeedbackInterface
{
    public const ID = 'id';
    public const TYPE = 'type';
    public const GUNBROKER_FEEDBACK_ID = 'gunbroker_feedback_id';
    public const MAGENTO_ORDER_ITEM_ID = 'magento_order_item_id';
    public const GUNBROKER_ITEM_ID = 'gunbroker_item_id';
    public const RATING_LETTER = 'rating_letter';
    public const RATING_SCORE = 'rating_score';
    public const COMMENT = 'comment';
    public const COMMENT_DATE = 'comment_date';
    public const COMMENTATOR = 'commentator';
    public const UPDATED = 'updated';
    public const RATING_MAP = [
        0 => 'F',
        1 => 'D',
        2 => 'C',
        3 => 'B',
        4 => 'A',
        5 => 'A+'
    ];

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @param int|null $id
     * @return OrderItemFeedbackInterface
     */
    public function setId(?int $id): OrderItemFeedbackInterface;

    /**
     * @param string $type
     * @return OrderItemFeedbackInterface
     * @throws InputException
     */
    public function setType(string $type): OrderItemFeedbackInterface;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return int
     */
    public function getGunbrokerFeedbackId(): int;

    /**
     * @param int $gunbrokerFeedbackId
     * @return OrderItemFeedbackInterface
     */
    public function setGunbrokerFeedbackId(int $gunbrokerFeedbackId): OrderItemFeedbackInterface;

    /**
     * @return int
     */
    public function getMagentoOrderItemId(): int;

    /**
     * @param int $magentoOrderItemId
     * @return OrderItemFeedbackInterface
     */
    public function setMagentoOrderItemId(int $magentoOrderItemId): OrderItemFeedbackInterface;

    /**
     * @return int
     */
    public function getGunbrokerItemId(): int;

    /**
     * @param int $gunbrokerItemId
     * @return OrderItemFeedbackInterface
     */
    public function setGunbrokerItemId(int $gunbrokerItemId): OrderItemFeedbackInterface;

    /**
     * @return string
     */
    public function getRatingLetter(): string;

    /**
     * @param string $ratingLetter
     * @return OrderItemFeedbackInterface
     */
    public function setRatingLetter(string $ratingLetter): OrderItemFeedbackInterface;

    /**
     * @return int
     */
    public function getRatingScore(): int;

    /**
     * @param int $ratingScore
     * @return OrderItemFeedbackInterface
     */
    public function setRatingScore(int $ratingScore): OrderItemFeedbackInterface;

    /**
     * @return string
     */
    public function getComment(): string;

    /**
     * @param string $comment
     * @return OrderItemFeedbackInterface
     */
    public function setComment(string $comment): OrderItemFeedbackInterface;

    /**
     * @return string
     */
    public function getCommentDate(): string;

    /**
     * @param string $commentDate
     * @return OrderItemFeedbackInterface
     */
    public function setCommentDate(string $commentDate): OrderItemFeedbackInterface;

    /**
     * @return string
     */
    public function getCommentator(): string;

    /**
     * @param string $commentator
     * @return OrderItemFeedbackInterface
     */
    public function setCommentator(string $commentator): OrderItemFeedbackInterface;

    /**
     * @return string
     */
    public function getUpdated(): string;

    /**
     * @param string $updated
     * @return OrderItemFeedbackInterface
     */
    public function setUpdated(string $updated): OrderItemFeedbackInterface;
}
