<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\ItemInventoryInterface;

interface ItemInventoryRepositoryInterface
{
    /**
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(ItemInventoryInterface $itemInventory): ItemInventoryInterface;

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $inventoryId): ItemInventoryInterface;

    /**
     * @throws NoSuchEntityException
     */
    public function getByItemId(int $itemId): ItemInventoryInterface;

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(ItemInventoryInterface $itemInventory): bool;

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $inventoryId): bool;

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteByItemId(int $itemId): bool;
}
