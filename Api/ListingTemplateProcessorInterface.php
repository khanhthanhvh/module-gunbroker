<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;

interface ListingTemplateProcessorInterface
{
    /**
     * @throws InputException
     * @throws LocalizedException
     * @throws Exception
     */
    public function process(ProductInterface $product): string;
}
