<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterface;

interface ItemListedEmailRepositoryInterface
{
    /**
     * @throws CouldNotSaveException
     */
    public function save(ItemListedEmailInterface $itemListedEmail): ItemListedEmailInterface;

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $emailId): ItemListedEmailInterface;

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(ItemListedEmailInterface $itemListedEmail): bool;

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $emailId): bool;
}
