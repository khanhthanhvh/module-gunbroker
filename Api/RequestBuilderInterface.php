<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

use Spatie\DataTransferObject\DataTransferObject;

interface RequestBuilderInterface
{
    /**
     * @param mixed[] $requestData
     */
    public function build(array $requestData): ?DataTransferObject;
}
