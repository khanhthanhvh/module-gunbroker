<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

interface ConfigInterface
{
    public function isEnabled(?int $websiteId = null): bool;

    public function isLoggingEnabled(?int $websiteId = null): bool;

    public function getEmailSenderIdentity(?int $storeId = null): string;

    public function getEmailRecipientIdentity(?int $storeId = null): string;

    public function sendItemListedConfirmationEmail(?int $storeId = null): bool;

    public function getItemListedConfirmationEmailTemplate(?int $storeId = null): string;

    public function getSendEmailsAsynchronously(?int $storeId = null): bool;

    public function getEmailBatchSize(?int $storeId = null): int;

    public function getApiEnvironment(?int $websiteId = null): string;

    public function getApiDevKey(?int $websiteId = null): string;

    public function getApiSandboxUsername(?int $websiteId = null): string;

    public function getApiSandboxPassword(?int $websiteId = null): string;

    public function getApiProductionUsername(?int $websiteId = null): string;

    public function getApiProductionPassword(?int $websiteId = null): string;

    public function getApiUserId(?int $websiteId = null): ?int;

    /**
     * @return array{username: string, password: string}
     */
    public function getApiCredentials(?int $websiteId = null): array;

    public function getInspectionPeriod(?int $storeId = null): int;

    /**
     * @return array<string, array{method: string, accepted: int}>
     */
    public function getPaymentMethods(?int $storeId = null): array;

    public function getPostalCode(?int $storeId = null): string;

    /**
     * @return array<string, array{state: string, tax_rate: float}>
     */
    public function getSalesTaxes(?int $storeId = null): array;

    public function getStandardTextId(?int $storeId = null): int;

    public function getUseDefaultSalesTax(?int $storeId = null): bool;

    public function getWillShipInternational(?int $storeId = null): bool;

    public function isFeedbackEnabled(?int $storeId = null): bool;

    public function getDefaultFeedbackComment(?int $storeId = null): string;

    public function getDefaultFeedbackRating(?int $storeId = null): int;
}
