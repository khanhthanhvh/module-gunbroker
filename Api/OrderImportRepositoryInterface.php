<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\OrderImportInterface;

interface OrderImportRepositoryInterface
{
    /**
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(OrderImportInterface $orderImport): OrderImportInterface;

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $orderImportId): OrderImportInterface;

    /**
     * @throws NoSuchEntityException
     */
    public function getByGunBrokerOrderId(int $gunBrokerOrderId): OrderImportInterface;

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(OrderImportInterface $orderImport): bool;

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $orderImportId): bool;
}
