<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;

interface OrderItemFeedbackRepositoryInterface
{
    /**
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(OrderItemFeedbackInterface $orderItemFeedback): OrderItemFeedbackInterface;

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $id): OrderItemFeedbackInterface;

    /**
     * @return OrderItemFeedbackInterface[]
     * @throws NoSuchEntityException
     */
    public function getByMagentoOrderItemId(int $magentoOrderItemId, ?string $type = null): array;

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(OrderItemFeedbackInterface $orderItemFeedback): bool;

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool;
}
