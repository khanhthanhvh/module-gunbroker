<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\ApiLogInterface;

interface ApiLogRepositoryInterface
{
    /**
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(ApiLogInterface $apiLog): ApiLogInterface;

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $logId): ApiLogInterface;

    /**
     * @throws LocalizedException
     */
    public function getByStatus(string $status): SearchResultsInterface;

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface;

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(ApiLogInterface $apiLog): bool;

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $logId): bool;
}
