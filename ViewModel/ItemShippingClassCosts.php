<?php

declare(strict_types=1);

namespace Reeds\GunBroker\ViewModel;

use Magento\Framework\Phrase;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Reeds\GunBroker\Model\Attribute\Source\ShippingClass;

use function array_column;
use function array_search;

class ItemShippingClassCosts implements ArgumentInterface
{
    private ShippingClass $shippingClassSource;
    private PriceCurrencyInterface $priceCurrency;
    /**
     * @var array<string, float>
     */
    private array $shippingClassCosts = [];
    /**
     * @var array<int, array{label: Phrase, amount: string}>|null
     */
    private ?array $formattedShippingClassCosts = null;

    public function __construct(ShippingClass $shippingClassSource, PriceCurrencyInterface $priceCurrency)
    {
        $this->shippingClassSource = $shippingClassSource;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @param array<string, float> $shippingClassCosts
     */
    public function setShippingClassCosts(array $shippingClassCosts): void
    {
        $this->shippingClassCosts = $shippingClassCosts;
    }

    /**
     * @return array<int, array{label: Phrase, amount: string}>
     */
    public function getFormattedShippingClassCosts(): array
    {
        if ($this->formattedShippingClassCosts !== null) {
            return $this->formattedShippingClassCosts;
        }

        $shippingClasses = $this->shippingClassSource->getAllOptions();
        $this->formattedShippingClassCosts = [];

        foreach ($this->shippingClassCosts as $class => $cost) {
            $key = array_search($class, array_column($shippingClasses, 'value'), true);

            if ($key === false || !($cost > 0.00)) {
                continue;
            }

            $this->formattedShippingClassCosts[] = [
                'label' => $shippingClasses[$key]['label'],
                'amount' => $this->priceCurrency->convertAndFormat($cost)
            ];
        }

        return $this->formattedShippingClassCosts;
    }
}
