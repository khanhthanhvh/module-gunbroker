<?php

declare(strict_types=1);

namespace Reeds\GunBroker\HTTPClient\Plugin\History;

use Http\Client\Common\Plugin\Journal;
use JsonException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ApiLogRepositoryInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\Data\ApiLogInterface;
use Reeds\GunBroker\Api\Data\ApiLogInterfaceFactory;
use Reeds\GunBroker\Util\Redactor;

use function json_decode;

use const JSON_THROW_ON_ERROR;

class ApiLog implements Journal
{
    private LoggerInterface $logger;
    private ApiLogRepositoryInterface $apiLogRepository;
    private ApiLogInterfaceFactory $apiLogFactory;
    private ConfigInterface $config;
    private ?int $websiteId = null;

    public function __construct(
        LoggerInterface $logger,
        ApiLogRepositoryInterface $apiLogRepository,
        ApiLogInterfaceFactory $apiLogFactory,
        ConfigInterface $config
    ) {
        $this->logger = $logger;
        $this->apiLogRepository = $apiLogRepository;
        $this->apiLogFactory = $apiLogFactory;
        $this->config = $config;
    }

    public function setWebsiteId(?int $websiteId = null): self
    {
        $this->websiteId = $websiteId;

        return $this;
    }

    public function addSuccess(RequestInterface $request, ResponseInterface $response): void
    {
        /** @var ApiLogInterface $apiLog */
        $apiLog = $this->apiLogFactory->create();

        $apiLog->setHttpMethod($request->getMethod());
        $apiLog->setHttpStatus($response->getStatusCode() . ' ' . $response->getReasonPhrase());
        $apiLog->setRequestUrl($this->getApiUrl($request->getUri()));

        try {
            $apiLog->setMessageData(
                [
                    'request' => Redactor::redact(
                        json_decode($request->getBody()->__toString(), false, 512, JSON_THROW_ON_ERROR),
                        ['password', 'accessToken']
                    ),
                    'response' => Redactor::redact(
                        json_decode($response->getBody()->__toString(), false, 512, JSON_THROW_ON_ERROR),
                        ['password', 'accessToken']
                    )
                ]
            );
        } catch (JsonException $e) {
            // No further action necessary
        }

        $apiLog->setStatus($response->getStatusCode() === 200 ? 'SUCCESS' : 'FAILURE');

        $this->saveLog($apiLog);
    }

    public function addFailure(RequestInterface $request, ClientExceptionInterface $exception): void
    {
        /** @var ApiLogInterface $apiLog */
        $apiLog = $this->apiLogFactory->create();

        $apiLog->setHttpMethod($request->getMethod());
        $apiLog->setRequestUrl($this->getApiUrl($request->getUri()));

        try {
            $apiLog->setMessageData(
                [
                    'request' => Redactor::redact(
                        json_decode($request->getBody()->__toString(), false, 512, JSON_THROW_ON_ERROR),
                        ['password', 'accessToken']
                    ),
                    'exception' => $exception->__toString()
                ]
            );
        } catch (JsonException $e) {
            // No further action necessary
        }

        $apiLog->setExceptionMessage($exception->getMessage());
        $apiLog->setStatus('FAILURE');

        $this->saveLog($apiLog);
    }

    private function saveLog(ApiLogInterface $apiLog): void
    {
        try {
            $this->apiLogRepository->save($apiLog);
        } catch (CouldNotSaveException | NoSuchEntityException $e) {
            $this->logger->error(__('Could not record API history log entry. Error: %s', $e->getMessage()));
        }
    }

    private function getApiUrl(UriInterface $uri): string
    {
        $apiRequestUrl = '';

        if ($uri->getHost() === '') {
            $apiRequestUrl .= $this->config->getApiEnvironment($this->websiteId) === 'sandbox'
                ? 'https://api.sandbox.gunbroker.com/v1'
                : 'https://api.gunbroker.com/v1';
        }

        $apiRequestUrl .= $uri->__toString();

        return $apiRequestUrl;
    }
}
