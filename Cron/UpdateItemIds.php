<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Cron;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Service\ItemIdUpdater;

use function __;
use function array_merge;
use function count;

class UpdateItemIds
{
    private ItemIdUpdater $itemIdUpdater;
    private LoggerInterface $logger;

    public function __construct(ItemIdUpdater $itemIdUpdater, LoggerInterface $logger)
    {
        $this->itemIdUpdater = $itemIdUpdater;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        $updatedProducts = $this->itemIdUpdater->updateInWebsites();

        if (count($updatedProducts) === 0) {
            $this->logger->info(
                __('No item identifiers were available to update in products during scheduled update.')
            );

            return;
        }

        $this->logger->info(
            __(
                '%1 products were updated during scheduled item identifier update.',
                count(array_merge([], ...$updatedProducts))
            )
        );
    }
}
