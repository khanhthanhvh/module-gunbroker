<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Cron;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Service\OrderImporter;

use function __;
use function count;

class ImportOrders
{
    private OrderImporter $orderImporter;
    private LoggerInterface $logger;

    public function __construct(OrderImporter $orderImporter, LoggerInterface $logger)
    {
        $this->orderImporter = $orderImporter;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        $importedOrders = $this->orderImporter->importOrders();

        if ($importedOrders === null) {
            $this->logger->info(__('No orders were available to import during scheduled import.'));

            return;
        }

        if (count($importedOrders['failed']) > 0) {
            $this->logger->info(
                __('%1 orders could not be imported during scheduled import.', count($importedOrders['failed'])),
                [
                    'failed_orders' => $importedOrders['failed']
                ]
            );
        }

        if (count($importedOrders['complete']) > 0) {
            $this->logger->info(
                __('%1 orders were imported successfully during scheduled import.', count($importedOrders['failed'])),
                [
                    'successful_orders' => $importedOrders['complete']
                ]
            );
        }
    }
}
