<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Cron;

use Reeds\GunBroker\Service\ListItemEmailSender\Asynchronous as AsynchronousItemListedEmailSender;

class SendItemListedEmails
{
    private AsynchronousItemListedEmailSender $itemListedEmailSender;

    public function __construct(AsynchronousItemListedEmailSender $itemListedEmailSender)
    {
        $this->itemListedEmailSender = $itemListedEmailSender;
    }

    public function execute(): void
    {
        $this->itemListedEmailSender->sendEmails();
    }
}
