<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Cron;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Service\FeedbackImporter;

use function __;
use function array_filter;
use function array_map;
use function array_merge;
use function count;

class ImportFeedback
{
    private FeedbackImporter $feedbackImporter;
    private LoggerInterface $logger;

    public function __construct(FeedbackImporter $feedbackImporter, LoggerInterface $logger)
    {
        $this->feedbackImporter = $feedbackImporter;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        $importedFeedback = array_filter($this->feedbackImporter->importInWebsites());

        if (count($importedFeedback) === 0) {
            $this->logger->info(
                __('No feedback was available to import during scheduled import.')
            );

            return;
        }

        $failedImports = array_merge([], ...array_column($importedFeedback, 'failed'));
        $successfulImports = array_merge([], ...array_column($importedFeedback, 'succeeded'));
        $getFeedbackData = static fn(OrderItemFeedbackInterface $orderItemFeedback): array
            => $orderItemFeedback->getData();

        if (count($failedImports) > 0) {
            $this->logger->warning(
                __(
                    '%1 feedback could not be imported during scheduled import.',
                    count($failedImports)
                ),
                ['feedback_data' => array_map($getFeedbackData, $failedImports)]
            );
        }

        if (count($successfulImports) > 0) {
            $this->logger->info(
                __(
                    '%1 feedback were successfully imported during scheduled import.',
                    count($successfulImports)
                ),
                ['feedback_data' => array_map($getFeedbackData, $successfulImports)]
            );
        }
    }
}
