<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Cron;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\UnsoldItemsProcessor;

use function __;
use function count;

class ProcessUnsoldItems
{
    private ConfigInterface $config;
    private UnsoldItemsProcessor $unsoldItemsProcessor;
    private LoggerInterface $logger;

    public function __construct(
        ConfigInterface $config,
        UnsoldItemsProcessor $unsoldItemsProcessor,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->unsoldItemsProcessor = $unsoldItemsProcessor;
        $this->logger = $logger;
    }

    public function execute(): void
    {
        if (!$this->config->isEnabled()) {
            return;
        }

        $processedProductAndItemIds = $this->unsoldItemsProcessor->process();

        if (count($processedProductAndItemIds) === 0) {
            return;
        }

        $this->logger->info(
            __('Processed %1 unsold item(s).', count($processedProductAndItemIds)),
            ['product_and_item_ids' => $processedProductAndItemIds]
        );
    }
}
