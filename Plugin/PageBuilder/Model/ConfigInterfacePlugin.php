<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\PageBuilder\Model;

use Magento\PageBuilder\Model\ConfigInterface;
use Reeds\GunBroker\Service\BlockEditorDisabler;

class ConfigInterfacePlugin
{
    private BlockEditorDisabler $blockEditorDisabler;

    public function __construct(BlockEditorDisabler $blockEditorDisabler)
    {
        $this->blockEditorDisabler = $blockEditorDisabler;
    }

    /** @noinspection PhpUnusedParameterInspection */
    public function afterIsEnabled(ConfigInterface $subject, bool $result): bool
    {
        return $this->blockEditorDisabler->disableForGunBrokerBlocks($result);
    }
}
