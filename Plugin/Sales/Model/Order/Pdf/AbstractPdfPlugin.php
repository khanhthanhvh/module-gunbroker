<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Sales\Model\Order\Pdf;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Pdf\AbstractPdf;
use Magento\Sales\Model\Order\Shipment;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;
use Zend_Pdf_Color_GrayScale;
use Zend_Pdf_Font;
use Zend_Pdf_Page;

use function __;

class AbstractPdfPlugin
{
    private OrderImportRepositoryInterface $orderImportRepository;
    private ReadInterface $rootDirectory;

    public function __construct(OrderImportRepositoryInterface $orderImportRepository, Filesystem $filesystem)
    {
        $this->orderImportRepository = $orderImportRepository;
        $this->rootDirectory = $filesystem->getDirectoryRead(DirectoryList::ROOT);
    }

    /**
     * @return null
     */
    public function aroundInsertOrder(
        AbstractPdf $subject,
        callable $proceed,
        Zend_Pdf_Page $page,
        AbstractModel $obj,
        bool $putOrderId = true
    ) {
        if ($obj instanceof Order) {
            $order = $obj;
        } elseif ($obj instanceof Shipment) {
            $order = $obj->getOrder();
        } else {
            return $proceed($page, $obj, $putOrderId, false);
        }

        $isGunBrokerOrder = $this->isGunBrokerOrder($order);
        $result = $proceed($page, $obj, $putOrderId, $isGunBrokerOrder);

        if (!$isGunBrokerOrder) {
            return $result;
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $docHeader = $subject->getDocHeaderCoordinates();
        $font = Zend_Pdf_Font::fontWithPath(
            $this->rootDirectory->getAbsolutePath('lib/internal/GnuFreeFont/FreeSerif.ttf')
        );

        $page->setFont($font, 10);
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(1));
        $page->drawText(__('GunBroker.com Order # %1', $order->getExtOrderId()), 35, $docHeader[1] - 45, 'UTF-8');

        return $result;
    }

    private function isGunBrokerOrder(Order $order): bool
    {
        if ($order->getExtOrderId() === null) {
            return false;
        }

        try {
            $this->orderImportRepository->getByGunBrokerOrderId((int)$order->getExtOrderId());
        } catch (LocalizedException | NoSuchEntityException $e) {
            return false;
        }

        return true;
    }
}
