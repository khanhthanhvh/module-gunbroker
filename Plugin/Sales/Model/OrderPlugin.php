<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Sales\Model;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\SerializedProductDelister;

use function array_walk;
use function count;
use function str_contains;

class OrderPlugin
{
    private ConfigInterface $config;
    private SerializedProductDelister $serializedProductDelister;
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    private ProductRepositoryInterface $productRepository;

    public function __construct(
        ConfigInterface $config,
        SerializedProductDelister $serializedProductDelister,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductRepositoryInterface $productRepository
    ) {
        $this->config = $config;
        $this->serializedProductDelister = $serializedProductDelister;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
    }

    /**
     * @see \Magento\Sales\Model\Order::place()
     */
    public function aroundPlace(Order $subject, callable $proceed): Order
    {
        $websiteId = (int)$subject->getStore()->getWebsiteId();
        $storeId = (int)$subject->getStore()->getId();

        if (!$this->config->isEnabled($websiteId)) {
            return $proceed();
        }

        $products = $this->getSerializedGunBrokerProducts($subject->getItems());
        $result = $proceed();

        if (count($products) === 0) {
            return $result;
        }

        array_walk(
            $products,
            function (ProductInterface $product) use ($websiteId, $storeId) {
                $this->serializedProductDelister->delist($product, $websiteId, $storeId);
            }
        );

        return $result;
    }

    /**
     * @param OrderItemInterface[] $orderItems
     * @return ProductInterface[]
     */
    private function getSerializedGunBrokerProducts(array $orderItems): array
    {
        $serializedProductSkus = [];

        foreach ($orderItems as $orderItem) {
            if (!str_contains($orderItem->getSku(), '~')) {
                continue;
            }

            $serializedProductSkus[] = $orderItem->getSku();
        }

        if (count($serializedProductSkus) === 0) {
            return [];
        }

        $searchCriteria = $this->searchCriteriaBuilder->addFilter('sku', $serializedProductSkus, 'in')
            ->addFilter('is_gunbroker_item', 1)
            ->addFilter('gunbroker_item_id', '', 'notnull')
            ->create();

        return $this->productRepository->getList($searchCriteria)->getItems();
    }
}
