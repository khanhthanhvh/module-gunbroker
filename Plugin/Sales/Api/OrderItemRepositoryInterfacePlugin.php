<?php

/**
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 * @noinspection NullPointerExceptionInspection
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Sales\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderItemSearchResultInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;

class OrderItemRepositoryInterfacePlugin
{
    private OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository;

    public function __construct(OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository)
    {
        $this->orderItemFeedbackRepository = $orderItemFeedbackRepository;
    }

    public function afterGetList(
        OrderItemRepositoryInterface $subject,
        OrderItemSearchResultInterface $result,
        SearchCriteriaInterface $searchCriteria
    ): OrderItemSearchResultInterface {
        foreach ($result->getItems() as $orderItem) {
            $this->afterGet($subject, $orderItem, $orderItem->getItemId());
        }

        return $result;
    }

    /**
     * @param int|null $id
     */
    public function afterGet(OrderItemRepositoryInterface $subject, OrderItemInterface $result, $id): OrderItemInterface
    {
        try {
            $orderItemFeedback = $this->orderItemFeedbackRepository->getByMagentoOrderItemId((int)$id);
        } catch (NoSuchEntityException $e) {
            return $result;
        }

        $orderItemExtensionAttributes = $result->getExtensionAttributes();

        $orderItemExtensionAttributes->setGunbrokerOrderItemFeedback($orderItemFeedback);

        return $result;
    }
}
