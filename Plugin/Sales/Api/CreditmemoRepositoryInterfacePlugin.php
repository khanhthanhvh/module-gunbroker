<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Sales\Api;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Reeds\GunBroker\Service\OrderCanceler;
use RuntimeException;

use function __;

class CreditmemoRepositoryInterfacePlugin
{
    private State $state;
    private ManagerInterface $messageManager;
    private OrderCanceler $orderCanceler;

    public function __construct(OrderCanceler $orderCanceler, State $state, ManagerInterface $messageManager)
    {
        $this->state = $state;
        $this->messageManager = $messageManager;
        $this->orderCanceler = $orderCanceler;
    }

    public function afterSave(
        CreditmemoRepositoryInterface $subject,
        CreditmemoInterface $creditmemo
    ): CreditmemoInterface {
        try {
            $isCanceled = $this->orderCanceler->cancelOrder($creditmemo);
        } catch (RuntimeException $runtimeException) {
            $this->maybeAddMessage(
                (string)__(
                    'Could not mark GunBroker.com order as canceled. Error: %1',
                    $runtimeException->getMessage()
                ),
                'error'
            );

            return $creditmemo;
        }

        if ($isCanceled) {
            $this->maybeAddMessage((string)__('GunBroker.com order successfully marked as canceled.'), 'success');
        }

        return $creditmemo;
    }

    /**
     * @phpstan-param 'success'|'error' $type
     */
    private function maybeAddMessage(string $message, string $type): void
    {
        try {
            if ($this->state->getAreaCode() !== Area::AREA_ADMINHTML) {
                return;
            }
        } catch (LocalizedException $localizedException) {
            return;
        }

        if ($type === 'error') {
            $this->messageManager->addErrorMessage($message);

            return;
        }

        $this->messageManager->addSuccessMessage($message);
    }
}
