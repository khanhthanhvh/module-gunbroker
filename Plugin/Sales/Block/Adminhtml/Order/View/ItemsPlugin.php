<?php

/**
 * @noinspection PhpUnused
 * @noinspection PhpUnusedParameterInspection
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Sales\Block\Adminhtml\Order\View;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Block\Adminhtml\Order\View\Items;
use Magento\Sales\Model\ResourceModel\Order\Item\Collection as OrderItemCollection;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;

use function array_key_exists;
use function array_map;

class ItemsPlugin
{
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    private OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository;

    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderItemFeedbackRepository = $orderItemFeedbackRepository;
    }

    /**
     * @param OrderItemCollection<OrderItemInterface> $result
     * @return OrderItemCollection<OrderItemInterface>
     */
    public function afterGetItemsCollection(Items $subject, OrderItemCollection $result): OrderItemCollection
    {
        $orderItemIds = array_map(
            static fn(OrderItemInterface $orderItem): int => (int)$orderItem->getItemId(),
            $result->getItems()
        );
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('magento_order_item_id', $orderItemIds, 'in')
            ->create();
        $searchResults = $this->orderItemFeedbackRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return $result;
        }

        /** @var OrderItemFeedbackInterface[] $orderItemFeedbacks */
        $orderItemFeedbacks = $searchResults->getItems();
        $feedbackByOrderItem = [];

        foreach ($orderItemFeedbacks as $feedback) {
            $feedbackByOrderItem[$feedback->getMagentoOrderItemId()][] = $feedback;
        }

        /** @var OrderItemInterface $orderItem */
        foreach ($result->getItems() as $orderItem) {
            $itemId = $orderItem->getItemId();

            if ($itemId === null || !array_key_exists($itemId, $feedbackByOrderItem)) {
                continue;
            }

            $orderItem->getExtensionAttributes()
                ->setGunbrokerOrderItemFeedback($feedbackByOrderItem[$itemId]);
        }

        return $result;
    }
}
