<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Config\Model;

use Magento\Config\Model\Config;
use Reeds\GunBroker\Service\UserIdManagement;

class ConfigPlugin
{
    private UserIdManagement $userIdManagement;

    public function __construct(UserIdManagement $userIdManagement)
    {
        $this->userIdManagement = $userIdManagement;
    }

    public function aroundSave(Config $subject, callable $proceed): Config
    {
        if ($subject->getSection() !== 'gunbroker_api') {
            return $proceed();
        }

        return $this->userIdManagement->updateUserId($subject->getWebsite(), $proceed);
    }
}
