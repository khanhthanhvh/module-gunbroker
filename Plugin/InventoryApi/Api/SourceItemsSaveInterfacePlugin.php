<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\InventoryApi\Api;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\FlagManager;
use Magento\InventoryApi\Api\Data\SourceItemInterface;
use Magento\InventoryApi\Api\SourceItemsSaveInterface;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\BulkUpdateItemInventory;

use function array_filter;
use function array_map;
use function count;
use function implode;

/**
 * Plug-in for {@see SourceItemsSaveInterface}
 */
class SourceItemsSaveInterfacePlugin
{
    private StoreManagerInterface $storeManager;
    private ConfigInterface $config;
    private BulkUpdateItemInventory $bulkUpdateItemInventory;
    private FlagManager $flagManager;

    public function __construct(
        StoreManagerInterface $storeManager,
        ConfigInterface $config,
        FlagManager $flagManager,
        BulkUpdateItemInventory $bulkUpdateItemInventory
    ) {
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->bulkUpdateItemInventory = $bulkUpdateItemInventory;
        $this->flagManager = $flagManager;
    }

    /**
     * @param null $result
     * @param SourceItemInterface[] $sourceItems
     *
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterExecute(SourceItemsSaveInterface $subject, $result, array $sourceItems): void
    {
        if (count($sourceItems) === 0) {
            return;
        }

        try {
            $websiteId = (int)($this->storeManager->getStore()->getWebsiteId() ?? 0);
        } catch (NoSuchEntityException $e) {
            $websiteId = 0;
        }

        if (!$this->config->isEnabled($websiteId)) {
            return;
        }

        $productSkus = array_filter(
            array_map(static fn(SourceItemInterface $sourceItem): ?string => $sourceItem->getSku(), $sourceItems)
        );

        if (count($productSkus) === 0) {
            return;
        }

        $flagCode = "gunbroker_product_shipped_{$websiteId}_" . implode('_', $productSkus);

        if ($this->flagManager->getFlagData($flagCode)) {
            return;
        }

        $this->bulkUpdateItemInventory->execute($productSkus);
    }
}
