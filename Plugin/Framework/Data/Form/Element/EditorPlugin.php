<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Framework\Data\Form\Element;

use Magento\Framework\Data\Form\Element\Editor;
use Reeds\GunBroker\Service\BlockEditorDisabler;

class EditorPlugin
{
    private BlockEditorDisabler $blockEditorDisabler;

    public function __construct(BlockEditorDisabler $blockEditorDisabler)
    {
        $this->blockEditorDisabler = $blockEditorDisabler;
    }

    /** @noinspection PhpUnusedParameterInspection */
    public function afterIsEnabled(Editor $subject, bool $result): bool
    {
        return $this->blockEditorDisabler->disableForGunBrokerBlocks($result);
    }
}
