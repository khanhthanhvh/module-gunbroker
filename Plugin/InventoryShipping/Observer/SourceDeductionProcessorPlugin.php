<?php

/** @noinspection PhpUnusedParameterInspection */

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\InventoryShipping\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\FlagManager;
use Magento\InventoryShipping\Observer\SourceDeductionProcessor;
use Magento\Sales\Api\Data\ShipmentItemInterface;
use Magento\Sales\Model\Order\Shipment;

use function array_filter;
use function array_map;
use function implode;

class SourceDeductionProcessorPlugin
{
    private FlagManager $flagManager;
    private string $flagCode = 'gunbroker_product_shipped_';

    public function __construct(FlagManager $flagManager)
    {
        $this->flagManager = $flagManager;
    }

    /**
     * @return Observer[]
     */
    public function beforeExecute(SourceDeductionProcessor $subject, Observer $observer): array
    {
        /** @var Shipment $shipment */
        $shipment = $observer->getEvent()->getData('shipment');
        $productSkus = array_filter(
            array_map(
                static fn(ShipmentItemInterface $shipmentItem) => $shipmentItem->getSku(),
                $shipment->getItems()
            )
        );
        $this->flagCode .= implode('_', $productSkus);

        $this->flagManager->saveFlag($this->flagCode, true);

        return [$observer];
    }

    /**
     * @param null $result
     * @return null
     */
    public function afterExecute(SourceDeductionProcessor $subject, $result)
    {
        $this->flagManager->deleteFlag($this->flagCode);

        return $result;
    }
}
