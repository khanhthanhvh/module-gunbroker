<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\App\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Serialize\SerializerInterface;

use function is_array;

class ValuePlugin
{
    private SerializerInterface $serializer;
    private ScopeConfigInterface $config;

    public function __construct(SerializerInterface $serializer, ScopeConfigInterface $config)
    {
        $this->serializer = $serializer;
        $this->config = $config;
    }

    public function aroundIsValueChanged(Value $subject, callable $proceed): bool
    {
        if (
            $subject->getPath() !== 'gunbroker/attributes/payment_methods'
            && $subject->getPath() !== 'gunbroker/attributes/sales_taxes'
        ) {
            return $proceed();
        }

        $oldValue = $this->config->getValue(
            $subject->getPath(),
            $subject->getScope() ?: ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
            $subject->getScopeCode()
        );

        if (!is_array($oldValue)) {
            return $proceed();
        }

        $oldValue = $this->serializer->serialize($oldValue);

        return $oldValue !== $subject->getValue();
    }
}
