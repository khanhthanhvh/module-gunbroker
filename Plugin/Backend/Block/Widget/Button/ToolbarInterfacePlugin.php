<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Backend\Block\Widget\Button;

use Magento\Backend\Block\Widget\Button\ButtonList;
use Magento\Backend\Block\Widget\Button\ToolbarInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Sales\Api\Data\OrderInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;

/** @noinspection PhpUnused */
class ToolbarInterfacePlugin
{
    private OrderImportRepositoryInterface $orderImportRepository;
    private ConfigInterface $config;

    public function __construct(OrderImportRepositoryInterface $orderImportRepository, ConfigInterface $config)
    {
        $this->orderImportRepository = $orderImportRepository;
        $this->config = $config;
    }

    /**
     * @return array{AbstractBlock, ButtonList}
     *
     * @noinspection PhpUnused
     * @noinspection PhpUnusedParameterInspection
     */
    public function beforePushButtons(ToolbarInterface $subject, AbstractBlock $context, ButtonList $buttonList): array
    {
        $nameInLayout = $context->getNameInLayout();
        $order = $this->getOrder($nameInLayout, $context);

        if ($order === null || !$this->isGunBrokerOrder($order) || $order->getExtOrderId() === null) {
            return [$context, $buttonList];
        }

        $gunBrokerOrderUrl = $this->getGunBrokerOrderUrl($order->getExtOrderId());

        $buttonList->remove('send_notification');
        $buttonList->remove('order_hold');
        $buttonList->remove('order_reorder');
        $buttonList->remove('add');
        $buttonList->add(
            'view_gunbroker_order',
            [
                'label' => __('View order on GunBroker.com'),
                'class' => 'view-gunbroker-order',
                'id' => 'view-gunbroker-order-button',
                'onclick' => "window.open('$gunBrokerOrderUrl', '_blank') || setLocation('$gunBrokerOrderUrl')"
            ]
        );

        return [$context, $buttonList];
    }

    private function getOrder(string $nameInLayout, AbstractBlock $context): ?OrderInterface
    {
        $order = null;

        if ($nameInLayout === 'sales_order_edit') {
            $order = $context->getOrder();
        } elseif ($nameInLayout === 'sales_invoice_view') {
            $order = $context->getInvoice()->getOrder();
        } elseif ($nameInLayout === 'sales_shipment_view') {
            $order = $context->getShipment()->getOrder();
        }

        return $order;
    }

    private function isGunBrokerOrder(OrderInterface $order): bool
    {
        try {
            $this->orderImportRepository->getByGunBrokerOrderId((int)$order->getExtOrderId());
        } catch (LocalizedException | NoSuchEntityException $e) {
            return false;
        }

        return true;
    }

    private function getGunBrokerOrderUrl(string $gunBrokerOrderId): string
    {
        $gunBrokerOrderUrlPrefix = 'https://www.';

        if ($this->config->getApiEnvironment() === 'sandbox') {
            $gunBrokerOrderUrlPrefix .= 'sandbox.';
        }

        return $gunBrokerOrderUrlPrefix . 'gunbroker.com/order?orderid=' . $gunBrokerOrderId;
    }
}
