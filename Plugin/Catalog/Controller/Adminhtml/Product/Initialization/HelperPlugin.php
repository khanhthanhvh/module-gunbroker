<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Plugin\Catalog\Controller\Adminhtml\Product\Initialization;

use Laminas\Stdlib\ParametersInterface;
use Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\RequestInterface;

use function array_key_exists;
use function is_array;
use function is_numeric;

class HelperPlugin
{
    private RequestInterface $request;

    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @param mixed[] $productData
     * @return mixed[]
     *
     * @noinspection PhpUnusedParameterInspection
     * @noinspection AdditionOperationOnArraysInspection
     */
    public function beforeInitializeFromData(Helper $subject, Product $product, array $productData): array
    {
        /** @var ParametersInterface<mixed> $postParameters */
        $postParameters = $this->request->getPost();
        $useDefaults = (array)$postParameters->get('use_default', []);

        $useDefaults += $this->promoteUseDefaultsForAttribute($productData, 'gunbroker_premium_features');
        $useDefaults += $this->promoteUseDefaultsForAttribute($productData, 'gunbroker_shipping_class_costs');
        $useDefaults += $this->promoteUseDefaultsForAttribute($productData, 'gunbroker_shipping_classes_supported');

        $postParameters->set('use_default', $useDefaults);

        $this->resetAttributeValue($productData, $product, 'gunbroker_premium_features');
        $this->resetAttributeValue($productData, $product, 'gunbroker_shipping_class_costs');
        $this->resetAttributeValue($productData, $product, 'gunbroker_shipping_classes_supported');
        $this->normalizeAttributeValues($productData, 'gunbroker_premium_features');
        $this->normalizeAttributeValues($productData, 'gunbroker_shipping_class_costs');
        $this->normalizeAttributeValues($productData, 'gunbroker_shipping_classes_supported');

        return [$product, $productData];
    }

    /**
     * @param mixed[] $productData
     * @return mixed[]
     */
    private function promoteUseDefaultsForAttribute(array &$productData, string $attributeCode): array
    {
        $useDefaults = [];

        if (!array_key_exists("use_default_$attributeCode", $productData)) {
            return $useDefaults;
        }

        $useDefaults[$attributeCode] = $productData["use_default_$attributeCode"];

        unset($productData["use_default_$attributeCode"]);

        return $useDefaults;
    }

    /**
     * @param mixed[] $productData
     */
    private function resetAttributeValue(array &$productData, Product $product, string $attributeCode): void
    {
        if (
            !array_key_exists($attributeCode, $productData)
            && $product->hasData($attributeCode)
        ) {
            $productData[$attributeCode] = null;
        }
    }

    /**
     * @param mixed[] $productData
     */
    private function normalizeAttributeValues(array &$productData, string $attributeCode): void
    {
        if (!array_key_exists($attributeCode, $productData)) {
            return;
        }

        if (is_array($productData[$attributeCode])) {
            foreach ($productData[$attributeCode] as &$attributeData) {
                foreach ($attributeData as $key => $value) {
                    if ($value === 'true' || $value === 'false') {
                        $attributeData[$key] = $value === 'true';
                    }

                    if (is_numeric($value)) {
                        $attributeData[$key] += 0;
                    }
                }
            }
        } else {
            if ($productData[$attributeCode] === 'true' || $productData[$attributeCode] === 'false') {
                $productData[$attributeCode] = $productData[$attributeCode] === 'true';
            }

            if (is_numeric($productData[$attributeCode])) {
                $productData[$attributeCode] += 0;
            }
        }
    }
}
