<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Controller\Adminhtml\Apilog;

use Magento\Backend\Model\View\Result\RedirectFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\Result\PageFactory;

use function __;

class Index implements HttpGetActionInterface
{
    private AuthorizationInterface $authorization;
    private PageFactory $pageFactory;
    private RedirectFactory $redirectFactory;

    public function __construct(
        AuthorizationInterface $authorization,
        PageFactory $pageFactory,
        RedirectFactory $redirectFactory
    ) {
        $this->authorization = $authorization;
        $this->pageFactory = $pageFactory;
        $this->redirectFactory = $redirectFactory;
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        if (!$this->authorization->isAllowed('Reeds_GunBroker::admin_tools_apilog')) {
            return $this->redirectFactory->create()->setPath('admin/denied');
        }

        $page = $this->pageFactory->create();

        $page->setActiveMenu('Reeds_GunBroker::admin_tools_apilog');
        $page->getConfig()->getTitle()->prepend(__('API History Log'));

        return $page;
    }
}
