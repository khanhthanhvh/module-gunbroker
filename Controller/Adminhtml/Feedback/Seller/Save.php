<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Controller\Adminhtml\Feedback\Seller;

use Magento\Backend\App\AbstractAction;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Reeds\GunBroker\Service\FeedbackCreator;

use function __;

class Save extends AbstractAction implements HttpPostActionInterface
{
    public const ADMIN_RESOURCE = 'Reeds_GunBroker::admin_order_item_feedback_save';

    private FeedbackCreator $feedbackCreator;

    public function __construct(Context $context, FeedbackCreator $feedbackCreator)
    {
        parent::__construct($context);

        $this->feedbackCreator = $feedbackCreator;
    }

    /**
     * @inheritDoc
     */
    public function execute(): ResultInterface
    {
        [
            'magentoOrderItemId' => $magentoOrderItemId,
            'gunBrokerItemId' => $gunBrokerItemId,
            'comment' => $comment,
            'rating' => $rating
        ] = $this->_request->getParams();
        $websiteId = (int)$this->_request->getParam('website', 0);
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $isFeedbackCreated = $this->feedbackCreator->createFeedback(
            (int)$magentoOrderItemId,
            (int)$gunBrokerItemId,
            $comment,
            (int)$rating,
            $websiteId
        );

        $resultJson->setData(
            [
                'error' => !$isFeedbackCreated,
                'message' => $isFeedbackCreated
                    ? __('Successfully created seller feedback at GunBroker.com for item %1.', $gunBrokerItemId)
                    : __(
                        'Could not create seller feedback at GunBroker.com for item %1. See extension log for details.',
                        $gunBrokerItemId
                    )
            ]
        );

        return $resultJson;
    }
}
