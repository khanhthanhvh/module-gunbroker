<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Controller\Adminhtml\Apiuserid;

use Magento\Backend\App\AbstractAction;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Reeds\GunBroker\Service\UserIdManagement;

class Get extends AbstractAction implements HttpGetActionInterface
{
    public const ADMIN_RESOURCE = 'Reeds_GunBroker::config';

    private UserIdManagement $userIdManagement;

    public function __construct(Context $context, UserIdManagement $userIdManagement)
    {
        parent::__construct($context);

        $this->userIdManagement = $userIdManagement;
    }

    public function execute(): ResultInterface
    {
        $websiteId = (int)$this->_request->getParam('website', 0);
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        $resultJson->setData(
            [
                'userId' => $this->userIdManagement->retrieveAndStoreUserId($websiteId)
            ]
        );

        return $resultJson;
    }
}
