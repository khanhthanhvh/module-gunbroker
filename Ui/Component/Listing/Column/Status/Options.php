<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Ui\Component\Listing\Column\Status;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Phrase;

class Options implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @return array<int, array{value: string, label: Phrase}>
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => 'SUCCESS',
                'label' => __('SUCCESS')
            ],
            [
                'value' => 'FAILURE',
                'label' => __('FAILURE')
            ]
        ];
    }
}
