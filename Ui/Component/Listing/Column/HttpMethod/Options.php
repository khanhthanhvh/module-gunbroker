<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Ui\Component\Listing\Column\HttpMethod;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Phrase;

class Options implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @return array<int, array{value: string, label: Phrase}>
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => 'GET',
                'label' => __('GET')
            ],
            [
                'value' => 'POST',
                'label' => __('POST')
            ],
            [
                'value' => 'PUT',
                'label' => __('PUT')
            ],
            [
                'value' => 'DELETE',
                'label' => __('DELETE')
            ]
        ];
    }
}
