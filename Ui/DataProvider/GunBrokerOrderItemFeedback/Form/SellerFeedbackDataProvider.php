<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Ui\DataProvider\GunBrokerOrderItemFeedback\Form;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Reeds\GunBroker\Model\ResourceModel\OrderItemFeedback\CollectionFactory;

class SellerFeedbackDataProvider extends AbstractDataProvider
{
    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param mixed[] $meta
     * @param mixed[] $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

        $this->collection = $collectionFactory->create();
    }

    /**
     * @inheritDoc
     */
    public function getData(): array
    {
        return [];
    }
}
