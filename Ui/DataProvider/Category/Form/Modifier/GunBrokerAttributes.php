<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Ui\DataProvider\Category\Form\Modifier;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as EavAttribute;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Eav\Model\Entity\Attribute\Source\SpecificSourceInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Store\Model\Store;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\DataProvider\EavValidationRules;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Reeds\GunBroker\Api\ConfigInterface;

use function count;

class GunBrokerAttributes implements ModifierInterface
{
    private EavConfig $eavConfig;
    private Registry $registry;
    private RequestInterface $request;
    private CategoryRepositoryInterface $categoryRepository;
    private EavValidationRules $eavValidationRules;
    private ConfigInterface $config;

    public function __construct(
        EavConfig $eavConfig,
        Registry $registry,
        RequestInterface $request,
        CategoryRepositoryInterface $categoryRepository,
        EavValidationRules $eavValidationRules,
        ConfigInterface $config
    ) {
        $this->eavConfig = $eavConfig;
        $this->registry = $registry;
        $this->request = $request;
        $this->categoryRepository = $categoryRepository;
        $this->eavValidationRules = $eavValidationRules;
        $this->config = $config;
    }

    /**
     * @inheritDoc
     * @param mixed[] $data
     * @return mixed[]
     */
    public function modifyData(array $data): array
    {
        return $data;
    }

    /**
     * @inheritDoc
     * @param mixed[] $meta
     * @return mixed[]
     */
    public function modifyMeta(array $meta): array
    {
        try {
            $attributes = $this->eavConfig->getEntityType(Category::ENTITY)->getAttributeCollection();
        } catch (LocalizedException $e) {
            return $meta;
        }

        $attributes->addFieldToFilter('attribute_code', ['like' => '%gunbroker%']);

        if ($attributes->count() === 0) {
            return $meta;
        }

        $meta['gunbroker'] = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => 'fieldset',
                        'label' => 'GunBroker',
                        'visible' => $this->config->isEnabled()
                    ]
                ]
            ],
            'children' => []
        ];

        if (!$this->config->isEnabled()) {
            return $meta;
        }

        /** @var EavAttribute $attribute */
        foreach ($attributes as $attribute) {
            $code = $attribute->getAttributeCode();
            $meta['gunbroker']['children'][$code]['arguments']['data']['config'] = $this->getAttributeMeta(
                $attribute,
                $this->getCurrentCategory()
            );
        }

        return $meta;
    }

    private function getCurrentCategory(): ?CategoryInterface
    {
        $currentCategory = $this->registry->registry('category');

        if ($currentCategory !== null) {
            return $currentCategory;
        }

        $categoryId = $this->request->getParam('id');
        $storeId = $this->request->getParam('store', Store::DEFAULT_STORE_ID);

        if ($categoryId === null) {
            return null;
        }

        try {
            $category = $this->categoryRepository->get($categoryId, $storeId);
        } catch (NoSuchEntityException $e) {
            return null;
        }

        return $category;
    }

    /**
     * @return array<string, mixed>
     * @throws LocalizedException
     */
    private function getAttributeMeta(EavAttribute $attribute, ?CategoryInterface $currentCategory): array
    {
        $dataType = $attribute->getDataUsingMethod('frontend_input');
        $attributeMeta = [
            'componentType' => Field::NAME,
            'dataType' => $dataType,
            'visible' => (bool)$attribute->getDataUsingMethod('is_visible'),
            'required' => (bool)$attribute->getDataUsingMethod('is_required'),
            'label' => $attribute->getDataUsingMethod('frontend_label'),
            'sortOrder' => $attribute->getDataUsingMethod('sort_order'),
            'notice' => $attribute->getDataUsingMethod('note'),
            'default' => $attribute->getDataUsingMethod('default_value'),
            'size' => $attribute->getDataUsingMethod('multiline_count'),
            'scopeLabel' => $this->getScopeLabel($attribute)
        ];

        if ($dataType === 'text') {
            $attributeMeta['formElement'] = 'input';
        } elseif ($dataType === 'boolean') {
            $attributeMeta['formElement'] = 'checkbox';
        } else {
            $attributeMeta['formElement'] = $dataType;
        }

        if ($attribute->usesSource()) {
            $source = $attribute->getSource();

            if ($source instanceof SpecificSourceInterface && $currentCategory !== null) {
                $options = $source->getOptionsFor($currentCategory);
            } else {
                $options = $source->getAllOptions();
            }

            foreach ($options as &$option) {
                $option['__disableTmpl'] = true;
            }

            unset($option);

            $attributeMeta['options'] = $options;
        }

        $rules = $this->eavValidationRules->build($attribute, $attributeMeta);

        if (count($rules) > 0) {
            $attributeMeta['validation'] = $rules;
        }

        return $attributeMeta;
    }

    private function getScopeLabel(EavAttribute $attribute): string
    {
        $html = '';

        if ($attribute->getFrontendInput() === AttributeInterface::FRONTEND_INPUT) {
            return $html;
        }

        if ($attribute->isScopeGlobal()) {
            $html .= __('[GLOBAL]');
        } elseif ($attribute->isScopeWebsite()) {
            $html .= __('[WEBSITE]');
        } elseif ($attribute->isScopeStore()) {
            $html .= __('[STORE VIEW]');
        }

        return $html;
    }
}
