<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Attribute\ScopeOverriddenValue;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Ui\Component\Form\Element\DataType\Boolean;
use Magento\Ui\Component\Form\Element\DataType\Price;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Model\Attribute\Source\PremiumFeatures as PremiumFeaturesSource;
use Reeds\GunBroker\Model\Attribute\Source\ShippingClass as ShippingClassSource;

use function __;
use function array_key_exists;
use function count;

class GunBrokerAttributes implements ModifierInterface
{
    private LocatorInterface $locator;
    private ScopeOverriddenValue $scopeOverriddenValue;
    private ArrayManager $arrayManager;
    private ConfigInterface $config;
    private PremiumFeaturesSource $premiumFeaturesSource;
    private ShippingClassSource $shippingClassSource;

    public function __construct(
        LocatorInterface $locator,
        ScopeOverriddenValue $scopeOverriddenValue,
        ArrayManager $arrayManager,
        ConfigInterface $config,
        PremiumFeaturesSource $premiumFeaturesSource,
        ShippingClassSource $shippingClassSource
    ) {
        $this->locator = $locator;
        $this->scopeOverriddenValue = $scopeOverriddenValue;
        $this->arrayManager = $arrayManager;
        $this->config = $config;
        $this->premiumFeaturesSource = $premiumFeaturesSource;
        $this->shippingClassSource = $shippingClassSource;
    }

    /**
     * @inheritDoc
     * @param mixed[] $data
     * @return mixed[]
     */
    public function modifyData(array $data): array
    {
        return $data;
    }

    /**
     * @inheritDoc
     * @param mixed[] $meta
     * @return mixed[]
     */
    public function modifyMeta(array $meta): array
    {
        if (!array_key_exists('gunbroker', $meta)) {
            return $meta;
        }

        try {
            $websiteId = $this->locator->getStore()->getWebsiteId();
        } catch (NotFoundException $e) {
            $websiteId = null;
        }

        if ($websiteId !== null) {
            $websiteId = (int)$websiteId;
        }

        $meta['gunbroker']['arguments']['data']['config']['visible'] = $this->config->isEnabled($websiteId);
        $gunBrokerItemUrlPath = $this->arrayManager->findPath(
            'gunbroker_item_url',
            $meta,
            null,
            'children'
        );
        $premiumFeaturesPath = $this->arrayManager->findPath(
            'gunbroker_premium_features',
            $meta,
            null,
            'children'
        );
        $premiumFeaturesScheduledStartDatePath = $this->arrayManager->findPath(
            'gunbroker_premium_features_scheduled_start_date',
            $meta,
            null,
            'children'
        );
        $quantityPercentagePath = $this->arrayManager->findPath(
            'gunbroker_quantity_percentage',
            $meta,
            null,
            'children'
        );
        $shippingClassCostsPath = $this->arrayManager->findPath(
            'gunbroker_shipping_class_costs',
            $meta,
            null,
            'children'
        );
        $shippingClassesSupportedPath = $this->arrayManager->findPath(
            'gunbroker_shipping_classes_supported',
            $meta,
            null,
            'children'
        );

        $this->fixPriceAttributeMeta($meta);

        if ($gunBrokerItemUrlPath !== null) {
            $this->modifyGunBrokerItemUrlMeta($meta, $gunBrokerItemUrlPath);
        }

        if ($premiumFeaturesPath !== null) {
            $this->modifyPremiumFeaturesMeta($meta, $premiumFeaturesPath);
        }

        if ($premiumFeaturesScheduledStartDatePath !== null) {
            $this->modifyPremiumFeaturesScheduledStartDateMeta($meta, $premiumFeaturesScheduledStartDatePath);
        }

        if ($quantityPercentagePath !== null) {
            $this->modifyQuantityPercentageMeta($meta, $quantityPercentagePath);
        }

        if ($shippingClassCostsPath !== null) {
            $this->modifyShippingClassCostsMeta($meta, $shippingClassCostsPath);
        }

        if ($shippingClassesSupportedPath !== null) {
            $this->modifyShippingClassesSupportedMeta($meta, $shippingClassesSupportedPath);
        }

        return $meta;
    }

    /**
     * @param mixed[] $meta
     */
    private function fixPriceAttributeMeta(array &$meta): void
    {
        try {
            $product = $this->locator->getProduct();
        } catch (NotFoundException $e) {
            $product = null;
        }

        try {
            $store = $this->locator->getStore();
        } catch (NotFoundException $e) {
            $store = null;
        }

        foreach ($meta['gunbroker']['children'] as &$container) {
            foreach ($container['children'] as $code => &$attribute) {
                if ($attribute['arguments']['data']['config']['dataType'] !== 'price') {
                    continue;
                }

                $attribute['arguments']['data']['config']['scopeLabel'] = __('[STORE VIEW]');
                $attribute['arguments']['data']['config']['globalScope'] = false;

                // Add "Use Default Value" checkbox if in store scope
                if ($product !== null && $product->getId() !== null && $store !== null && (int)$store->getId() !== 0) {
                    $attribute['arguments']['data']['config']['service']['template'] = 'ui/form/element/helper/service';
                    /** @noinspection PhpParamsInspection */
                    $attribute['arguments']['data']['config']['disabled'] = !$this->scopeOverriddenValue->containsValue(
                        ProductInterface::class,
                        $product,
                        $code,
                        $store->getId()
                    );
                }
            }
        }
    }

    /**
     * @param mixed[] $meta
     */
    private function modifyGunBrokerItemUrlMeta(array &$meta, string $path): void
    {
        $containerMetaPath = $this->arrayManager->slicePath($path, 0, -2);
        $meta = $this->arrayManager->merge(
            $containerMetaPath . '/arguments/data/config',
            $meta,
            [
                'sortOrder' => '15',
            ]
        );
        $meta = $this->arrayManager->merge(
            $path . '/arguments/data/config',
            $meta,
            [
                'sortOrder' => '15',
                'elementTmpl' => 'ui/form/element/text'
            ]
        );
    }

    /**
     * @param mixed[] $meta
     */
    private function modifyPremiumFeaturesMeta(array &$meta, string $path): void
    {
        $childRowsMeta = [
            'feature' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Field::NAME,
                            'dataType' => Text::NAME,
                            'formElement' => Select::NAME,
                            'dataScope' => 'feature',
                            'label' => __('Premium Feature'),
                            'options' => $this->premiumFeaturesSource->getAllOptions(),
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 10
                        ]
                    ]
                ]
            ],
            'enabled' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Field::NAME,
                            'dataType' => Boolean::NAME,
                            'formElement' => Checkbox::NAME,
                            'dataScope' => 'enabled',
                            'prefer' => 'toggle',
                            'label' => __('Enabled'),
                            'valueMap' => [
                                'false' => false,
                                'true' => true
                            ],
                            'default' => 'false',
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 20
                        ]
                    ]
                ]
            ]
        ];
        $dynamicRowsMeta = $this->getDynamicRowsMeta(
            $childRowsMeta,
            (bool)$this->arrayManager->get($path . '/arguments/data/config/disabled', $meta)
        );
        $meta = $this->arrayManager->merge($path, $meta, $dynamicRowsMeta);
        $containerMetaPath = $this->arrayManager->slicePath($path, 0, -2);
        $useDefaultMeta = $this->getUseDefaultMeta('gunbroker_premium_features');

        if (count($useDefaultMeta) > 0) {
            $meta = $this->arrayManager->set(
                $containerMetaPath . '/children/use_default_premium_features',
                $meta,
                $useDefaultMeta
            );
        }
    }

    /**
     * @param mixed[] $meta
     */
    private function modifyPremiumFeaturesScheduledStartDateMeta(array &$meta, ?string $path): void
    {
        $meta = $this->arrayManager->merge(
            $path . '/arguments/data/config',
            $meta,
            [
                'additionalClasses' => 'admin__field-gunbroker-datetime'
            ]
        );
    }

    /**
     * @param mixed[] $meta
     */
    private function modifyQuantityPercentageMeta(array &$meta, ?string $path): void
    {
        $meta = $this->arrayManager->merge(
            $path . '/arguments/data/config',
            $meta,
            [
                'addafter' => '%',
                'additionalClasses' => 'admin__field-small',
                'notice' => __('Must be between 1 and 100.'),
                'validation' => [
                    'validate-digits-range' => '1-100'
                ]
            ]
        );
    }

    /**
     * @param mixed[] $meta
     */
    private function modifyShippingClassCostsMeta(array &$meta, string $path): void
    {
        $childRowsMeta = [
            'shipping_class' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Field::NAME,
                            'dataType' => Text::NAME,
                            'formElement' => Select::NAME,
                            'dataScope' => 'shipping_class',
                            'label' => __('Shipping Class'),
                            'options' => $this->shippingClassSource->getAllOptions(),
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 10
                        ]
                    ]
                ]
            ],
            'cost' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Field::NAME,
                            'dataType' => Price::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => 'cost',
                            'label' => __('Cost'),
                            'addbefore' => $this->locator->getStore()
                                ->getBaseCurrency()
                                ->getCurrencySymbol(),
                            'validation' => [
                                'required-entry' => true,
                                'validate-greater-than-zero' => true,
                                'validate-number' => true
                            ],
                            'sortOrder' => 20
                        ]
                    ]
                ]
            ]
        ];
        $dynamicRowsMeta = $this->getDynamicRowsMeta(
            $childRowsMeta,
            (bool)$this->arrayManager->get($path . '/arguments/data/config/disabled', $meta)
        );
        $meta = $this->arrayManager->merge($path, $meta, $dynamicRowsMeta);
        $containerMetaPath = $this->arrayManager->slicePath($path, 0, -2);
        $useDefaultMeta = $this->getUseDefaultMeta('gunbroker_shipping_class_costs');

        if (count($useDefaultMeta) > 0) {
            $meta = $this->arrayManager->set(
                $containerMetaPath . '/children/use_default_shipping_class_costs',
                $meta,
                $useDefaultMeta
            );
        }
    }

    /**
     * @param mixed[] $meta
     */
    private function modifyShippingClassesSupportedMeta(array &$meta, string $path): void
    {
        $childRowsMeta = [
            'shipping_class' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Field::NAME,
                            'dataType' => Text::NAME,
                            'formElement' => Select::NAME,
                            'dataScope' => 'shipping_class',
                            'label' => __('Shipping Class'),
                            'options' => $this->shippingClassSource->getAllOptions(),
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 10
                        ]
                    ]
                ]
            ],
            'supported' => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'componentType' => Field::NAME,
                            'dataType' => Boolean::NAME,
                            'formElement' => Checkbox::NAME,
                            'dataScope' => 'supported',
                            'prefer' => 'toggle',
                            'label' => __('Supported'),
                            'valueMap' => [
                                'false' => false,
                                'true' => true
                            ],
                            'default' => 'false',
                            'validation' => [
                                'required-entry' => true
                            ],
                            'sortOrder' => 20
                        ]
                    ]
                ]
            ]
        ];
        $dynamicRowsMeta = $this->getDynamicRowsMeta(
            $childRowsMeta,
            (bool)$this->arrayManager->get($path . '/arguments/data/config/disabled', $meta)
        );
        $meta = $this->arrayManager->merge($path, $meta, $dynamicRowsMeta);
        $containerMetaPath = $this->arrayManager->slicePath($path, 0, -2);
        $useDefaultMeta = $this->getUseDefaultMeta('gunbroker_shipping_classes_supported');

        if (count($useDefaultMeta) > 0) {
            $meta = $this->arrayManager->set(
                $containerMetaPath . '/children/use_default_shipping_classes_supported',
                $meta,
                $useDefaultMeta
            );
        }
    }

    /**
     * @param mixed[] $childFieldsMeta
     * @return mixed[]
     */
    private function getDynamicRowsMeta(array $childFieldsMeta, bool $disabled): array
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => DynamicRows::NAME,
                        'recordTemplate' => 'record',
                        'deleteProperty' => 'is_deleted',
                        'dataScope' => '',
                        'dndConfig' => [
                            'enabled' => false
                        ],
                        'disabled' => $disabled,
                        'additionalClasses' => 'admin__field-gunbroker-rows'
                    ]
                ]
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'dataScope' => ''
                            ],
                        ],
                    ],
                    'children' => $childFieldsMeta + [
                            'actionDelete' => [
                                'arguments' => [
                                    'data' => [
                                        'config' => [
                                            'componentType' => 'actionDelete',
                                            'dataType' => Text::NAME,
                                            'label' => '',
                                            'sortOrder' => 30
                                        ]
                                    ]
                                ]
                            ]
                        ]
                ]
            ]
        ];
    }

    /**
     * @return mixed[]
     */
    private function getUseDefaultMeta(string $attributeCode): array
    {
        try {
            $product = $this->locator->getProduct();
        } catch (NotFoundException $e) {
            $product = null;
        }

        try {
            $store = $this->locator->getStore();
        } catch (NotFoundException $e) {
            $store = null;
        }

        if ($product === null || $product->getId() === null || $store === null || (int)$store->getId() === 0) {
            return [];
        }

        /** @noinspection PhpParamsInspection */
        $hasDefault = !$this->scopeOverriddenValue->containsValue(
            ProductInterface::class,
            $product,
            'gunbroker_shipping_class_costs',
            $store->getId()
        );

        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'dataType' => Text::NAME,
                        'formElement' => Checkbox::NAME,
                        'dataScope' => "use_default_{$attributeCode}",
                        'description' => __('Use Default Value'),
                        'valueMap' => [
                            'false' => 0,
                            'true' => 1
                        ],
                        'value' => (int)$hasDefault,
                        'checked' => $hasDefault,
                        'exports' => [
                            'checked' => "\${\$.parentName}.{$attributeCode}:disabled",
                            '__disableTmpl' => ['checked' => false]
                        ],
                        'additionalClasses' => 'admin__field-gunbroker-use-default'
                    ]
                ]
            ]
        ];
    }
}
