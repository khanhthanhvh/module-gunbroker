<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Queue\Consumer\Handler;

use Reeds\GunBroker\Model\ListItemMessage;
use Reeds\GunBroker\Service\GunBrokerItemCreator;

class ListItem
{
    private GunBrokerItemCreator $gunBrokerItemCreator;

    public function __construct(GunBrokerItemCreator $gunBrokerItemCreator)
    {
        $this->gunBrokerItemCreator = $gunBrokerItemCreator;
    }

    public function handle(ListItemMessage $listItemMessage): void
    {
        $this->gunBrokerItemCreator->createItem($listItemMessage->getProductId(), $listItemMessage->getStoreId());
    }
}
