<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Queue\Consumer\Handler;

use Reeds\GunBroker\Api\Data\UpdateItemMessageInterface;
use Reeds\GunBroker\Service\ItemInventoryManagement;
use Reeds\GunBroker\Service\UpdateItemApiRequest;

class UpdateItem
{
    private UpdateItemApiRequest $updateItemApiRequest;
    private ItemInventoryManagement $itemInventoryManagement;

    public function __construct(
        UpdateItemApiRequest $updateItemApiRequest,
        ItemInventoryManagement $itemInventoryManagement
    ) {
        $this->updateItemApiRequest = $updateItemApiRequest;
        $this->itemInventoryManagement = $itemInventoryManagement;
    }

    public function handle(UpdateItemMessageInterface $updateItemMessage): void
    {
        $updatedItemData = $updateItemMessage->__toArray();
        $itemId = $updateItemMessage->getItemId();
        $websiteId = $updateItemMessage->getWebsiteId();

        unset($updatedItemData['itemId'], $updatedItemData['websiteId']);

        $this->updateItemApiRequest->setWebsiteId($websiteId)
            ->setItemId($itemId)
            ->setUpdatedItemData($updatedItemData)
            ->getResult();

        if ($this->updateItemApiRequest->getUpdatedItemQuantity() !== null) {
            $this->itemInventoryManagement->updateStatus($itemId);
        }
    }
}
