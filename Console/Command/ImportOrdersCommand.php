<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Reeds\GunBroker\Service\MagentoOrderCreator;
use Reeds\GunBroker\Service\MagentoOrderCreatorFactory;
use Reeds\GunBroker\Service\OrderImporter;
use Reeds\GunBroker\Service\OrderImporterFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Logger\ConsoleLoggerFactory;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Style\SymfonyStyleFactory;

use function __;
use function count;
use function sprintf;
use function ucfirst;

class ImportOrdersCommand extends Command
{
    public const COMMAND_NAME = 'gunbroker:orders:import';

    private State $state;
    private ConsoleLoggerFactory $consoleLoggerFactory;
    private OrderImporterFactory $orderImporterFactory;
    private SymfonyStyleFactory $symfonyStyleFactory;
    private MagentoOrderCreatorFactory $magentoOrderCreatorFactory;

    public function __construct(
        State $state,
        ConsoleLoggerFactory $consoleLoggerFactory,
        OrderImporterFactory $orderImporterFactory,
        SymfonyStyleFactory $symfonyStyleFactory,
        MagentoOrderCreatorFactory $magentoOrderCreatorFactory,
        string $name = null
    ) {
        parent::__construct($name);

        $this->state = $state;
        $this->consoleLoggerFactory = $consoleLoggerFactory;
        $this->orderImporterFactory = $orderImporterFactory;
        $this->symfonyStyleFactory = $symfonyStyleFactory;
        $this->magentoOrderCreatorFactory = $magentoOrderCreatorFactory;
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(__('Imports orders from GunBroker.com into Magento.'));
        $this->addOption(
            'website',
            'w',
            InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
            __('Identifier of the Web site to import orders for.')
        );
        $this->addOption(
            'time-frame',
            't',
            InputOption::VALUE_REQUIRED,
            __('Timeframe to return orders for (see https://api.sandbox.gunbroker.com/User/Help/OrdersSold).'),
            1
        );
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $this->state->setAreaCode(Area::AREA_GLOBAL);
        }

        $websiteIds = $input->getOption('website');
        $timeFrame = (int)$input->getOption('time-frame');
        /** @var SymfonyStyle $symfonyStyle */
        $symfonyStyle = $this->symfonyStyleFactory->create(['input' => $input, 'output' => $output]);
        /** @var ConsoleLogger $consoleLogger */
        $consoleLogger = $this->consoleLoggerFactory->create(['output' => $output]);
        /** @var MagentoOrderCreator $magentoOrderCreator */
        $magentoOrderCreator = $this->magentoOrderCreatorFactory->create(['logger' => $consoleLogger]);
        /** @var OrderImporter $orderImporter */
        $orderImporter = $this->orderImporterFactory->create(
            [
                'logger' => $consoleLogger,
                'magentoOrderCreator' => $magentoOrderCreator
            ]
        );

        $symfonyStyle->title(__('GunBroker.com Order Import'));
        $symfonyStyle->text(__('Importing Orders from GunBroker.com...'));

        $importResults = $orderImporter->importOrders($websiteIds, $timeFrame);

        if ($importResults === null) {
            $symfonyStyle->newLine();
            $symfonyStyle->writeln(sprintf('<comment>%s</>', __('No orders are available for import.')));

            return 0;
        }

        $symfonyStyle->text(__('Import complete.'));
        $symfonyStyle->newLine();

        $tableRows = [];

        foreach ($importResults as $status => $importResultsByStatus) {
            foreach ($importResultsByStatus as $gunBrokerOrderId => $magentoOrderId) {
                $colorTag = '<fg=green>%s</>';

                if ($status === 'failed') {
                    $colorTag = '<fg=red>%s</>';
                }

                $tableRows[] = [
                    $gunBrokerOrderId,
                    $magentoOrderId,
                    sprintf($colorTag, __(ucfirst($status)))
                ];
            }
        }

        $symfonyStyle->text(__('Import Results:'));
        $symfonyStyle->table(
            [
                __('GunBroker Order ID'),
                __('Magento Order ID'),
                __('Status')
            ],
            $tableRows
        );

        return count($importResults['failed']) === 0 ? 0 : 1;
    }
}
