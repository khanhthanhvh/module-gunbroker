<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Service\FeedbackImporter;
use Reeds\GunBroker\Service\FeedbackImporterFactory;
use Reeds\GunBroker\Service\FeedbackSearchApiRequest;
use Reeds\GunBroker\Service\FeedbackSearchApiRequestFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Logger\ConsoleLoggerFactory;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Style\SymfonyStyleFactory;

use function __;
use function array_filter;
use function array_map;
use function count;
use function implode;
use function sprintf;
use function ucfirst;

class ImportFeedbackCommand extends Command
{
    public const COMMAND_NAME = 'gunbroker:feedback:import';

    private State $state;
    private SymfonyStyleFactory $symfonyStyleFactory;
    private ConsoleLoggerFactory $consoleLoggerFactory;
    private FeedbackSearchApiRequestFactory $feedbackSearchApiRequestFactory;
    private FeedbackImporterFactory $feedbackImporterFactory;

    public function __construct(
        State $state,
        SymfonyStyleFactory $symfonyStyleFactory,
        ConsoleLoggerFactory $consoleLoggerFactory,
        FeedbackSearchApiRequestFactory $feedbackSearchApiRequestFactory,
        FeedbackImporterFactory $feedbackImporterFactory,
        string $name = null
    ) {
        parent::__construct($name);

        $this->state = $state;
        $this->symfonyStyleFactory = $symfonyStyleFactory;
        $this->consoleLoggerFactory = $consoleLoggerFactory;
        $this->feedbackSearchApiRequestFactory = $feedbackSearchApiRequestFactory;
        $this->feedbackImporterFactory = $feedbackImporterFactory;
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(__('Imports order item feedback from GunBroker.com into Magento.'));
        $this->addOption(
            'website',
            'w',
            InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
            __('Identifier of the website to import feedback for.')
        );
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $this->state->setAreaCode(Area::AREA_GLOBAL);
        }

        $websiteIds = $input->getOption('website');
        /** @var SymfonyStyle $symfonyStyle */
        $symfonyStyle = $this->symfonyStyleFactory->create(['input' => $input, 'output' => $output]);
        /** @var ConsoleLogger $consoleLogger */
        $consoleLogger = $this->consoleLoggerFactory->create(['output' => $output]);
        /** @var FeedbackSearchApiRequest $feedbackSearchApiRequest */
        $feedbackSearchApiRequest = $this->feedbackSearchApiRequestFactory->create(
            [
                'logger' => $consoleLogger
            ]
        );
        /** @var FeedbackImporter $feedbackImporter */
        $feedbackImporter = $this->feedbackImporterFactory->create(
            [
                'logger' => $consoleLogger,
                'feedbackSearchApiRequest' => $feedbackSearchApiRequest
            ]
        );

        $symfonyStyle->title(__('GunBroker.com Feedback Importer'));

        if (count($websiteIds) === 0) {
            $symfonyStyle->text(__('Importing feedback for order items in all websites...'));
        } else {
            $symfonyStyle->text(
                __('Importing feedback for order items in websites %1...', implode(', ', $websiteIds))
            );
        }

        $importedFeedback = array_filter(array_map('array_filter', $feedbackImporter->importInWebsites($websiteIds)));

        if (count($importedFeedback) === 0) {
            $symfonyStyle->newLine();

            $symfonyStyle->text(
                sprintf('<comment>%s</>', __('No order item feedback is available for import.'))
            );

            return 0;
        }

        $symfonyStyle->text(__('Import complete.'));
        $symfonyStyle->newLine();

        $tableRows = [];
        $failedImportCount = 0;

        foreach ($importedFeedback as $websiteId => $feedbackByWebsite) {
            foreach ($feedbackByWebsite as $status => $feedbackByStatus) {
                /** @var OrderItemFeedbackInterface $orderItemFeedback */
                foreach ($feedbackByStatus as $orderItemFeedback) {
                    $colorTag = '<fg=green>%s</>';

                    if ($status === 'failed') {
                        $colorTag = '<fg=red>%s</>';

                        $failedImportCount++;
                    }

                    $tableRows[] = [
                        $websiteId,
                        $orderItemFeedback->getGunbrokerItemId(),
                        $orderItemFeedback->getMagentoOrderItemId(),
                        sprintf($colorTag, __(ucfirst($status)))
                    ];
                }
            }
        }

        $symfonyStyle->text(__('Import Results:'));
        $symfonyStyle->table(
            [
                __('Website ID'),
                __('GunBroker Item ID'),
                __('Magento Item ID'),
                __('Status')
            ],
            $tableRows
        );

        return $failedImportCount === 0 ? 0 : 1;
    }
}
