<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Console\Command;

use Exception;
use InvalidArgumentException;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\GunBrokerItemCreatorFactory;
use Reeds\GunBroker\Service\ListItemApiRequestFactory;
use Reeds\GunBroker\Service\ListItemMessagePublisher;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Logger\ConsoleLoggerFactory;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Style\SymfonyStyleFactory;

use function __;

class ListItemCommand extends Command
{
    public const COMMAND_NAME = 'gunbroker:item:list';

    private State $state;
    private StoreManagerInterface $storeManager;
    private ConfigInterface $config;
    private SymfonyStyleFactory $symfonyStyleFactory;
    private ProductRepositoryInterface $productRepository;
    private ListItemMessagePublisher $listItemMessagePublisher;
    private ConsoleLoggerFactory $consoleLoggerFactory;
    private ListItemApiRequestFactory $listItemApiRequestFactory;
    private GunBrokerItemCreatorFactory $gunBrokerItemCreatorFactory;

    public function __construct(
        State $state,
        StoreManagerInterface $storeManager,
        ConfigInterface $config,
        SymfonyStyleFactory $symfonyStyleFactory,
        ProductRepositoryInterface $productRepository,
        ListItemMessagePublisher $listItemMessagePublisher,
        ConsoleLoggerFactory $consoleLoggerFactory,
        ListItemApiRequestFactory $listItemApiRequestFactory,
        GunBrokerItemCreatorFactory $gunBrokerItemCreatorFactory,
        string $name = null
    ) {
        parent::__construct($name);

        $this->state = $state;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->symfonyStyleFactory = $symfonyStyleFactory;
        $this->productRepository = $productRepository;
        $this->listItemMessagePublisher = $listItemMessagePublisher;
        $this->consoleLoggerFactory = $consoleLoggerFactory;
        $this->listItemApiRequestFactory = $listItemApiRequestFactory;
        $this->gunBrokerItemCreatorFactory = $gunBrokerItemCreatorFactory;
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription((string)__('Lists an item for auction on GunBroker.com.'));
        $this->addArgument(
            'product_id',
            InputArgument::REQUIRED,
            (string)__('Identifier of the product to create the listing from.')
        );
        $this->addOption(
            'queue',
            'Q',
            InputOption::VALUE_NONE,
            (string)__('Defer the listing the process by placing it in the queue.')
        );
        $this->addOption(
            'notify',
            'N',
            InputOption::VALUE_NONE,
            (string)__(
                'Send a confirmation e-mail after the item is successfully listed. (Ignored for queued listings.)'
            )
        );
        $this->addOption(
            'store',
            's',
            InputOption::VALUE_REQUIRED,
            (string)__('Identifier of the store to create the listing from.')
        );

        parent::configure();
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $this->state->setAreaCode(Area::AREA_FRONTEND);
        }

        $storeId = (int)$input->getOption('store');
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();

        if ($websiteId !== null) {
            /** @noinspection UnnecessaryCastingInspection */
            $websiteId = (int)$websiteId;
        }

        if (!$this->config->isEnabled($websiteId)) {
            throw new RuntimeException((string)__('The GunBroker extension is not enabled in the provided store.'));
        }

        $productId = (int)$input->getArgument('product_id');
        $useQueue = $input->getOption('queue');
        $notify = $input->getOption('notify');
        /** @var SymfonyStyle $symfonyStyle */
        $symfonyStyle = $this->symfonyStyleFactory->create(['input' => $input, 'output' => $output]);

        $symfonyStyle->title(__('GunBroker.com Item Creator'));

        try {
            $product = $this->productRepository->getById($productId, false, $storeId);
        } catch (NoSuchEntityException $e) {
            $symfonyStyle->error(__('Product with ID "%1" does not exist in store "%2."', $productId, $storeId));

            return 1;
        }

        if ($useQueue) {
            $this->listItemMessagePublisher->publishMessage($productId, $storeId);

            $symfonyStyle->success(__('The item has been queued for listing on GunBroker.com.'));

            return 0;
        }

        /** @var ConsoleLogger $consoleLogger */
        $consoleLogger = $this->consoleLoggerFactory->create(['output' => $output]);
        $listItemApiRequest = $this->listItemApiRequestFactory->create(
            [
                'logger' => $consoleLogger
            ]
        );
        $gunBrokerItemCreator = $this->gunBrokerItemCreatorFactory->create(
            [
                'logger' => $consoleLogger,
                'listItemApiRequest' => $listItemApiRequest
            ]
        );

        $symfonyStyle->text(__('Creating item from product "%1"...', $product->getSku()));

        try {
            $isListingSuccessful = $gunBrokerItemCreator->createItem($productId, $storeId, $notify);
        } catch (Exception $exception) {
            $isListingSuccessful = false;
        }

        if (!$isListingSuccessful) {
            $symfonyStyle->error(__('The item could not be created on GunBroker.com.'));

            return 1;
        }

        $symfonyStyle->success(__('The item has successfully been created on GunBroker.com.'));

        $gunBrokerItemId = $this->getGunBrokerItemId($productId);

        if ($gunBrokerItemId !== null) {
            $symfonyStyle->text(__('<info>GunBroker.com item identifier: %1</info>', $gunBrokerItemId));
        }

        return 0;
    }

    private function getGunBrokerItemId(int $productId): ?int
    {
        try {
            $product = $this->productRepository->getById($productId);
        } catch (NoSuchEntityException $e) {
            return null;
        }

        $gunbrokerItemIdAttribute = $product->getCustomAttribute('gunbroker_item_id');

        if ($gunbrokerItemIdAttribute === null) {
            return null;
        }

        return (int)$gunbrokerItemIdAttribute->getValue();
    }
}
