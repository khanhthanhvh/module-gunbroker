<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Reeds\GunBroker\Service\ItemIdUpdater;
use Reeds\GunBroker\Service\ItemIdUpdaterFactory;
use Reeds\GunBroker\Service\ItemsSellingApiRequest;
use Reeds\GunBroker\Service\ItemsSellingApiRequestFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Logger\ConsoleLoggerFactory;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Style\SymfonyStyleFactory;

use function __;
use function count;
use function implode;
use function sprintf;

class UpdateItemIdsCommand extends Command
{
    public const COMMAND_NAME = 'gunbroker:item-ids:update';

    private State $state;
    private SymfonyStyleFactory $symfonyStyleFactory;
    private ConsoleLoggerFactory $consoleLoggerFactory;
    private ItemsSellingApiRequestFactory $itemsSellingApiRequestFactory;
    private ItemIdUpdaterFactory $itemIdUpdaterFactory;

    public function __construct(
        State $state,
        SymfonyStyleFactory $symfonyStyleFactory,
        ConsoleLoggerFactory $consoleLoggerFactory,
        ItemsSellingApiRequestFactory $itemsSellingApiRequestFactory,
        ItemIdUpdaterFactory $itemIdUpdaterFactory,
        string $name = null
    ) {
        parent::__construct($name);

        $this->state = $state;
        $this->symfonyStyleFactory = $symfonyStyleFactory;
        $this->consoleLoggerFactory = $consoleLoggerFactory;
        $this->itemsSellingApiRequestFactory = $itemsSellingApiRequestFactory;
        $this->itemIdUpdaterFactory = $itemIdUpdaterFactory;
    }

    /**
     * @inheritDoc
     */
    protected function configure(): void
    {
        $this->setName(self::COMMAND_NAME);
        $this->setDescription(__('Updates GunBroker.com item identifiers in their corresponding Magento products.'));
        $this->addOption(
            'website',
            'w',
            InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY,
            __('Identifier of the website to update item identifiers for.')
        );
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $this->state->setAreaCode(Area::AREA_GLOBAL);
        }

        $websiteIds = $input->getOption('website');
        /** @var SymfonyStyle $symfonyStyle */
        $symfonyStyle = $this->symfonyStyleFactory->create(['input' => $input, 'output' => $output]);
        /** @var ConsoleLogger $consoleLogger */
        $consoleLogger = $this->consoleLoggerFactory->create(['output' => $output]);
        /** @var ItemsSellingApiRequest $itemsSellingApiRequest */
        $itemsSellingApiRequest = $this->itemsSellingApiRequestFactory->create(
            [
                'logger' => $consoleLogger
            ]
        );
        /** @var ItemIdUpdater $itemIdUpdater */
        $itemIdUpdater = $this->itemIdUpdaterFactory->create(
            [
                'logger' => $consoleLogger,
                'itemsSellingApiRequest' => $itemsSellingApiRequest
            ]
        );

        $symfonyStyle->title(__('GunBroker.com Item Identifier Updater'));

        if (count($websiteIds) === 0) {
            $symfonyStyle->text(__('Updating item identifiers for products in all websites...'));
        } else {
            $symfonyStyle->text(
                __('Updating item identifiers for products in websites %1...', implode(', ', $websiteIds))
            );
        }

        $updatedProducts = $itemIdUpdater->updateInWebsites($websiteIds);

        if (count($updatedProducts) === 0) {
            $symfonyStyle->newLine();
            $symfonyStyle->writeln(sprintf('<comment>%s</>', __('No new item identifiers are available for update.')));

            return 0;
        }

        $tableRows = [];

        foreach ($updatedProducts as $websiteId => $productIds) {
            foreach ($productIds as $itemId => $productId) {
                $tableRows[] = [
                    $websiteId,
                    $itemId,
                    $productId
                ];
            }
        }

        $symfonyStyle->text(__('Update complete.'));
        $symfonyStyle->success(__('Updated item identifiers for the following products:'));
        $symfonyStyle->table([__('Website ID'), __('GunBroker Item ID'), __('Magento Product ID')], $tableRows);

        return 0;
    }
}
