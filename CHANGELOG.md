# Changelog for Reeds GunBroker

All notable changes to this extension will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this extension adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

For more information about the extension, please refer to the
[README](./README.md) document.

## [Unreleased]

## [1.0.1] - 2021-12-20
### Removed
- Removed work-around for issues with HTTP Client discovery caused by Laminas
  Diactoros, as that library is no longer a dependency of Adobe Commerce. This
  will enable future compatibility with Adobe Commerce 2.4.4 which uses Guzzle
  ^7.0. 

## [1.0.0] - 2021-12-14
### Added
- Initial release of the extension

[Unreleased]: https://bitbucket.org/wagento-global/module-gunbroker/branches/compare/1.0.1...HEAD
[1.0.1]: https://bitbucket.org/wagento-global/module-gunbroker/src/1.0.1
[1.0.0]: https://bitbucket.org/wagento-global/module-gunbroker/src/1.0.0