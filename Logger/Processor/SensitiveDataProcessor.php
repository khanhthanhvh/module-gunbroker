<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Logger\Processor;

use Reeds\GunBroker\Util\Redactor;

use function array_merge;

class SensitiveDataProcessor
{
    /**
     * @var string[]
     */
    private array $sensitiveFields = ['password'];

    /**
     * @param string[] $sensitiveFields
     */
    public function __construct(array $sensitiveFields = [])
    {
        $this->sensitiveFields = array_merge($this->sensitiveFields, $sensitiveFields);
    }

    /**
     * @param mixed[] $record
     * @return mixed[]
     */
    public function __invoke(array $record): array
    {
        $record['context'] = Redactor::redact($record['context'], $this->sensitiveFields);
        $record['extra'] = Redactor::redact($record['extra'], $this->sensitiveFields);

        return $record;
    }
}
