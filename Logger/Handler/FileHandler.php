<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Logger\Handler;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem\DriverInterface;
use Magento\Framework\Logger\Handler\Base;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Api\ConfigInterface;

class FileHandler extends Base
{
    private StoreManagerInterface $storeManager;
    private ConfigInterface $config;

    public function __construct(
        DriverInterface $filesystem,
        StoreManagerInterface $storeManager,
        ConfigInterface $config,
        ?string $filePath = null,
        ?string $fileName = null
    ) {
        parent::__construct($filesystem, $filePath, $fileName);

        $this->storeManager = $storeManager;
        $this->config = $config;
    }

    /**
     * @inheritDoc
     * @param mixed[] $record
     */
    public function isHandling(array $record)
    {
        try {
            $websiteId = (int)$this->storeManager->getStore()->getWebsiteId();
        } catch (NoSuchEntityException $e) {
            $websiteId = null;
        }

        return !(!$this->config->isEnabled($websiteId) || !$this->config->isLoggingEnabled($websiteId));
    }
}
