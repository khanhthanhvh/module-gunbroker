<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Factory;

use Http\Client\Common\Plugin\HistoryPlugin;
use Magento\Framework\ObjectManagerInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Exceptions\ConfigurationException;
use Reeds\GunBroker\HTTPClient\Plugin\History\ApiLog;
use Wagento\GunBrokerApi\ApiFactory;
use Wagento\GunBrokerApi\ConnectorInterface;
use Wagento\GunBrokerApi\Exception\ConnectorException;
use Wagento\GunBrokerApi\HttpClient\Factory as HttpClientFactory;

use function __;
use function trim;

class GunBrokerConnectorFactory
{
    private LoggerInterface $logger;
    private ObjectManagerInterface $objectManager;
    private ConfigInterface $config;

    public function __construct(LoggerInterface $logger, ObjectManagerInterface $objectManager, ConfigInterface $config)
    {
        $this->logger = $logger;
        $this->objectManager = $objectManager;
        $this->config = $config;
    }

    /**
     * @throws ConfigurationException
     * @throws ConnectorException
     */
    public function create(?string $accessToken = null, ?int $websiteId = null): ConnectorInterface
    {
        $devKey = $this->config->getApiDevKey($websiteId);
        ['username' => $username, 'password' => $password] = $this->config->getApiCredentials($websiteId);

        if (trim($devKey) === '') {
            throw new ConfigurationException(
                __('The GunBroker.com API DevKey has not been configured. Please check your settings and try again.')
            );
        }

        if (trim($username) === '' || trim($password) === '') {
            throw new ConfigurationException(
                __('The GunBroker.com API credentials have not been configured. Please check your settings and try '
                    . 'again.')
            );
        }

        /** @var HttpClientFactory $httpClientFactory */
        $httpClientFactory = $this->objectManager->create(HttpClientFactory::class);
        $apiLogJournal = $this->objectManager->create(ApiLog::class);

        $apiLogJournal->setWebsiteId($websiteId);

        $httpClientFactory->addPlugin(
            $this->objectManager->create(HistoryPlugin::class, ['journal' => $apiLogJournal])
        );

        /** @var ApiFactory $apiFactory */
        $apiFactory = $this->objectManager->create(ApiFactory::class);
        /** @var ConnectorInterface $gunBrokerConnector */
        $gunBrokerConnector = $this->objectManager->create(
            ConnectorInterface::class,
            [
                'httpClientFactory' => $httpClientFactory,
                'apiFactory' => $apiFactory
            ]
        );

        try {
            $gunBrokerConnector->setApiEnvironment($this->config->getApiEnvironment($websiteId));
            $gunBrokerConnector->setApiDevKey($devKey);
        } catch (ConnectorException $e) {
            throw new ConfigurationException(__($e->getMessage()), $e);
        }

        $gunBrokerConnector->setLogger($this->logger);
        $gunBrokerConnector->configureAuthentication($username, $password, $accessToken);

        return $gunBrokerConnector;
    }
}
