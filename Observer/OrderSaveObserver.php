<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Reeds\GunBroker\Service\FeedbackAutomator;

/**
 * Observer for `sales_order_save_after` event
 *
 * @see \Magento\Sales\Model\Order
 */
class OrderSaveObserver implements ObserverInterface
{
    private FeedbackAutomator $feedbackAutomator;

    public function __construct(FeedbackAutomator $feedbackAutomator)
    {
        $this->feedbackAutomator = $feedbackAutomator;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer): void
    {
        /** @var OrderInterface|null $order */
        $order = $observer->getEvent()->getData('order');

        if ($order === null) {
            return;
        }

        $this->feedbackAutomator->automate($order);
    }
}
