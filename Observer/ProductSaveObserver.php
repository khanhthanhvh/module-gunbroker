<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\ListItemMessagePublisher;
use Reeds\GunBroker\Service\UpdateItemMessagePublisher;

/**
 * Observer for `catalog_product_save_after` event
 */
class ProductSaveObserver implements ObserverInterface
{
    private StoreManagerInterface $storeManager;
    private ConfigInterface $config;
    private ListItemMessagePublisher $listItemMessagePublisher;
    private UpdateItemMessagePublisher $updateItemMessagePublisher;

    public function __construct(
        StoreManagerInterface $storeManager,
        ConfigInterface $config,
        ListItemMessagePublisher $listItemMessagePublisher,
        UpdateItemMessagePublisher $updateItemMessagePublisher
    ) {
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->listItemMessagePublisher = $listItemMessagePublisher;
        $this->updateItemMessagePublisher = $updateItemMessagePublisher;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer): void
    {
        try {
            $store = $this->storeManager->getStore();
            $websiteId = (int)$store->getWebsiteId();
        } catch (NoSuchEntityException $e) {
            $store = null;
            $websiteId = null;
        }

        /** @var Product $product */
        $product = $observer->getData('product');

        if (
            !$this->config->isEnabled($websiteId)
            || $product->getExtensionAttributes()->getGunbrokerDoNotQueueItem()
            || !$this->isGunBrokerItem($product)
        ) {
            return;
        }

        if (!$this->isListedItem($product)) {
            $this->listItemMessagePublisher->publishMessage(
                (int)$product->getId(),
                $store !== null ? (int)$store->getId() : 0
            );

            return;
        }

        $this->updateItemMessagePublisher->publishMessage($product, [], $websiteId);
    }

    private function isGunBrokerItem(Product $product): bool
    {
        $isGunBrokerItemAttribute = $product->getCustomAttribute('is_gunbroker_item');

        if ($isGunBrokerItemAttribute === null) {
            return false;
        }

        return (bool)$isGunBrokerItemAttribute->getValue();
    }

    private function isListedItem(Product $product): bool
    {
        $gunBrokerItemIdAttribute = $product->getCustomAttribute('gunbroker_item_id');

        return !($gunBrokerItemIdAttribute === null || empty($gunBrokerItemIdAttribute->getValue()));
    }
}
