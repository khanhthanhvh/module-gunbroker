<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Observer;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Model\Order\Shipment;
use Reeds\GunBroker\Service\ShipmentExporter;
use RuntimeException;

use function __;

class ShipmentSaveObserver implements ObserverInterface
{
    private ShipmentExporter $shipmentExporter;
    private State $state;
    private ManagerInterface $messageManager;

    public function __construct(ShipmentExporter $shipmentExporter, State $state, ManagerInterface $messageManager)
    {
        $this->shipmentExporter = $shipmentExporter;
        $this->state = $state;
        $this->messageManager = $messageManager;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer): void
    {
        /** @var Shipment $shipment */
        $shipment = $observer->getEvent()->getShipment();

        try {
            $shipmentCreated = $this->shipmentExporter->exportShipment($shipment);
        } catch (RuntimeException $runtimeException) {
            $this->maybeAddMessage(
                (string)__(
                    'Could not export shipment details to GunBroker.com. Error: %1',
                    $runtimeException->getMessage()
                ),
                'error'
            );

            return;
        }

        if (!$shipmentCreated) {
            return;
        }

        $this->maybeAddMessage((string)__('Successfully exported shipment details to GunBroker.com.'), 'success');
    }

    private function maybeAddMessage(string $message, string $type): void
    {
        try {
            if ($this->state->getAreaCode() !== Area::AREA_ADMINHTML) {
                return;
            }
        } catch (LocalizedException $localizedException) {
            return;
        }

        if ($type === 'error') {
            $this->messageManager->addErrorMessage($message);

            return;
        }

        $this->messageManager->addSuccessMessage($message);
    }
}
