<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\RequestBuilder\ItemsUnsoldFactory as ItemsUnsoldRequestBuilderFactory;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemsUnsold as ItemsUnsoldResponse;

use function array_merge;
use function ceil;
use function count;

/**
 * @method ItemsUnsoldResponse|null makeApiRequest(int $pageSize = 1)
 */
class UnsoldItemsApiRequest extends PaginatedApiRequest
{
    protected const API_METHOD = 'getItemsUnsoldByUser';

    private ItemsUnsoldRequestBuilderFactory $itemsUnsoldRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        ItemsUnsoldRequestBuilderFactory $itemsUnsoldRequestBuilderFactory,
        int $apiResultLimit = 25
    ) {
        parent::__construct($connectorFactory, $logger, $apiResultLimit);

        $this->itemsUnsoldRequestBuilderFactory = $itemsUnsoldRequestBuilderFactory;
    }

    protected function instantiateRequestBuilder(): ?RequestBuilderInterface
    {
        return $this->itemsUnsoldRequestBuilderFactory->create();
    }
}
