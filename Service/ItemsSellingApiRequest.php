<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\RequestBuilder\ItemsSellingFactory as ItemsSellingRequestBuilderFactory;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;

class ItemsSellingApiRequest extends PaginatedApiRequest
{
    protected const API_METHOD = 'getActiveItems';

    protected array $requestParameters = [
        'sort' => 1 // Item ID Descending
    ];
    private ItemsSellingRequestBuilderFactory $itemsSellingRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        ItemsSellingRequestBuilderFactory $itemsSellingRequestBuilderFactory,
        int $apiResultLimit = 25
    ) {
        parent::__construct($connectorFactory, $logger, $apiResultLimit);

        $this->itemsSellingRequestBuilderFactory = $itemsSellingRequestBuilderFactory;
    }

    protected function instantiateRequestBuilder(): RequestBuilderInterface
    {
        return $this->itemsSellingRequestBuilderFactory->create();
    }
}
