<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Store\Api\Data\StoreInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\RequestBuilder\ItemFactory as ItemRequestBuilderFactory;
use RuntimeException;
use Wagento\GunBrokerApi\ApiObjects\Input\Item as ItemRequest;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

use function __;

/**
 * @method MessageResponse makeApiRequest()
 */
class ListItemApiRequest extends ApiRequest
{
    protected const API_METHOD = 'listItem';

    private ItemRequestBuilderFactory $itemRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        ItemRequestBuilderFactory $itemRequestBuilderFactory
    ) {
        parent::__construct($connectorFactory, $logger);

        $this->itemRequestBuilderFactory = $itemRequestBuilderFactory;
    }

    public function setProduct(ProductInterface $product): ListItemApiRequest
    {
        $this->requestParameters['product'] = $product;

        return $this;
    }

    public function setStore(StoreInterface $store): ListItemApiRequest
    {
        $this->requestParameters['store'] = $store;

        return $this;
    }

    public function getResult(): ?MessageResponse
    {
        try {
            /** @var MessageResponse $result */
            $result = parent::getResult();
        } catch (RuntimeException $exception) {
            $this->logger->error(
                __(
                    'Product with ID "%1" does not have enough inventory to be listed on GunBroker.com.',
                    $this->requestParameters['product']->getId()
                ),
                [
                    'quantity' => $this->request->quantity
                ]
            );

            return null;
        }

        return $result;
    }

    public function getListedItem(): ItemRequest
    {
        return $this->request;
    }

    protected function instantiateRequestBuilder(): ?RequestBuilderInterface
    {
        return $this->itemRequestBuilderFactory->create();
    }

    /**
     * @inheritDoc
     * @throws RuntimeException
     */
    protected function beforeSendRequest(array $arguments): void
    {
        $this->checkItemQuantity();
    }

    /**
     * @throws RuntimeException
     */
    private function checkItemQuantity(): void
    {
        if ($this->request->quantity > 0) {
            return;
        }

        throw new RuntimeException('Invalid product quantity');
    }
}
