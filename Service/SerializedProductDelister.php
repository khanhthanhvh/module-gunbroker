<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;

use function __;

class SerializedProductDelister
{
    private ConfigInterface $config;
    private DeleteItemListingApiRequest $deleteItemListingApiRequest;
    private LoggerInterface $logger;

    public function __construct(
        ConfigInterface $config,
        DeleteItemListingApiRequest $deleteItemListingApiRequest,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->deleteItemListingApiRequest = $deleteItemListingApiRequest;
        $this->logger = $logger;
    }

    public function delist(ProductInterface $product, int $websiteId, int $storeId): ?int
    {
        if (
            !$this->config->isEnabled($websiteId)
            || !$this->isGunBrokerItem($product)
            || !$this->isListedItem($product)
            || !$this->isSerializedFirearm($product)
        ) {
            return null;
        }

        /** @noinspection NullPointerExceptionInspection */
        $gunBrokerItemId = (int)$product->getCustomAttribute('gunbroker_item_id')->getValue();
        $isDelisted = $this->deleteItemListingApiRequest->execute($gunBrokerItemId, $websiteId);

        if (!$isDelisted) {
            return null;
        }

        $product->setCustomAttribute('gunbroker_item_id', null);

        try {
            $product->addAttributeUpdate('gunbroker_item_id', null, $storeId);
        } catch (Exception $e) {
            $this->logger->critical(
                __('Could not remove GunBroker item identifier from product. Error: %1', $e->getMessage()),
                [
                    'website_id' => $websiteId,
                    'product_id' => $product->getId(),
                    'product_sku' => $product->getSku(),
                    'gunbroker_item_id' => $gunBrokerItemId
                ]
            );
        }

        return $gunBrokerItemId;
    }

    private function isGunBrokerItem(ProductInterface $product): bool
    {
        $isGunBrokerItemAttribute = $product->getCustomAttribute('is_gunbroker_item');

        return $isGunBrokerItemAttribute !== null && $isGunBrokerItemAttribute->getValue();
    }

    private function isListedItem(ProductInterface $product): bool
    {
        $gunBrokerItemIdAttribute = $product->getCustomAttribute('gunbroker_item_id');

        return $gunBrokerItemIdAttribute !== null && !empty($gunBrokerItemIdAttribute->getValue());
    }

    private function isSerializedFirearm(ProductInterface $product): bool
    {
        $serialNumberAttribute = $product->getCustomAttribute('serial_number');

        return $serialNumberAttribute !== null && !empty($serialNumberAttribute->getValue());
    }
}
