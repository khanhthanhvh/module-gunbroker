<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Api\ConfigInterface;

use function array_filter;
use function count;

class ConfigLocator
{
    private StoreManagerInterface $storeManager;
    private ConfigInterface $config;
    /**
     * @var WebsiteInterface[]
     */
    private array $websites;

    public function __construct(StoreManagerInterface $storeManager, ConfigInterface $config)
    {
        $this->storeManager = $storeManager;
        $this->config = $config;
    }

    /**
     * @return WebsiteInterface[]
     */
    public function getConfiguredWebsites(): array
    {
        if (!isset($this->websites)) {
            $this->websites = $this->storeManager->getWebsites(true);
        }

        if (count($this->websites) === 0) {
            return [];
        }

       return array_filter(
            $this->websites,
            fn(WebsiteInterface $website): bool => $this->config->isEnabled((int)$website->getId())
                && $this->config->getApiDevKey((int)$website->getId()) !== ''
       );
    }
}
