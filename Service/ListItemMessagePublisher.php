<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\MessageQueue\PublisherInterface;
use Reeds\GunBroker\Api\Data\ListItemMessageInterface;
use Reeds\GunBroker\Api\Data\ListItemMessageInterfaceFactory;

class ListItemMessagePublisher
{
    private const TOPIC = 'gunbroker.item.list';

    private ListItemMessageInterfaceFactory $listItemMessageFactory;
    private PublisherInterface $publisher;

    public function __construct(ListItemMessageInterfaceFactory $listItemMessageFactory, PublisherInterface $publisher)
    {
        $this->listItemMessageFactory = $listItemMessageFactory;
        $this->publisher = $publisher;
    }

    public function publishMessage(int $productId, int $storeId = 0): void
    {
        /** @var ListItemMessageInterface $listItemMessage */
        $listItemMessage = $this->listItemMessageFactory->create();

        $listItemMessage->setProductId($productId);
        $listItemMessage->setStoreId($storeId);

        $this->publisher->publish(self::TOPIC, $listItemMessage);
    }
}
