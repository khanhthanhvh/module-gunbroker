<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Catalog\Model\Product;
use Magento\Framework\MessageQueue\PublisherInterface;
use Reeds\GunBroker\Api\Data\UpdateItemMessageInterface;
use Reeds\GunBroker\Api\Data\UpdateItemMessageInterfaceFactory;

use function array_merge;
use function count;
use function str_ends_with;

class UpdateItemMessagePublisher
{
    private const TOPIC = 'gunbroker.item.update';

    private UpdateItemMessageInterfaceFactory $updateItemMessageFactory;
    private PublisherInterface $publisher;

    public function __construct(
        UpdateItemMessageInterfaceFactory $updateItemMessageFactory,
        PublisherInterface $publisher
    ) {
        $this->updateItemMessageFactory = $updateItemMessageFactory;
        $this->publisher = $publisher;
    }

    /**
     * @param mixed[] $messageProperties
     */
    public function publishMessage(Product $product, array $messageProperties = [], ?int $websiteId = null): void
    {
        $messageProperties = array_merge($messageProperties, $this->getMessageProperties($product));
        $messagePropertiesWithoutItemId = $messageProperties;

        unset($messagePropertiesWithoutItemId['itemId']);

        if (count($messagePropertiesWithoutItemId) === 0) {
            return;
        }

        /** @var UpdateItemMessageInterface $updateItemMessage */
        $updateItemMessage = $this->updateItemMessageFactory->create(['data' => $messageProperties]);

        if ($websiteId !== null) {
            $updateItemMessage->setWebsiteId($websiteId);
        }

        $this->publisher->publish(self::TOPIC, $updateItemMessage);
    }

    /**
     * @return mixed[]
     */
    private function getMessageProperties(Product $product): array
    {
        $messageProperties = [];
        $attributeMap = [
            'gtin' => 'gtin',
            'gunbroker_auto_accept_price' => 'autoAcceptPrice',
            'gunbroker_auto_reject_price' => 'autoRejectPrice',
            'gunbroker_buy_now_price' => 'buyNowPrice',
            'gunbroker_can_offer' => 'canOffer',
            'gunbroker_fixed_price' => 'fixedPrice',
            'gunbroker_item_id' => 'itemId',
            'gunbroker_reserve_price' => 'reservePrice',
            'gunbroker_starting_bid' => 'startingBid',
            'gunbroker_premium_features_subtitle' => 'subTitle',
            'name' => 'title',
            'sku' => 'sku',
            'style' => 'mfgPartNumber',
            'upc_ean' => 'upc',
        ];

        foreach ($attributeMap as $attributeCode => $messageProperty) {
            if ($attributeCode !== 'gunbroker_item_id' && !$product->dataHasChangedFor($attributeCode)) {
                continue;
            }

            $attributeValue = $product->getData($attributeCode);

            if (
                str_ends_with($attributeCode, 'price')
                || $attributeCode === 'gunbroker_item_id'
                || $attributeCode === 'gunbroker_starting_bid'
            ) {
                $attributeValue += 0;
            }

            if ($attributeCode === 'gunbroker_can_offer') {
                $attributeValue = (bool)$attributeValue;
            }

            $messageProperties[$messageProperty] = $attributeValue;
        }

        return $messageProperties;
    }
}
