<?php

/** @noinspection PhpRedundantCatchClauseInspection */

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;
use RuntimeException;

class OrderCanceler
{
    private StoreManagerInterface $storeManager;
    private OrderRepositoryInterface $orderRepository;
    private OrderImportRepositoryInterface $orderImportRepository;
    private OrdersFlagsApiRequest $ordersFlagsApiRequest;

    public function __construct(
        StoreManagerInterface $storeManager,
        OrderRepositoryInterface $orderRepository,
        OrderImportRepositoryInterface $orderImportRepository,
        OrdersFlagsApiRequest $ordersFlagsApiRequest
    ) {
        $this->storeManager = $storeManager;
        $this->orderRepository = $orderRepository;
        $this->orderImportRepository = $orderImportRepository;
        $this->ordersFlagsApiRequest = $ordersFlagsApiRequest;
    }

    /**
     * @throws RuntimeException
     */
    public function cancelOrder(CreditmemoInterface $creditmemo): bool
    {
        try {
            $store = $this->storeManager->getStore($creditmemo->getStoreId());
            /** @noinspection PhpCastIsUnnecessaryInspection */
            $websiteId = (int)$store->getWebsiteId();
        } catch (NoSuchEntityException $noSuchEntityException) {
            $websiteId = 0;
        }

        try {
            $order = $this->orderRepository->get($creditmemo->getOrderId());
        } catch (InputException | NoSuchEntityException $exception) {
            return false;
        }

        $gunBrokerOrderId = $order->getExtOrderId();

        if ($gunBrokerOrderId === null || !$this->isGunBrokerOrder((int)$gunBrokerOrderId)) {
            return false;
        }

        if ($order->hasShipments()) {
            throw new RuntimeException(
                (string)__(
                    'Order #%1 has already been shipped and therefore cannot be canceled on GunBroker.com.',
                    $gunBrokerOrderId
                )
            );
        }

        $this->ordersFlagsApiRequest->setWebsiteId($websiteId)
            ->setGunBrokerOrderId((int)$gunBrokerOrderId)
            ->setFlagName('cancelOrder')
            ->getResult();

        return true;
    }

    private function isGunBrokerOrder(int $gunBrokerOrderId): bool
    {
        try {
            $this->orderImportRepository->getByGunBrokerOrderId($gunBrokerOrderId);
        } catch (LocalizedException | NoSuchEntityException $e) {
            return false;
        }

        return true;
    }
}
