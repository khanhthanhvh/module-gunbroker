<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\Exceptions\ConfigurationException;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;
use Wagento\GunBrokerApi\ConnectorInterface;
use Wagento\GunBrokerApi\Exception\ConnectorException;

use function __;
use function array_key_exists;
use function array_values;
use function method_exists;

abstract class ApiRequest
{
    protected const API_METHOD = self::API_METHOD;

    protected GunBrokerConnectorFactory $connectorFactory;
    protected LoggerInterface $logger;
    protected ConnectorInterface $connector;
    /**
     * @var mixed[]
     */
    protected array $queryParameters = [];
    /**
     * @var mixed[]
     */
    protected array $requestParameters = [];
    protected int $websiteId = 0;
    protected DataTransferObject $request;

    abstract protected function instantiateRequestBuilder(): ?RequestBuilderInterface;

    public function __construct(GunBrokerConnectorFactory $connectorFactory, LoggerInterface $logger)
    {
        $this->connectorFactory = $connectorFactory;
        $this->logger = $logger;
    }

    public function setWebsiteId(int $websiteId): ApiRequest
    {
        $this->websiteId = $websiteId;

        return $this;
    }

    public function getResult(): ?DataTransferObject
    {
        $connectorIsInstantiated = $this->instantiateConnector();

        if (!$connectorIsInstantiated) {
            return null;
        }

        return $this->makeApiRequest();
    }

    protected function instantiateConnector(): bool
    {
        try {
            $this->connector = $this->connectorFactory->create(null, $this->websiteId);
        } catch (ConfigurationException $configurationException) {
            $this->logger->critical(
                __('The GunBroker.com API is not configured properly. Error: %1', $configurationException->getMessage())
            );

            return false;
        } catch (ConnectorException $connectorException) {
            $this->logger->critical(
                __(
                    'An error occurred while instantiating the GunBroker.com API connector. Error: %1',
                    $connectorException->getMessage()
                )
            );

            return false;
        }

        return true;
    }

    /**
     * Perform an action prior to sending the API request
     *
     * @param mixed[] $arguments
     */
    protected function beforeSendRequest(array $arguments): void
    {
    }

    protected function makeApiRequest(): ?DataTransferObject
    {
        $requestBuilder = $this->instantiateRequestBuilder();
        $arguments = $this->queryParameters;

        try {
            if ($requestBuilder !== null) {
                $this->request = $requestBuilder->build($this->requestParameters);
                $arguments['request'] = $this->request;
            }

            $this->beforeSendRequest($arguments);

            $response = $this->connector->{static::API_METHOD}(...array_values($arguments));
        } catch (ConnectorException $connectorException) {
            $context = [
                'api_endpoint' => static::API_METHOD,
                'request_data' => array_key_exists('request', $arguments) ? $arguments['request']->all() : []
            ];

            if (method_exists($connectorException, 'getMessageResponse')) {
                /** @var MessageResponse $messageResponse */
                $messageResponse = $connectorException->getMessageResponse();

                if ($messageResponse !== null) {
                    $context['developer_message'] = $messageResponse->developerMessage;
                }
            }

            $this->logger->critical(
                __(
                    'An error occurred while making the API request to GunBroker.com. Error: %1',
                    $connectorException->getMessage()
                ),
                $context
            );

            return null;
        }

        return $response;
    }
}
