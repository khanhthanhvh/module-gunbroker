<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Exception;
use Magento\Catalog\Model\Product;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;

use function __;
use function preg_replace;
use function str_ends_with;

class ItemUrlUpdater
{
    private ConfigInterface $config;
    private LoggerInterface $logger;

    public function __construct(ConfigInterface $config, LoggerInterface $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    public function updateByProduct(Product $product, ?int $itemId): bool
    {
        $gunBrokerItemUrlAttribute = $product->getCustomAttribute('gunbroker_item_url');

        if (
            $itemId !== null
            && $gunBrokerItemUrlAttribute !== null
            && $gunBrokerItemUrlAttribute->getValue() !== null
            && str_ends_with($gunBrokerItemUrlAttribute->getValue(), (string)$itemId)
        ) {
            return false;
        }

        $newItemUrl = null;

        // Build a new item URL if one doesn't exist
        if ($gunBrokerItemUrlAttribute === null && $itemId !== null) {
            $newItemUrl = $this->buildItemUrl(
                $itemId,
                $product->getStore()->getWebsiteId() !== null ? (int)$product->getStore()->getWebsiteId() : null
            );
        }

        // Update the item URL if it exists
        if ($gunBrokerItemUrlAttribute !== null && $itemId !== null) {
            /** @var string|null $newItemUrl */
            $newItemUrl = preg_replace('/(\d+)$/', (string)$itemId, $gunBrokerItemUrlAttribute->getValue());
        }

        // If either of the two conditions above fail, we assume that the attribute is to be deleted.

        if ($newItemUrl === null) {
            // Fix attribute value is not deleted by `addAttributeUpdate()` if it is NULL
            $product->setCustomAttribute('gunbroker_item_url', null);
        }

        try {
            $product->addAttributeUpdate('gunbroker_item_url', $newItemUrl, $product->getStoreId());
        } catch (Exception $e) {
            $this->logger->critical(
                __(
                    'Could not update GunBroker item URL for product with ID %1. Error: %2',
                    $product->getId(),
                    $e->getMessage()
                ),
                [
                    'old_item_url' => $gunBrokerItemUrlAttribute->getValue(),
                    'new_item_url' => $newItemUrl
                ]
            );

            return false;
        }

        return true;
    }

    private function buildItemUrl(int $itemId, ?int $websiteId): string
    {
        $itemUrl = 'https://www.';

        if ($this->config->getApiEnvironment($websiteId) === 'sandbox') {
            $itemUrl .= 'sandbox.';
        }

        $itemUrl .= "gunbroker.com/item/$itemId";

        return $itemUrl;
    }
}
