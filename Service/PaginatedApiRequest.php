<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Generator;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Spatie\DataTransferObject\DataTransferObject;

use function array_merge;
use function ceil;
use function count;

abstract class PaginatedApiRequest extends ApiRequest
{
    protected int $apiResultLimit;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        int $apiResultLimit = 25
    ) {
        parent::__construct($connectorFactory, $logger);

        $this->apiResultLimit = $apiResultLimit;
    }

    public function getResults(int $currentPage = 1): Generator
    {
        $connectorIsInstantiated = $this->instantiateConnector();

        if (!$connectorIsInstantiated) {
            return;
        }

        do {
            $result = $this->makeApiRequest($currentPage);

            if ($result === null || count($result->results) === 0) {
                break;
            }

            $pages = ceil($result->count / $result->pageSize);

            yield $result->results;

            $currentPage++;
        } while ($currentPage <= $pages);
    }

    protected function makeApiRequest(int $page = 1): ?DataTransferObject
    {
        $this->requestParameters = array_merge(
            [
                'limit' => $this->apiResultLimit,
                'page' => $page
            ],
            $this->requestParameters
        );

        return parent::makeApiRequest();
    }
}
