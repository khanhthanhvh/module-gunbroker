<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Service\ListItemEmailSender\Asynchronous;
use Reeds\GunBroker\Service\ListItemEmailSender\Synchronous;
use Wagento\GunBrokerApi\ApiObjects\Input\Item;

class ListItemEmailSender
{
    private ConfigInterface $config;
    private Asynchronous $asynchronousSender;
    private Synchronous $synchronousSender;

    public function __construct(
        ConfigInterface $config,
        Asynchronous $asynchronousSender,
        Synchronous $synchronousSender
    ) {
        $this->config = $config;
        $this->asynchronousSender = $asynchronousSender;
        $this->synchronousSender = $synchronousSender;
    }

    public function sendEmail(
        int $storeId,
        int $itemId,
        Item $item,
        string $itemListingUrl,
        string $magentoProductUrl
    ): bool {
        if (!$this->config->sendItemListedConfirmationEmail($storeId)) {
            return false;
        }

        if ($this->config->getSendEmailsAsynchronously($storeId)) {
            return $this->asynchronousSender->queueEmail($storeId, $itemId, $item, $itemListingUrl, $magentoProductUrl);
        }

        return $this->synchronousSender->sendEmail($storeId, $item, $itemListingUrl, $magentoProductUrl);
    }
}
