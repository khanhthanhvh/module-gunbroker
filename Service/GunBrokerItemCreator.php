<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\Data\ItemInventoryInterface;

use function __;
use function filter_var;
use function is_numeric;
use function str_replace;

use const FILTER_SANITIZE_URL;

class GunBrokerItemCreator
{
    private Emulation $emulation;
    private StoreManagerInterface $storeManager;
    private ProductRepositoryInterface $productRepository;
    private LoggerInterface $logger;
    private ListItemApiRequest $listItemApiRequest;
    private ItemInventoryManagement $itemInventoryManagement;
    private ListItemEmailSender $listItemEmailSender;

    public function __construct(
        Emulation $emulation,
        StoreManagerInterface $storeManager,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger,
        ListItemApiRequest $listItemApiRequest,
        ItemInventoryManagement $itemInventoryManagement,
        ListItemEmailSender $listItemEmailSender
    ) {
        $this->emulation = $emulation;
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->listItemApiRequest = $listItemApiRequest;
        $this->itemInventoryManagement = $itemInventoryManagement;
        $this->listItemEmailSender = $listItemEmailSender;
    }

    public function createItem(int $magentoProductId, int $storeId, bool $notify = true): bool
    {
        $this->emulation->startEnvironmentEmulation($storeId);

        try {
            /** @var StoreInterface|Store $store */
            $store = $this->storeManager->getStore();
            $websiteId = (int)($store->getWebsiteId() ?? 0);
        } catch (NoSuchEntityException $noSuchEntityException) {
            $store = null;
            $websiteId = 0;
        }

        try {
            /** @var ProductInterface|Product $product */
            $product = $this->productRepository->getById($magentoProductId, false, $storeId);
        } catch (NoSuchEntityException $noSuchEntityException) {
            $this->logger->critical(__('Product with ID "%1" not found in Magento.', $magentoProductId));
            $this->emulation->stopEnvironmentEmulation();

            return false;
        }

        if (!$this->isGunBrokerItem($product)) {
            $this->logger->critical(__('Product with ID "%1" is not a GunBroker.com auction item.', $product->getId()));
            $this->emulation->stopEnvironmentEmulation();

            return false;
        }

        $gunbrokerItemId = $product->getCustomAttribute('gunbroker_item_id');

        if ($gunbrokerItemId !== null) {
            $this->logger->critical(
                __(
                    'Product with ID "%1" has already been listed on GunBroker.com. Item listing identifier: %2.',
                    $magentoProductId,
                    $gunbrokerItemId
                )
            );
            $this->emulation->stopEnvironmentEmulation();

            return false;
        }

        $result = $this->listItemApiRequest->setWebsiteId($websiteId)
            ->setProduct($product)
            ->setStore($store)
            ->getResult();
        $listedItem = $this->listItemApiRequest->getListedItem();

        if ($result === null || count($result->links) === 0 || !is_numeric($result->links[0]->title)) {
            $this->emulation->stopEnvironmentEmulation();

            return false;
        }

        $newItemId = (int)$result->links[0]->title;

        $this->itemInventoryManagement->saveQuantity(
            $product->getSku(),
            $newItemId,
            $listedItem->quantity,
            ItemInventoryInterface::STATUS_UPDATED
        );

        try {
            $product->addAttributeUpdate('gunbroker_item_id', $newItemId, $storeId);
        } catch (Exception $exception) {
            $this->logger->critical(
                __(
                    'Could not save GunBroker.com item ID for product with ID "%1". Error: %2',
                    $product->getId(),
                    $exception->getMessage()
                ),
                [
                    'gunbroker_item_id' => $newItemId
                ]
            );
        }

        $itemListingUrl = filter_var(
            str_replace(['api', '/v1', 'items'], ['www', '', 'item'], $result->links[0]->href),
            FILTER_SANITIZE_URL
        ) ?: '';

        try {
            $product->addAttributeUpdate('gunbroker_item_url', $itemListingUrl, $storeId);
        } catch (Exception $exception) {
            $this->logger->critical(
                __(
                    'Could not save GunBroker.com item URL for product with ID "%1". Error: %2',
                    $product->getId(),
                    $exception->getMessage()
                ),
                [
                    'gunbroker_item_url' => $itemListingUrl
                ]
            );
        }

        $this->logger->info(
            __(
                'Product with ID "%1" was successfully listed on GunBroker.com. Item ID: %2',
                $product->getId(),
                $result->links[0]->title
            )
        );

        if ($notify) {
            $this->listItemEmailSender->sendEmail(
                $storeId,
                (int)$result->links[0]->title,
                $listedItem,
                $itemListingUrl,
                $product->getProductUrl()
            );
        }

        $this->emulation->stopEnvironmentEmulation();

        return true;
    }

    private function isGunBrokerItem(ProductInterface $product): bool
    {
        $attribute = $product->getCustomAttribute('is_gunbroker_item');

        return $attribute !== null ? (bool)$attribute->getValue() : false;
    }
}
