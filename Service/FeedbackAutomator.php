<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderStatusHistoryRepositoryInterface;
use Magento\Sales\Model\Order;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;

use function __;
use function array_map;
use function count;
use function current;
use function implode;
use function in_array;

class FeedbackAutomator
{
    private ConfigInterface $config;
    private OrderImportRepositoryInterface $orderImportRepository;
    private FeedbackCreator $feedbackCreator;
    private OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository;
    private SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory;
    private OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository;

    public function __construct(
        ConfigInterface $config,
        OrderImportRepositoryInterface $orderImportRepository,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository,
        FeedbackCreator $feedbackCreator,
        OrderStatusHistoryRepositoryInterface $orderStatusHistoryRepository
    ) {
        $this->config = $config;
        $this->orderImportRepository = $orderImportRepository;
        $this->feedbackCreator = $feedbackCreator;
        $this->orderStatusHistoryRepository = $orderStatusHistoryRepository;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->orderItemFeedbackRepository = $orderItemFeedbackRepository;
    }

    /**
     * @return int[] Array of order item IDs for which feedback was created.
     */
    public function automate(OrderInterface $order): array
    {
        $itemIds = [];
        $store = $order->getStore();
        $websiteId = (int)$store->getWebsiteId();
        $storeId = (int)$store->getId();

        if (
            $order->getExtOrderId() === null
            || $order->getState() !== Order::STATE_COMPLETE
            || !$this->config->isEnabled($websiteId)
            || !$this->config->isFeedbackEnabled($storeId)
            || !$this->isGunBrokerOrder((int)$order->getExtOrderId())
        ) {
            return $itemIds;
        }

        $comment = $this->config->getDefaultFeedbackComment($storeId);
        $rating = $this->config->getDefaultFeedbackRating($storeId);
        $itemsWithFeedback = $this->getItemsWithFeedback($order);

        foreach ($order->getItems() as $orderItem) {
            if (
                $orderItem->getExtOrderItemId() === null
                || in_array($orderItem->getItemId(), $itemsWithFeedback, true)
            ) {
                continue;
            }

            $isFeedbackCreated = $this->feedbackCreator->createFeedback(
                (int)$orderItem->getItemId(),
                (int)$orderItem->getExtOrderItemId(),
                $comment,
                $rating,
                $websiteId
            );

            if ($isFeedbackCreated) {
                $itemIds[] = (int)$orderItem->getExtOrderItemId();
            }
        }

        if (count($itemIds) === 0) {
            return $itemIds;
        }

        if (count($itemIds) === 1) {
            $orderComment = __(
                'Feedback has automatically been sent to GunBroker for the following item: %1.',
                current($itemIds)
            );
        }

        if (count($itemIds) > 1) {
            $orderComment = __(
                'Feedback has automatically been sent to GunBroker for the following items: %1.',
                implode(', ', $itemIds)
            );
        }

        $statusHistory = $order->addCommentToStatusHistory($orderComment);

        try {
            $this->orderStatusHistoryRepository->save($statusHistory);
        } catch (CouldNotSaveException $e) {
            // no further action necessary
        }

        return $itemIds;
    }

    private function isGunBrokerOrder(int $gunBrokerOrderId): bool
    {
        try {
            $this->orderImportRepository->getByGunBrokerOrderId($gunBrokerOrderId);
        } catch (LocalizedException | NoSuchEntityException $e) {
            return false;
        }

        return true;
    }

    /**
     * @param OrderInterface $order
     * @return int[]
     */
    private function getItemsWithFeedback(OrderInterface $order): array
    {
        $itemsWithFeedback = [];
        $gunBrokerItemIds = array_map(
            static fn(OrderItemInterface $orderItem): int => (int)$orderItem->getExtOrderItemId(),
            $order->getItems()
        );

        if (count($gunBrokerItemIds) === 0) {
            return $itemsWithFeedback;
        }

        $searchCriteria = $this->searchCriteriaBuilderFactory->create()
            ->addFilter('type', 'seller')
            ->addFilter('gunbroker_item_id', $gunBrokerItemIds, 'in')
            ->create();
        $searchResults = $this->orderItemFeedbackRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return $itemsWithFeedback;
        }

        $itemsWithFeedback = array_map(
            static fn(OrderItemFeedbackInterface $feedback): int => $feedback->getMagentoOrderItemId(),
            $searchResults->getItems()
        );

        return $itemsWithFeedback;
    }
}
