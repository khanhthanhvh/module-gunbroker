<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\RequestBuilder\OrdersSold as OrdersSoldRequestBuilder;
use Reeds\GunBroker\RequestBuilder\OrdersSoldFactory as OrdersSoldRequestBuilderFactory;
use Wagento\GunBrokerApi\ApiObjects\Output\OrdersSold as OrdersSoldResponse;

/**
 * @method OrdersSoldResponse makeApiRequest(int $page = 1)
 */
class OrdersSoldApiRequest extends PaginatedApiRequest
{
    protected const API_METHOD = 'getSoldOrders';

    protected array $requestParameters = [
        'timeFrame' => 1
    ];
    private OrdersSoldRequestBuilderFactory $ordersSoldRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        OrdersSoldRequestBuilderFactory $ordersSoldRequestBuilderFactory,
        int $apiResultLimit = 25
    ) {
        parent::__construct($connectorFactory, $logger, $apiResultLimit);

        $this->ordersSoldRequestBuilderFactory = $ordersSoldRequestBuilderFactory;
    }

    public function setTimeFrame(int $timeFrame): OrdersSoldApiRequest
    {
        $this->requestParameters['timeFrame'] = $timeFrame;

        return $this;
    }

    protected function instantiateRequestBuilder(): OrdersSoldRequestBuilder
    {
        return $this->ordersSoldRequestBuilderFactory->create();
    }
}
