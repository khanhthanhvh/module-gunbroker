<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\RequestBuilder\OrdersShippingFactory as OrdersShippingRequestBuilderFactory;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

/**
 * @method MessageResponse makeApiRequest()
 * @method MessageResponse getResult()
 */
class OrdersShippingApiRequest extends ApiRequest
{
    protected const API_METHOD = 'updateOrderShippingDetails';

    private OrdersShippingRequestBuilderFactory $ordersShippingRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        OrdersShippingRequestBuilderFactory $ordersShippingRequestBuilderFactory
    ) {
        parent::__construct($connectorFactory, $logger);

        $this->ordersShippingRequestBuilderFactory = $ordersShippingRequestBuilderFactory;
    }

    public function setGunBrokerOrderId(int $gunBrokerOrderId): OrdersShippingApiRequest
    {
        $this->queryParameters['orderID'] = $gunBrokerOrderId;

        return $this;
    }

    public function setTrackingNumber(string $trackingNumber): OrdersShippingApiRequest
    {
        $this->requestParameters['trackingNumber'] = $trackingNumber;

        return $this;
    }

    public function setCarrier(int $carrier): OrdersShippingApiRequest
    {
        $this->requestParameters['carrier'] = $carrier;

        return $this;
    }

    protected function instantiateRequestBuilder(): ?RequestBuilderInterface
    {
        return $this->ordersShippingRequestBuilderFactory->create();
    }
}
