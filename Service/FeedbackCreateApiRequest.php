<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\RequestBuilder\FeedbackCreateFactory as FeedbackCreateRequestBuilderFactory;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;

class FeedbackCreateApiRequest extends ApiRequest
{
    protected const API_METHOD = 'postItemFeedback';

    private FeedbackCreateRequestBuilderFactory $feedbackCreateRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        FeedbackCreateRequestBuilderFactory $feedbackCreateRequestBuilderFactory
    ) {
        parent::__construct($connectorFactory, $logger);

        $this->feedbackCreateRequestBuilderFactory = $feedbackCreateRequestBuilderFactory;
    }

    /**
     * @param array{gunBrokerItemId: int, comment: string, rating: int} $feedbackData
     */
    public function setFeedbackData(array $feedbackData): void
    {
        $this->requestParameters = $feedbackData;
    }

    protected function instantiateRequestBuilder(): RequestBuilderInterface
    {
        return $this->feedbackCreateRequestBuilderFactory->create();
    }
}
