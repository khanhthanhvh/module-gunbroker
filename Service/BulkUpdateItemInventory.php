<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\StoreManagerInterface;

use function array_walk;
use function count;
use function is_numeric;

class BulkUpdateItemInventory
{
    private StoreManagerInterface $storeManager;
    private SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory;
    private ProductRepositoryInterface $productRepository;
    private ItemInventoryCalculator $itemInventoryCalculator;
    private ItemInventoryManagement $itemInventoryManagement;
    private UpdateItemMessagePublisher $updateItemMessagePublisher;

    public function __construct(
        StoreManagerInterface $storeManager,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        ProductRepositoryInterface $productRepository,
        ItemInventoryCalculator $itemInventoryCalculator,
        ItemInventoryManagement $itemInventoryManagement,
        UpdateItemMessagePublisher $updateItemMessagePublisher
    ) {
        $this->storeManager = $storeManager;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->productRepository = $productRepository;
        $this->itemInventoryCalculator = $itemInventoryCalculator;
        $this->itemInventoryManagement = $itemInventoryManagement;
        $this->updateItemMessagePublisher = $updateItemMessagePublisher;
    }

    /**
     * @param string[] $productSkus
     */
    public function execute(array $productSkus): void
    {
        $itemQuantities = $this->itemInventoryManagement->getItemQuantities($productSkus);

        if (count($itemQuantities) === 0) {
            return;
        }

        try {
            $store = $this->storeManager->getStore();

            if ($store->getId() == 0) {
                $store = $this->storeManager->getDefaultStoreView();
            }

            $websiteCode = $store !== null ? $store->getWebsite()->getCode() : 'base';
            $websiteId = $store !== null ? (int)$store->getWebsite()->getId() : 0;
        } catch (LocalizedException $localizedException) {
            return;
        }

        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();
        $searchCriteria = $searchCriteriaBuilder->addFilter('sku', $productSkus, 'in')->create();
        $searchResults = $this->productRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return;
        }

        $products = $searchResults->getItems();

        array_walk(
            $products,
            function (Product $product) use ($websiteCode, $websiteId, $itemQuantities): void {
                $itemId = $this->getItemId($product);

                if (!$this->isGunBrokerItem($product) || $itemId === null) {
                    return;
                }

                $quantity = $this->itemInventoryCalculator->calculate($websiteCode, $product);
                $savedQuantity = $itemQuantities[$itemId] ?? 0;

                if ($quantity === 0 || $quantity === $savedQuantity) {
                    return;
                }

                $this->itemInventoryManagement->saveQuantity($product->getSku(), $itemId, $quantity);
                $this->updateItemMessagePublisher->publishMessage($product, ['quantity' => $quantity], $websiteId);
            }
        );
    }

    private function getItemId(ProductInterface $product): ?int
    {
        $gunBrokerItemIdAttribute = $product->getCustomAttribute('gunbroker_item_id');

        if ($gunBrokerItemIdAttribute === null) {
            return null;
        }

        $gunBrokerItemId = $gunBrokerItemIdAttribute->getValue();

        return is_numeric($gunBrokerItemId) ? (int)$gunBrokerItemId : null;
    }

    private function isGunBrokerItem(ProductInterface $product): bool
    {
        $isGunBrokerItemAttribute = $product->getCustomAttribute('is_gunbroker_item');

        if ($isGunBrokerItemAttribute === null) {
            return false;
        }

        return (bool)$isGunBrokerItemAttribute->getValue();
    }
}
