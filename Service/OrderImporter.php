<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\WebsiteInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\Data\OrderImportInterface;
use Reeds\GunBroker\Api\Data\OrderImportInterfaceFactory;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;
use Wagento\GunBrokerApi\ApiObjects\Output\OrderSold;

use function array_filter;
use function array_key_exists;
use function array_map;
use function array_merge;
use function array_walk;
use function count;
use function in_array;
use function iterator_to_array;

class OrderImporter
{
    private ConfigLocator $configLocator;
    private ConfigInterface $config;
    private OrdersSoldApiRequest $ordersSoldApiRequest;
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    private OrderImportRepositoryInterface $orderImportRepository;
    private OrderImportInterfaceFactory $orderImportFactory;
    private MagentoOrderCreator $magentoOrderCreator;
    private LoggerInterface $logger;
    /**
     * @var OrderImportInterface[]
     */
    private array $orderImports = [];

    public function __construct(
        ConfigLocator $configLocator,
        ConfigInterface $config,
        OrdersSoldApiRequest $ordersSoldApiRequest,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderImportRepositoryInterface $orderImportRepository,
        OrderImportInterfaceFactory $orderImportFactory,
        MagentoOrderCreator $magentoOrderCreator,
        LoggerInterface $logger
    ) {
        $this->configLocator = $configLocator;
        $this->config = $config;
        $this->ordersSoldApiRequest = $ordersSoldApiRequest;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderImportRepository = $orderImportRepository;
        $this->orderImportFactory = $orderImportFactory;
        $this->magentoOrderCreator = $magentoOrderCreator;
        $this->logger = $logger;
    }

    /**
     * @param int[] $websiteIds
     * @param int $timeFrame
     * @return array{complete: array<int, ?int>, failed: array<int, ?int>}|null
     */
    public function importOrders(array $websiteIds = [], int $timeFrame = 1): ?array
    {
        $results = [
            'complete' => [],
            'failed' => []
        ];
        $ordersToImport = $this->getOrdersToImport($websiteIds, $timeFrame);

        if (count($ordersToImport) === 0) {
            return null;
        }

        foreach ($ordersToImport as $websiteId => $orders) {
            foreach ($orders as $order) {
                $this->recordImportStatus($order->orderID, 'processing');

                $magentoOrderId = $this->magentoOrderCreator->createOrder($order, $websiteId);
                $status = $magentoOrderId !== null ? 'complete' : 'failed';
                $results[$status][$order->orderID] = $magentoOrderId;

                $this->recordImportStatus($order->orderID, $status, $magentoOrderId);
            }
        }

        return $results;
    }

    /**
     * @param int[] $websiteIds
     * @return array<int, OrderSold[]>
     */
    private function getOrdersToImport(array $websiteIds, int $timeFrame): array
    {
        $ordersToImport = [];
        $checkIfEnabledInSite = true;

        if (count($websiteIds) === 0) {
            $websiteIds = array_map(
                static fn(WebsiteInterface $website): int => (int)$website->getId(),
                $this->configLocator->getConfiguredWebsites()
            );

            if (count($websiteIds) === 0) {
                return $ordersToImport;
            }

            $checkIfEnabledInSite = false;
        }

        array_walk(
            $websiteIds,
            function (int $websiteId) use (&$ordersToImport, $timeFrame, $checkIfEnabledInSite): void {
                if ($checkIfEnabledInSite && !$this->config->isEnabled($websiteId)) {
                    return;
                }

                $ordersSold = iterator_to_array(
                    $this->ordersSoldApiRequest->setWebsiteId($websiteId)
                        ->setTimeFrame($timeFrame)
                        ->getResults(),
                    false
                );

                if (count($ordersSold) === 0) {
                    return;
                }

                $ordersToImport[$websiteId] = array_merge([], ...$ordersSold);

                $this->filterOutImportedOrders($ordersToImport[$websiteId]);
            }
        );

        return array_filter($ordersToImport);
    }

    /**
     * @param OrderSold[] $ordersToImport
     */
    private function filterOutImportedOrders(array &$ordersToImport): void
    {
        $gunBrokerOrderIds = array_map(static fn(OrderSold $orderSold): int => $orderSold->orderID, $ordersToImport);
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('gunbroker_order_id', $gunBrokerOrderIds, 'in')
            ->addFilter('import_status', 'failed', 'neq')
            ->create();
        $orderImports = $this->orderImportRepository->getList($searchCriteria)->getItems();
        $importedOrderIds = array_map(
            static fn(OrderImportInterface $orderImport): int => $orderImport->getGunBrokerOrderId(),
            $orderImports
        );
        $ordersToImport = array_filter(
            $ordersToImport,
            static fn(OrderSold $orderSold): bool => !in_array($orderSold->orderID, $importedOrderIds)
        );
    }

    private function recordImportStatus(int $gunBrokerOrderId, string $importStatus, ?int $magentoOrderId = null): void
    {
        if (!array_key_exists($gunBrokerOrderId, $this->orderImports)) {
            /** @var OrderImportInterface $orderImport */
            $orderImport = $this->orderImportFactory->create();

            $orderImport->setGunBrokerOrderId($gunBrokerOrderId);

            $this->orderImports[$gunBrokerOrderId] = $orderImport;
        }

        $this->orderImports[$gunBrokerOrderId]->setImportStatus($importStatus);

        if ($magentoOrderId !== null) {
            $this->orderImports[$gunBrokerOrderId]->setMagentoOrderId($magentoOrderId);
        }

        try {
            $this->orderImports[$gunBrokerOrderId] = $this->orderImportRepository->save(
                $this->orderImports[$gunBrokerOrderId]
            );
        } catch (CouldNotSaveException | NoSuchEntityException $e) {
            $this->logger->critical(
                __(
                    'Could not record import status for GunBroker order %1. Error: %2',
                    $gunBrokerOrderId,
                    $e->getMessage()
                ),
                [
                    'gunbroker_order_id' => $gunBrokerOrderId,
                    'magento_order_id' => $magentoOrderId,
                    'import_status' => $importStatus
                ]
            );
        }
    }
}
