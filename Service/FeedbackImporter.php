<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderItemRepositoryInterface as MagentoOrderItemRepositoryInterface;
use Magento\Store\Api\Data\WebsiteInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterfaceFactory;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;
use Reeds\GunBroker\Model\OrderItemFeedback;
use Wagento\GunBrokerApi\ApiObjects\Output\FeedbackSearchResult;

use function __;
use function array_key_exists;
use function array_map;
use function array_walk;
use function in_array;

class FeedbackImporter
{
    private ConfigLocator $configLocator;
    private ConfigInterface $config;
    private FeedbackSearchApiRequest $feedbackSearchApiRequest;
    private SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory;
    private OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository;
    private MagentoOrderItemRepositoryInterface $magentoOrderItemRepository;
    private OrderItemFeedbackInterfaceFactory $orderItemFeedbackFactory;
    private LoggerInterface $logger;
    /**
     * @var string[]
     */
    private array $sellerUserName = [];

    public function __construct(
        ConfigLocator $configLocator,
        ConfigInterface $config,
        FeedbackSearchApiRequest $feedbackSearchApiRequest,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository,
        MagentoOrderItemRepositoryInterface $magentoOrderItemRepository,
        OrderItemFeedbackInterfaceFactory $orderItemFeedbackFactory,
        LoggerInterface $logger
    ) {
        $this->configLocator = $configLocator;
        $this->config = $config;
        $this->feedbackSearchApiRequest = $feedbackSearchApiRequest;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->orderItemFeedbackRepository = $orderItemFeedbackRepository;
        $this->magentoOrderItemRepository = $magentoOrderItemRepository;
        $this->orderItemFeedbackFactory = $orderItemFeedbackFactory;
        $this->logger = $logger;
    }

    /**
     * @return array<string, OrderItemFeedbackInterface[]>
     */
    public function importByWebsite(int $websiteId): array
    {
        $importedFeedback = [
            'succeeded' => [],
            'failed' => [],
        ];

        if (!$this->config->isEnabled($websiteId)) {
            return $importedFeedback;
        }

        if (!array_key_exists($websiteId, $this->sellerUserName)) {
            $this->sellerUserName[$websiteId] = $this->config->getApiCredentials($websiteId)['username'];
        }

        $feedbackResults = $this->feedbackSearchApiRequest->setWebsiteId($websiteId)->getResults();

        /** @var FeedbackSearchResult[] $feedbackResultPage */
        foreach ($feedbackResults as $feedbackResultPage) {
            $itemIds = array_map(
                static fn(FeedbackSearchResult $feedbackSearchResult): int => $feedbackSearchResult->itemID,
                $feedbackResultPage
            );
            $importedFeedbackSearchCriteria = $this->searchCriteriaBuilderFactory->create()
                ->addFilter('gunbroker_item_id', $itemIds, 'in')
                ->create();
            $importedFeedbackSearchResults = $this->orderItemFeedbackRepository->getList(
                $importedFeedbackSearchCriteria
            );
            $importedFeedbackIds = array_map(
                static fn(OrderItemFeedbackInterface $orderItemFeedback): int
                    => $orderItemFeedback->getGunbrokerFeedbackId(),
                $importedFeedbackSearchResults->getItems()
            );
            $magentoOrderItemIds = $this->getMagentoOrderItemIds($itemIds);

            foreach ($feedbackResultPage as $feedbackResult) {
                if (
                    in_array($feedbackResult->feedbackID, $importedFeedbackIds, true)
                    || !array_key_exists($feedbackResult->itemID, $magentoOrderItemIds)
                ) {
                    continue;
                }

                [$orderItemFeedback, $status] = $this->createFeedback(
                    $feedbackResult,
                    $magentoOrderItemIds[$feedbackResult->itemID],
                    $websiteId
                );
                $importedFeedback[$status][] = $orderItemFeedback;
            }
        }

        return $importedFeedback;
    }

    /**
     * @param int[] $websiteIds
     * @return array<int, array<string, OrderItemFeedbackInterface[]>>
     */
    public function importInWebsites(array $websiteIds = []): array
    {
        $importedFeedback = [];

        if (count($websiteIds) === 0) {
            $websiteIds = array_map(
                static fn(WebsiteInterface $website): int => (int)$website->getId(),
                $this->configLocator->getConfiguredWebsites()
            );
        }

        if (count($websiteIds) === 0) {
            return $importedFeedback;
        }

        array_walk(
            $websiteIds,
            function (int $websiteId) use (&$importedFeedback) {
                $importedFeedback[$websiteId] = $this->importByWebsite($websiteId);
            }
        );

        return $importedFeedback;
    }

    /**
     * @param int[] $gunBrokerItemIds
     * @return array<int, int>
     */
    private function getMagentoOrderItemIds(array $gunBrokerItemIds): array
    {
        $magentoOrderItemIds = [];
        $searchCriteria = $this->searchCriteriaBuilderFactory->create()
            ->addFilter('ext_order_item_id', $gunBrokerItemIds, 'in')
            ->create();
        $searchResults = $this->magentoOrderItemRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return $magentoOrderItemIds;
        }

        foreach ($searchResults->getItems() as $orderItem) {
            $magentoOrderItemIds[(int)$orderItem->getExtOrderItemId()] = (int)$orderItem->getItemId();
        }

        return $magentoOrderItemIds;
    }

    /**
     * @return array{OrderItemFeedbackInterface, string}
     */
    private function createFeedback(
        FeedbackSearchResult $feedbackResult,
        int $magentoOrderItemId,
        int $websiteId
    ): array {
        /** @var OrderItemFeedbackInterface|OrderItemFeedback $orderItemFeedback */
        $orderItemFeedback = $this->orderItemFeedbackFactory->create();
        $feedbackType = $this->sellerUserName[$websiteId] === $feedbackResult->toUser->username ? 'buyer' : 'seller';

        $orderItemFeedback->setType($feedbackType);
        $orderItemFeedback->setGunbrokerFeedbackId($feedbackResult->feedbackID);
        $orderItemFeedback->setGunbrokerItemId($feedbackResult->itemID);
        $orderItemFeedback->setMagentoOrderItemId($magentoOrderItemId);
        $orderItemFeedback->setRatingLetter($feedbackResult->ratingLetter);
        $orderItemFeedback->setRatingScore((int)$feedbackResult->ratingScore);
        $orderItemFeedback->setComment($feedbackResult->comment);
        $orderItemFeedback->setCommentDate($feedbackResult->date);
        $orderItemFeedback->setCommentator($feedbackResult->fromUser->username);

        try {
            $this->orderItemFeedbackRepository->save($orderItemFeedback);

            $status = 'succeeded';
        } catch (InputException | CouldNotSaveException | NoSuchEntityException $e) {
            $this->logger->critical(
                __(
                    'Could not save feedback for GunBroker order item %1. Error: %2',
                    $feedbackResult->itemID,
                    $e->getMessage()
                ),
                ['feedback_data' => $orderItemFeedback->getData()]
            );

            $status = 'failed';
        }

        return [$orderItemFeedback, $status];
    }
}
