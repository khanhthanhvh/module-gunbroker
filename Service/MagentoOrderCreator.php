<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\GroupManagement;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DB\Transaction;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\Data\OrderAddressInterfaceFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderInterfaceFactory;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\Data\OrderItemInterfaceFactory;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Api\Data\OrderPaymentInterfaceFactory;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Model\ResourceModel\Product\Collection as ProductCollection;
use Reeds\GunBroker\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Wagento\GunBrokerApi\ApiObjects\OrderItem;
use Wagento\GunBrokerApi\ApiObjects\OrderItem as GunBrokerOrderItem;
use Wagento\GunBrokerApi\ApiObjects\Output\OrderSold;

use function __;
use function array_filter;
use function array_map;
use function array_reverse;
use function count;
use function explode;
use function implode;
use function in_array;
use function preg_match;
use function preg_replace;
use function reset;
use function round;
use function str_contains;
use function trim;

class MagentoOrderCreator
{
    private ProductCollectionFactory $productCollectionFactory;
    private LoggerInterface $logger;
    private OrderItemInterfaceFactory $orderItemFactory;
    private OrderInterfaceFactory $orderFactory;
    private OrderAddressInterfaceFactory $orderAddressFactory;
    private CustomerRepositoryInterface $customerRepository;
    private StoreManagerInterface $storeManager;
    private ScopeConfigInterface $scopeConfig;
    private RegionFactory $regionFactory;
    private OrderPaymentInterfaceFactory $orderPaymentFactory;
    private InvoiceService $invoiceService;
    private OrderManagementInterface $orderManagement;
    private TransactionFactory $databaseTransactionFactory;

    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        LoggerInterface $logger,
        OrderItemInterfaceFactory $orderItemFactory,
        OrderInterfaceFactory $orderFactory,
        OrderAddressInterfaceFactory $orderAddressFactory,
        CustomerRepositoryInterface $customerRepository,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        RegionFactory $regionFactory,
        OrderPaymentInterfaceFactory $orderPaymentFactory,
        InvoiceService $invoiceService,
        OrderManagementInterface $orderManagement,
        TransactionFactory $databaseTransactionFactory
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->logger = $logger;
        $this->orderItemFactory = $orderItemFactory;
        $this->orderFactory = $orderFactory;
        $this->orderAddressFactory = $orderAddressFactory;
        $this->customerRepository = $customerRepository;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->regionFactory = $regionFactory;
        $this->orderPaymentFactory = $orderPaymentFactory;
        $this->invoiceService = $invoiceService;
        $this->orderManagement = $orderManagement;
        $this->databaseTransactionFactory = $databaseTransactionFactory;
    }

    public function createOrder(OrderSold $gunBrokerOrder, int $websiteId): ?int
    {
        $orderItemSkus = array_map(
            static fn(OrderItem $orderItem): string => $orderItem->sku,
            $gunBrokerOrder->orderItemsCollection
        );
        $products = $this->getProducts($orderItemSkus, $websiteId);

        if (count($orderItemSkus) !== count($products)) {
            $this->logger->critical(
                __(
                    'Could not import order %1 from GunBroker.com. One or more items do not exist in Magento.',
                    $gunBrokerOrder->orderID
                ),
                ['missing_product_skus' => $this->getMissingProductSkus($orderItemSkus, $products)]
            );

            return null;
        }

        $totalQuantityOrdered = 0;
        $totalWeight = 0;
        $subTotal = 0;
        $discountPerItem = $gunBrokerOrder->couponValue > 0
            ? round($gunBrokerOrder->couponValue / count($gunBrokerOrder->orderItemsCollection), 2)
            : 0;

        foreach ($gunBrokerOrder->orderItemsCollection as $orderItem) {
            $totalQuantityOrdered += $orderItem->quantity;
            $totalWeight += $orderItem->weight * $orderItem->quantity;
            $subTotal += $orderItem->itemSubTotal;

            if ($discountPerItem === 0) {
                continue;
            }

            $itemPriceWithDiscount = $orderItem->itemSubTotal - $discountPerItem;
            $itemTaxWithDiscount = round($itemPriceWithDiscount * ($orderItem->salesTaxRate / 100), 2);

            if ($orderItem->salesTax !== $itemTaxWithDiscount) {
                $orderItem->salesTax = $itemTaxWithDiscount;
            }
        }

        /** @var Order $order */
        $order = $this->orderFactory->create();
        $product = reset($products);
        $storeId = (int)$product->getStoreId();
        $orderItems = $this->getOrderItems($products, $gunBrokerOrder->orderItemsCollection, $discountPerItem);
        [$shippingMethod, $shippingDescription] = $this->getShippingDetails($product);

        try {
            $customer = $this->customerRepository->get($gunBrokerOrder->billToEmail, $websiteId);
            $customerId = (int)$customer->getId();
        } catch (NoSuchEntityException | LocalizedException $e) {
            $customer = null;
            $customerId = null;
        }

        if ($storeId === 0) {
            $storeId = (int)$this->storeManager->getStore()->getId();
        }

        $order->setIncrementId(implode('-', $gunBrokerOrder->itemIDs));
        $order->setStoreId($storeId);
        $order->setIsVirtual(0);
        $order->setExtOrderId((string)$gunBrokerOrder->orderID);
        $order->setItems($orderItems);
        $order->setBaseTotalQtyOrdered($totalQuantityOrdered);
        $order->setTotalQtyOrdered($totalQuantityOrdered);
        $order->setBaseCurrencyCode('USD');
        $order->setOrderCurrencyCode('USD');
        $order->setGlobalCurrencyCode('USD');
        $order->setStoreCurrencyCode('USD');
        $order->setBaseSubtotal($subTotal);
        $order->setSubtotal($subTotal);
        $order->setBaseSubtotalInclTax($subTotal + $gunBrokerOrder->salesTaxTotal);
        $order->setSubtotalInclTax($subTotal + $gunBrokerOrder->salesTaxTotal);
        $order->setBaseTaxAmount($gunBrokerOrder->salesTaxTotal);
        $order->setTaxAmount($gunBrokerOrder->salesTaxTotal);
        $order->setBaseShippingAmount($gunBrokerOrder->shipCost);
        $order->setShippingAmount($gunBrokerOrder->shipCost);
        $order->setBaseShippingTaxAmount(0);
        $order->setShippingTaxAmount(0);
        $order->setBaseShippingInclTax($gunBrokerOrder->shipCost);
        $order->setShippingInclTax($gunBrokerOrder->shipCost);
        $order->setShippingMethod($shippingMethod);
        $order->setShippingDescription($shippingDescription);
        $order->setBaseDiscountAmount($gunBrokerOrder->couponValue);
        $order->setDiscountAmount($gunBrokerOrder->couponValue);
        $order->setDiscountDescription($gunBrokerOrder->couponCode ?? '');
        $order->setBaseShippingDiscountAmount(0);
        $order->setShippingDiscountAmount(0);
        $order->setBaseGrandTotal($gunBrokerOrder->totalPrice);
        $order->setGrandTotal($gunBrokerOrder->totalPrice);
        $order->setBaseTotalDue($gunBrokerOrder->totalPrice);
        $order->setTotalDue($gunBrokerOrder->totalPrice);
        $order->setBaseToGlobalRate(1);
        $order->setBaseToOrderRate(1);
        $order->setStoreToBaseRate(0);
        $order->setStoreToOrderRate(0);
        $order->setBillingAddress($this->createOrderBillingAddress($gunBrokerOrder, $customerId));
        $order->setShippingAddress($this->createOrderShippingAddress($gunBrokerOrder, $customerId));
        $order->setWeight($totalWeight);
        $order->setCustomerIsGuest($customer === null ? 1 : 0);
        $order->setCustomerEmail($gunBrokerOrder->billToEmail);
        $order->setExtCustomerId((string)$gunBrokerOrder->buyer->userID);

        if ($customer !== null) {
            $order->setCustomerId($customerId);
            $order->setCustomerGroupId($customer->getGroupId() ?? 0);
            $order->setCustomerPrefix($customer->getPrefix() ?? '');
            $order->setCustomerFirstname($customer->getFirstname() ?? '');
            $order->setCustomerMiddlename($customer->getMiddlename() ?? '');
            $order->setCustomerLastname($customer->getLastname() ?? '');
            $order->setCustomerSuffix($customer->getSuffix() ?? '');
        } else {
            [$firstName, $lastName] = $this->parseName($gunBrokerOrder->billToName);

            $order->setCustomerGroupId(GroupManagement::NOT_LOGGED_IN_ID);
            $order->setCustomerFirstname($firstName);
            $order->setCustomerLastname($lastName);
        }

        $orderPayment = $this->createOrderPayment($gunBrokerOrder);

        $order->setPayment($orderPayment);

        try {
            /** @var OrderInterface|Order $order */
            $order = $this->orderManagement->place($order);
        } catch (Exception $e) {
            $this->logger->critical(
                __(
                    'Could not import order %1 from GunBroker.com. Error: %2',
                    $gunBrokerOrder->orderID,
                    $e->getMessage()
                )
            );

            return null;
        }

        $invoice = $this->createInvoice($order);

        $order->addCommentToStatusHistory(
            __('GunBroker.com order #%1 imported into Magento successfully.', $gunBrokerOrder->orderID)
        );

        try {
            $this->saveOrderAndInvoice($order, $invoice);
        } catch (Exception $e) {
            $this->logger->critical(
                __(
                    'Could not save order and invoice for GunBroker.com order %1. Error: %2',
                    $gunBrokerOrder->orderID,
                    $e->getMessage()
                ),
                [
                    'magento_order_id' => $order->getId()
                ]
            );
        }

        return $order->getId() !== null ? (int)$order->getId() : null;
    }

    /**
     * @param string[] $orderItemSkus
     * @return Product[]
     */
    private function getProducts(array $orderItemSkus, int $websiteId): array
    {
        /** @var ProductCollection<Product> $productCollection */
        $productCollection = $this->productCollectionFactory->create();

        if ($websiteId !== 0) {
            $productCollection->addWebsiteFilter($websiteId)
                ->joinStoreTable();
        }

        $productCollection->addAttributeToSelect('gunbroker_item_id', 'left')
            ->addAttributeToSelect('is_handgun')
            ->addAttributeToFilter('sku', ['in' => $orderItemSkus])
            ->addAttributeToFilter('is_gunbroker_item', ['eq' => 1])
            ->load();

        /** @var Product[] $products */
        $products = $productCollection->getItems();

        return $products;
    }

    /**
     * @param string[] $orderItemSkus
     * @param Product[] $products
     * @return string[]
     */
    private function getMissingProductSkus(array $orderItemSkus, array $products): array
    {
        return array_filter(
            array_map(
                static fn(Product $product): ?string =>
                    !in_array($product->getSku(), $orderItemSkus) ? $product->getSku() : null,
                $products
            )
        );
    }

    /**
     * @param Product[] $products
     * @param GunBrokerOrderItem[] $gunBrokerOrderItems
     * @return OrderItemInterface[]
     */
    private function getOrderItems(array $products, array $gunBrokerOrderItems, float $discountAmount): array
    {
        return array_map(
            function (Product $product) use ($gunBrokerOrderItems, $discountAmount): OrderItemInterface {
                $gunBrokerOrderItemsFiltered = array_filter(
                    $gunBrokerOrderItems,
                    static fn(GunBrokerOrderItem $gunBrokerOrderItem): bool =>
                        $product->getSku() === $gunBrokerOrderItem->sku
                );

                return $this->createOrderItem($product, reset($gunBrokerOrderItemsFiltered), $discountAmount);
            },
            $products
        );
    }

    private function createOrderItem(
        Product $product,
        GunBrokerOrderItem $gunBrokerOrderItem,
        float $discountAmount
    ): OrderItemInterface {
        /** @var OrderItemInterface $orderItem */
        $orderItem = $this->orderItemFactory->create();
        $taxPerUnit = $gunBrokerOrderItem->salesTax > 0
            ? round($gunBrokerOrderItem->salesTax / $gunBrokerOrderItem->quantity, 2)
            : 0;

        $orderItem->setIsVirtual(0);
        $orderItem->setProductId($product->getId());
        $orderItem->setProductType($product->getTypeId());
        $orderItem->setQtyOrdered((float)$gunBrokerOrderItem->quantity);
        $orderItem->setBaseOriginalPrice($gunBrokerOrderItem->itemPrice);
        $orderItem->setOriginalPrice($gunBrokerOrderItem->itemPrice);
        $orderItem->setBasePrice($gunBrokerOrderItem->itemPrice);
        $orderItem->setPrice($gunBrokerOrderItem->itemPrice);
        $orderItem->setBasePriceInclTax($gunBrokerOrderItem->itemPrice + $taxPerUnit);
        $orderItem->setPriceInclTax($gunBrokerOrderItem->itemPrice + $taxPerUnit);
        $orderItem->setBaseTaxAmount($gunBrokerOrderItem->salesTax);
        $orderItem->setTaxAmount($gunBrokerOrderItem->salesTax);
        $orderItem->setTaxPercent($gunBrokerOrderItem->salesTaxRate);
        $orderItem->setBaseDiscountAmount($discountAmount);
        $orderItem->setDiscountAmount($discountAmount);
        $orderItem->setBaseRowTotal($gunBrokerOrderItem->itemSubTotal);
        $orderItem->setRowTotal($gunBrokerOrderItem->itemSubTotal);
        $orderItem->setBaseRowTotalInclTax($gunBrokerOrderItem->itemSubTotal + $gunBrokerOrderItem->salesTax);
        $orderItem->setRowTotalInclTax($gunBrokerOrderItem->itemSubTotal + $gunBrokerOrderItem->salesTax);
        $orderItem->setName($gunBrokerOrderItem->title);
        $orderItem->setSku($gunBrokerOrderItem->sku);
        $orderItem->setWeight($gunBrokerOrderItem->weight);
        $orderItem->setRowWeight($gunBrokerOrderItem->weight * $gunBrokerOrderItem->quantity);
        $orderItem->setExtOrderItemId((string)$gunBrokerOrderItem->itemID);

        return $orderItem;
    }

    /**
     * @return string[]
     */
    private function getShippingDetails(Product $product): array
    {
        $isHandgunAttribute = $product->getCustomAttribute('is_handgun');
        $isHandgun = $isHandgunAttribute !== null && $isHandgunAttribute->getValue();
        $configKey = $isHandgun ? 'handgun' : 'non_handgun';

        return [
            $this->scopeConfig->getValue(
                "gunbroker/shipping/method_$configKey",
                ScopeInterface::SCOPE_STORE,
                $product->getStoreId()
            ),
            $this->scopeConfig->getValue(
                "gunbroker/shipping/description_$configKey",
                ScopeInterface::SCOPE_STORE,
                $product->getStoreId()
            )
        ];
    }

    private function createOrderBillingAddress(
        OrderSold $gunBrokerOrder,
        ?int $customerId = null
    ): OrderAddressInterface {
        /** @var OrderAddressInterface $orderAddress */
        $orderAddress = $this->orderAddressFactory->create();
        [$firstName, $lastName] = $this->parseName($gunBrokerOrder->billToName);
        $region = $this->regionFactory->create()->loadByCode($gunBrokerOrder->billToState, 'US');

        $orderAddress->setAddressType('billing');
        $orderAddress->setEmail($gunBrokerOrder->billToEmail);
        $orderAddress->setTelephone($this->sanitizePhoneNumber($gunBrokerOrder->billToPhone));
        $orderAddress->setFirstname($firstName);
        $orderAddress->setLastname($lastName);
        $orderAddress->setStreet([$gunBrokerOrder->billToAddress1, $gunBrokerOrder->billToAddress2]);
        $orderAddress->setCity($gunBrokerOrder->billToCity);
        $orderAddress->setPostcode($gunBrokerOrder->billToPostalCode);
        $orderAddress->setRegion($region->getName());
        $orderAddress->setRegionCode($region->getCode());
        $orderAddress->setRegionId($region->getRegionId());
        $orderAddress->setCountryId($gunBrokerOrder->billToCountryCode);

        if ($customerId !== null) {
            $orderAddress->setCustomerId($customerId);
        }

        return $orderAddress;
    }

    private function createOrderShippingAddress(
        OrderSold $gunBrokerOrder,
        ?int $customerId = null
    ): OrderAddressInterface {
        /** @var OrderAddressInterface $orderAddress */
        $orderAddress = $this->orderAddressFactory->create();
        [$firstName, $lastName] = $this->parseName($gunBrokerOrder->shipToName);
        $region = $this->regionFactory->create()->loadByCode($gunBrokerOrder->shipToState, 'US');

        $orderAddress->setAddressType('shipping');
        $orderAddress->setEmail($gunBrokerOrder->shipToEmail);
        $orderAddress->setTelephone($this->sanitizePhoneNumber($gunBrokerOrder->shipToPhone));
        $orderAddress->setFirstname($firstName);
        $orderAddress->setLastname($lastName);
        $orderAddress->setStreet([$gunBrokerOrder->shipToAddress1, $gunBrokerOrder->shipToAddress2]);
        $orderAddress->setCity($gunBrokerOrder->shipToCity);
        $orderAddress->setPostcode($gunBrokerOrder->shipToPostalCode);
        $orderAddress->setRegion($region->getName());
        $orderAddress->setRegionCode($region->getCode());
        $orderAddress->setRegionId($region->getRegionId());
        $orderAddress->setCountryId($gunBrokerOrder->shipToCountryCode);

        if ($customerId !== null) {
            $orderAddress->setCustomerId($customerId);
        }

        return $orderAddress;
    }

    /**
     * @return string[]
     */
    private function parseName(string $name): array
    {
        if (str_contains($name, ',')) {
            return array_reverse(explode(',', $name, 2));
        }

        return explode(' ', $name, 2);
    }

    private function sanitizePhoneNumber(string $phoneNumber): string
    {
        if (
            preg_match(
                // source: https://stackoverflow.com/a/123666/1470731
                '/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1' .
                '[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})' .
                '\s*(?:[.-]\s*)?(\d{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/',
                $phoneNumber
            )
        ) {
            return $phoneNumber;
        }

        return trim(preg_replace('/(?!^\+)[^\d()\s-]+/', '', $phoneNumber) ?? '');
    }

    private function createOrderPayment(OrderSold $gunBrokerOrder): OrderPaymentInterface
    {
        /** @var OrderPaymentInterface $orderPayment */
        $orderPayment = $this->orderPaymentFactory->create();

        $orderPayment->setMethod('gunbroker');
        $orderPayment->setBaseAmountOrdered($gunBrokerOrder->totalPrice);
        $orderPayment->setAmountOrdered($gunBrokerOrder->totalPrice);
        $orderPayment->setBaseAmountPaid($gunBrokerOrder->totalPrice);
        $orderPayment->setAmountPaid($gunBrokerOrder->totalPrice);
        $orderPayment->setBaseShippingAmount($gunBrokerOrder->shipCost);
        $orderPayment->setShippingAmount($gunBrokerOrder->shipCost);
        $orderPayment->setAdditionalInformation([
            'payment_date' => $gunBrokerOrder->paymentReceivedDateUTC
        ]);

        return $orderPayment;
    }

    private function createInvoice(Order $order): ?Invoice
    {
        try {
            $invoice = $this->invoiceService->prepareInvoice($order);
        } catch (LocalizedException $e) {
            $this->logger->critical(
                __(
                    'Could not prepare invoice for GunBroker.com order %1. Error: %2',
                    $order->getExtOrderId(),
                    $e->getMessage()
                )
            );

            return null;
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $invoice->setRequestedCaptureCase(Invoice::CAPTURE_OFFLINE);
        $invoice->register();

        return $invoice;
    }

    /**
     * @throws Exception
     */
    private function saveOrderAndInvoice(Order $order, ?Invoice $invoice): void
    {
        /** @var Transaction $databaseTransaction */
        $databaseTransaction = $this->databaseTransactionFactory->create();

        if ($invoice !== null) {
            $databaseTransaction->addObject($invoice);
        }

        $databaseTransaction->addObject($order);
        $databaseTransaction->save();
    }
}
