<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterfaceFactory;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

use function compact;
use function current;
use function gmdate;

class FeedbackCreator
{
    private FeedbackCreateApiRequest $feedbackCreateApiRequest;
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    private OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository;
    private OrderItemFeedbackInterfaceFactory $orderItemFeedbackFactory;
    private ConfigInterface $config;
    private LoggerInterface $logger;

    public function __construct(
        FeedbackCreateApiRequest $feedbackCreateApiRequest,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderItemFeedbackRepositoryInterface $orderItemFeedbackRepository,
        OrderItemFeedbackInterfaceFactory $orderItemFeedbackFactory,
        ConfigInterface $config,
        LoggerInterface $logger
    ) {
        $this->feedbackCreateApiRequest = $feedbackCreateApiRequest;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderItemFeedbackRepository = $orderItemFeedbackRepository;
        $this->orderItemFeedbackFactory = $orderItemFeedbackFactory;
        $this->config = $config;
        $this->logger = $logger;
    }

    public function createFeedback(
        int $magentoOrderItemId,
        int $gunBrokerItemId,
        string $comment,
        int $rating,
        int $websiteId = 0
    ): bool {
        $this->feedbackCreateApiRequest->setFeedbackData(compact('gunBrokerItemId', 'comment', 'rating'));

        $result = $this->feedbackCreateApiRequest->setWebsiteId($websiteId)->getResult();
        $isCreated = $result instanceof MessageResponse;

        if ($isCreated) {
            $this->recordFeedback($magentoOrderItemId, $gunBrokerItemId, $comment, $rating, $websiteId);
        }

        return $isCreated;
    }

    private function recordFeedback(
        int $magentoOrderItemId,
        int $gunBrokerItemId,
        string $comment,
        int $rating,
        int $websiteId
    ): void {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('gunbroker_item_id', $gunBrokerItemId)
            ->addFilter('type', 'seller')
            ->setPageSize(1)
            ->setCurrentPage(1)
            ->create();
        $searchResults = $this->orderItemFeedbackRepository->getList($searchCriteria);
        /** @var OrderItemFeedbackInterface $orderItemFeedback */
        $orderItemFeedback = $searchResults->getTotalCount() > 0
            ? current($searchResults->getItems())
            : $this->orderItemFeedbackFactory->create(
                [
                    'data' => [
                        'gunbroker_item_id' => $gunBrokerItemId,
                        'type' => 'seller'
                    ]
                ]
            );

        $orderItemFeedback->setMagentoOrderItemId($magentoOrderItemId);
        $orderItemFeedback->setComment($comment);
        $orderItemFeedback->setCommentator($this->config->getApiCredentials($websiteId)['username']);
        $orderItemFeedback->setCommentDate(gmdate('Y-m-d H:i:s'));
        $orderItemFeedback->setRatingScore($rating);
        $orderItemFeedback->setRatingLetter(OrderItemFeedbackInterface::RATING_MAP[$rating]);

        try {
            $this->orderItemFeedbackRepository->save($orderItemFeedback);
        } catch (CouldNotSaveException | NoSuchEntityException $e) {
            $this->logger->critical($e->getMessage(), ['order_item_feedback' => $orderItemFeedback->getData()]);
        }
    }
}
