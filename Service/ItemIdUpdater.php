<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Framework\DataObject;
use Magento\Store\Api\Data\WebsiteInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Model\ResourceModel\Product\Collection as ProductCollection;
use Reeds\GunBroker\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemSelling;

use function __;
use function array_filter;
use function array_key_exists;
use function array_map;
use function array_search;
use function array_unique;
use function array_walk;
use function count;

class ItemIdUpdater
{
    private ConfigLocator $configLocator;
    private ConfigInterface $config;
    private ItemsSellingApiRequest $itemsSellingApiRequest;
    private ProductCollectionFactory $productCollectionFactory;
    private LoggerInterface $logger;
    private ItemUrlUpdater $itemUrlUpdater;

    public function __construct(
        ConfigLocator $configLocator,
        ConfigInterface $config,
        ItemsSellingApiRequest $itemsSellingApiRequest,
        ProductCollectionFactory $productCollectionFactory,
        LoggerInterface $logger,
        ItemUrlUpdater $itemUrlUpdater
    ) {
        $this->configLocator = $configLocator;
        $this->config = $config;
        $this->itemsSellingApiRequest = $itemsSellingApiRequest;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->logger = $logger;
        $this->itemUrlUpdater = $itemUrlUpdater;
    }

    /**
     * @param int[] $websiteIds
     * @return array<int, int[]>
     */
    public function updateInWebsites(array $websiteIds = []): array
    {
        $updatedProducts = [];

        if (count($websiteIds) === 0) {
            $websiteIds = array_map(
                static fn(WebsiteInterface $website): int => (int)$website->getId(),
                $this->configLocator->getConfiguredWebsites()
            );
        }

        if (count($websiteIds) === 0) {
            return $updatedProducts;
        }

        array_walk(
            $websiteIds,
            function (int $websiteId) use (&$updatedProducts) {
                $updatedProducts[$websiteId] = $this->updateByWebsite($websiteId);
            }
        );

        return array_filter($updatedProducts);
    }

    /**
     * @return int[]
     */
    public function updateByWebsite(int $websiteId): array
    {
        $updatedProducts = [];

        if (!$this->config->isEnabled($websiteId)) {
            return $updatedProducts;
        }

        /** @var ItemSelling[] $itemsSelling */
        foreach ($this->itemsSellingApiRequest->setWebsiteId($websiteId)->getResults() as $itemsSelling) {
            $updatedProducts += $this->updateListedProducts($itemsSelling, $websiteId);
        }

        return $updatedProducts;
    }

    /**
     * @param ItemSelling[] $itemsSelling
     * @return int[]
     */
    private function updateListedProducts(array $itemsSelling, int $websiteId): array
    {
        $updatedProducts = [];
        $itemSkus = array_unique(
            array_map(static fn(ItemSelling $itemSelling): string => $itemSelling->sku, $itemsSelling)
        );
        $listedProducts = $this->getListedProducts($itemSkus, $websiteId);

        if (count($listedProducts) === 0) {
            return $updatedProducts;
        }

        array_walk(
            $listedProducts,
            function (Product $product) use ($itemSkus, $itemsSelling, &$updatedProducts): void {
                $itemKey = array_search($product->getSku(), $itemSkus, true);

                if ($itemKey === false || !array_key_exists($itemKey, $itemsSelling)) {
                    return;
                }

                $productUpdated = $this->updateItemIdByProduct($product, $itemsSelling[$itemKey]->itemID);

                if (!$productUpdated) {
                    return;
                }

                $this->itemUrlUpdater->updateByProduct($product, $itemsSelling[$itemKey]->itemID);

                $updatedProducts[$itemsSelling[$itemKey]->itemID] = (int)$product->getId();
            }
        );

        return $updatedProducts;
    }

    /**
     * @param string[] $itemSkus
     * @return Product[]|DataObject[]
     */
    private function getListedProducts(array $itemSkus, int $websiteId): array
    {
        /** @var ProductCollection<Product> $productCollection */
        $productCollection = $this->productCollectionFactory->create();

        if ($websiteId !== 0) {
            $productCollection->addWebsiteFilter($websiteId)
                ->joinStoreTable();
        }

        $productCollection->addAttributeToSelect('gunbroker_item_id', 'left')
            ->addAttributeToFilter('sku', ['in' => $itemSkus])
            ->addAttributeToFilter('is_gunbroker_item', ['eq' => 1])
            ->load();

        if ($productCollection->getSize() === 0) {
            return [];
        }

        return $productCollection->getItems();
    }

    private function updateItemIdByProduct(Product $product, int $itemId): bool
    {
        $gunBrokerItemIdAttribute = $product->getCustomAttribute('gunbroker_item_id');

        if ($gunBrokerItemIdAttribute === null || $gunBrokerItemIdAttribute->getValue() === null) {
            return false;
        }

        if ($itemId === (int)$gunBrokerItemIdAttribute->getValue()) {
            return false;
        }

        try {
            $product->addAttributeUpdate('gunbroker_item_id', $itemId, $product->getStoreId());
        } catch (Exception $e) {
            $this->logger->critical(
                __(
                    'Could not update GunBroker item identifier for product with ID %1. Error: %2',
                    $product->getId(),
                    $e->getMessage()
                ),
                [
                    'old_gunbroker_item_id' => $gunBrokerItemIdAttribute->getValue(),
                    'new_gunbroker_item_id' => $itemId
                ]
            );

            return false;
        }

        return true;
    }
}
