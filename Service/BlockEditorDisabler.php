<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;

use function preg_match;

/**
 * Used to disable the WYSIWYG editor and Magento Page Builder for the
 * GunBroker.com item listing content static blocks to prevent their HTML tags
 * from being destroyed.
 *
 * @see \Reeds\GunBroker\Plugin\Framework\Data\Form\Element\EditorPlugin::efterIsEnabled()
 * @see \Reeds\GunBroker\Plugin\PageBuilder\Model\ConfigInterfacePlugin::efterIsEnabled()
 */
class BlockEditorDisabler
{
    private UrlInterface $url;
    private BlockRepositoryInterface $blockRepository;

    public function __construct(UrlInterface $url, BlockRepositoryInterface $blockRepository)
    {
        $this->url = $url;
        $this->blockRepository = $blockRepository;
    }

    public function disableForGunBrokerBlocks(bool $isEditorEnabledInitially): bool
    {
        $currentUrl = $this->url->getCurrentUrl();

        if (!str_contains($currentUrl, 'cms/block/edit')) {
            return $isEditorEnabledInitially;
        }

        preg_match('#block_id/([\d]+)#', $currentUrl, $matches);

        try {
            $block = $this->blockRepository->getById($matches[1]);
        } catch (LocalizedException $e) {
            return $isEditorEnabledInitially;
        }

        return !str_starts_with($block->getIdentifier(), 'gunbroker');
    }
}
