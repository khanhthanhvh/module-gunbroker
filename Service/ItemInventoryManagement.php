<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\Data\ItemInventoryInterface;
use Reeds\GunBroker\Api\Data\ItemInventoryInterfaceFactory;
use Reeds\GunBroker\Api\ItemInventoryRepositoryInterface;

use function array_values;

class ItemInventoryManagement
{
    private ItemInventoryRepositoryInterface $itemInventoryRepository;
    private ItemInventoryInterfaceFactory $itemInventoryFactory;
    private LoggerInterface $logger;
    private SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory;

    public function __construct(
        ItemInventoryRepositoryInterface $itemInventoryRepository,
        ItemInventoryInterfaceFactory $itemInventoryFactory,
        LoggerInterface $logger,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory
    ) {
        $this->itemInventoryRepository = $itemInventoryRepository;
        $this->itemInventoryFactory = $itemInventoryFactory;
        $this->logger = $logger;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
    }

    public function saveQuantity(
        string $productSku,
        int $itemId,
        int $quantity,
        string $status = ItemInventoryInterface::STATUS_PENDING
    ): void {
        $itemInventory = $this->getItemInventory($itemId);

        $itemInventory->setProductSku($productSku);
        $itemInventory->setQuantity($quantity);
        $itemInventory->setStatus($status);

        try {
            $this->itemInventoryRepository->save($itemInventory);
        } catch (CouldNotSaveException | NoSuchEntityException $exception) {
            $this->logger->critical(
                __('Could not save inventory record for item %1.', $itemId),
                ['quantity' => $quantity]
            );
        }
    }

    public function getQuantity(int $itemId): int
    {
        try {
            $itemInventory = $this->itemInventoryRepository->getByItemId($itemId);
        } catch (NoSuchEntityException $noSuchEntityException) {
            return 0;
        }

        return $itemInventory->getQuantity();
    }

    /**
     * @param string[] $productSkus
     * @return int[]
     */
    public function getItemQuantities(
        array $productSkus,
        string $status = ItemInventoryInterface::STATUS_UPDATED
    ): array {
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();
        $searchCriteria = $searchCriteriaBuilder->addFilter('status', $status)
            ->addFilter('product_sku', array_values($productSkus), 'in')
            ->create();
        $searchResults = $this->itemInventoryRepository->getList($searchCriteria);
        $itemQuantities = [];

        foreach ($searchResults->getItems() as $itemInventory) {
            $itemQuantities[$itemInventory->getItemId()] = $itemInventory->getQuantity();
        }

        return $itemQuantities;
    }

    public function updateStatus(int $itemId, string $status = ItemInventoryInterface::STATUS_UPDATED): void
    {
        $itemInventory = $this->getItemInventory($itemId);

        $itemInventory->setStatus($status);

        try {
            $this->itemInventoryRepository->save($itemInventory);
        } catch (CouldNotSaveException | NoSuchEntityException $exception) {
            $this->logger->critical(
                __('Could not update inventory status for item %1.', $itemId),
                ['status' => $status]
            );
        }
    }

    private function getItemInventory(int $itemId): ItemInventoryInterface
    {
        try {
            $itemInventory = $this->itemInventoryRepository->getByItemId($itemId);
        } catch (NoSuchEntityException $noSuchEntityException) {
            /** @var ItemInventoryInterface $itemInventory */
            $itemInventory = $this->itemInventoryFactory->create();

            $itemInventory->setItemId($itemId);
        }

        return $itemInventory;
    }
}
