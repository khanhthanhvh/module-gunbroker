<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\RequestBuilder\UpdatedItemFactory as UpdatedItemRequestBuilderFactory;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

class UpdateItemApiRequest extends ApiRequest
{
    protected const API_METHOD = 'updateListedItem';

    private UpdatedItemRequestBuilderFactory $updatedItemRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        UpdatedItemRequestBuilderFactory $updatedItemRequestBuilderFactory
    ) {
        parent::__construct($connectorFactory, $logger);

        $this->updatedItemRequestBuilderFactory = $updatedItemRequestBuilderFactory;
    }

    public function setItemId(int $itemId): self
    {
        $this->queryParameters['itemID'] = $itemId;

        return $this;
    }

    /**
     * @param mixed[] $updatedItemData
     * @see \Reeds\GunBroker\Api\Data\UpdateItemMessageInterface
     */
    public function setUpdatedItemData(array $updatedItemData): self
    {
        $this->requestParameters = $updatedItemData;

        return $this;
    }

    public function getResult(): ?MessageResponse
    {
        /** @var MessageResponse|null $updatedItemResponse */
        $updatedItemResponse = parent::getResult();

        if ($updatedItemResponse instanceof MessageResponse) {
            $this->logger->info($updatedItemResponse->developerMessage);
        }

        return $updatedItemResponse;
    }

    public function getUpdatedItemQuantity(): ?int
    {
        return $this->request->quantity;
    }

    protected function instantiateRequestBuilder(): RequestBuilderInterface
    {
        return $this->updatedItemRequestBuilderFactory->create();
    }
}
