<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Generator;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\RequestBuilder\FeedbackSearchFactory as FeedbackSearchRequestBuilderFactory;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Wagento\GunBrokerApi\ApiObjects\Output\FeedbackSearchResult;

use function array_key_exists;

class FeedbackSearchApiRequest extends PaginatedApiRequest
{
    protected const API_METHOD = 'getUserFeedback';

    private FeedbackSearchRequestBuilderFactory $feedbackSearchRequestBuilderFactory;
    private ConfigInterface $config;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        FeedbackSearchRequestBuilderFactory $feedbackSearchRequestBuilderFactory,
        ConfigInterface $config,
        int $apiResultLimit = 25
    ) {
        parent::__construct($connectorFactory, $logger, $apiResultLimit);

        $this->feedbackSearchRequestBuilderFactory = $feedbackSearchRequestBuilderFactory;
        $this->config = $config;
    }

    protected function instantiateRequestBuilder(): RequestBuilderInterface
    {
        return $this->feedbackSearchRequestBuilderFactory->create();
    }

    /**
     * @param int $currentPage
     * @return Generator<FeedbackSearchResult[]>
     */
    public function getResults(int $currentPage = 1): Generator
    {
        if (!array_key_exists('userId', $this->queryParameters)) {
            $this->setUserIdParameter();
        }

        return parent::getResults($currentPage);
    }

    private function setUserIdParameter(): void
    {
        $userId = $this->config->getApiUserId($this->websiteId);

        if ($userId === null) {
            return;
        }

        $this->queryParameters['userId'] = $userId;
    }
}
