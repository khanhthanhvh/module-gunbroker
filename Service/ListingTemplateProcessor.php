<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\DataObject;
use Magento\Framework\DataObject\Factory;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filter\Template;
use Magento\Framework\Filter\TemplateFactory;
use Magento\Framework\Phrase;
use Reeds\GunBroker\Api\ListingTemplateProcessorInterface;

use function __;
use function array_keys;
use function array_merge;
use function array_search;
use function array_slice;
use function count;
use function str_contains;
use function uksort;

class ListingTemplateProcessor implements ListingTemplateProcessorInterface
{
    private BlockRepositoryInterface $blockRepository;
    private TemplateFactory $templateFilterFactory;
    private Factory $dataObjectFactory;
    private AttributeRepositoryInterface $attributeRepository;
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    public function __construct(
        BlockRepositoryInterface $blockRepository,
        TemplateFactory $templateFilterFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        AttributeRepositoryInterface $attributeRepository,
        Factory $dataObjectFactory
    ) {
        $this->blockRepository = $blockRepository;
        $this->templateFilterFactory = $templateFilterFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @throws InputException
     * @throws LocalizedException
     * @throws Exception
     */
    public function process(ProductInterface $product): string
    {
        $templateAttribute = $product->getCustomAttribute('gunbroker_description_template');

        if ($templateAttribute === null) {
            throw new InputException(
                __('No GunBroker listing template configured for product with ID "%1".', $product->getId())
            );
        }

        $templateId = $templateAttribute->getValue();

        if (str_contains($templateId, 'handgun')) {
            $attributes = $this->getHandgunAttributes($product);
        } elseif (str_contains($templateId, 'shotgun')) {
            $attributes = $this->getShotgunAttributes($product);
        } elseif (str_contains($templateId, 'rifle')) {
            $attributes = $this->getRifleAttributes($product);
        } elseif (str_contains($templateId, 'ammo')) {
            $attributes = $this->getAmmoAttributes($product);
        } else {
            $attributes = [];
        }

        $block = $this->blockRepository->getById($templateId);
        /** @var Template $templateFilter */
        $templateFilter = $this->templateFilterFactory->create();

        $templateFilter->setVariables(
            [
                'product_name' => $product->getName(),
                'product_attributes' => count($attributes) > 0 ? $attributes : null,
            ]
        );

        return $templateFilter->filter($block->getContent() ?? '');
    }

    /**
     * @return DataObject[]
     */
    private function getHandgunAttributes(ProductInterface $product): array
    {
        $attributeCodes = [
            // attribute_code => label (if different from attribute label, null if same)
            'brand' => null,
            'atf_model' => __('Model #'),
            'caliber_gauge' => __('Caliber'),
            'barrel_length' => __('Barrel'),
            'stock_finish' => null,
            'frame_material' => null,
            'frame_finish' => null,
            'slide_material' => null,
            'slide_finish' => null,
            'capacity' => null,
            'front_sights' => null,
            'rear_sights' => null
        ];

        return $this->getAttributes($attributeCodes, $product);
    }

    /**
     * @return DataObject[]
     */
    private function getShotgunAttributes(ProductInterface $product): array
    {
        $attributeCodes = [
            // attribute_code => label (if different from attribute label, null if same)
            'brand' => null,
            'atf_model' => __('Model #'),
            'caliber_gauge' => __('Gauge'),
            'barrel_length' => __('Barrel'),
            'stock_material' => null,
            'stock_finish' => null,
            'receiver_material' => null,
            'receiver_finish' => null,
            'capacity' => null,
            'chamber' => null,
            'choke_tubes' => null,
            'front_sights' => null
        ];

        return $this->getAttributes($attributeCodes, $product);
    }

    /**
     * @return DataObject[]
     */
    private function getRifleAttributes(ProductInterface $product): array
    {
        $attributeCodes = [
            // attribute_code => label (if different from attribute label, null if same)
            'brand' => null,
            'atf_model' => __('Model #'),
            'caliber_gauge' => __('Caliber'),
            'barrel_length' => __('Barrel'),
            'stock_material' => null,
            'stock_finish' => null,
            'receiver_material' => null,
            'receiver_finish' => null,
            'capacity' => null,
            'front_sights' => null,
            'rear_sights' => null
        ];

        return $this->getAttributes($attributeCodes, $product);
    }

    /**
     * @return DataObject[]
     */
    private function getAmmoAttributes(ProductInterface $product): array
    {
        $attributeCodes = [
            // attribute_code => label (if different from attribute label, null if same)
            'brand' => null,
            'atf_model' => __('Model #'),
            'caliber_gauge' => __('Gauge'),
            'quantity' => null
        ];

        return $this->getAttributes($attributeCodes, $product);
    }

    /**
     * @param array<string, Phrase|null> $attributeCodes
     * @return DataObject[]
     */
    private function getAttributes(array $attributeCodes, ProductInterface $product): array
    {
        $attributes = [];
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('attribute_code', array_keys($attributeCodes), 'in')
            ->create();
        $searchResults = $this->attributeRepository->getList(Product::ENTITY, $searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return $attributes;
        }

        $items = $searchResults->getItems();

        foreach ($items as $attribute) {
            $productAttribute = $product->getCustomAttribute($attribute->getAttributeCode());

            if ($productAttribute === null) {
                continue;
            }

            $label = $attribute->getStoreLabel();
            $value = $productAttribute->getValue();

            if ($attributeCodes[$attribute->getAttributeCode()] !== null) {
                $label = $attributeCodes[$attribute->getAttributeCode()];
            }

            if (!empty($attribute->getOptions())) {
                $value = $product->getAttributeText($attribute->getAttributeCode());
            }

            $dataObject = $this->dataObjectFactory->create();

            $dataObject->setData('label', (string)$label);
            $dataObject->setData('value', $value);

            $attributes[$attribute->getAttributeCode()] = $dataObject;
        }

        $nameAttributeObject = $this->dataObjectFactory->create();
        $skuAttributeObject = $this->dataObjectFactory->create();

        $nameAttributeObject->setData('label', (string)__('Model'));
        $nameAttributeObject->setData('value', $product->getName() ?? '');

        $skuAttributeObject->setData('label', (string)__('UPC'));
        $skuAttributeObject->setData('value', $product->getSku() ?? '');

        $attributes += [
            'name' => $nameAttributeObject,
            'sku' => $skuAttributeObject
        ];
        $attributeCodes = array_keys($attributeCodes);
        $attributeCodes = array_merge(
            array_slice($attributeCodes, 0, 1, true),
            ['name'],
            array_slice($attributeCodes, 1, null, true)
        );
        $attributeCodes = array_merge(
            array_slice($attributeCodes, 0, 3, true),
            ['sku'],
            array_slice($attributeCodes, 3, null, true)
        );

        uksort(
            $attributes,
            static fn(string $key1, string $key2): int =>
                array_search($key1, $attributeCodes, true) <=> array_search($key2, $attributeCodes, true)
        );

        return $attributes;
    }
}
