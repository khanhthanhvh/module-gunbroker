<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\WebsiteInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ItemInventoryRepositoryInterface;
use Reeds\GunBroker\Model\ResourceModel\Product\Collection as ProductCollection;
use Reeds\GunBroker\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Model\ResourceModel\Product\ItemId;
use Wagento\GunBrokerApi\ApiObjects\Output\ItemUnsold;

use function array_filter;
use function array_keys;
use function array_map;
use function array_merge;
use function array_walk;
use function count;
use function iterator_to_array;

class UnsoldItemsProcessor
{
    private ConfigLocator $configLocator;
    private ConfigInterface $config;
    private UnsoldItemsApiRequest $unsoldItemsApiRequest;
    private ProductCollectionFactory $productCollectionFactory;
    private ItemId $itemIdResource;
    private LoggerInterface $logger;
    private ItemInventoryRepositoryInterface $itemInventoryRepository;
    private ListItemMessagePublisher $listItemMessagePublisher;

    public function __construct(
        ConfigLocator $configLocator,
        ConfigInterface $config,
        UnsoldItemsApiRequest $unsoldItemsApiRequest,
        ProductCollectionFactory $productCollectionFactory,
        ItemId $itemIdResource,
        LoggerInterface $logger,
        ItemInventoryRepositoryInterface $itemInventoryRepository,
        ListItemMessagePublisher $listItemMessagePublisher
    ) {
        $this->configLocator = $configLocator;
        $this->config = $config;
        $this->unsoldItemsApiRequest = $unsoldItemsApiRequest;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->itemIdResource = $itemIdResource;
        $this->logger = $logger;
        $this->itemInventoryRepository = $itemInventoryRepository;
        $this->listItemMessagePublisher = $listItemMessagePublisher;
    }

    /**
     * Retrieves the unsold auction items from GunBroker.com, removes their
     * item identifiers from their corresponding products, and requeues them for
     * listing.
     *
     * @return array<int, array{itemId: int, storeId: int}> Identifiers of matching products with their
     *   corresponding listing identifiers.
     */
    public function process(): array
    {
        $productsToRelist = [];
        $websiteIds = array_map(
            static fn(WebsiteInterface $website): int => (int)$website->getId(),
            $this->configLocator->getConfiguredWebsites()
        );
        $unsoldItems = [];

        if (count($websiteIds) === 0) {
            return $productsToRelist;
        }

        array_walk(
            $websiteIds,
            function (int $websiteId) use (&$unsoldItems) {
                if (!$this->config->isEnabled($websiteId)) {
                    return;
                }

                $unsoldItems[] = iterator_to_array(
                    $this->unsoldItemsApiRequest->setWebsiteId($websiteId)->getResults(),
                    false
                );
            }
        );

        $unsoldItems = array_filter(array_merge([], ...$unsoldItems));

        if (count($unsoldItems) === 0) {
            return $productsToRelist;
        }

        $itemIds = array_map(static fn(ItemUnsold $itemUnsold): int => $itemUnsold->itemID, $unsoldItems);
        /** @var ProductCollection<Product> $productCollection */
        $productCollection = $this->productCollectionFactory->create();

        $productCollection->addAttributeToFilter('gunbroker_item_id', ['in' => $itemIds])
            ->load();

        if ($productCollection->getSize() === 0) {
            return $productsToRelist;
        }

        $listedProducts = $productCollection->getItems();

        array_walk(
            $listedProducts,
            function (Product $product) use (&$productsToRelist): void {
                $gunBrokerItemIdAttribute = $product->getCustomAttribute('gunbroker_item_id');

                if ($gunBrokerItemIdAttribute === null || $gunBrokerItemIdAttribute->getValue() === null) {
                    return;
                }

                $productsToRelist[(int)$product->getId()] = [
                    'itemId' => (int)$gunBrokerItemIdAttribute->getValue(),
                    'storeId' => $product->getStoreId()
                ];
            }
        );

        try {
            $this->itemIdResource->removeFromProducts(array_keys($productsToRelist));
        } catch (LocalizedException $e) {
            $this->logger->critical(
                __('Could not remove GunBroker item identifiers from products. Error: %1', $e->getMessage()),
                ['products' => $productsToRelist]
            );

            return [];
        }

        array_walk(
            $productsToRelist,
            function (array $productData, int $productId): void {
                try {
                    $this->itemInventoryRepository->deleteByItemId($productData['itemId']);
                } catch (CouldNotSaveException | NoSuchEntityException $exception) {
                    // No further action necessary
                }

                $this->listItemMessagePublisher->publishMessage($productId, $productData['storeId']);
            }
        );

        return $productsToRelist;
    }
}
