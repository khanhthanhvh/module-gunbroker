<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Shipment;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;
use RuntimeException;

use function __;
use function array_key_exists;

class ShipmentExporter
{
    private OrderImportRepositoryInterface $orderImportRepository;
    private OrdersShippingApiRequest $ordersShippingApiRequest;
    private OrdersFlagsApiRequest $ordersFlagsApiRequest;
    private OrderRepositoryInterface $orderRepository;

    public function __construct(
        OrderImportRepositoryInterface $orderImportRepository,
        OrdersShippingApiRequest $ordersShippingApiRequest,
        OrdersFlagsApiRequest $ordersFlagsApiRequest,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->orderImportRepository = $orderImportRepository;
        $this->ordersShippingApiRequest = $ordersShippingApiRequest;
        $this->ordersFlagsApiRequest = $ordersFlagsApiRequest;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @throws RuntimeException
     *
     * @noinspection PhpRedundantCatchClauseInspection
     */
    public function exportShipment(Shipment $shipment): bool
    {
        $order = $shipment->getOrder();
        $gunBrokerOrderId = $order->getExtOrderId();
        $tracks = $shipment->getTracks();

        if ($gunBrokerOrderId === null || empty($tracks) || !$this->isGunBrokerOrder((int)$gunBrokerOrderId)) {
            return false;
        }

        $websiteId = $shipment->getStore()->getWebsiteId();
        $carrierId = $this->getCarrierId($tracks[0]->getCarrierCode());

        $this->ordersShippingApiRequest->setWebsiteId((int)$websiteId)
            ->setGunBrokerOrderId((int)$gunBrokerOrderId)
            ->setTrackingNumber($tracks[0]->getTrackNumber())
            ->setCarrier($carrierId)
            ->getResult();

        $this->ordersFlagsApiRequest->setWebsiteId((int)$websiteId)
            ->setGunBrokerOrderId((int)$gunBrokerOrderId)
            ->setFlagName('orderShipped')
            ->getResult();

        $order->addCommentToStatusHistory(
            __('Exported shipment carrier and tracking number to GunBroker.com order and marked it as shipped.'),
            $order->getStatus() ?? false
        );

        try {
            $this->orderRepository->save($order);
        } catch (InputException | NoSuchEntityException | AlreadyExistsException $e) {
            // no further action necessary
        }

        return true;
    }

    private function isGunBrokerOrder(int $gunBrokerOrderId): bool
    {
        try {
            $this->orderImportRepository->getByGunBrokerOrderId($gunBrokerOrderId);
        } catch (LocalizedException | NoSuchEntityException $e) {
            return false;
        }

        return true;
    }

    /**
     * @throws RuntimeException
     */
    private function getCarrierId(string $carrierCode): int
    {
        $carriers = [
            'fedex' => 1,
            'ups' => 2,
            'usps' => 3
        ];

        if (!array_key_exists($carrierCode, $carriers)) {
            throw new RuntimeException((string)__('Shipment carrier must either be FedEx, UPS or USPS.'));
        }

        return $carriers[$carrierCode];
    }
}
