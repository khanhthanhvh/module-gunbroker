<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Config\Model\Config as ConfigModel;
use Magento\Config\Model\Config\Loader as ConfigLoader;
use Magento\Framework\App\Cache\Type\Config as ConfigCacheType;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface as StoreScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

use function array_diff_assoc;
use function count;
use function trim;

class UserIdManagement
{
    private const CONFIG_PATH_GUNBROKER_API_USER_ID = 'gunbroker/api/user_id';

    private StoreManagerInterface $storeManager;
    private AccountInfoApiRequest $accountInfoApiRequest;
    private WriterInterface $configWriter;
    private TypeListInterface $cacheTypeList;
    private ConfigLoader $configLoader;

    public function __construct(
        StoreManagerInterface $storeManager,
        AccountInfoApiRequest $accountInfoApiRequest,
        WriterInterface $configWriter,
        TypeListInterface $cacheTypeList,
        ConfigLoader $configLoader
    ) {
        $this->storeManager = $storeManager;
        $this->accountInfoApiRequest = $accountInfoApiRequest;
        $this->configWriter = $configWriter;
        $this->cacheTypeList = $cacheTypeList;
        $this->configLoader = $configLoader;
    }

    public function retrieveAndStoreUserId(?int $websiteId = null): ?int
    {
        if ($websiteId === null) {
            try {
                $websiteId = (int)$this->storeManager->getWebsite()->getId();
            } catch (LocalizedException $e) {
                $websiteId = 0;
            }
        }

        $accountInfo = $this->accountInfoApiRequest->setWebsiteId($websiteId)->getResult();

        if ($accountInfo === null) {
            return null;
        }

        $this->saveUserId($accountInfo->userSummary->userID, $websiteId);
        $this->clearConfigCache();

        return $accountInfo->userSummary->userID;
    }

    /**
     * @param callable(): ConfigModel $updateCallback
     */
    public function updateUserId(?string $websiteCode, callable $updateCallback): ConfigModel
    {
        $websiteId = 0;

        if ($websiteCode !== null) {
            try {
                $websiteId = (int)$this->storeManager->getWebsite($websiteCode)->getId();
            } catch (LocalizedException $e) {
                // no further action necessary
            }
        }

        $originalConfig = $this->getApiConfig($websiteCode, $websiteId);
        $result = $updateCallback();
        $newConfig = $this->getApiConfig($websiteCode, $websiteId);

        if (!$this->apiConfigHasChanged($originalConfig, $newConfig)) {
            return $result;
        }

        ['username' => $username, 'password' => $password] = $this->getApiCredentials($newConfig);

        if (empty(trim($username)) || empty(trim($password))) {
            if (!empty($originalConfig['gunbroker/api/user_id'])) {
                $this->removeUserId($websiteId);
                $this->clearConfigCache();
            }

            return $result;
        }

        $userId = $this->retrieveAndStoreUserId($websiteId);

        if ($userId === null) {
            $this->removeUserId($websiteId);
            $this->clearConfigCache();
        }

        return $result;
    }

    private function saveUserId(int $userId, int $websiteId): void
    {
        $this->configWriter->save(
            self::CONFIG_PATH_GUNBROKER_API_USER_ID,
            $userId,
            $websiteId === 0 ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : StoreScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );
    }

    private function removeUserId(int $websiteId): void
    {
        $this->configWriter->delete(
            self::CONFIG_PATH_GUNBROKER_API_USER_ID,
            $websiteId === 0 ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : StoreScopeInterface::SCOPE_WEBSITES,
            $websiteId
        );
    }

    private function clearConfigCache(): void
    {
        $this->cacheTypeList->cleanType(ConfigCacheType::TYPE_IDENTIFIER);
    }

    /**
     * @return array<string, string|int|null>
     */
    private function getApiConfig(?string $websiteCode, int $websiteId): array
    {
        return $this->configLoader->getConfigByPath(
            'gunbroker/api',
            $websiteCode === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : StoreScopeInterface::SCOPE_WEBSITES,
            $websiteId,
            false
        );
    }

    /**
     * @param mixed[] $originalConfig
     * @param mixed[] $newConfig
     */
    private function apiConfigHasChanged(array $originalConfig, array $newConfig): bool
    {
        return count(array_diff_assoc($newConfig, $originalConfig)) > 0;
    }

    /**
     * @param mixed[] $config
     * @return string[]
     */
    private function getApiCredentials(array $config): array
    {
        $apiCredentials = [];

        if ($config['gunbroker/api/environment'] === 'production') {
            $apiCredentials['username'] = $config['gunbroker/api/production_username'] ?? '';
            $apiCredentials['password'] = $config['gunbroker/api/production_password'] ?? '';
        } else {
            $apiCredentials['username'] = $config['gunbroker/api/sandbox_username'] ?? '';
            $apiCredentials['password'] = $config['gunbroker/api/sandbox_password'] ?? '';
        }

        return $apiCredentials;
    }
}
