<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service\ListItemEmailSender;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterface;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterfaceFactory;
use Reeds\GunBroker\Api\ItemListedEmailRepositoryInterface;
use Wagento\GunBrokerApi\ApiObjects\Input\Item;
use Wagento\GunBrokerApi\ApiObjects\Input\ItemFactory;

use function __;

class Asynchronous
{
    private ItemListedEmailInterfaceFactory $itemListedEmailFactory;
    private ItemListedEmailRepositoryInterface $itemListedEmailRepository;
    private LoggerInterface $logger;
    private ConfigInterface $config;
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    private ItemFactory $gunBrokerItemFactory;
    private Synchronous $synchronousEmailSender;
    private DateTime $dateTime;

    public function __construct(
        ItemListedEmailInterfaceFactory $itemListedEmailFactory,
        ItemListedEmailRepositoryInterface $itemListedEmailRepository,
        LoggerInterface $logger,
        ConfigInterface $config,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ItemFactory $gunBrokerItemFactory,
        Synchronous $synchronousEmailSender,
        DateTime $dateTime
    ) {
        $this->itemListedEmailFactory = $itemListedEmailFactory;
        $this->itemListedEmailRepository = $itemListedEmailRepository;
        $this->logger = $logger;
        $this->config = $config;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->gunBrokerItemFactory = $gunBrokerItemFactory;
        $this->synchronousEmailSender = $synchronousEmailSender;
        $this->dateTime = $dateTime;
    }

    public function queueEmail(
        int $storeId,
        int $itemId,
        Item $item,
        string $itemListingUrl,
        string $magentoProductUrl
    ): bool {
        /** @var ItemListedEmailInterface $itemListedEmail */
        $itemListedEmail = $this->itemListedEmailFactory->create();

        $itemListedEmail->setItemId($itemId);
        $itemListedEmail->setStoreId($storeId);
        $itemListedEmail->setItemListingUrl($itemListingUrl);
        $itemListedEmail->setItemData($item->toArray());
        $itemListedEmail->setProductUrl($magentoProductUrl);

        try {
            $this->itemListedEmailRepository->save($itemListedEmail);
        } catch (CouldNotSaveException $e) {
            $previousException = $e->getPrevious();
            $error = $previousException !== null ? $previousException->getMessage() : $e->getMessage();

            $this->logger->critical(__('Could not queue item listing e-mail for sending. Error: %1', $error));

            return false;
        }

        return true;
    }

    public function sendEmails(?int $storeId = null): bool
    {
        $limit = $this->config->getEmailBatchSize($storeId);
        $this->searchCriteriaBuilder->addFilter('sent_at', '', 'null')
            ->setPageSize($limit)
            ->setCurrentPage(1);

        if ($storeId !== null) {
            $this->searchCriteriaBuilder->addFilter('store_id', $storeId);
        }

        $searchCriteria =  $this->searchCriteriaBuilder->create();
        $searchResults = $this->itemListedEmailRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return false;
        }

        $sentEmailCount = 0;

        /** @var ItemListedEmailInterface $itemListedEmail */
        foreach ($searchResults->getItems() as $itemListedEmail) {
            /** @var Item $item */
            $item = $this->gunBrokerItemFactory->create(['parameters' => $itemListedEmail->getItemData()]);
            $isSent = $this->synchronousEmailSender->sendEmail(
                $itemListedEmail->getStoreId(),
                $item,
                $itemListedEmail->getItemListingUrl(),
                $itemListedEmail->getProductUrl()
            );

            if (!$isSent) {
                continue;
            }

            $itemListedEmail->setSentAt($this->dateTime->gmtDate());

            try {
                $this->itemListedEmailRepository->save($itemListedEmail);
            } catch (CouldNotSaveException $e) {
                $this->logger->critical($e->getMessage());

                continue;
            }

            $sentEmailCount++;
        }

        return $sentEmailCount === $searchResults->getTotalCount();
    }
}
