<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service\ListItemEmailSender;

use DateTime;
use IntlDateFormatter;
use Magento\Catalog\Api\CategoryListInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Area;
use Magento\Framework\DataObject;
use Magento\Framework\DataObject\Factory as DataObjectFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Mail\Template\SenderResolverInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\ConfigInterface;
use Spatie\DataTransferObject\DataTransferObject;
use Wagento\GunBrokerApi\ApiObjects\Input\Item;

use function __;
use function array_walk;

class Synchronous
{
    private ConfigInterface $config;
    private TransportBuilder $transportBuilder;
    private DataObjectFactory $dataObjectFactory;
    private LoggerInterface $logger;
    private TimezoneInterface $timezone;
    private ResolverInterface $localeResolver;
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    private CategoryListInterface $categoryRepository;
    private SenderResolverInterface $senderResolver;

    public function __construct(
        ConfigInterface $config,
        TransportBuilder $transportBuilder,
        DataObjectFactory $dataObjectFactory,
        LoggerInterface $logger,
        TimezoneInterface $timezone,
        ResolverInterface $localeResolver,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CategoryListInterface $categoryRepository,
        SenderResolverInterface $senderResolver
    ) {
        $this->config = $config;
        $this->transportBuilder = $transportBuilder;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->logger = $logger;
        $this->timezone = $timezone;
        $this->localeResolver = $localeResolver;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->categoryRepository = $categoryRepository;
        $this->senderResolver = $senderResolver;
    }

    public function sendEmail(int $storeId, Item $item, string $itemListingUrl, string $magentoProductUrl): bool
    {
        $this->transportBuilder->setTemplateIdentifier($this->config->getItemListedConfirmationEmailTemplate($storeId));
        $this->transportBuilder->setTemplateOptions(
            [
                'area' => Area::AREA_FRONTEND,
                'store' => $storeId
            ]
        );
        $this->transportBuilder->setTemplateVars(
            [
                'item' => $this->convertItemToDataObject($item),
                'gunbroker_auction_url' => $itemListingUrl,
                'category_name' => $this->getCategoryNameByGunBrokerCategoryId($item->categoryID),
                'start_date_formatted' => $item->premiumFeatures !== null
                    && $item->premiumFeatures->scheduledStartingDate !== null
                    ? $this->getDateTimeFormatted($item->premiumFeatures->scheduledStartingDate, $storeId)
                    : null,
                'product_url' => $magentoProductUrl
            ]
        );

        try {
            $this->transportBuilder->setFromByScope($this->config->getEmailSenderIdentity($storeId), $storeId);
        } catch (MailException $mailException) {
            $this->logger->critical(
                __(
                    'Could not get sender name and address for GunBroker Item Listing Confirmation e-mail. Error: %1',
                    $mailException->getMessage()
                )
            );

            return false;
        }

        try {
            [
                'name' => $recipientName,
                'email' => $recipientEmail
            ] = $this->senderResolver->resolve(
                $this->config->getEmailRecipientIdentity($storeId),
                $storeId
            );
        } catch (MailException $mailException) {
            $this->logger->critical(
                __(
                    'Could not get recipient name and address for GunBroker Item Listing Confirmation e-mail. Error: %1',
                    $mailException->getMessage()
                )
            );

            return false;
        }

        $this->transportBuilder->addTo($recipientEmail, $recipientName);

        try {
            $transport = $this->transportBuilder->getTransport();
        } catch (LocalizedException $exception) {
            $this->logger->critical(
                __(
                    'Could not get transport object for GunBroker Item Listing Confirmation e-mail. Error: %1',
                    $exception->getMessage()
                )
            );

            return false;
        }

        try {
            $transport->sendMessage();
        } catch (MailException $mailException) {
            $this->logger->critical(
                __('Could not send GunBroker Item Listing Confirmation e-mail. Error: %1', $mailException->getMessage())
            );

            return false;
        }

        return true;
    }

    private function convertItemToDataObject(Item $item): DataObject
    {
        $data = $item->all();

        array_walk(
            $data,
            /**
             * @param mixed $datum
             */
            static function (&$datum): void {
                if (!$datum instanceof DataTransferObject) {
                    return;
                }

                $datum = $datum->all();
            }
        );

        return $this->dataObjectFactory->create($data);
    }

    private function getDateTimeFormatted(string $dateTime, int $storeId): string
    {
        return $this->timezone->formatDateTime(
            new DateTime($dateTime),
            IntlDateFormatter::SHORT,
            IntlDateFormatter::SHORT,
            $this->localeResolver->getDefaultLocale(),
            $this->timezone->getConfigTimezone('store', $storeId)
        );
    }

    private function getCategoryNameByGunBrokerCategoryId(int $gunBrokerCategoryId): ?string
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('gunbroker_category_id', $gunBrokerCategoryId)
            ->setPageSize(1)
            ->setCurrentPage(1)
            ->create();
        $searchResults = $this->categoryRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return null;
        }

        return $searchResults->getItems()[0]->getName();
    }
}
