<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

class DeleteItemListingApiRequest extends ApiRequest
{
    protected const API_METHOD = 'deleteListedItem';

    protected array $queryParameters = [
        'itemID' => null
    ];

    public function execute(int $gunBrokerItemId, int $websiteId = 0): bool
    {
        $this->queryParameters['itemID'] = $gunBrokerItemId;

        $this->setWebsiteId($websiteId);

        /** @var MessageResponse|null $result */
        $result = $this->getResult();

        if ($result === null) {
            return false;
        }

        $this->logger->info($result->developerMessage);

        return true;
    }

    protected function instantiateRequestBuilder(): ?RequestBuilderInterface
    {
        return null;
    }
}
