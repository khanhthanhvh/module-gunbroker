<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use Magento\InventorySalesApi\Model\GetAssignedStockIdForWebsiteInterface;

use function floor;

class ItemInventoryCalculator
{
    private GetAssignedStockIdForWebsiteInterface $getAssignedStockIdForWebsite;
    private GetProductSalableQtyInterface $getProductSalableQty;

    public function __construct(
        GetAssignedStockIdForWebsiteInterface $getAssignedStockIdForWebsite,
        GetProductSalableQtyInterface $getProductSalableQty
    ) {
        $this->getAssignedStockIdForWebsite = $getAssignedStockIdForWebsite;
        $this->getProductSalableQty = $getProductSalableQty;
    }

    public function calculate(string $websiteCode, ProductInterface $product): int
    {
        if (!$this->isFixedPriceItem($product)) {
            return 1;
        }

        $stockId = $this->getAssignedStockIdForWebsite->execute($websiteCode);

        if ($stockId === null) {
            return 0;
        }

        try {
            $quantity = $this->getProductSalableQty->execute($product->getSku(), $stockId);
        } catch (LocalizedException $e) {
            return 0;
        }

        $quantityPercentageAttribute = $product->getCustomAttribute('gunbroker_quantity_percentage');
        $quantityPercentage = $quantityPercentageAttribute !== null ? (int)$quantityPercentageAttribute->getValue() : 0;

        if ($quantity > 0 && $quantityPercentage > 0) {
            $quantity = ($quantityPercentage / 100) * $quantity;
        }

        return (int)floor($quantity);
    }

    private function isFixedPriceItem(ProductInterface $product): bool
    {
        $attribute = $product->getCustomAttribute('gunbroker_fixed_price');

        return $attribute !== null && (float)$attribute->getValue() > 0;
    }
}
