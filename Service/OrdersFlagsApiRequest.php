<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Magento\Framework\Exception\InputException;
use Psr\Log\LoggerInterface;
use Reeds\GunBroker\Api\RequestBuilderInterface;
use Reeds\GunBroker\Factory\GunBrokerConnectorFactory;
use Reeds\GunBroker\RequestBuilder\OrdersFlagsFactory as OrdersFlagsRequestBuilderFactory;
use Wagento\GunBrokerApi\ApiObjects\MessageResponse;

/**
 * @method MessageResponse makeApiRequest()
 * @method MessageResponse getResult()
 */
class OrdersFlagsApiRequest extends ApiRequest
{
    protected const API_METHOD = 'updateOrderFlags';

    private OrdersFlagsRequestBuilderFactory $ordersFlagsRequestBuilderFactory;

    public function __construct(
        GunBrokerConnectorFactory $connectorFactory,
        LoggerInterface $logger,
        OrdersFlagsRequestBuilderFactory $ordersFlagsRequestBuilderFactory
    ) {
        parent::__construct($connectorFactory, $logger);

        $this->ordersFlagsRequestBuilderFactory = $ordersFlagsRequestBuilderFactory;
    }

    public function setGunBrokerOrderId(int $gunBrokerOrderId): OrdersFlagsApiRequest
    {
        $this->queryParameters['orderID'] = $gunBrokerOrderId;

        return $this;
    }

    /**
     * @phpstan-param 'cancelOrder'|'orderShipped' $flagName
     *
     * @throws InputException
     */
    public function setFlagName(string $flagName): OrdersFlagsApiRequest
    {
        if ($flagName !== 'cancelOrder' && $flagName !== 'orderShipped') {
            throw InputException::invalidFieldValue('flagName', $flagName);
        }

        $this->requestParameters[$flagName] = true;

        return $this;
    }

    protected function instantiateRequestBuilder(): ?RequestBuilderInterface
    {
        return $this->ordersFlagsRequestBuilderFactory->create();
    }
}
