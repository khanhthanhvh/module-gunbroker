<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Service;

use Reeds\GunBroker\Api\RequestBuilderInterface;
use Wagento\GunBrokerApi\ApiObjects\Output\Users\AccountInfo;

/**
 * @method AccountInfo getResult()
 */
class AccountInfoApiRequest extends ApiRequest
{
    protected const API_METHOD = 'getAccountDetails';

    protected function instantiateRequestBuilder(): ?RequestBuilderInterface
    {
        return null;
    }
}
