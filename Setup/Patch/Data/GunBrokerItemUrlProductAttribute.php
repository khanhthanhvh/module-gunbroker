<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

use function __;

class GunBrokerItemUrlProductAttribute implements DataPatchInterface, PatchRevertableInterface
{
    private ModuleDataSetupInterface $setup;
    private EavSetupFactory $eavSetupFactory;

    public function __construct(ModuleDataSetupInterface $setup, EavSetupFactory $eavSetupFactory)
    {
        $this->setup = $setup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies(): array
    {
        return [
            ProductAttributes::class
        ];
    }

    /**
     * @inheritDoc
     */
    public function getAliases(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply(): DataPatchInterface
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->addAttribute(
            Product::ENTITY,
            'gunbroker_item_url',
            [
                'type' => 'varchar',
                'label' => __('GunBroker.com Item URL'),
                'input' => 'text',
                'required' => false,
                'sort_order' => 25,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ]
        );
        $eavSetup->addAttributeToGroup(Product::ENTITY, 'Firearms', 'GunBroker', 'gunbroker_item_url', 25);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function revert(): void
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_item_url');
    }
}
