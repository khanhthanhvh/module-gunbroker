<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Setup\Patch\Data;

use Magento\Catalog\Model\Product;
use Magento\Eav\Api\AttributeSetManagementInterface;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Eav\Api\Data\AttributeSetInterfaceFactory;
use Magento\Eav\Model\Entity\Attribute\Backend\Datetime as DatetimeBackend;
use Magento\Eav\Model\Entity\Attribute\Frontend\Datetime as DatetimeFrontend;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Reeds\GunBroker\Model\Attribute\Source\AutoRelist;
use Reeds\GunBroker\Model\Attribute\Source\Condition;
use Reeds\GunBroker\Model\Attribute\Source\ListingDuration;
use Reeds\GunBroker\Model\Attribute\Source\ListingTemplates;
use Reeds\GunBroker\Model\Attribute\Source\TitleColor;
use Reeds\GunBroker\Model\Attribute\Source\WhoPaysForShipping;
use Reeds\GunBroker\Model\Entity\Attribute\Backend\GunBrokerItemId;
use Reeds\GunBroker\Model\Entity\Attribute\Backend\JsonEncoded;

use function __;

class ProductAttributes implements DataPatchInterface, PatchRevertableInterface
{
    private ModuleDataSetupInterface $setup;
    private EavSetupFactory $eavSetupFactory;
    private AttributeSetInterfaceFactory $attributeSetFactory;
    private AttributeSetManagementInterface $attributeSetManagement;

    public function __construct(
        ModuleDataSetupInterface $setup,
        EavSetupFactory $eavSetupFactory,
        AttributeSetInterfaceFactory $attributeSetFactory,
        AttributeSetManagementInterface $attributeSetManagement
    ) {
        $this->setup = $setup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->attributeSetManagement = $attributeSetManagement;
    }

    /**
     * @inheritDoc
     */
    public function apply(): DataPatchInterface
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);
        $attributes = [
            'is_gunbroker_item' => [
                'type' => 'int',
                'default' => 0,
                'label' => __('Is GunBroker.com Auction Item'),
                'input' => 'boolean',
                'source' => Boolean::class,
                'required' => true,
                'sort_order' => 10,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_item_id' => [
                'type' => 'int',
                'label' => __('GunBroker.com Item ID'),
                'backend' => GunBrokerItemId::class,
                'input' => 'text',
                'required' => false,
                'sort_order' => 20,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_auto_accept_price' => [
                'type' => 'decimal',
                'label' => __('Auto Accept Price'),
                'input' => 'price',
                'required' => false,
                'sort_order' => 30,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_auto_reject_price' => [
                'type' => 'decimal',
                'label' => __('Auto Reject Price'),
                'input' => 'price',
                'required' => false,
                'sort_order' => 40,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_auto_relist' => [
                'type' => 'int',
                'label' => __('Auto Relist'),
                'input' => 'select',
                'source' => AutoRelist::class,
                'required' => false,
                'sort_order' => 50,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_auto_relist_fixed_count' => [
                'type' => 'int',
                'label' => __('Auto Relist Fixed Count'),
                'input' => 'text',
                'required' => false,
                'sort_order' => 60,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_buy_now_price' => [
                'type' => 'decimal',
                'label' => __('Buy Now Price'),
                'input' => 'price',
                'required' => false,
                'sort_order' => 70,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_can_offer' => [
                'type' => 'int',
                'default' => 0,
                'label' => __('Can Offer'),
                'input' => 'boolean',
                'source' => Boolean::class,
                'required' => false,
                'sort_order' => 80,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_collect_taxes' => [
                'type' => 'int',
                'label' => __('Collect Taxes'),
                'input' => 'boolean',
                'source' => Boolean::class,
                'required' => false,
                'sort_order' => 90,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_condition' => [
                'type' => 'int',
                'label' => __('Condition'),
                'input' => 'select',
                'source' => Condition::class,
                'required' => false,
                'sort_order' => 100,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_description_template' => [
                'type' => 'varchar',
                'label' => __('Description Template'),
                'input' => 'select',
                'source' => ListingTemplates::class,
                'required' => false,
                'sort_order' => 105,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_fixed_price' => [
                'type' => 'decimal',
                'label' => __('Fixed Price'),
                'input' => 'price',
                'required' => false,
                'sort_order' => 110,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_is_ffl_required' => [
                'type' => 'int',
                'default' => 1,
                'label' => __('Is FFL Required'),
                'input' => 'boolean',
                'source' => Boolean::class,
                'required' => false,
                'sort_order' => 120,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_listing_duration' => [
                'type' => 'int',
                'label' => __('Listing Duration'),
                'input' => 'select',
                'source' => ListingDuration::class,
                'required' => false,
                'sort_order' => 130,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_premium_features' => [
                'type' => 'text',
                'label' => __('Premium Features'),
                'note' => __('Using premium features will incur additional listing fees.'),
                'backend' => JsonEncoded::class,
                'required' => false,
                'sort_order' => 131,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_premium_features_scheduled_start_date' => [
                'type' => 'datetime',
                'label' => __('Scheduled Start Date (Premium Feature)'),
                'backend' => DatetimeBackend::class,
                'frontend' => DatetimeFrontend::class,
                'input' => 'datetime',
                'required' => false,
                'sort_order' => 132,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_premium_features_subtitle' => [
                'type' => 'varchar',
                'label' => __('Subtitle (Premium Feature)'),
                'required' => false,
                'sort_order' => 133,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_premium_features_thumbnail_url' => [
                'type' => 'varchar',
                'label' => __('Thumbnail URL (Premium Feature)'),
                'note' => __('Relative to catalog media URL (i.e. "https://www.reedssports.com/media/catalog/product/")'),
                'required' => false,
                'sort_order' => 133,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_premium_features_title_color' => [
                'type' => 'varchar',
                'label' => __('Title Color (Premium Feature)'),
                'input' => 'select',
                'source' => TitleColor::class,
                'required' => false,
                'sort_order' => 134,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_quantity_percentage' => [
                'type' => 'int',
                'label' => __('Percent of Inventory Allocated for Listing'),
                'input' => 'text',
                'required' => false,
                'sort_order' => 135,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_starting_bid' => [
                'type' => 'decimal',
                'label' => __('Starting Bid'),
                'input' => 'price',
                'required' => false,
                'sort_order' => 140,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_shipping_class_costs' => [
                'type' => 'text',
                'label' => __('Shipping Class Costs'),
                'backend' => JsonEncoded::class,
                'required' => false,
                'sort_order' => 150,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_shipping_classes_supported' => [
                'type' => 'text',
                'label' => __('Shipping Classes Supported'),
                'backend' => JsonEncoded::class,
                'required' => false,
                'sort_order' => 160,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_shipping_profile_id' => [
                'type' => 'int',
                'label' => __('Shipping Profile ID'),
                'input' => 'text',
                'required' => false,
                'sort_order' => 165,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ],
            'gunbroker_who_pays_for_shipping' => [
                'type' => 'int',
                'label' => __('Who Pays for Shipping'),
                'input' => 'select',
                'source' => WhoPaysForShipping::class,
                'required' => false,
                'sort_order' => 170,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'user_defined' => true,
                'apply_to' => 'simple'
            ]
        ];

        if (!$eavSetup->getAttributeSet(Product::ENTITY, 'Firearms')) {
            /** @var AttributeSetInterface $attributeSet */
            $attributeSet = $this->attributeSetFactory->create();

            $attributeSet->setAttributeSetName('Firearms');
            $attributeSet->setSortOrder(100);

            $this->attributeSetManagement->create(
                Product::ENTITY,
                $attributeSet,
                $eavSetup->getDefaultAttributeSetId(Product::ENTITY)
            );
        }

        $eavSetup->addAttributeGroup(Product::ENTITY, 'Firearms', 'GunBroker', 100);

        foreach ($attributes as $attributeCode => $attributeData) {
            $eavSetup->addAttribute(Product::ENTITY, $attributeCode, $attributeData);
            $eavSetup->addAttributeToGroup(
                Product::ENTITY,
                'Firearms',
                'GunBroker',
                $attributeCode,
                $attributeData['sort_order']
            );
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function revert(): void
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->removeAttribute(Product::ENTITY, 'is_gunbroker_item');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_item_id');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_auto_accept_price');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_auto_reject_price');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_auto_relist');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_auto_relist_fixed_count');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_buy_now_price');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_can_offer');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_collect_taxes');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_condition');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_description_template');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_fixed_price');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_is_ffl_required');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_listing_duration');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_premium_features');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_starting_bid');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_shipping_class_costs');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_shipping_classes_supported');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_shipping_profile_id');
        $eavSetup->removeAttribute(Product::ENTITY, 'gunbroker_who_pays_for_shipping');
        $eavSetup->removeAttributeGroup(Product::ENTITY, 'Firearms', 'GunBroker');
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases(): array
    {
        return [];
    }
}
