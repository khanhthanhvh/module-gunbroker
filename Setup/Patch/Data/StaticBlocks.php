<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Setup\Patch\Data;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Api\Data\BlockInterfaceFactory;
use Magento\Cms\Model\Block;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

class StaticBlocks implements DataPatchInterface, PatchRevertableInterface
{
    private BlockInterfaceFactory $blockFactory;
    private BlockRepositoryInterface $blockRepository;

    public function __construct(BlockInterfaceFactory $blockFactory, BlockRepositoryInterface $blockRepository)
    {
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
    }

    public function apply(): DataPatchInterface
    {
        $blocks = [
            [
                'identifier' => 'gunbroker_listing_handgun',
                'title' => 'GunBroker Listing - Handgun',
                'content' => '',
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 1000
            ],
            [
                'identifier' => 'gunbroker_listing_rifle',
                'title' => 'GunBroker Listing - Rifle',
                'content' => '',
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 1001
            ],
            [
                'identifier' => 'gunbroker_listing_shotgun',
                'title' => 'GunBroker Listing - Shotgun',
                'content' => '',
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 1002
            ],
            [
                'identifier' => 'gunbroker_listing_ammo',
                'title' => 'GunBroker Listing - Ammunition',
                'content' => '',
                'is_active' => 1,
                'stores' => [0],
                'sort_order' => 1003
            ]
        ];

        foreach ($blocks as $blockData) {
            /** @var Block $block */
            $block = $this->blockFactory->create();

            $block->setData($blockData);

            $this->blockRepository->save($block);
        }

        return $this;
    }

    public function revert(): PatchRevertableInterface
    {
        $this->blockRepository->deleteById('gunbroker_listing_generic_firearm');
        $this->blockRepository->deleteById('gunbroker_listing_wood_firearm');
        $this->blockRepository->deleteById('gunbroker_listing_ammo');

        return $this;
    }

    public static function getDependencies(): array
    {
        return [];
    }

    public function getAliases(): array
    {
        return [];
    }
}
