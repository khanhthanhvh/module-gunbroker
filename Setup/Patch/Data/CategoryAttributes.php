<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Setup\Patch\Data;

use Magento\Catalog\Model\Category;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

use function __;

class CategoryAttributes implements DataPatchInterface, PatchRevertableInterface
{
    private ModuleDataSetupInterface $setup;
    private EavSetupFactory $eavSetupFactory;

    public function __construct(ModuleDataSetupInterface $setup, EavSetupFactory $eavSetupFactory)
    {
        $this->setup = $setup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply(): DataPatchInterface
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->addAttributeGroup(Category::ENTITY, 'Default', 'GunBroker', 100);
        $eavSetup->addAttribute(
            Category::ENTITY,
            'is_gunbroker_category',
            [
                'type' => 'int',
                'label' => __('Is GunBroker.com Category'),
                'input' => 'select',
                'source' => Boolean::class,
                'required' => false,
                'sort_order' => 10,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'GunBroker',
            ]
        );
        $eavSetup->addAttribute(
            Category::ENTITY,
            'gunbroker_category_id',
            [
                'type' => 'int',
                'label' => __('GunBroker.com Category ID'),
                'input' => 'text',
                'required' => false,
                'sort_order' => 20,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'GunBroker',
            ]
        );

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function revert(): void
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);

        $eavSetup->removeAttribute(Category::ENTITY, 'is_gunbroker_category');
        $eavSetup->removeAttribute(Category::ENTITY, 'gunbroker_category_id');
        $eavSetup->removeAttributeGroup(Category::ENTITY, 'Default', 'GunBroker');
    }

    /**
     * @inheritDoc
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases(): array
    {
        return [];
    }
}
