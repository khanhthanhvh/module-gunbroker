# Reeds GunBroker

GunBroker is a bespoke extension written for [Reeds Family Outdoor Outfitters]
that integrates Adobe Commerce with the [GunBroker.com] firearm auction
platform.

## Requirements

* PHP 7.4.16+ with JSON, INTL and PCRE extensions
* Adobe Commerce 2.4.3+
* RabbitMQ 3.8.10+
* [Wagento GunBroker API Connector]
* `php-http/guzzle6-adapter` (Adobe Commerce < 2.4.4) _or_
  `php-http/guzzle7-adapter` (Adobe Commerce >= 2.4.4)

## Installation

The GunBroker extension is available for installation via Composer by entering
the following commands into your terminal or command prompt:

    cd /path/to/your/site
    composer config repositories.gunbroker vcs git@bitbucket.org:wagento-global/module-gunbroker.git
    composer require reeds/module-gunbroker

## Updating

To update the GunBroker extension using Composer, run these commands from your
terminal or command prompt:

    cd /path/to/your/site
    composer update reeds/module-gunbroker

## Post-Install or Post-Update

To complete the installation or update process, please run these commands:

    cd /path/to/your/site
    php bin/magento setup:upgrade
    php bin/magento setup:di:compile
    php bin/magento setup:static-content:deploy

## Supported Features
* Creates a new GunBroker auction item listing when a product with the
  `is_gunbroker_item` attribute is created or updated (if it does not already
  have a value for the `gunbroker_item_id` attribute) 
* Syncs modified data to the corresponding GunBroker auction item when a product
  with values in the `is_gunbroker_item` and `gunbroker_item_id` attributes is
  updated
* Imports won auction listings into Adobe Commerce as orders
* Marks auction listings as "Shipped" and exports shipment details to
  GunBroker.com when the corresponding order is shipped in Adobe Commerce
* Imports seller feedback and ratings into Adobe Commerce
* Exports buyer feedback and ratings into GunBroker.com (created manually or
  automatically)
* Automatically updates auction item available quantity when inventory is
  updated
* Relists expired auction items that were not won by a bidder
* Cancels won auctions on GunBroker.com when a Credit Memo is created in Adobe
  Commerce

## Unsupported Features
* Payment processing must be done manually by Reeds' staff
* Once posted, buyer feedback cannot be edited as GunBroker.com does not provide
  update functionality

## Known Issues
* The `is_gunbroker_item` and `gunbroker_item_id` attributes must be configured
  in the store scope so that the extension correctly detects that the product
  has a corresponding GunBroker.com auction item
* This extension is not compatible with Laminas Diactoros

## Configuration

The extension's settings can be configured in the Adobe Commerce Admin panel by 
going to "Stores > Settings > Configuration > GunBroker.com".

### Sections
| Name              | Description                                   |
|-------------------|-----------------------------------------------|
| General           | Basic extension settings                      |
| API Configuration | API keys and environment type                 |
| Cron              | Cron job timing configurations                |
| Catalog           | Default auction item attribute configurations |
| Sales             | Order-related settings                        |
| Developer         | Internal configuration settings               |

## Technical Details

The GunBroker extension ties into Adobe Commerce through multiple plug-ins and 
event observers. It also provides Cron jobs to perform tasks at scheduled
intervals as well as console commands to perform common tasks. 

### Plug-ins

| Method                                                                                  | Area      | Purpose                                                                     |
|-----------------------------------------------------------------------------------------|-----------|-----------------------------------------------------------------------------|
| \Magento\Framework\App\Config\Value::isValueChanged                                     | global    | Fixes comparison errors with serialized config fields                       |
| \Magento\InventoryApi\Api\SourceItemsSaveInterface::execute                             | global    | Syncs inventory updates to GunBroker                                        |
| \Magento\Sales\Api\OrderItemRepositoryInterface::get(List)                              | global    | Loads feedback into order item extension attributes                         |
| \Magento\Sales\Model\Order::place                                                       | global    | Removes listings for serialized products                                    |
| \Magento\Backend\Block\Widget\Button\ToolbarInterface::pushButtons                      | adminhtml | Modifies buttons shown for GunBroker orders on Admin Sales Order view page  |
| \Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper::initializeFromData | adminhtml | Fixes rendering of custom GunBroker Attributes on Admin Product Edit page   |
| \Magento\Config\Model\Config::save                                                      | adminhtml | Retrieves user account ID from GunBroker.com and saves it in a config field |
| \Magento\Framework\Data\Form\Element\Editor::isEnabled                                  | adminhtml | Disables the WYSIWYG editor for the GunBroker Item Listing Template blocks  |
| \Magento\PageBuilder\Model\ConfigInterface::isEnabled                                   | adminhtml | Disables Page Builder for the GunBroker Item Listing Template blocks        |
| \Magento\InventoryShipping\Observer\SourceDeductionProcessor::execute                   | adminhtml | Sets and removes flag to prevent inventory update during order shipment     |
| \Magento\Sales\Block\Adminhtml\Order\View\Items::getItemsCollection                     | adminhtml | Loads feedback into order item extension attributes                         |
| \Magento\Sales\Model\Order\Pdf\AbstractPdf::insertOrder                                 | adminhtml | Renders GunBroker order ID in Order PDF                                     |

### Event Observers

| Event                      | Purpose                                                                  |
|----------------------------|--------------------------------------------------------------------------|
| catalog_product_save_after | Queues product to create or update an item listing for it                |
| sales_order_save_after     | Automatically creates buyer feedback for completed order (if configured) |

### Commands

| Command                   | Description                                                                                  |
|---------------------------|----------------------------------------------------------------------------------------------|
| gunbroker:feedback:import | Imports seller feedback from GunBroker.com into Adobe Commerce                               |
| gunbroker:orders:import   | Imports won auctions into Adobe Commerce as orders                                           |
| gunbroker:item:list       | Creates an auction listing item from a given product                                         |
| gunbroker:item-ids:update | Updates the `gunbroker_item_id` product attribute for items with updated listing identifiers |

### Cron Jobs

| Name                              | Purpose                                                                                      |
|-----------------------------------|----------------------------------------------------------------------------------------------|
| gunbroker_import_feedback         | Imports seller feedback from GunBroker.com into Adobe Commerce                               |
| gunbroker_import_orders           | Imports won auctions into Adobe Commerce as orders                                           |
| gunbroker_process_unsold_items    | Relists items whose auctions were not won buy a bidder                                       |
| gunbroker_send_item_listed_emails | Sends e-mails when an item is listed for auction asynchronously                              |
| gunbroker_update_item_ids         | Updates the `gunbroker_item_id` product attribute for items with updated listing identifiers |

### Patches

| Name                                           | Description                                                                                               | Affected Modules                |
|------------------------------------------------|-----------------------------------------------------------------------------------------------------------|---------------------------------|
| Modify-Sales-Order-PDF-header-text-coordinates | Adds an additional parameter to determine whether to render the GunBroker order ID in the Sales Order PDF | Magento_Sales, Amasty_Orderattr |

### Architecture

This extension is broken down into many service classes that perform the work
for the plug-ins, observers, console commands and Cron jobs. These services
allow the underlying logic to be reused in many places across the extension. 

### Workflow

The Git repository for this extension uses the [Git Flow] workflow. It is highly
recommended that developers install the [Git Flow AVH] command line tool and use
it to create and merge feature branches as well as final version releases.

Here are examples of common Git Flow commands:

* Create a new feature branch:

        git flow feature start add-xyz-feature

* Merge a feature back into the `develop` branch:

        git flow feature finish add-xyz-feature

* Create a new version release¹:

        git flow release start 1.1.0

* Merge a version release:

        git flow release finish -sp 1.1.0

¹After creating a release, be sure to update the [Change Log]
document.

## Testing
* Integration tests can be run with the following commands:

        cd /path/to/your/site
        ./vendor/bin/phpunit -c vendor/reeds/module-gunbroker/Test/Integration/phpunit.xml vendor/reeds/module-gunbroker/Test/Integration

* Static testing can be run through PHPStan with the following commands:

        cd /path/to/your/site
        ./vendor/bin/phpstan analyse -c vendor/reeds/module-gunbroker/phpstan.neon vendor/reeds/module-gunbroker

* Coding standards can be checked with PHP Code Sniffer using the following
  commands:

        cd /path/to/your/site
        ./vendor/bin/phpcs -ps --standard=Magento2,PSR12 --ignore=./vendor/reeds/module-gunbroker/Test --extensions=php ./vendor/reeds/module-gunbroker

* Manual tests can be run by following the scenarios outlined in the 
  [GunBroker Extension Testing Plan] document

## License

The source code contained in this extension is proprietary to 
[Reeds Family Outdoor Outfitters].

## History

A full history of the extension can be found in the [CHANGELOG.md] file.

## TODO
* Create Admin grids that contain only GunBroker products and orders and link
  them in the appropriate Admin menus (effectively filtered versions of the
  standard grids)
* Remove the GunBroker Admin menu and move the API Log link to the Tools menu
* Improve test coverage
* Add tests for errors and edge cases (expand tests beyond "happy paths")
* Improve organization of Service classes (move into subdirectories?)
* Convert code to PHP 8.1 after release of Adobe Commerce 2.4.4
* Implement [Generators] properly and replace usages of the `iterator_to_array`
  function (ran out of time on this initially)
* Break up the Order Creator service logic into multiple methods and/or
  subclasses for easier readability and understandability
* Test and improve code performance
* Clean up extension configuration and possibly move into an existing section

[Reeds Family Outdoor Outfitters]: https://www.reedssports.com/
[GunBroker.com]: https://www.gunbroker.com/
[Wagento GunBroker API Connector]: https://bitbucket.org/wagento-global/gunbroker-connector/
[Git Flow]: https://jeffkreeftmeijer.com/git-flow/
[Git Flow AVH]: https://github.com/petervanderdoes/gitflow-avh
[Change Log]: ./CHANGELOG.md
[GunBroker Extension Testing Plan]: ./docs/GunBroker_Extension_Testing_Plan.md
[CHANGELOG.md]: ./CHANGELOG.md
[Generators]: https://www.php.net/manual/en/language.generators.overview.php