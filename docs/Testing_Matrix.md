# GunBroker Extension Testing Matrix

**Date:** 2021-07-07 & 2021-07-08  
**Environment:** Local with single (default) website, one-time listing and
feedback automation disabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| List product           | PASS   |                                            |
| Import order           | PASS   | Tax not added to total on GunBroker.com    |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | OTHER  | Feedback submitted successfully but did not save to database correctly (missing data - REED-1160) |
| Import seller feedback | FAIL   | Does not set user ID field correctly       |
| Import seller feedback | PASS   | Fixed issue                                |

**Date:** 2021-07-09  
**Environment:** Local with single (default) website, one-time listing and
feedback automation disabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| List product           | OTHER  | Relisted product on GunBroker.com          |
| Import order           | PASS   |                                            |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |

**Date:** 2021-07-12 & 2021-07-14  
**Environment:** Local with single (default) website, automatically relisted
listing and feedback automation enabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| List product           | PASS   |                                            |
| Import order           | PASS   |                                            |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |
| Update item ID         | PASS   |                                            |

**Date:** 2021-07-22  
**Environment:** Local with multiple websites, automatically relisted
listing and feedback automation enabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| List product           | PASS   |                                            |
| Import order           | FAIL   | Error caused by lack of store filter       |

**Date:** 2021-07-26 & 2021-07-27  
**Environment:** Local with multiple websites, automatically relisted
listing and feedback automation enabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| Import order           | PASS   |                                            |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |
| Update item ID         | PASS   |                                            |

**Date:** 2021-07-28 & 2021-07-29  
**Environment:** Local with single (default) website, automatically relisted
listing and feedback automation enabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| List product           | PASS   |                                            |
| Import order           | PASS   |                                            |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |
| Update item ID         | PASS   |                                            |

**Date:** 2021-10-27 & 2021-10-28  
**Environment:** Local with multiple websites, config in default scope,
one-time listing and feedback automation disabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| List product           | PASS   | E-mail failed to send due to system misconfiguration |
| Import order           | FAIL   | New properties received in API results     |
| Import order           | FAIL   | Invalid parameter type error thrown by OrderImporter::filterOutImportedOrders() |
| Import order           | PASS   | Incorrect shipping method applied due to missing attribute - fixed |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | FAIL   | Incorrect item ID sent resulting in 404 error |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |
| Update item ID         | PASS   |                                            |

**Date:** 2021-10-28  
**Environment:** Local with multiple websites, config in default scope,
one-time listing and feedback automation enabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| Import order           | PASS   |                                            |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |
| Update item ID         | PASS   |                                            |

**Date:** 2021-10-28  
**Environment:** Local with multiple websites, config in website scope,
one-time listing and feedback automation disabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| List product           | PASS   |                                            |
| Import order           | FAIL   | Config retrieved from default instead of store scope |

**Date:** 2021-11-01  
**Environment:** Local with multiple websites, config in website scope,
one-time listing and feedback automation disabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| Import order           | PASS   |                                            |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |
| Update item ID         | PASS   |                                            |

**Date:** 2021-11-01  
**Environment:** Local with multiple websites, config in website scope,
one-time listing and feedback automation enabled

| Scenario               | Result | Notes                                      |
|------------------------|--------|--------------------------------------------|
| Import order           | PASS   |                                            |
| Ship order             | PASS   |                                            |
| Add buyer feedback     | PASS   |                                            |
| Import seller feedback | PASS   |                                            |
| Update item ID         | PASS   |                                            |
