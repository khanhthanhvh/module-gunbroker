# GunBroker Extension Testing Plan

**Scenario:** As an Administrator, I want to list a product for auction on GunBroker.com  
**Steps:**
1. Configure the GunBroker extension in the desired store
2. Create or edit a product in Adobe Commerce with data for the GunBroker attributes
    - Ensure that "Is GunBroker Item" is toggled on
3. Using the CLI, run the `gunbroker:item:list` command, passing the ID of the product to list
   (i.e. `gunbroker:item:list 2050`)
    - If RabbitMQ is configured, the product data will be stored in a message queue
        * **Remove the product from the queue prior to running the above command, or trigger the queue manually with 
          `bin/magento queue:consumer:start --max-messages=1 gunbroker.item.list` if it is not configured to run
          automatically**
4. Log into GunBroker sandbox and verify that item was listed correctly and all data is in the proper fields
5. Verify that the product in Adobe Commerce now has a value in the "GunBroker Item Identifier" field
6. Check e-mail and verify that the item listing e-mail was sent

**Scenario:** As an Administrator, I want to import an order from GunBroker.com  
**Steps:**
1. In the GunBroker.com sandbox, place an order for a product that exists in Adobe Commerce (see previous scenario)
2. If Cron is running on the test server, wait for the order to automatically be imported into Adobe Commerce at the
   configured interval, then proceed to step 4
3. If Cron is not configured, run the CLI command `bin/magento gunbroker:orders:import`, optionally specifying the ID
   of the website to import orders for (defaults to all websites)
4.Verify that all order data was imported correctly and matches what is in the GunBroker.com sandbox
   - Double-check that all order totals including shipping, tax and any discounts are calculated correctly for each 
     order item and the whole order
   - Ensure order status matches what is configured for the GunBroker Order Payment payment method

**Scenario:** As an Administrator, I want to ship an order in Adobe Commerce and mark it shipped on GunBroker.com  
**Steps:**
1. In the Adobe Commerce Admin panel, proceed to the desired GunBroker order and click on the "Ship" button
2. Verify the shipment details, make any necessary changes, and click on the "Create Shipment" button
3. In the GunBroker.com sandbox, verify that the order is marked as "shipped"
4. If automatic feedback is configured in Adobe Commerce, verify that the default feedback was added to the order in the
   GunBroker.com sandbox

**Scenario:** As an Administrator, I want to add seller feedback to an order and export it to GunBroker.com  
**Steps:**
1. In the extension configuration in Adobe Commerce, disable the automated feedback functionality
2. Go to the desired order and click on the "Add Feedback" button for the order item that you wish to provide feedback
   for
3. Provide a rating and comment in the dialog that pop-up and click on "Send Feedback"
4. If there are no errors in the browser console or extension log ("var/log/gunbroker.log"), verify that the feedback is
   visible in the GunBroker sandbox

**Scenario:** As an Administrator, I want to import buyer feedback for an order from GunBroker.com  
**Steps:**
1. In the GunBroker.com sandbox, as a buyer, add feedback to an existing order (see step 1 of the Import Order scenario)
2. If Cron is running on the server for the Adobe Commerce website, wait until the configured interval and proceed to
   step 4
3. If Cron is not running on the server, run the `bin/magento gunbroker:feedback:import` command, optionally
   specifying the ID of the website to import feedback for (defaults to all websites)
4. View the order in the Adobe Commerce Admin panel and verify that the buyer feedback is displayed for the 
   corresponding order item

**Scenario:** As an Administrator, I want to update the GunBroker item identifier for a relisted product  
**Steps:**
1. In the GunBroker.com sandbox, as a buyer, place an order for a fixed-price item which allows relisting
2. If Cron is running on the server for the Adobe Commerce website, wait until the configured interval and proceed to
   step 4
3. If Cron is not running on the server, run the `bin/magento gunbroker:item-ids:update` command, optionally
   specifying the ID of the website to update item identifiers for (defaults to all websites)
4. View the product in the Adobe Commerce Admin panel and verify that the value for the GunBroker Item Identifier field
   matches the identifier of the relisted item
