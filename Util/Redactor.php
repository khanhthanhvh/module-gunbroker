<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Util;

use JsonException;

use function in_array;
use function is_array;
use function is_numeric;
use function is_object;
use function is_string;
use function json_decode;
use function json_encode;
use function json_last_error;
use function str_repeat;
use function strlen;

use const JSON_ERROR_NONE;
use const JSON_THROW_ON_ERROR;

class Redactor
{
    /**
     * @param mixed[]|object $data
     * @param string[] $sensitiveFields
     * @return mixed[]|object
     * @throws JsonException
     */
    public static function redact($data, array $sensitiveFields)
    {
        if (!is_array($data) && !is_object($data)) {
            return [];
        }

        foreach ($data as $name => $field) {
            if (is_string($field) && !is_numeric($field) && self::isJson($field)) {
                if (is_array($data)) {
                    $data[$name] = json_encode(
                        self::redact(json_decode($field, false, 512, JSON_THROW_ON_ERROR), $sensitiveFields),
                        JSON_THROW_ON_ERROR
                    );
                } else {
                    $data->{$name} = json_encode(
                        self::redact(json_decode($field, false, 512, JSON_THROW_ON_ERROR), $sensitiveFields),
                        JSON_THROW_ON_ERROR
                    );
                }

                continue;
            }

            if (is_array($field) || is_object($field)) {
                if (is_array($data)) {
                    $data[$name] = self::redact($field, $sensitiveFields);
                } else {
                    $data->{$name} = self::redact($field, $sensitiveFields);
                }

                continue;
            }

            if (in_array($name, $sensitiveFields, true)) {
                $redactedData = str_repeat('*', strlen($field));

                if (is_array($data)) {
                    $data[$name] = $redactedData;
                } else {
                    $data->{$name} = $redactedData;
                }
            }
        }

        return $data;
    }

    private static function isJson(string $string): bool
    {
        json_decode($string);

        return json_last_error() === JSON_ERROR_NONE;
    }
}
