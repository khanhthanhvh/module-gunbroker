<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Phrase;

use function __;

class WhoPaysForShipping extends AbstractSource
{
    /**
     * @inheritDoc
     * @return array<array{value: int, label: Phrase}>
     */
    public function getAllOptions(): array
    {
        if ($this->_options !== null) {
            return $this->_options;
        }

        $this->_options = [
            [
                'value' => 1,
                'label' => __('See item description')
            ],
            [
                'value' => 2,
                'label' => __('Seller pays for shipping')
            ],
            [
                'value' => 4,
                'label' => __('Buyer pays actual shipping cost')
            ],
            [
                'value' => 8,
                'label' => __('Buyer pays fixed amount')
            ],
            [
                'value' => 16,
                'label' => __('Use shipping profile')
            ]
        ];

        return $this->_options;
    }
}
