<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Phrase;

use function __;

class Condition extends AbstractSource
{
    /**
     * @inheritDoc
     * @return array<array{value: int, label: Phrase}>
     */
    public function getAllOptions()
    {
        if ($this->_options !== null) {
            return $this->_options;
        }

        $this->_options = [
            [
                'value' => 1,
                'label' => __('Factory New')
            ],
            [
                'value' => 2,
                'label' => __('New Old Stock')
            ],
            [
                'value' => 3,
                'label' => __('Used')
            ]
        ];

        return $this->_options;
    }
}
