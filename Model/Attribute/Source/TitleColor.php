<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Phrase;

use function __;

class TitleColor extends AbstractSource
{
    /**
     * @inheritDoc
     * @return array<array{value: string, label: Phrase}>
     */
    public function getAllOptions(): array
    {
        return $this->_options = [
            [
                'value' => '',
                'label' => __('None')
            ],
            [
                'value' => 'Red',
                'label' => __('Red')
            ],
            [
                'value' => 'Green',
                'label' => __('Green')
            ],
            [
                'value' => 'Blue',
                'label' => __('Blue')
            ]
        ];
    }
}
