<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Api\SearchCriteriaBuilder;

class ListingTemplates extends AbstractSource
{
    private BlockRepositoryInterface $blockRepository;
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    /**
     * @var array<array{value: string, label: ?string}>
     */
    private ?array $options = null;

    public function __construct(BlockRepositoryInterface $blockRepository, SearchCriteriaBuilder $searchCriteriaBuilder)
    {
        $this->blockRepository = $blockRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @inheritDoc
     * @return array<array{value: string, label: ?string}>
     */
    public function getAllOptions(): ?array
    {
        if ($this->options !== null) {
            return $this->options;
        }

        $this->options = [];
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('identifier', 'gunbroker_listing%', 'like')
            ->addFilter('is_active', 1)
            ->create();
        $searchResults = $this->blockRepository->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            return $this->options;
        }

        $blocks = $searchResults->getItems();

        foreach ($blocks as $block) {
            $this->options[] = [
                'value' => $block->getIdentifier(),
                'label' => $block->getTitle()
            ];
        }

        return $this->options;
    }
}
