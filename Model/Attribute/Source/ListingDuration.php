<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Phrase;

use function __;

class ListingDuration extends AbstractSource
{
    /**
     * @inheritDoc
     * @return array<array{value: int, label: Phrase}>
     */
    public function getAllOptions()
    {
        if ($this->_options !== null) {
            return $this->_options;
        }

        $this->_options = [
            [
                'value' => 1,
                'label' => __('One day')
            ],
            [
                'value' => 3,
                'label' => __('Three days')
            ],
            [
                'value' => 5,
                'label' => __('Five days')
            ],
            [
                'value' => 7,
                'label' => __('Seven days')
            ],
            [
                'value' => 9,
                'label' => __('Nine days')
            ],
            [
                'value' => 10,
                'label' => __('Ten days')
            ],
            [
                'value' => 11,
                'label' => __('Eleven days')
            ],
            [
                'value' => 12,
                'label' => __('Twelve days')
            ],
            [
                'value' => 13,
                'label' => __('Thirteen days')
            ],
            [
                'value' => 14,
                'label' => __('Fourteen days')
            ],
            [
                'value' => 30,
                'label' => __('Thirty days (Fixed price items only)')
            ],
            [
                'value' => 60,
                'label' => __('Sixty days (Fixed price items only)')
            ],
            [
                'value' => 90,
                'label' => __('Ninety days (Fixed price items only)')
            ]
        ];

        return $this->_options;
    }
}
