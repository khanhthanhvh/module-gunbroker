<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Phrase;

use function __;

class AutoRelist extends AbstractSource
{
    /**
     * @inheritDoc
     * @return array<array{value: int, label: Phrase}>
     */
    public function getAllOptions()
    {
        if ($this->_options !== null) {
            return $this->_options;
        }

        $this->_options = [
            [
                'value' => 1,
                'label' => __('Do Not Relist')
            ],
            [
                'value' => 2,
                'label' => __('Relist Until Sold')
            ],
            [
                'value' => 3,
                'label' => __('Relist Fixed Count')
            ],
            [
                'value' => 4,
                'label' => __('Relist Fixed Price')
            ]
        ];

        return $this->_options;
    }
}
