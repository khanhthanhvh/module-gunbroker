<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Phrase;

use function __;

class ShippingClass extends AbstractSource
{
    /**
     * @inheritDoc
     * @return array<array{value: string, label: Phrase}>
     */
    public function getAllOptions()
    {
        return $this->_options = [
            [
                'value' => 'overnight',
                'label' => __('Overnight')
            ],
            [
                'value' => 'twoDay',
                'label' => __('Two Day')
            ],
            [
                'value' => 'threeDay',
                'label' => __('Three Day')
            ],
            [
                'value' => 'ground',
                'label' => __('Ground')
            ],
            [
                'value' => 'firstClass',
                'label' => __('First Class')
            ],
            [
                'value' => 'priority',
                'label' => __('Priority')
            ],
            [
                'value' => 'other',
                'label' => __('Other')
            ]
        ];
    }
}
