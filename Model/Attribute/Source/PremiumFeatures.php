<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Phrase;

use function __;

class PremiumFeatures extends AbstractSource
{
    /**
     * @inheritDoc
     * @return array<array{value: string, label: Phrase}>
     */
    public function getAllOptions()
    {
        return $this->_options = [
            [
                'value' => 'hasViewCounter',
                'label' => __('Has View Counter')
            ],
            [
                'value' => 'isFeaturedItem',
                'label' => __('Is Featured Item')
            ],
            [
                'value' => 'isHighlighted',
                'label' => __('Is Highlighted')
            ],
            [
                'value' => 'isShowCaseItem',
                'label' => __('Is Showcase Item')
            ],
            [
                'value' => 'isSponsoredOnsite',
                'label' => __('Is Sponsored Onsite')
            ],
            [
                'value' => 'isTitleBoldface',
                'label' => __('Is Title Boldface')
            ]
        ];
    }
}
