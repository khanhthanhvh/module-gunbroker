<?php

/** @noinspection PhpParamsInspection */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\ItemInventoryInterface;
use Reeds\GunBroker\Api\Data\ItemInventoryInterfaceFactory;
use Reeds\GunBroker\Api\ItemInventoryRepositoryInterface;
use Reeds\GunBroker\Model\ResourceModel\ItemInventory as ItemInventoryResourceModel;
use Reeds\GunBroker\Model\ResourceModel\ItemInventory\Collection as ItemInventoryCollection;
use Reeds\GunBroker\Model\ResourceModel\ItemInventory\CollectionFactory as ItemInventoryCollectionFactory;

use function __;

class ItemInventoryRepository implements ItemInventoryRepositoryInterface
{
    private ItemInventoryResourceModel $resourceModel;
    private ItemInventoryInterfaceFactory $itemInventoryFactory;
    private ItemInventoryCollectionFactory $collectionFactory;
    private SearchResultsInterfaceFactory $searchResultsFactory;
    private CollectionProcessorInterface $collectionProcessor;

    public function __construct(
        ItemInventoryResourceModel $resourceModel,
        ItemInventoryInterfaceFactory $itemInventoryFactory,
        ItemInventoryCollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resourceModel = $resourceModel;
        $this->itemInventoryFactory = $itemInventoryFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(ItemInventoryInterface $itemInventory): ItemInventoryInterface
    {
        try {
            $this->resourceModel->save($itemInventory);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save item inventory record with identifier "%s". Error: %s',
                    $itemInventory->getInventoryId(),
                    $exception->getMessage()
                ),
                $exception
            );
        }

        if ($itemInventory->getInventoryId() === null) {
            throw new CouldNotSaveException(
                __('Could not get identifier of item inventory record. Please check if it was saved properly.')
            );
        }

        return $this->getById($itemInventory->getInventoryId());
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $inventoryId): ItemInventoryInterface
    {
        /** @var ItemInventory $itemInventory */
        $itemInventory = $this->itemInventoryFactory->create();

        $this->resourceModel->load($itemInventory, $inventoryId);

        if (!$itemInventory->getInventoryId()) {
            throw new NoSuchEntityException(
                __('An item inventory record with the identifier "%s" was not found.', $inventoryId)
            );
        }

        return $itemInventory;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getByItemId(int $itemId): ItemInventoryInterface
    {
        /** @var ItemInventory $itemInventory */
        $itemInventory = $this->itemInventoryFactory->create();

        $this->resourceModel->load($itemInventory, $itemId, 'item_id');

        if (!$itemInventory->getInventoryId()) {
            throw new NoSuchEntityException(
                __('An item inventory record with the item ID "%s" was not found.', $itemId)
            );
        }

        return $itemInventory;
    }

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        /** @var ItemInventoryCollection<ItemInventoryInterface> $itemInventoryCollection */
        $itemInventoryCollection = $this->collectionFactory->create();
        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        $this->collectionProcessor->process($searchCriteria, $itemInventoryCollection);

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($itemInventoryCollection->getItems());
        $searchResults->setTotalCount($itemInventoryCollection->getSize());

        return $searchResults;
    }

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(ItemInventoryInterface $itemInventory): bool
    {
        try {
            $this->resourceModel->delete($itemInventory);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete item inventory record with identifier "%s". Error: %s',
                    $itemInventory->getInventoryId(),
                    $exception->getMessage()
                ),
                $exception
            );
        }

        return true;
    }

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $inventoryId): bool
    {
        return $this->delete($this->getById($inventoryId));
    }

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteByItemId(int $itemId): bool
    {
        return $this->delete($this->getByItemId($itemId));
    }
}
