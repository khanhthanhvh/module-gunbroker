<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Entity\Attribute\Backend;

class JsonEncoded extends \Magento\Eav\Model\Entity\Attribute\Backend\JsonEncoded
{
    /**
     * @inheritDoc
     */
    public function beforeSave($object)
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();

        // Allow NULL values in attribute data
        if ($object->hasData($attributeCode) && $object->getData($attributeCode) !== null) {
            parent::beforeSave($object);
        }

        return $this;
    }
}
