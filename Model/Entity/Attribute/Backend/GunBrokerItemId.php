<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Entity\Attribute\Backend;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\DataObject;
use Reeds\GunBroker\Service\ItemUrlUpdater;

class GunBrokerItemId extends AbstractBackend
{
    private ItemUrlUpdater $itemUrlUpdater;

    public function __construct(ItemUrlUpdater $itemUrlUpdater)
    {
        $this->itemUrlUpdater = $itemUrlUpdater;
    }

    /**
     * @inheritDoc
     * @param DataObject&Product $object
     */
    public function afterSave($object): GunBrokerItemId
    {
        parent::afterSave($object);

        $itemId = $object->getData($this->getAttribute()->getAttributeCode());

        if ($itemId !== null) {
            $itemId = (int)$itemId;
        }

        $this->itemUrlUpdater->updateByProduct($object, $itemId);

        return $this;
    }
}
