<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Model\AbstractModel;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Model\ResourceModel\OrderItemFeedback as ResourceModel;

use function __;

class OrderItemFeedback extends AbstractModel implements OrderItemFeedbackInterface
{
    /**
     * @inheritDoc
     */
    protected $_eventPrefix = 'gunbroker_item_feedback';
    /**
     * @inheritDoc
     */
    protected $_eventObject = 'feedback';

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return $this->getData(self::ID) !== null ? (int)$this->getData(self::ID) : null;
    }

    /**
     * @inheritDoc
     */
    public function setId($id): OrderItemFeedbackInterface
    {
        $this->setData(self::ID, $id !== null ? (int)$id : null);

        return $this;
    }

    /**
     * @param string $type
     * @return OrderItemFeedbackInterface
     * @throws InputException
     */
    public function setType(string $type): OrderItemFeedbackInterface
    {
        if ($type !== 'buyer' && $type !== 'seller') {
            throw new InputException(
                __('Feedback type must be either "buyer" or "seller." Type "%1" is invalid.', $type)
            );
        }

        $this->setData(self::TYPE, $type);

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->getData(OrderItemFeedbackInterface::TYPE);
    }

    /**
     * @inheritDoc
     */
    public function getGunbrokerFeedbackId(): int
    {
        return (int)$this->getData(self::GUNBROKER_FEEDBACK_ID);
    }

    /**
     * @inheritDoc
     */
    public function setGunbrokerFeedbackId(int $gunbrokerFeedbackId): OrderItemFeedbackInterface
    {
        $this->setData(self::GUNBROKER_FEEDBACK_ID, $gunbrokerFeedbackId);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getMagentoOrderItemId(): int
    {
        return (int)$this->getData(self::MAGENTO_ORDER_ITEM_ID);
    }

    /**
     * @inheritDoc
     */
    public function setMagentoOrderItemId(int $magentoOrderItemId): OrderItemFeedbackInterface
    {
        $this->setData(self::MAGENTO_ORDER_ITEM_ID, $magentoOrderItemId);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getGunbrokerItemId(): int
    {
        return (int)$this->getData(self::GUNBROKER_ITEM_ID);
    }

    /**
     * @inheritDoc
     */
    public function setGunbrokerItemId(int $gunbrokerItemId): OrderItemFeedbackInterface
    {
        $this->setData(self::GUNBROKER_ITEM_ID, $gunbrokerItemId);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRatingLetter(): string
    {
        return $this->getData(self::RATING_LETTER);
    }

    /**
     * @inheritDoc
     */
    public function setRatingLetter(string $ratingLetter): OrderItemFeedbackInterface
    {
        $this->setData(self::RATING_LETTER, $ratingLetter);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRatingScore(): int
    {
        return (int)$this->getData(self::RATING_SCORE);
    }

    /**
     * @inheritDoc
     */
    public function setRatingScore(int $ratingScore): OrderItemFeedbackInterface
    {
        $this->setData(self::RATING_SCORE, $ratingScore);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getComment(): string
    {
        return $this->getData(self::COMMENT);
    }

    /**
     * @inheritDoc
     */
    public function setComment(string $comment): OrderItemFeedbackInterface
    {
        $this->setData(self::COMMENT, $comment);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCommentDate(): string
    {
        return $this->getData(self::COMMENT_DATE);
    }

    /**
     * @inheritDoc
     */
    public function setCommentDate(string $commentDate): OrderItemFeedbackInterface
    {
        $this->setData(self::COMMENT_DATE, $commentDate);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCommentator(): string
    {
        return $this->getData(self::COMMENTATOR);
    }

    /**
     * @inheritDoc
     */
    public function setCommentator(string $commentator): OrderItemFeedbackInterface
    {
        $this->setData(self::COMMENTATOR, $commentator);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUpdated(): string
    {
        return $this->getData(self::UPDATED);
    }

    /**
     * @inheritDoc
     */
    public function setUpdated(string $updated): OrderItemFeedbackInterface
    {
        $this->setData(self::UPDATED, $updated);

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
