<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Magento\Framework\Api\AbstractSimpleObject;
use Reeds\GunBroker\Api\Data\UpdateItemMessageInterface;

class UpdateItemMessage extends AbstractSimpleObject implements UpdateItemMessageInterface
{
    /**
     * @param int $itemId
     * @return UpdateItemMessageInterface
     */
    public function setItemId(int $itemId): UpdateItemMessageInterface
    {
        return $this->setData(self::ITEM_ID, $itemId);
    }

    public function getItemId(): int
    {
        return (int)$this->_get(self::ITEM_ID);
    }

    /**
     * @param float $autoAcceptPrice
     * @return UpdateItemMessageInterface
     */
    public function setAutoAcceptPrice(float $autoAcceptPrice): UpdateItemMessageInterface
    {
        return $this->setData(self::AUTO_ACCEPT_PRICE, $autoAcceptPrice);
    }

    /**
     * @return float|null
     */
    public function getAutoAcceptPrice(): ?float
    {
        $autoAcceptPrice = $this->_get(self::AUTO_ACCEPT_PRICE);

        if ($autoAcceptPrice !== null) {
            $autoAcceptPrice = (float)$autoAcceptPrice;
        }

        return $autoAcceptPrice;
    }

    /**
     * @param float $autoRejectPrice
     * @return UpdateItemMessageInterface
     */
    public function setAutoRejectPrice(float $autoRejectPrice): UpdateItemMessageInterface
    {
        return $this->setData(self::AUTO_REJECT_PRICE, $autoRejectPrice);
    }

    /**
     * @return float|null
     */
    public function getAutoRejectPrice(): ?float
    {
        $autoRejectPrice = $this->_get(self::AUTO_REJECT_PRICE);

        if ($autoRejectPrice !== null) {
            $autoRejectPrice = (float)$autoRejectPrice;
        }

        return $autoRejectPrice;
    }

    /**
     * @param float $buyNowPrice
     * @return UpdateItemMessageInterface
     */
    public function setBuyNowPrice(float $buyNowPrice): UpdateItemMessageInterface
    {
        return $this->setData(self::BUY_NOW_PRICE, $buyNowPrice);
    }

    /**
     * @return float|null
     */
    public function getBuyNowPrice(): ?float
    {
        $buyNowPrice = $this->_get(self::BUY_NOW_PRICE);

        if ($buyNowPrice !== null) {
            $buyNowPrice = (float)$buyNowPrice;
        }

        return $buyNowPrice;
    }

    /**
     * @param bool $canOffer
     * @return UpdateItemMessageInterface
     */
    public function setCanOffer(bool $canOffer): UpdateItemMessageInterface
    {
        return $this->setData(self::CAN_OFFER, $canOffer);
    }

    /**
     * @return bool|null
     */
    public function getCanOffer(): ?bool
    {
        $canOffer = $this->_get(self::CAN_OFFER);

        if ($canOffer !== null) {
            $canOffer = (bool)$canOffer;
        }

        return $canOffer;
    }

    /**
     * @param float $fixedPrice
     * @return UpdateItemMessageInterface
     */
    public function setFixedPrice(float $fixedPrice): UpdateItemMessageInterface
    {
        return $this->setData(self::FIXED_PRICE, $fixedPrice);
    }

    /**
     * @return float|null
     */
    public function getFixedPrice(): ?float
    {
        $fixedPrice = $this->_get(self::FIXED_PRICE);

        if ($fixedPrice !== null) {
            $fixedPrice = (float)$fixedPrice;
        }

        return $fixedPrice;
    }

    /**
     * @param string $gtin
     * @return UpdateItemMessageInterface
     */
    public function setGtin(string $gtin): UpdateItemMessageInterface
    {
        return $this->setData(self::GTIN, $gtin);
    }

    /**
     * @return string|null
     */
    public function getGtin(): ?string
    {
        return $this->_get(self::GTIN);
    }

    /**
     * @param string $mfgPartNumber
     * @return UpdateItemMessageInterface
     */
    public function setMfgPartNumber(string $mfgPartNumber): UpdateItemMessageInterface
    {
        return $this->setData(self::MFG_PART_NUMBER, $mfgPartNumber);
    }

    /**
     * @return string|null
     */
    public function getMfgPartNumber(): ?string
    {
        return $this->_get(self::MFG_PART_NUMBER);
    }

    /**
     * @param int $quantity
     * @return UpdateItemMessageInterface
     */
    public function setQuantity(int $quantity): UpdateItemMessageInterface
    {
        return $this->setData(self::QUANTITY, $quantity);
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        $quantity = $this->_get(self::QUANTITY);

        if ($quantity !== null) {
            $quantity = (int)$quantity;
        }

        return $quantity;
    }

    /**
     * @param float $reservePrice
     * @return UpdateItemMessageInterface
     */
    public function setReservePrice(float $reservePrice): UpdateItemMessageInterface
    {
        return $this->setData(self::RESERVE_PRICE, $reservePrice);
    }

    /**
     * @return float|null
     */
    public function getReservePrice(): ?float
    {
        $reservePrice = $this->_get(self::RESERVE_PRICE);

        if ($reservePrice !== null) {
            $reservePrice = (float)$reservePrice;
        }

        return $reservePrice;
    }

    /**
     * @param string $sku
     * @return UpdateItemMessageInterface
     */
    public function setSku(string $sku): UpdateItemMessageInterface
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * @return string|null
     */
    public function getSku(): ?string
    {
        return $this->_get(self::SKU);
    }

    /**
     * @param float $startingBid
     * @return UpdateItemMessageInterface
     */
    public function setStartingBid(float $startingBid): UpdateItemMessageInterface
    {
        return $this->setData(self::STARTING_BID, $startingBid);
    }

    /**
     * @return float|null
     */
    public function getStartingBid(): ?float
    {
        $startingBid = $this->_get(self::STARTING_BID);

        if ($startingBid !== null) {
            $startingBid = (float)$startingBid;
        }

        return $startingBid;
    }

    /**
     * @param string $subtitle
     * @return UpdateItemMessageInterface
     */
    public function setSubtitle(string $subtitle): UpdateItemMessageInterface
    {
        return $this->setData(self::SUBTITLE, $subtitle);
    }

    /**
     * @return string|null
     */
    public function getSubtitle(): ?string
    {
        return $this->_get(self::SUBTITLE);
    }

    /**
     * @param string $title
     * @return UpdateItemMessageInterface
     */
    public function setTitle(string $title): UpdateItemMessageInterface
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->_get(self::TITLE);
    }

    /**
     * @param string $upc
     * @return UpdateItemMessageInterface
     */
    public function setUpc(string $upc): UpdateItemMessageInterface
    {
        return $this->setData(self::UPC, $upc);
    }

    /**
     * @return string|null
     */
    public function getUpc(): ?string
    {
        return $this->_get(self::UPC);
    }

    /**
     * @param int $websiteId
     * @return UpdateItemMessageInterface
     */
    public function setWebsiteId(int $websiteId): UpdateItemMessageInterface
    {
        return $this->setData(self::WEBSITE_ID, $websiteId);
    }

    /**
     * @return int|null
     */
    public function getWebsiteId(): ?int
    {
        $websiteId = $this->_get(self::WEBSITE_ID);

        if ($websiteId !== null) {
            $websiteId = (int)$websiteId;
        }

        return $websiteId;
    }
}
