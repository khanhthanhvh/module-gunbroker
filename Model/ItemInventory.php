<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Magento\Framework\Model\AbstractModel;
use Reeds\GunBroker\Api\Data\ItemInventoryInterface;
use Reeds\GunBroker\Model\ResourceModel\ItemInventory as ResourceModel;

class ItemInventory extends AbstractModel implements ItemInventoryInterface
{
    /**
     * @inheritDoc
     */
    protected $_eventPrefix = 'gunbroker_item_inventory';

    public function setInventoryId(?int $inventoryId): ItemInventoryInterface
    {
        return $this->setData(self::KEY_INVENTORY_ID, $inventoryId);
    }

    public function getInventoryId(): ?int
    {
        $inventoryId = $this->getData(self::KEY_INVENTORY_ID);

        return $inventoryId !== null ? (int)$inventoryId : null;
    }

    public function setProductSku(string $productSku): ItemInventoryInterface
    {
        return $this->setData(self::KEY_PRODUCT_SKU, $productSku);
    }

    public function getProductSku(): string
    {
        return $this->getData(self::KEY_PRODUCT_SKU);
    }

    public function setItemId(int $itemId): ItemInventoryInterface
    {
        return $this->setData(self::KEY_ITEM_ID, $itemId);
    }

    public function getItemId(): int
    {
        return (int)$this->getData(self::KEY_ITEM_ID);
    }

    public function setQuantity(int $quantity): ItemInventoryInterface
    {
        return $this->setData(self::KEY_QUANTITY, $quantity);
    }

    public function getQuantity(): int
    {
        return (int)$this->getData(self::KEY_QUANTITY);
    }

    public function setStatus(string $status): ItemInventoryInterface
    {
        return $this->setData(self::KEY_STATUS, $status);
    }

    public function getStatus(): string
    {
        return $this->getData(self::KEY_STATUS);
    }

    public function setUpdated(string $updated): ItemInventoryInterface
    {
        return $this->setData(self::KEY_UPDATED, $updated);
    }

    public function getUpdated(): string
    {
        return $this->getData(self::KEY_UPDATED);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
