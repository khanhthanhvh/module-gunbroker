<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use DateTime;
use DateTimeInterface;
use Exception;
use JsonException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Model\AbstractModel;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterface;
use Reeds\GunBroker\Model\ResourceModel\ItemListedEmail as ItemListedEmailResourceModel;

use function is_array;
use function is_string;
use function json_decode;
use function json_encode;

use const JSON_PRESERVE_ZERO_FRACTION;
use const JSON_THROW_ON_ERROR;

class ItemListedEmail extends AbstractModel implements ItemListedEmailInterface
{
    /**
     * @inheriDoc
     */
    protected $_eventPrefix = 'gunbroker_item_listed_email';
    /**
     * @inheriDoc
     */
    protected $_eventObject = 'item_listed_email';

    public function setEmailId(int $emailId): ItemListedEmailInterface
    {
        $this->setData(self::EMAIL_ID, $emailId);

        return $this;
    }

    public function getEmailId(): ?int
    {
        $emailId = $this->getData(self::EMAIL_ID);

        return $emailId !== null ? (int)$emailId : null;
    }

    public function setItemId(int $emailId): ItemListedEmailInterface
    {
        $this->setData(self::ITEM_ID, $emailId);

        return $this;
    }

    public function getItemId(): int
    {
        return (int)($this->getData(self::ITEM_ID));
    }

    public function setStoreId(int $storeId): ItemListedEmailInterface
    {
        $this->setData(self::STORE_ID, $storeId);

        return $this;
    }

    /**
     * @inheriDoc
     */
    public function getStoreId(): int
    {
        return (int)$this->getData(self::STORE_ID);
    }

    /**
     * @inheriDoc
     * @throws JsonException
     */
    public function setItemData($itemData): ItemListedEmailInterface
    {
        if (!is_string($itemData) && !is_array($itemData)) {
            throw InputException::invalidFieldValue('itemData', $itemData);
        }

        if (is_array($itemData)) {
            $itemData = json_encode($itemData, JSON_THROW_ON_ERROR | JSON_PRESERVE_ZERO_FRACTION);
        }

        $this->setData(self::ITEM_DATA, $itemData);

        return $this;
    }

    /**
     * @inheriDoc
     * @throws JsonException
     */
    public function getItemData(): array
    {
        $itemData = $this->getData(self::ITEM_DATA);

        if ($itemData === null) {
            return [];
        }

        return json_decode($itemData, true, 512, JSON_THROW_ON_ERROR);
    }

    public function setItemListingUrl(string $itemListingUrl): ItemListedEmailInterface
    {
        $this->setData(self::ITEM_LISTING_URL, $itemListingUrl);

        return $this;
    }

    public function getItemListingUrl(): string
    {
        return (string)($this->getData(self::ITEM_LISTING_URL) ?? '');
    }

    public function setProductUrl(string $productUrl): ItemListedEmailInterface
    {
        $this->setData(self::PRODUCT_URL, $productUrl);

        return $this;
    }

    public function getProductUrl(): string
    {
        return (string)($this->getData(self::PRODUCT_URL) ?? '');
    }

    /**
     * @inheriDoc
     */
    public function setSentAt($sentAt): ItemListedEmailInterface
    {
        if ($sentAt instanceof DateTimeInterface) {
            $sentAt = $sentAt->format('Y-m-d H:i:s');
        }

        $this->setData(self::SENT_AT, $sentAt);

        return $this;
    }

    /**
     * @inheriDoc
     * @throws Exception
     */
    public function getSentAt(bool $raw = true)
    {
        /** @var string|null $sentAt */
        $sentAt = $this->getData(self::SENT_AT);

        if ($raw || $sentAt === null) {
            return $sentAt;
        }

        return new DateTime($sentAt);
    }

    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        $this->_init(ItemListedEmailResourceModel::class);
    }
}
