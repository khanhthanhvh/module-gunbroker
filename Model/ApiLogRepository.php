<?php

/**
 * @noinspection PhpUndefinedMethodInspection
 * @noinspection PhpParamsInspection
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\ApiLogRepositoryInterface;
use Reeds\GunBroker\Api\Data\ApiLogInterface;
use Reeds\GunBroker\Model\ResourceModel\ApiLog as ApiLogResourceModel;
use Reeds\GunBroker\Model\ResourceModel\ApiLog\Collection as ApiLogCollection;
use Reeds\GunBroker\Model\ResourceModel\ApiLog\CollectionFactory as ApiLogCollectionFactory;

use function __;
use function strtoupper;

class ApiLogRepository implements ApiLogRepositoryInterface
{
    private SearchResultsInterfaceFactory $searchResultsFactory;
    private CollectionProcessorInterface $collectionProcessor;
    private SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory;
    private ApiLogFactory $apiLogFactory;
    private ApiLogResourceModel $resourceModel;
    private ApiLogCollectionFactory $collectionFactory;

    public function __construct(
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        ApiLogFactory $apiLogFactory,
        ApiLogResourceModel $resourceModel,
        ApiLogCollectionFactory $collectionFactory
    ) {
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->apiLogFactory = $apiLogFactory;
        $this->resourceModel = $resourceModel;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(ApiLogInterface $apiLog): ApiLogInterface
    {
        try {
            $this->resourceModel->save($apiLog);
        } catch (Exception $e) {
            throw new CouldNotSaveException(
                __(
                    'Could not save API log record with identifier "%s". Error: %s',
                    $apiLog->getLogId() ?? '',
                    $e->getMessage()
                ),
                $e
            );
        }

        if ($apiLog->getLogId() === null) {
            throw new CouldNotSaveException(
                __('Could not get identifier of API log record. Please check if it was saved properly.')
            );
        }

        return $this->getById($apiLog->getLogId());
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $logId): ApiLogInterface
    {
        /** @var ApiLog $apiLog */
        $apiLog = $this->apiLogFactory->create();

        $this->resourceModel->load($apiLog, $logId);

        if (!$apiLog->getLogId()) {
            throw new NoSuchEntityException(__('An API log record with the identifier "%s" was not found.', $logId));
        }

        return $apiLog;
    }

    /**
     * @throws LocalizedException
     */
    public function getByStatus(string $status): SearchResultsInterface
    {
        $status = strtoupper($status);

        if ($status !== 'SUCCESS' && $status !== 'FAILURE') {
            throw new LocalizedException(__('Status must be either "SUCCESS" or "FAILURE".'));
        }

        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = $this->searchCriteriaBuilderFactory->create();

        $searchCriteriaBuilder->addFilter('status', $status);

        return $this->getList($searchCriteriaBuilder->create());
    }

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        /** @var ApiLogCollection<ApiLogInterface> $apiLogCollection */
        $apiLogCollection = $this->collectionFactory->create();
        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        $this->collectionProcessor->process($searchCriteria, $apiLogCollection);

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($apiLogCollection->getItems());
        $searchResults->setTotalCount($apiLogCollection->getSize());

        return $searchResults;
    }

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(ApiLogInterface $apiLog): bool
    {
        try {
            $this->resourceModel->delete($apiLog);
        } catch (Exception $e) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete API log record with identifier "%s". Error: %s',
                    $apiLog->getLogId(),
                    $e->getMessage()
                ),
                $e
            );
        }

        return true;
    }

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $logId): bool
    {
        return $this->delete($this->getById($logId));
    }
}
