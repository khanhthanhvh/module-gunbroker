<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\ScopeInterface;
use Reeds\GunBroker\Api\ConfigInterface;

use function is_array;

class Config implements ConfigInterface
{
    private const CONFIG_PATH_ENABLED = 'gunbroker/integration/enabled';
    private const CONFIG_PATH_EMAIL_SENDER_IDENTITY = 'gunbroker/email/sender_identity';
    private const CONFIG_PATH_EMAIL_RECIPIENT_IDENTITY = 'gunbroker/email/recipient_identity';
    private const CONFIG_PATH_EMAIL_CONFIRM_ITEM_LISTED = 'gunbroker/email/confirm_item_listed';
    private const CONFIG_PATH_EMAIL_ITEM_LISTED_TEMPLATE = 'gunbroker/email/item_listed_template';
    private const CONFIG_PATH_EMAIL_SEND_ASYNCHRONOUSLY = 'gunbroker/email/send_asynchronously';
    private const CONFIG_PATH_EMAIL_BATCH_SIZE = 'gunbroker/email/batch_size';
    private const CONFIG_PATH_API_ENVIRONMENT = 'gunbroker/api/environment';
    private const CONFIG_PATH_API_DEVKEY = 'gunbroker/api/devkey';
    private const CONFIG_PATH_API_SANDBOX_USERNAME = 'gunbroker/api/sandbox_username';
    private const CONFIG_PATH_API_SANDBOX_PASSWORD = 'gunbroker/api/sandbox_password';
    private const CONFIG_PATH_API_PRODUCTION_USERNAME = 'gunbroker/api/production_username';
    private const CONFIG_PATH_API_PRODUCTION_PASSWORD = 'gunbroker/api/production_password';
    private const CONFIG_PATH_API_USER_ID = 'gunbroker/api/user_id';
    private const CONFIG_PATH_ATTRIBUTES_INSPECTION_PERIOD = 'gunbroker/attributes/inspection_period';
    private const CONFIG_PATH_ATTRIBUTES_PAYMENT_METHODS = 'gunbroker/attributes/payment_methods';
    private const CONFIG_PATH_ATTRIBUTES_POSTAL_CODE = 'gunbroker/attributes/postal_code';
    private const CONFIG_PATH_ATTRIBUTES_SALES_TAXES = 'gunbroker/attributes/sales_taxes';
    private const CONFIG_PATH_ATTRIBUTES_STANDARD_TEXT_ID = 'gunbroker/attributes/standard_text_id';
    private const CONFIG_PATH_ATTRIBUTES_USE_DEFAULT_SALES_TAX = 'gunbroker/attributes/use_default_sales_tax';
    private const CONFIG_PATH_ATTRIBUTES_WILL_SHIP_INTERNATIONAL = 'gunbroker/attributes/will_ship_international';
    private const CONFIG_PATH_LOGGING_ENABLED = 'gunbroker/logging/enabled';
    private const CONFIG_PATH_FEEDBACK_ENABLED = 'gunbroker/feedback/enabled';
    private const CONFIG_PATH_FEEDBACK_DEFAULT_COMMENT = 'gunbroker/feedback/default_comment';
    private const CONFIG_PATH_FEEDBACK_DEFAULT_RATING = 'gunbroker/feedback/default_rating';

    private ScopeConfigInterface $scopeConfig;
    private SerializerInterface $serializer;

    public function __construct(ScopeConfigInterface $scopeConfig, SerializerInterface $serializer)
    {
        $this->scopeConfig = $scopeConfig;
        $this->serializer = $serializer;
    }

    public function isEnabled(?int $websiteId = null): bool
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->isSetFlag(self::CONFIG_PATH_ENABLED, $scope, $websiteId);
    }

    public function isLoggingEnabled(?int $websiteId = null): bool
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->isSetFlag(self::CONFIG_PATH_LOGGING_ENABLED, $scope, $websiteId);
    }

    public function getEmailSenderIdentity(?int $storeId = null): string
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_EMAIL_SENDER_IDENTITY, $scope, $storeId);
    }

    public function getEmailRecipientIdentity(?int $storeId = null): string
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_EMAIL_RECIPIENT_IDENTITY, $scope, $storeId);
    }

    public function sendItemListedConfirmationEmail(?int $storeId = null): bool
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->isSetFlag(self::CONFIG_PATH_EMAIL_CONFIRM_ITEM_LISTED, $scope, $storeId);
    }

    public function getItemListedConfirmationEmailTemplate(?int $storeId = null): string
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_EMAIL_ITEM_LISTED_TEMPLATE, $scope, $storeId);
    }

    public function getSendEmailsAsynchronously(?int $storeId = null): bool
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->isSetFlag(self::CONFIG_PATH_EMAIL_SEND_ASYNCHRONOUSLY, $scope, $storeId);
    }

    public function getEmailBatchSize(?int $storeId = null): int
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return (int)$this->scopeConfig->getValue(self::CONFIG_PATH_EMAIL_BATCH_SIZE, $scope, $storeId);
    }

    public function getApiEnvironment(?int $websiteId = null): string
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_API_ENVIRONMENT, $scope, $websiteId) ?? 'sandbox';
    }

    public function getApiDevKey(?int $websiteId = null): string
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_API_DEVKEY, $scope, $websiteId) ?? '';
    }

    public function getApiSandboxUsername(?int $websiteId = null): string
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_API_SANDBOX_USERNAME, $scope, $websiteId) ?? '';
    }

    public function getApiSandboxPassword(?int $websiteId = null): string
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_API_SANDBOX_PASSWORD, $scope, $websiteId) ?? '';
    }

    public function getApiProductionUsername(?int $websiteId = null): string
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_API_PRODUCTION_USERNAME, $scope, $websiteId) ?? '';
    }

    public function getApiProductionPassword(?int $websiteId = null): string
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_API_PRODUCTION_PASSWORD, $scope, $websiteId) ?? '';
    }

    public function getApiUserId(?int $websiteId = null): ?int
    {
        $scope = $websiteId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_WEBSITE;
        $userId = $this->scopeConfig->getValue(self::CONFIG_PATH_API_USER_ID, $scope, $websiteId);

        if ($userId !== null) {
            $userId = (int)$userId;
        }

        return $userId;
    }

    public function getApiCredentials(?int $websiteId = null): array
    {
        $environment = $this->getApiEnvironment($websiteId);

        return [
            'username' => $environment === 'sandbox' ? $this->getApiSandboxUsername($websiteId)
                : $this->getApiProductionUsername($websiteId),
            'password' => $environment === 'sandbox' ? $this->getApiSandboxPassword($websiteId)
                : $this->getApiProductionPassword($websiteId)
        ];
    }

    public function getInspectionPeriod(?int $storeId = null): int
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return (int)$this->scopeConfig->getValue(self::CONFIG_PATH_ATTRIBUTES_INSPECTION_PERIOD, $scope, $storeId);
    }

    /**
     * @return array<string, array{method: string, accepted: int}>
     */
    public function getPaymentMethods(?int $storeId = null): array
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;
        $paymentMethods = $this->scopeConfig->getValue(self::CONFIG_PATH_ATTRIBUTES_PAYMENT_METHODS, $scope, $storeId)
            ?? [];

        return !is_array($paymentMethods) ? (array)$this->serializer->unserialize($paymentMethods) : $paymentMethods;
    }

    public function getPostalCode(?int $storeId = null): string
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_ATTRIBUTES_POSTAL_CODE, $scope, $storeId) ?? '';
    }

    /**
     * @return array<string, array{state: string, tax_rate: float}>
     */
    public function getSalesTaxes(?int $storeId = null): array
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;
        $salesTaxes = $this->scopeConfig->getValue(self::CONFIG_PATH_ATTRIBUTES_SALES_TAXES, $scope, $storeId) ?? [];

        return !is_array($salesTaxes) ? (array)$this->serializer->unserialize($salesTaxes) : $salesTaxes;
    }

    public function getStandardTextId(?int $storeId = null): int
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return (int)$this->scopeConfig->getValue(self::CONFIG_PATH_ATTRIBUTES_STANDARD_TEXT_ID, $scope, $storeId);
    }

    public function getUseDefaultSalesTax(?int $storeId = null): bool
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->isSetFlag(self::CONFIG_PATH_ATTRIBUTES_USE_DEFAULT_SALES_TAX, $scope, $storeId);
    }

    public function getWillShipInternational(?int $storeId = null): bool
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->isSetFlag(self::CONFIG_PATH_ATTRIBUTES_WILL_SHIP_INTERNATIONAL, $scope, $storeId);
    }

    public function isFeedbackEnabled(?int $storeId = null): bool
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->isSetFlag(self::CONFIG_PATH_FEEDBACK_ENABLED, $scope, $storeId);
    }

    public function getDefaultFeedbackComment(?int $storeId = null): string
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;

        return $this->scopeConfig->getValue(self::CONFIG_PATH_FEEDBACK_DEFAULT_COMMENT, $scope, $storeId) ?? '';
    }

    public function getDefaultFeedbackRating(?int $storeId = null): int
    {
        $scope = $storeId === null ? ScopeConfigInterface::SCOPE_TYPE_DEFAULT : ScopeInterface::SCOPE_STORE;
        $defaultRating = $this->scopeConfig->getValue(self::CONFIG_PATH_FEEDBACK_DEFAULT_RATING, $scope, $storeId);

        return $defaultRating !== null ? (int)$defaultRating : 5;
    }
}
