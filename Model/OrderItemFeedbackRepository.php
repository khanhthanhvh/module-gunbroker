<?php

/** @noinspection PhpParamsInspection */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterface;
use Reeds\GunBroker\Api\Data\OrderItemFeedbackInterfaceFactory;
use Reeds\GunBroker\Api\OrderItemFeedbackRepositoryInterface;
use Reeds\GunBroker\Model\ResourceModel\OrderItemFeedback as OrderItemFeedbackResourceModel;
use Reeds\GunBroker\Model\ResourceModel\OrderItemFeedback\Collection as OrderItemFeedbackCollection;
use Reeds\GunBroker\Model\ResourceModel\OrderItemFeedback\CollectionFactory as OrderItemFeedbackCollectionFactory;

class OrderItemFeedbackRepository implements OrderItemFeedbackRepositoryInterface
{
    private OrderItemFeedbackResourceModel $resourceModel;
    private OrderItemFeedbackInterfaceFactory $orderItemFeedbackFactory;
    private OrderItemFeedbackCollectionFactory $collectionFactory;
    private SearchResultsInterfaceFactory $searchResultsFactory;
    private CollectionProcessorInterface $collectionProcessor;
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    public function __construct(
        OrderItemFeedbackResourceModel $resourceModel,
        OrderItemFeedbackInterfaceFactory $orderItemFeedbackFactory,
        OrderItemFeedbackCollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->resourceModel = $resourceModel;
        $this->orderItemFeedbackFactory = $orderItemFeedbackFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @inheritDoc
     */
    public function save(OrderItemFeedbackInterface $orderItemFeedback): OrderItemFeedbackInterface
    {
        try {
            $this->resourceModel->save($orderItemFeedback);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save order item feedback record with identifier "%1". Error: %2',
                    $orderItemFeedback->getId(),
                    $exception->getMessage()
                ),
                $exception
            );
        }

        if ($orderItemFeedback->getId() === null) {
            throw new CouldNotSaveException(
                __('Could not get identifier of order item feedback record. Please check if it was saved properly.')
            );
        }

        return $this->getById($orderItemFeedback->getId());
    }

    /**
     * @inheritDoc
     */
    public function getById(int $id): OrderItemFeedbackInterface
    {
        /** @var OrderItemFeedbackInterface $orderItemFeedback */
        $orderItemFeedback = $this->orderItemFeedbackFactory->create();

        $this->resourceModel->load($orderItemFeedback, $id);

        if ($orderItemFeedback->getId() === null) {
            throw new NoSuchEntityException(
                __('An order item feedback record with the identifier "%1" was not found.', $id)
            );
        }

        return $orderItemFeedback;
    }

    /**
     * @inheritDoc
     */
    public function getByMagentoOrderItemId(int $magentoOrderItemId, ?string $type = null): array
    {
        $searchCriteriaBuilder = $this->searchCriteriaBuilder->addFilter('magento_order_item_id', $magentoOrderItemId);

        if ($type !== null) {
            $searchCriteriaBuilder->addFilter('type', $type);
        }

        $searchCriteria = $searchCriteriaBuilder->create();
        $searchResults = $this->getList($searchCriteria);

        if ($searchResults->getTotalCount() === 0) {
            throw new NoSuchEntityException(__('No feedback was found for order item %1.', $magentoOrderItemId));
        }

        return $searchResults->getItems();
    }

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        /** @var OrderItemFeedbackCollection<OrderItemFeedbackInterface> $orderItemFeedbackCollection */
        $orderItemFeedbackCollection = $this->collectionFactory->create();
        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        $this->collectionProcessor->process($searchCriteria, $orderItemFeedbackCollection);

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($orderItemFeedbackCollection->getItems());
        $searchResults->setTotalCount($orderItemFeedbackCollection->getSize());

        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(OrderItemFeedbackInterface $orderItemFeedback): bool
    {
        try {
            $this->resourceModel->delete($orderItemFeedback);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete order item feedback record with identifier "%1". Error: %2',
                    $orderItemFeedback->getId(),
                    $exception->getMessage()
                ),
                $exception
            );
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->getById($id));
    }
}
