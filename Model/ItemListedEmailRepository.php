<?php

/** @noinspection PhpParamsInspection */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterface;
use Reeds\GunBroker\Api\Data\ItemListedEmailInterfaceFactory;
use Reeds\GunBroker\Api\ItemListedEmailRepositoryInterface;
use Reeds\GunBroker\Model\ResourceModel\ItemListedEmail as ItemListedEmailResourceModel;
use Reeds\GunBroker\Model\ResourceModel\ItemListedEmail\Collection as ItemListedEmailCollection;
use Reeds\GunBroker\Model\ResourceModel\ItemListedEmail\CollectionFactory as ItemListedEmailCollectionFactory;

use function __;

class ItemListedEmailRepository implements ItemListedEmailRepositoryInterface
{
    private ItemListedEmailResourceModel $resourceModel;
    private ItemListedEmailInterfaceFactory $itemListedEmailFactory;
    private ItemListedEmailCollectionFactory $collectionFactory;
    private SearchResultsInterfaceFactory $searchResultsFactory;
    private CollectionProcessorInterface $collectionProcessor;

    public function __construct(
        ItemListedEmailResourceModel $resourceModel,
        ItemListedEmailInterfaceFactory $itemListedEmailFactory,
        ItemListedEmailCollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resourceModel = $resourceModel;
        $this->itemListedEmailFactory = $itemListedEmailFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @throws CouldNotSaveException
     */
    public function save(ItemListedEmailInterface $itemListedEmail): ItemListedEmailInterface
    {
        try {
            $this->resourceModel->save($itemListedEmail);
        } catch (Exception $e) {
            throw new CouldNotSaveException(__('Could not save item listed e-mail. Error: %1', $e->getMessage()), $e);
        }

        return $itemListedEmail;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $emailId): ItemListedEmailInterface
    {
        /** @var ItemListedEmailInterface $itemListedEmail */
        $itemListedEmail = $this->itemListedEmailFactory->create();

        $this->resourceModel->load($itemListedEmail, $emailId);

        if ($itemListedEmail->getEmailId() === null) {
            throw new NoSuchEntityException(
                __('Could not find an item listed e-mail with the identifier "%1." ', $emailId)
            );
        }

        return $itemListedEmail;
    }

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        /** @var ItemListedEmailCollection<ItemListedEmailInterface> $itemListedEmailCollection */
        $itemListedEmailCollection = $this->collectionFactory->create();
        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        $this->collectionProcessor->process($searchCriteria, $itemListedEmailCollection);

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($itemListedEmailCollection->getItems());
        $searchResults->setTotalCount($itemListedEmailCollection->getSize());

        return $searchResults;
    }

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(ItemListedEmailInterface $itemListedEmail): bool
    {
        try {
            $this->resourceModel->delete($itemListedEmail);
        } catch (Exception $e) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete item listed e-mail with identifier "%1." Error: %2',
                    $itemListedEmail->getEmailId(),
                    $e->getMessage()
                ),
                $e
            );
        }

        return true;
    }

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $emailId): bool
    {
        return $this->delete($this->getById($emailId));
    }
}
