<?php

/** @noinspection MagicMethodsValidityInspection */

// phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use DateTime;
use JsonException;
use Magento\Framework\Model\AbstractModel;
use Reeds\GunBroker\Api\Data\ApiLogInterface;

use function is_string;
use function json_decode;
use function json_encode;

class ApiLog extends AbstractModel implements ApiLogInterface
{
    protected $_eventPrefix = 'gunbroker_api_log';
    protected $_eventObject = 'apilog';

    public function setLogId(int $logId): ApiLogInterface
    {
        $this->setData(self::LOG_ID, $logId);

        return $this;
    }

    public function getLogId(): ?int
    {
        $logId = $this->getData(self::LOG_ID);

        return $logId !== null ? (int)$logId : null;
    }

    public function setHttpMethod(string $httpMethod): ApiLogInterface
    {
        $this->setData(self::HTTP_METHOD, $httpMethod);

        return $this;
    }

    public function getHttpMethod(): string
    {
        return (string)$this->getData(self::HTTP_METHOD);
    }

    public function setHttpStatus(string $httpStatus): ApiLogInterface
    {
        $this->setData(self::HTTP_STATUS, $httpStatus);

        return $this;
    }

    public function getHttpStatus(): string
    {
        return (string)$this->getData(self::HTTP_STATUS);
    }

    public function setRequestUrl(string $requestUrl): ApiLogInterface
    {
        $this->setData(self::REQUEST_URL, $requestUrl);

        return $this;
    }

    public function getRequestUrl(): string
    {
        return (string)$this->getData(self::REQUEST_URL);
    }

    /**
     * @param string|mixed[]|object $messageData
     * @throws JsonException
     */
    public function setMessageData($messageData): ApiLogInterface
    {
        if (!is_string($messageData)) {
            $messageData = json_encode($messageData, JSON_THROW_ON_ERROR);
        }

        $this->setData(self::MESSAGE_DATA, $messageData);

        return $this;
    }

    /**
     * @return object|string|null
     * @throws JsonException
     */
    public function getMessageData(bool $asJson = true)
    {
        $messageData = $this->getData(self::MESSAGE_DATA);

        if ($messageData === null && $asJson) {
            return $messageData;
        }

        return $asJson ? json_decode($messageData, false, 512, JSON_THROW_ON_ERROR) : (string)$messageData;
    }

    public function setExceptionMessage(string $exceptionMessage): ApiLogInterface
    {
        $this->setData(self::EXCEPTION_MESSAGE, $exceptionMessage);

        return $this;
    }

    public function getExceptionMessage(): string
    {
        return (string)$this->getData(self::EXCEPTION_MESSAGE);
    }

    public function setStatus(string $status): ApiLogInterface
    {
        $this->setData(self::STATUS, $status);

        return $this;
    }

    public function getStatus(): string
    {
        return (string)$this->getData(self::STATUS);
    }

    /**
     * @param string|DateTime $recordedAt
     */
    public function setRecordedAt($recordedAt): ApiLogInterface
    {
        if ($recordedAt instanceof DateTime) {
            $recordedAt = $recordedAt->format('Y-m-d H:i:s');
        }

        $this->setData(self::RECORDED_AT, $recordedAt);

        return $this;
    }

    /**
     * @return string|DateTime
     */
    public function getRecordedAt(bool $raw = true)
    {
        $recordedAt = $this->getData(self::RECORDED_AT);

        return $raw ? (string)$recordedAt : new DateTime($recordedAt);
    }

    protected function _construct()
    {
        parent::_construct();

        $this->_init(ResourceModel\ApiLog::class);
    }
}
