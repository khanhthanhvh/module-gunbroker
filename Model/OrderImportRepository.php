<?php

/** @noinspection PhpParamsInspection */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Reeds\GunBroker\Api\Data\OrderImportInterface;
use Reeds\GunBroker\Api\Data\OrderImportInterfaceFactory;
use Reeds\GunBroker\Api\OrderImportRepositoryInterface;
use Reeds\GunBroker\Model\ResourceModel\OrderImport as OrderImportResourceModel;
use Reeds\GunBroker\Model\ResourceModel\OrderImport\Collection as OrderImportCollection;
use Reeds\GunBroker\Model\ResourceModel\OrderImport\CollectionFactory as OrderImportCollectionFactory;

use function __;

class OrderImportRepository implements OrderImportRepositoryInterface
{
    private OrderImportResourceModel $resourceModel;
    private OrderImportInterfaceFactory $orderImportFactory;
    private OrderImportCollectionFactory $collectionFactory;
    private SearchResultsInterfaceFactory $searchResultsFactory;
    private CollectionProcessorInterface $collectionProcessor;

    public function __construct(
        OrderImportResourceModel $resourceModel,
        OrderImportInterfaceFactory $orderImportFactory,
        OrderImportCollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resourceModel = $resourceModel;
        $this->orderImportFactory = $orderImportFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    public function save(OrderImportInterface $orderImport): OrderImportInterface
    {
        try {
            $this->resourceModel->save($orderImport);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save order import with identifier "%1". Error: %2',
                    $orderImport->getImportId(),
                    $exception->getMessage()
                ),
                $exception
            );
        }

        if ($orderImport->getImportId() === null) {
            throw new CouldNotSaveException(
                __('Could not get identifier of order import. Please check if it was saved properly.')
            );
        }

        return $this->getById($orderImport->getImportId());
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getById(int $orderImportId): OrderImportInterface
    {
        /** @var OrderImport $orderImport */
        $orderImport = $this->orderImportFactory->create();

        $this->resourceModel->load($orderImport, $orderImportId);

        if (!$orderImport->getImportId()) {
            throw new NoSuchEntityException(
                __('An order import with the identifier "%1" was not found.', $orderImportId)
            );
        }

        return $orderImport;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function getByGunBrokerOrderId(int $gunBrokerOrderId): OrderImportInterface
    {
        /** @var OrderImport $orderImport */
        $orderImport = $this->orderImportFactory->create();

        $this->resourceModel->load($orderImport, $gunBrokerOrderId, 'gunbroker_order_id');

        if ($orderImport->getImportId() === null) {
            throw new NoSuchEntityException(
                __('An order import with the GunBroker.com order identifier "%1" was not found.', $gunBrokerOrderId)
            );
        }

        return $orderImport;
    }

    public function getList(SearchCriteriaInterface $searchCriteria): SearchResultsInterface
    {
        /** @var OrderImportCollection<OrderImportInterface> $orderImportCollection */
        $orderImportCollection = $this->collectionFactory->create();
        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        $this->collectionProcessor->process($searchCriteria, $orderImportCollection);

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($orderImportCollection->getItems());
        $searchResults->setTotalCount($orderImportCollection->getSize());

        return $searchResults;
    }

    /**
     * @throws CouldNotDeleteException
     */
    public function delete(OrderImportInterface $orderImport): bool
    {
        try {
            $this->resourceModel->delete($orderImport);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete order import with identifier "%1". Error: %2',
                    $orderImport->getImportId(),
                    $exception->getMessage()
                ),
                $exception
            );
        }

        return true;
    }

    /**
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById(int $orderImportId): bool
    {
        return $this->delete($this->getById($orderImportId));
    }
}
