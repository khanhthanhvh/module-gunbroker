<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Phrase;

use function __;

class ApiEnvironment implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @return array<array{value: string, label: Phrase}>
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => 'sandbox',
                'label' => __('Sandbox')
            ],
            [
                'value' => 'production',
                'label' => __('Production')
            ]
        ];
    }
}
