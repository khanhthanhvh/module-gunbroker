<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class FeedbackRating implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @return array<array{value: string, label: string}>
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => '',
                'label' => '&nbsp;'
            ],
            [
                'value' => '5',
                'label' => 'A+'
            ],
            [
                'value' => '4',
                'label' => 'A'
            ],
            [
                'value' => '3',
                'label' => 'B'
            ],
            [
                'value' => '2',
                'label' => 'C'
            ],
            [
                'value' => '1',
                'label' => 'D'
            ],
            [
                'value' => '0',
                'label' => 'F'
            ],
        ];
    }
}
