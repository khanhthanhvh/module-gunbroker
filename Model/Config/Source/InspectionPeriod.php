<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Phrase;

class InspectionPeriod implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @return array<array{value: string, label: Phrase}>
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => '1',
                'label' => __('AS IS - No refund or exchange')
            ],
            [
                'value' => '2',
                'label' => __('No refund but item can be returned for exchange or store credit within fourteen days')
            ],
            [
                'value' => '3',
                'label' => __('No refund but item can be returned for exchange or store credit within thirty days')
            ],
            [
                'value' => '4',
                'label' => __('Three days from the date the item is received')
            ],
            [
                'value' => '5',
                'label' => __('Three days from the date the item is received, including the cost of shipping')
            ],
            [
                'value' => '6',
                'label' => __('Five days from the date the item is received')
            ],
            [
                'value' => '7',
                'label' => __('Five days from the date the item is received, including the cost of shipping')
            ],
            [
                'value' => '8',
                'label' => __('Seven days from the date the item is received')
            ],
            [
                'value' => '9',
                'label' => __('Seven days from the date the item is received, including the cost of shipping')
            ],
            [
                'value' => '10',
                'label' => __('Fourteen days from the date the item is received')
            ],
            [
                'value' => '11',
                'label' => __('Fourteen Days from the date the item is received, including the cost of shipping')
            ],
            [
                'value' => '12',
                'label' => __('30 day money back guarantee')
            ],
            [
                'value' => '13',
                'label' => __('30 day money back guarantee including the cost of shipping')
            ],
            [
                'value' => '14',
                'label' => __('Factory Warranty')
            ]
        ];
    }
}
