<?php

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Magento\Framework\Api\AbstractSimpleObject;
use Reeds\GunBroker\Api\Data\ListItemMessageInterface;

class ListItemMessage extends AbstractSimpleObject implements ListItemMessageInterface
{
    /**
     * @param int $productId
     * @return ListItemMessageInterface
     */
    public function setProductId(int $productId): ListItemMessageInterface
    {
        $this->setData(self::KEY_PRODUCT_ID, $productId);

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->_get(self::KEY_PRODUCT_ID);
    }

    /**
     * @param int $storeId
     * @return ListItemMessageInterface
     */
    public function setStoreId(int $storeId): ListItemMessageInterface
    {
        $this->setData(self::KEY_STORE_ID, $storeId);

        return $this;
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return (int)$this->_get(self::KEY_STORE_ID);
    }
}
