<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel\ItemListedEmail;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Reeds\GunBroker\Model\ItemListedEmail as ItemListedEmailModel;
use Reeds\GunBroker\Model\ResourceModel\ItemListedEmail as ItemListedEmailResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @inheritDoc
     */
    protected $_eventPrefix = 'gunbroker_item_listed_email_collection';
    /**
     * @inheritDoc
     */
    protected $_eventObject = 'item_listed_email_collection';

    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        $this->_init(ItemListedEmailModel::class, ItemListedEmailResourceModel::class);
    }
}
