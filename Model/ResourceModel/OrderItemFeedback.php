<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OrderItemFeedback extends AbstractDb
{
    public const MAIN_TABLE = 'gunbroker_order_item_feedback';
    public const ID_FIELD_NAME = 'id';

    protected function _construct(): void
    {
        $this->_init(self::MAIN_TABLE, self::ID_FIELD_NAME);
    }
}
