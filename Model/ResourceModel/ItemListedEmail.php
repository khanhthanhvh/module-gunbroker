<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ItemListedEmail extends AbstractDb
{
    public const MAIN_TABLE = 'gunbroker_item_listed_email';
    public const ID_FIELD_NAME = 'email_id';

    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        $this->_init(self::MAIN_TABLE, self::ID_FIELD_NAME);
    }
}
