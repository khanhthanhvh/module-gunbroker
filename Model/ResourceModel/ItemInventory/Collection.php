<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel\ItemInventory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Reeds\GunBroker\Model\ItemInventory as Model;
use Reeds\GunBroker\Model\ResourceModel\ItemInventory as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @inheritDoc
     */
    protected $_eventPrefix = 'gunbroker_item_inventory';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
