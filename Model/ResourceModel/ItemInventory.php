<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ItemInventory extends AbstractDb
{
    public const MAIN_TABLE = 'gunbroker_item_inventory';
    public const ID_FIELD_NAME = 'inventory_id';

    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        $this->_init(self::MAIN_TABLE, self::ID_FIELD_NAME);
    }

    /**
     * @inheritDoc
     */
    protected function _initUniqueFields(): self
    {
        parent::_initUniqueFields();

        $this->_uniqueFields[] = [
            'field' => 'item_id',
            'title' => __('Only one quantity per item is allowed')
        ];

        return $this;
    }
}
