<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel;

use Exception;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class OrderImport extends AbstractDb
{
    public const TABLE_NAME = 'gunbroker_order_import';
    public const IDENTITY_COLUMN = 'import_id';

    /**
     * @inheritDoc
     */
    public function save(AbstractModel $object)
    {
        if ($object->isDeleted()) {
            return $this->delete($object);
        }

        $connection = $this->getConnection();

        $connection->beginTransaction();

        try {
            if (!$this->isModified($object)) {
                $this->processNotModifiedSave($object);

                $connection->commit();

                $object->setHasDataChanges(false);

                return $this;
            }

            $object->validateBeforeSave();
            $object->beforeSave();

            if ($object->isSaveAllowed()) {
                $this->_serializeFields($object);
                $this->_beforeSave($object);
                $this->_checkUnique($object);

                $this->objectRelationProcessor->validateDataIntegrity($this->getMainTable(), $object->getData());

                $connection->insertOnDuplicate($this->getMainTable(), $this->_prepareDataForSave($object));

                if ($this->_isPkAutoIncrement && $object->getId() === null) {
                    $object->setId($connection->lastInsertId($this->getMainTable()));
                }

                $this->unserializeFields($object);
                $this->processAfterSaves($object);
            }

            $this->addCommitCallback([$object, 'afterCommitCallback']);

            $connection->commit();

            $object->setHasDataChanges(false);
        } catch (Exception $e) {
            $this->rollBack();

            $object->setHasDataChanges(true);

            throw $e;
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, self::IDENTITY_COLUMN);
    }
}
