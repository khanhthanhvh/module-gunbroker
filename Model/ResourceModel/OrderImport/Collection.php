<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel\OrderImport;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Reeds\GunBroker\Model\OrderImport as Model;
use Reeds\GunBroker\Model\ResourceModel\OrderImport as ResourceModel;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = ResourceModel::IDENTITY_COLUMN;
    /**
     * @var string
     */
    protected $_eventPrefix = 'gunbroker_order_import_collection';
    /**
     * @var string
     */
    protected $_eventObject = 'order_import_collection';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(Model::class, ResourceModel::class);
    }
}
