<?php

// phpcs:disable PSR2.Methods.MethodDeclaration.Underscore

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel\Product;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection as CoreProductCollection;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\DataObject;

use function array_key_exists;
use function get_class;

class Collection extends CoreProductCollection
{
    private bool $isStoreTableJoined = false;

    /**
     * @return Collection<ProductInterface>
     */
    public function joinStoreTable(): self
    {
        if (!isset($this->_productLimitationFilters['website_ids'])) {
            return $this;
        }

        $this->getSelect()->join(
            ['product_website_store' => $this->getTable('store')],
            'product_website_store.website_id = product_website.website_id',
            // Replace above line with below line to allow products with global attributes to be returned
            //'product_website_store.website_id = product_website.website_id OR product_website_store.store_id = 0',
            ['store_id' => 'product_website_store.store_id']
        );

        $this->isStoreTableJoined = true;

        return $this;
    }

    /**
     * @inheritDoc
     * @return Collection<ProductInterface>
     */
    public function addItem(DataObject $item): self
    {
        /**
         * @var string|int|null $storeId
         */
        $storeId = $item->getData('store_id');

        if ($storeId === null) {
            $this->_addItem($item);

            return $this;
        }

        if (array_key_exists($storeId, $this->_items)) {
            // phpcs:ignore Magento2.Exceptions.DirectThrow
            throw new Exception(
                (string)__(
                    'Item "(%1)" with the same store ID "%2" already exists in "%3."',
                    get_class($item),
                    $item->getId(),
                    static::class
                )
            );
        }

        $this->_items[$storeId] = $item;

        return $this;
    }

    /**
     * @inheritDoc
     * @param string[] $condition
     * @return Collection<ProductInterface>
     */
    protected function _joinAttributeToSelect(
        $method,
        $attribute,
        $tableAlias,
        $condition,
        $fieldCode,
        $fieldAlias
    ): self {
        if ($this->isStoreTableJoined) {
            $condition[] = "$tableAlias.store_id = product_website_store.store_id";
        } else {
            if ($fieldCode === 'gunbroker_item_id') {
                $this->getSelect()->columns(['at_store_id' =>  "$tableAlias.store_id"]);
            }
        }

        /* Bypass `\Magento\Catalog\Model\ResourceModel\Collection\AbstractCollection::_joinAttributeToSelect()` to
           avoid adding the store filter. */
        AbstractCollection::_joinAttributeToSelect(
            $method,
            $attribute,
            $tableAlias,
            $condition,
            $fieldCode,
            $fieldAlias
        );

        return $this;
    }

    /**
     * @inheritDoc
     * @return Collection<ProductInterface>
     */
    protected function prepareStoreId(): self
    {
        if ($this->isStoreTableJoined) {
            return $this;
        }

        foreach ($this->_items as $item) {
            if (!$item->hasData('at_store_id')) {
                continue;
            }

            $item->setStoreId((int)$item->getData('at_store_id'));
            $item->unsetData('at_store_id');
        }

        return $this;
    }
}
