<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel\Product;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\AbstractResource;
use Magento\Framework\Exception\LocalizedException;

use function __;

class ItemId extends AbstractResource
{
    /**
     * @param int[] $productIds
     * @throws LocalizedException
     */
    public function removeFromProducts(array $productIds): int
    {
        $deletedIdCount = 0;
        $connection = $this->getConnection();
        $attribute = $this->getAttribute('gunbroker_item_id');

        if ($attribute === false) {
            throw new LocalizedException(__('Could not find "gunbroker_item_id" attribute!'));
        }

        $connection->beginTransaction();

        try {
            $deletedIdCount = $connection->delete(
                $attribute->getBackend()->getTable(),
                [
                    'attribute_id = ?' => $attribute->getAttributeId(),
                    $this->getLinkField() . ' IN (?)' => $this->resolveProductIds($productIds)
                ]
            );

            $connection->commit();
        } catch (Exception $e) {
            $connection->rollBack();
        }

        return $deletedIdCount;
    }

    /**
     * @inheritDoc
     */
    protected function _construct(): void
    {
        parent::_construct();

        $this->setType(Product::ENTITY)->setConnection($this->_resource->getConnection('catalog'));
    }

    /**
     * @param int[] $productIds
     * @return array<int|string>
     */
    private function resolveProductIds(array $productIds): array
    {
        if ($this->getIdFieldName() === $this->getLinkField()) {
            return $productIds;
        }

        $connection = $this->getConnection();
        $select = $connection->select();
        $tableName = $this->_resource->getTableName('catalog_product_entity');

        $select->from($tableName, [$this->getLinkField()])->where('entity_id IN (?)', $productIds);

        return $connection->fetchAll($select);
    }
}
