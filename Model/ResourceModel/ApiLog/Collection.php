<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel\ApiLog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Reeds\GunBroker\Model\ApiLog as ApiLogModel;
use Reeds\GunBroker\Model\ResourceModel\ApiLog as ApiLogResourceModel;

class Collection extends AbstractCollection
{
    protected $_idFieldName = ApiLogResourceModel::IDENTITY_COLUMN;

    protected function _construct()
    {
        $this->_init(ApiLogModel::class, ApiLogResourceModel::class);
    }
}
