<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel\OrderItemFeedback;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Reeds\GunBroker\Model\OrderItemFeedback as OrderItemFeedbackModel;
use Reeds\GunBroker\Model\ResourceModel\OrderItemFeedback as OrderItemFeedbackResourceModel;

class Collection extends AbstractCollection
{
    protected function _construct(): void
    {
        $this->_init(OrderItemFeedbackModel::class, OrderItemFeedbackResourceModel::class);
    }
}
