<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ApiLog extends AbstractDb
{
    public const TABLE_NAME = 'gunbroker_api_log';
    public const IDENTITY_COLUMN = 'log_id';

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, self::IDENTITY_COLUMN);
    }
}
