<?php

/**
 * @noinspection MagicMethodsValidityInspection
 * phpcs:disable PSR2.Classes.PropertyDeclaration.Underscore
 * phpcs:disable PSR2.Methods.MethodDeclaration.Underscore
 */

declare(strict_types=1);

namespace Reeds\GunBroker\Model;

use Magento\Framework\Exception\InputException;
use Magento\Framework\Model\AbstractModel;
use Reeds\GunBroker\Api\Data\OrderImportInterface;
use Reeds\GunBroker\Model\ResourceModel\OrderImport as ResourceModel;

use function __;
use function in_array;

class OrderImport extends AbstractModel implements OrderImportInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'gunbroker_order_import';
    /**
     * @var string
     */
    protected $_eventObject = 'order_import';

    public function setImportId(int $importId): OrderImportInterface
    {
        $this->setData(self::IMPORT_ID, $importId);

        return $this;
    }

    public function getImportId(): ?int
    {
        $importId = $this->getData(self::IMPORT_ID);

        return $importId !== null ? (int)$importId : null;
    }

    public function setGunBrokerOrderId(int $gunBrokerOrderId): OrderImportInterface
    {
        $this->setData(self::GUNBROKER_ORDER_ID, $gunBrokerOrderId);

        return $this;
    }

    public function getGunBrokerOrderId(): int
    {
        return (int)$this->getData(self::GUNBROKER_ORDER_ID);
    }

    public function setMagentoOrderId(int $magentoOrderId): OrderImportInterface
    {
        $this->setData(self::MAGENTO_ORDER_ID, $magentoOrderId);

        return $this;
    }

    public function getMagentoOrderId(): ?int
    {
        $magentoOrderId = $this->getData(self::MAGENTO_ORDER_ID);

        return $magentoOrderId !== null ? (int)$magentoOrderId : null;
    }

    public function setImportStatus(string $importStatus): OrderImportInterface
    {
        if (!in_array($importStatus, ['pending', 'processing', 'complete', 'failed'])) {
            throw new InputException(
                __('Order import status must be one of: "pending," "processing," "complete" or "failed."')
            );
        }

        $this->setData(self::IMPORT_STATUS, $importStatus);

        return $this;
    }

    public function getImportStatus(): string
    {
        return (string)$this->getData(self::IMPORT_STATUS);
    }

    public function setImportCreated(string $importCreated): OrderImportInterface
    {
        $this->setData(self::IMPORT_CREATED, $importCreated);

        return $this;
    }

    public function getImportCreated(): string
    {
        return (string)$this->getData(self::IMPORT_CREATED);
    }

    public function setImportUpdated(string $importUpdated): OrderImportInterface
    {
        $this->setData(self::IMPORT_UPDATED, $importUpdated);

        return $this;
    }

    public function getImportUpdated(): string
    {
        return (string)$this->getData(self::IMPORT_UPDATED);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }
}
